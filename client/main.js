import { ReactiveVar } from 'meteor/reactive-var';
import '../imports/client/ui/main.scss';

import '/imports/client/ui/ClientsForm.scss';
import '/imports/client/ui/RutinesForm.scss';

import '/imports/api/collections/Clients.js';
import '/imports/api/collections/GrupsMusculars.js';
import '/imports/api/collections/Exercicis.js';
import '/imports/api/collections/Sessions.js';
import '/imports/api/collections/Rutines.js';

import '/imports/client/ui/layouts/MainLayout.scss';
import '/imports/client/routes.js';

import '/imports/client/ui/js/printThis.js';

import Modal from 'react-modal';

/*******************************************************************************************************************************
* Estils GLOBALS per a  REACT-MODAL:
**********************************************************************************************************************************/

Modal.defaultStyles = {
    overlay: {
        position: 'fixed',
        display: 'grid',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor : 'rgba(255, 255, 255, 0.75)',
        zIndex: 200000
    },
    content: {
        alignSelf: `center`,
        padding: `5%`,
        margin: `5%`,
        border: `1px solid gray`,
        textAlign: `center`,
        borderRadius: `1em`,
        boxShadow: `0px 1.2em 2em`,
        background: `rgba(63, 110, 229, 0.7) none repeat scroll 0% 0%`
    }
};
