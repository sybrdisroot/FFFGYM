import {Meteor} from 'meteor/meteor';

// Publish GrupsMusculars
Meteor.publish("userGrupsMusculars", function(){
  return GrupsMusculars.find({
    user: {$in: [ this.userId, "master" ]}
  });
});

// Publish Clients
Meteor.publish("userClients", function(){
  return Clients.find({
    user: this.userId
  });
});

// Publish Exercicis
Meteor.publish("userExercicis", function(){
  return Exercicis.find({
    user: {$in: [ this.userId, "master" ]}
  });
});

// Publish Rutines
Meteor.publish("userRutines", function(){
  return Rutines.find({
    user: {$in: [ this.userId, "master" ]}
  });
});

Meteor.publish("userSessions", function(){
  return Sessions.find({
    user: {$in: [ this.userId, "master" ]}
  });
});

// //===============
// // Publish Bars
// Meteor.publish("allBars", function(){
//   return Bars.find();
// });
//
// Meteor.publish("userBars", function(){
//   return Bars.find({
//     user: this.userId
//   });
// });
