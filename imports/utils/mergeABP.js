 
export default (a, b, p) => a.filter( 
    aa => ! b.find( 
        bb => aa[p] === bb[p]
    )
).concat(b);