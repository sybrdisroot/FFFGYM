import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import RutinesForm from './RutinesForm.jsx';
import RutinaSingle from './RutinaSingle.jsx';
import { check, Match } from 'meteor/check';
import { CSSTransitionGroup } from 'react-transition-group';
import Modal from 'react-modal';
import sanitizeHtml from 'sanitize-html-react';
import moment from 'moment';
import 'moment/locale/ca';
import Select from 'react-select';

class TaulaRutines extends Component {
    constructor(props, context) {
        super(props, context);

//        console.dir("ROWSAMBCLIENTLABEL: ", rowsAmbClientLabel);

        this.state = {
            rows: props.rutines,
            modalNovaRut: false,
            selectedRows: [],
            modalSelectClients: false,
            selectClientsValue: null
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            rows: nextProps.rutines
        })
    }

    rowGetter = (i) => {
        return this.state.rows[i];
    };

    handleGridSort = (sortColumn, sortDirection) => {
        const
            comparer = (a, b) => {
                if (sortDirection === 'ASC') {
                    return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
                } else if (sortDirection === 'DESC') {
                    return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
                }
            },
            rows = sortDirection === 'NONE' ? this.state.originalRows.slice(0) : this.state.rows.sort(comparer)
        ;

        this.setState({ rows });
    };

    handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
        let rows = this.state.rows.slice();

        for (let i = fromRow; i <= toRow; i++) {
            let rowToUpdate = rows[i];
            let updatedRow = update(rowToUpdate, {$merge: updated}); // <<<<<<<<<<<<<<< Meteor.method()
            rows[i] = updatedRow;
        }

        this.setState({ rows });
    };

    handleInsertButtonClick = (onClick) => {
        // Custom your onClick event here,
        // it's not necessary to implement this function if you have no any process before onClick
        console.log('This is my custom function for InserButton click event');
        onClick();
    };

    //createCustomInsertButton = ;

    handleDeleteButtonClick = (onClick) => {
      // Custom your onClick event here,
      // it's not necessary to implement this function if you have no any process before onClick
      console.log('This is my custom function for DeleteButton click event');
      onClick();
    };

    //createCustomDeleteButton = 

    createCustomSearchField = (props) => {
        return (
            <SearchField
                className='my-custom-class'
                defaultValue={ props.defaultSearch }
                placeholder="Busca rutines..."
            />
        );
    };

    beforeClose = (e) => {
        alert(`[Custom Event]: Before modal close event triggered!`);
    };

    handleModalClose = (closeModal) => {
        // Custom your onCloseModal event here,
        // it's not necessary to implement this function if you have no any process before modal close
        console.log('This is my custom function for modal close event');
        closeModal();
    };

    createCustomModalHeader = (onClose, onSave) => {
        const headerStyle = {
            fontWeight: 'bold',
            fontSize: 'large',
            textAlign: 'center',
            backgroundColor: '#eeeeee'
        };
        return (
            <div className='modal-header' style={ headerStyle }>
                <h3>Nova Rutina</h3>
                <button className='btn btn-info' onClick={onClose}>&times;</button>
            </div>
        );
    };

    createCustomModalFooter = (onClose, onSave) => {
        const style = {
            backgroundColor: '#ffffff'
        };
        return (
            <div className='modal-footer' style={ style }>
                <button className='btn btn-xs ' onClick={ onSave }>Guardar</button>
            </div>
        );
    };

    objecteIDs = (keyVal) => {
        console.log("keyVal: ", keyVal);
        console.dir("thisState: ", this.state);
        const
            idx = this.state.rows.findIndex((row) => row.clientiNomComplet === keyVal),
            oId = this.state.rows[idx]._id,
            uId = this.state.rows[idx].user
        ;
        //console.log("idx: ", idx);

        return {
            idx,
            _id: oId,
            user: uId
        };
    }

    onDeleteRow = (rows) => {
        //alert("Esborrant...");
        //  console.dir("rows: ", rows);
        //  console.dir("STATEROWS: ", this.state.rows);

        rows.map(
            (v,i,a) => {
                //let
            //     objIDs = this.objecteIDs(v)
                //;
                //console.dir("objIDs: ", objIDs);
                //console.log("MUser: ", Meteor.user());

                let
                    eliminador = this.state.rows.find(
                        w => w.nomRutina === v
                    )
                ;

                
                Meteor.call('rutines.delete', eliminador);
            }
        )
    }

    handleClose = () => this.setState({modalNovaRut: false});

    imprimeixLlista = () => {
    //    PHE.printElement(document.querySelector(".olLlistaGMs"));
        //alert('Després de PHE');
        $(".ulRutineslista").printThis();
    };

    showSelectClients = () => {
        this.setState({
            modalSelectClients: true
        });
    };

    selectClientsUpdateValue = (selectClientsValue) => {

        //this.selClientRef

        this.setState({
            selectClientsValue
        });
    }

    // setSelectedRows = (sR) => this.setState({
    //     selectedRows: sR
    // });

    render() {
        let
            onAfterInsertRow = (row)  => {
                let newRowStr = '';

                for (const prop in row) {
                    newRowStr += prop + ': ' + row[prop] + ' \n';
                }
                alert('The new row is:\n ' + newRowStr);
            },

            customConfirm = (next, dropRowKeys)  => {
                const dropRowKeysStr = dropRowKeys.join('\n ⇝ ');
                if (
                    confirm(`Vas a eliminar per complet ${dropRowKeys.length} element${dropRowKeys.length>1?"s":""}: \n ⇝ ${dropRowKeysStr} \n Estàs segur# de continuar?`)
                ) {
                    // If the confirmation is true, call the function that
                    // continues the deletion of the record.
                    next();
                }
            },

            afterDeleteRow = (rowKeys)  => {
                console.dir("rowKeys: ", rowKeys);
            },

            afterSearch = (searchText, result)  => {
      //          console.log('Your search text is ' + searchText);
    //            console.log('Result is:');
                for (let i = 0; i < result.length; i++) {
  //                  console.dir('Result: ', result[i]);
                }
            },

            rutinaLinkFormatter = (cell, row) => (
                <a
                    href={`/rutina/${row._id}`}
                    target="_self"
                    style={{
                        color: `navy`,
                        fontVariant: `small-caps`,
                        fontSize: `1.2em`,
                        textShadow: `1px 1px 0 rgba(255,255,255,.5)`
                    }}
                >{cell}
                </a>
            ),

            rutinaClientFormatter = (cell, row) => {
          //      console.dir("RRRow: ", row);
                return (
                        <div
                            dangerouslySetInnerHTML={{
                                __html:
                                row.client !== null
                                ?    sanitizeHtml(row.client.label)
                                :   ""
                            }}
                            style={{
                                overflow: 'hidden'
                            }}>
                        </div>
                    );
            },

            descripFormatter = (cell, row)  => (
                <div
                    dangerouslySetInnerHTML={{
                        __html:
                            row.observacions !== null
                            ?   sanitizeHtml(row.observacions)
                            :   ""
                    }}
                    style={{
                        overflow: 'hidden'
                    }}>
                </div>
            ),

            rutinaCreatedAtFormatter = (cell, row)  => (
                <div
                    dangerouslySetInnerHTML={{__html: sanitizeHtml(moment(row.createdAt).format("D/MM/YYYY"))}}
                    style={{
                        overflow: 'hidden'
                    }}>
                </div>
            )
        ;

    //    let setSelected = new Set(this.state.selectedRows);
        //let setSelected = [];
        let
            arrNomsClients = []
        ;

        this.props.clients.map(
            (v,i,a) => {
                //console.log(JSON.stringify(client));
                arrNomsClients.push({
                    value: v,
                    label: `${v.clientCognoms}, ${v.clientNom}`,
                    className: `autoCompleteSelectOption`
                });
            }
        );

        return  (
            <div>
                <BootstrapTable
                    ref={bt => this.bt = bt}
                    data={this.state.rows}
                    tableContainerClass="ulRutinesLlista"
                    headerContainerClass='my-custom-class'

                    striped
                    hover
                    search
                    insertRow
                    deleteRow

                    version='4'

                    headerStyle={{
                        background: 'rgba(255,255,255,.5)',
                        fontVariant: 'small-caps',
                        fontSize: '1.3em',
                        textShadow: '1px 1px 0px white'
                    }}

                    options={{
                        sortName: `createdAt`,
                        sortOrder: `desc`,
                        afterInsertRow: onAfterInsertRow,
                        handleConfirmDeleteRow: customConfirm,
                        onDeleteRow: this.onDeleteRow,
                        afterDeleteRow,
                        afterSearch: afterSearch,
                        insertBtn: onClick => (
                            <InsertButton
                                btnText='Nova'
                                className='my-custom-class'
                    
                                onClick={() => {
                                    //this.handleInsertButtonClick(onClick)
                                    this.props.toggleForm();
                                }}
                            />
                        ),
                        deleteBtn: onClick => (
                            <DeleteButton
                                btnText='Elimina'
                                className='my-custom-class'
                                onClick={ e => this.handleDeleteButtonClick(onClick) }
                            />
                        ),
                        clearSearch: false,
                        searchField: this.createCustomSearchField,
                        insertModalHeader: this.createCustomModalHeader,
                        insertModalFooter: this.createCustomModalFooter,
                        sortIndicator: false,
                        noDataText: "Carregant dades...",
                        toolbarPosition: 'both',
                        btnGroup: (props, state) => (
                            <ButtonGroup sizeClass="btn-group-md">
                                { props.insertBtn }
                                <button 
                                    className={ `btn btn-primary` }
                                    onClick={() => {
                                      //  alert("Copiant i assignant");
                                        // console.dir("DEL BOTÓ - THIS.STATE:", this.state);
                                        // console.dir("DEL BOTÓ - THIS.BT.STATE:", this.bt.state);
                                        // this.state.selectedRowKeys.map(
                                            //     (v,i,a) => {
                                                //         console.log(`Rutina seleccionada: ${i}: ${v}`);
                                                //     }
                                                // ))
                                    if (this.state.selectedRows.length > 0) {
                                        this.showSelectClients();
                                    } else {
                                        alert("No has seleccionat cap rutina.");
                                    }
                                }}
                                >
                                    Copia i assigna
                                </button>
                                { props.deleteBtn }
                            </ButtonGroup>
                        )
                    }}

                    selectRow={{
                        mode: `checkbox`,
                        onSelect: (row, isSelected, e) => {
                            // var rowStr = "";

                            // for(var prop in row){
                            //     rowStr+=prop+": '"+row[prop]+"' ";
                            // }
                            // alert("is selected: " + isSelected + ", " + rowStr);
                            
                            
                                    // if (isSelected) {
                                    //     this.setState(
                                    //         (prevState, props) => ({
                                    //             selectedRows: [...prevState.selectedRows, row]
                                    //         })
                                    //     );
                                    // }

                            //let setSelected;

                            //if (isSelected) {
                                console.dir("THIS.STATE:", this.state);
                                console.dir("THIS.BT.STATE:", this.bt.state);
                              //  setSelected = new Set(this.bt.state.selectedRows);
                            //}

                            //console.dir("SETSELECTED: ", setSelected);

    // console.dir("Seleccionades: ", this.state.rows.filter(
    //     (v,i,a) => {
    //         v._id === this.
    //     }
    // ))
                            //setSelected = ;
                                
                            if (isSelected) {

                                this.setState(
                                    (prevState, props) => {
                                        return {
                                            selectedRows: prevState.rows.filter(
                                                (v,i,a) => this.bt.state.selectedRowKeys.includes(v._id)
                                            )
                                        };
                                    }
                                );
                            } else {
                                this.setState(
                                    (prevState, props) => {
                                        return {
                                            selectedRows: prevState.selectedRows.filter(
                                                (v,i,a) => row._id !== v._id
                                            )
                                        };
                                    }
                                );
                            }

                           //console.dir("setSELECTED: ", setSelected);
                           console.dir("THIS.STATE2:", this.state);
                           console.dir("THIS.BT.STATE2:", this.bt.state);
                        }
                    }}

                >
                    <TableHeaderColumn
                        isKey
                        hidden
                        dataField='_id'
                    >ID
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        //isKey
                        dataSort
                        searchable
                        dataField='nomRutina'
                        dataFormat={rutinaLinkFormatter}
                    >Rutina
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataSort
                        searchable
                        dataField='observacions'
                        dataFormat={descripFormatter}
                    >Observacions
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataSort
                        searchable
                        dataField='clientLabel'
                        dataFormat={rutinaClientFormatter}

                    >Client
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataSort
                        dataField='createdAt'
                        dataFormat={rutinaCreatedAtFormatter}
                    >Creada
                    </TableHeaderColumn>

                </BootstrapTable>
                
                <Modal
                    shouldCloseOnOverlayClick
                    shouldCloseOnEsc
                    isOpen={this.state.modalSelectClients}
                    onRequestClose={this.handleClose}
                    ariaHideApp
                >
                    <button
                        //id="btTancaSessioModal2"
                        onClick={() => {
                            this.setState({
                                modalSelectClients: false
                            });
                        }}
                    >&times;
                    </button>
                    <h3>Copia i assigna</h3>
                    Selecciona el(s) client(s) que vols que reben la/es rutina/es:  
                    <Select
                        id="selClient"
                        ref={selRef => this.selRef = selRef}
                        multi
                        value={this.state.selectClientsValue}
                        options={arrNomsClients}
                        onChange={this.selectClientsUpdateValue}
                    />
                    <button
                        onClick={() => {
                            //alert("Copiem els registres a la base de dades.");

                            
                            if (this.selRef.props.value === null || this.selRef.props.value.length <= 0) {
                                alert("No has seleccionat el/s client/s per fer l'assignació.");
                            } else {
                                this.selRef.props.value.forEach(
                                    (clientSeleccionat , indexClientSeleccionat, arraiClientsSeleccionats) => {
                                        this.state.selectedRows.forEach(
                                            (rutinaSeleccionada, indexRutinaSeleccionada, arraiRutinesSeleccionades) => {
                                                //   alert(`${v.nomRutina}`);
                                                let
                                                    client = clientSeleccionat,
                                                    {
                                                        nomRutina,
                                                        dataIni,
                                                        dataFi,
                                                        entrenador,
                                                        observacions,
                                                        sessions,
                                                        opcionsImpressio
                                                    } = rutinaSeleccionada
                                                ;
        
                                                Meteor.call(
                                                    'rutines.insert',
                                                    nomRutina,
                                                    client,
                                                    dataIni,
                                                    dataFi,
                                                    entrenador,
                                                    observacions,
                                                    sessions,
                                                    opcionsImpressio
                                                );
        
                                                // console.dir("ObjecteDeconstr: ", {
                                                //     nomRutina,
                                                //     client,
                                                //     dataIni,
                                                //     dataFi,
                                                //     entrenador,
                                                //     observacions,
                                                //     sessions,
                                                //     opcionsImpressio
                                                // });
                                            }
                                        );
                                    }
                                );

                                

                                this.setState({
                                    modalSelectClients: false
                                });
                            }
                        }}
                    >
                        ESTABLEIX
                    </button>
                </Modal>
            </div>
        );
    }
}

class RutinesNoData extends Component {
  constructor(props) {
    super(props);

    this.state = {
        rutina: {},
        mostraForm: false
    };
  }

  handleClose = () => this.setState({mostraForm: false});

  toggleForm = () => {
      this.setState({
         mostraForm: !this.state.mostraForm
      });
  };

  render() {

    return (
        <div>
            <TaulaRutines
                clients={this.props.clients}
                rutines={this.props.rutines.map(
                    (v,i,a) => {
                  //      console.dir("VVVVVVV", v);
                        return ({
                            ...v,
                            "clientLabel": (v.client !== null && typeof v.client.label === "string") ? v.client.label : ""
                        });
                    }
                )}
                toggleForm={this.toggleForm}
            />

            <div
                style={{
                    textAlign: `right`
                }}
            >
                <button
                    onClick={() => $(".ulRutinesLlista").printThis()}
                >Imprimeix
                </button>
            </div>

            <Modal
                isOpen={this.state.mostraForm}
                onRequestClose={this.handleClose}
            >
                <RutinesForm
                    rutinaId={this.props.params.id}
                    clients={this.props.clients}
                    grups_musculars={this.props.grups_musculars}
                    exercicis={this.props.exercicis}
                />

            </Modal>
        </div>
    );
  }
}

// App.propTypes = {
// //  clients: PropTypes.array.isRequired
// };

export default createContainer(() => {
    const
        subscription = {
            clientsSubscription: Meteor.subscribe("userClients"),
            grups_muscularsSubscription: Meteor.subscribe("userGrupsMusculars"),
            //imatgesSubscription: Meteor.subscribe("userImatges"),
            exercicisSubscription: Meteor.subscribe("userExercicis"),
            sessionsSubscription: Meteor.subscribe("userSessions"),
            rutinesSubscription: Meteor.subscribe("userRutines")
        },

        loading = ! ( subscription.clientsSubscription.ready() &&
            subscription.grups_muscularsSubscription.ready() &&
            subscription.exercicisSubscription.ready() &&
            subscription.sessionsSubscription.ready() &&
            subscription.rutinesSubscription.ready()
        ),

        clients = subscription.clientsSubscription.ready() ? Clients.find().fetch() : [],
        grups_musculars = subscription.grups_muscularsSubscription.ready() ? GrupsMusculars.find().fetch() : [],
        exercicis = subscription.exercicisSubscription.ready() ? Exercicis.find().fetch() : [],
        sessions = subscription.sessionsSubscription.ready() ? Sessions.find().fetch() : [],
        rutines = subscription.rutinesSubscription.ready() ? Rutines.find().fetch() : []
    ;

  return {
    clients,
    grups_musculars,
    exercicis,
    sessions,
    rutines,
    loading
  }
}, RutinesNoData);
