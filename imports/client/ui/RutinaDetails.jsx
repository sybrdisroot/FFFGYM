// React
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { Meteor } from 'meteor/meteor';

import { createContainer } from 'meteor/react-meteor-data';
import RutinesForm from './RutinesForm.jsx';
import RutinaSingle from './RutinaSingle.jsx';

import { check, Match } from 'meteor/check';

import { CSSTransitionGroup } from 'react-transition-group';

import Modal from 'react-modal';
import sanitizeHtml from 'sanitize-html-react';


class RutinesNoData extends Component {
  constructor(props) {
    super(props);

    this.state = {
        rutina: {},
        mostraForm: false
    };
  }



  handleClose = () => this.setState({mostraForm: false})


  toggleForm = () => {
      this.setState({
         mostraForm: !this.state.mostraForm
      });
  }

  // componentDidMount(){
  //
  // }
  //
  // componentWillUnmount(){
  //   this.state.subscription.rutines.stop();
  // }

  /*renderResolutions(){
    return this.props.resolutions.map((resolution)=>(
      <ResolutionSingle key={resolution._id} resolution={resolution} />
    ));
  }*/

  render() {
  //  let resol = this.props.resolutions;
    //console.log(resol);

    // if (this.props.params.id) {
    //     alert(`ID de la Rutina: ${this.props.params.id}`);
    // }

    return (
        <div>
            {
                //     <CSSTransitionGroup
                //     id="divRutines"
                //     component="div"
                //     transitionName="route"
                //     transitionAppear={true}
                //     transitionAppearTimeout={600}
                //     transitionEnterTimeout={600}
                //     transitionLeaveTimeout={400}
                // >
            }

            {

                // <TaulaRutines
                //     rutines={this.props.rutines}
                //     toggleForm={this.toggleForm}
                // />
                //
                // {
                //
                //     // <div
                //     //     style={{
                //     //         display: this.state.mostraForm ? `block` : `none`
                //     //     }}
                //     // >
                // }
                // <Modal
                //     isOpen={this.state.mostraForm}
                //     onRequestClose={this.handleClose}
                // >
            }
                <div
                    id="laRutina"
                >
                    <RutinesForm
                        rutinaId={this.props.params.id}
                        clients={this.props.clients}
                        grups_musculars={this.props.grups_musculars}
                        exercicis={this.props.exercicis}
                    />
                </div>
                <div
                    style={{
                        textAlign: `right`
                    }}
                >
                    <button
                        onClick={() => $("#laRutina").printThis()}
                    >Imprimeix la rutina
                    </button>
                </div>

                {
                    // </Modal>


                    // </div>

                    // <CSSTransitionGroup
                    //     component="ul"
                    //     className="ulLlistaRutines"
                    //     transitionName="route"
                    //     transitionEnterTimeout={600}
                    //     transitionLeaveTimeout={400}
                    // >

            //</CSSTransitionGroup>
                //</CSSTransitionGroup>
              }
        </div>
    );
  }
}

// App.propTypes = {
// //  clients: PropTypes.array.isRequired
// };

export default createContainer(
    () => {
        const
            subscription = {
                clientsSubscription: Meteor.subscribe("userClients"),
                grups_muscularsSubscription: Meteor.subscribe("userGrupsMusculars"),
                //imatgesSubscription: Meteor.subscribe("userImatges"),
                exercicisSubscription: Meteor.subscribe("userExercicis"),
                sessionsSubscription: Meteor.subscribe("userSessions"),
                rutinesSubscription: Meteor.subscribe("userRutines")
            },

            loading = ! ( subscription.clientsSubscription.ready() &&
                subscription.grups_muscularsSubscription.ready() &&
                subscription.exercicisSubscription.ready() &&
                subscription.sessionsSubscription.ready() &&
                subscription.rutinesSubscription.ready()
            ),

            clients = subscription.clientsSubscription.ready() ? Clients.find().fetch() : [],
            grups_musculars = subscription.grups_muscularsSubscription.ready() ? GrupsMusculars.find().fetch() : [],
            exercicis = subscription.exercicisSubscription.ready() ? Exercicis.find().fetch() : [],
            sessions = subscription.sessionsSubscription.ready() ? Sessions.find().fetch() : [],
            rutines = subscription.rutinesSubscription.ready() ? Rutines.find().fetch() : []
        ;
        return {
            clients,
            grups_musculars,
            exercicis,
            sessions,
            rutines,
            loading
        }
    },
    RutinesNoData
);
