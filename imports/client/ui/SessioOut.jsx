import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';

import moment from 'moment';
import 'moment/locale/ca';
import capitalize from 'lodash/capitalize';
import QuadreSessio from './QuadreSessio.jsx';

export default class SessioOut extends Component {
    constructor(props) {
        super(props);

        this.state = {
            impOpcionsVisibles: props.impOpcionsVisibles,
            impNomSessio: props.impNomSessio,
            impObsSessio: props.impObsSessio,
            impImatgesExs: props.impImatgesExs,
            impDescExs: props.impDescExs,
            impEscalf: props.impEscalf,
            impTorCalma: props.impTorCalma,
            impTABULAR: props.impTABULAR,
            sessio: props.v
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            sessio: nextProps.v
        })
    }

    render() {
        let
            v = this.state.sessio,
            i = this.props.i
        ;
        return (
            <div
                style={{
                    margin: `1em auto`
                }}
            >
                <div
                    style={{
                        display: `grid`,
                        gridTemplateAreas: `"dia gms"`,
                        color: `steelblue`,
                        marginLeft: `-1em`,
                        paddingLeft: `1em`,
                        borderRadius: `0 1em 1em 0`,
                      //  transform: `skewX(-12deg)`,
                        background: `linear-gradient(to bottom, rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%)`
                    }}
                >
                    <div
                        style={{
                            gridArea: `dia`
                        }}
                    >
                        {
                            typeof(v.datesSessio) !== "undefined" && v.datesSessio.length > 0
                            ?   v.datesSessio.map(
                                    (w,j,b) => <h6 key={j}>
                                        {//`${capitalize(moment(w).format("dddd, ll"))}`
                                            this.props.impDIA ? `${capitalize(moment(w).format("dddd"))}` : `${capitalize(moment(w).format("dddd, ll"))}`
                                        }
                                    </h6>
                                )
                            :   typeof(v.periodeSessio) !== "undefined" && v.periodeSessio !== null && Object.keys({...v.periodeSessio}).length !== 0
                                ?   this.props.impDIA ? `${capitalize(moment(v.periodeSessio.start).format("dddd"))} → ${capitalize(moment(v.periodeSessio.end).format("dddd"))}` : `${capitalize(moment(v.periodeSessio.start).format("dddd, L"))} → ${capitalize(moment(v.periodeSessio.end).format("dddd, L"))}`
                                :   <h6
                                        style={{
                                            display: `inline`
                                        }}
                                        >{
                                            //`${capitalize(moment(v.dataSessio).format("dddd, ll"))}`
                                            this.props.impDIA ? `${capitalize(moment(v.dataSessio).format("dddd"))}` : `${capitalize(moment(v.dataSessio).format("dddd, ll"))}`
                                        }
                                    </h6>
                            // <h3
                            //     style={{
                            //         display: `inline`,
                            //         marginRight: `1em`
                            //     }}
                            //     >{`Sessió ${i+1}:  `}</h3>
                        }
                    </div>
                    <div
                        style={{
                            gridArea: `gms`
                   //         ,
                   //         transform: `skewX(-12deg)`
                        }}
                    >
                        {/* <h3
                            style={{
                                gridArea: `gms`
                            }}
                        >Grups Musculars:
                        </h3> */}
                            {
                                typeof(v.gms) !== "undefined" && v.gms.map(
                                    (w, j, b) => (
                                        <h6
                                            key={j}
                                            style={{
                                                display: `inline`,
                                                margin: `1em`,
                                                //background: `rgba(0,0,0,.2)`,
                                                padding: `.6em`,
                                                //borderRadius: `2em`,
                                                //border: `solid 2px`,
                                                color: `lightsteelblue`,
                                                transform: `skewX(-12deg)`
                                                //,
                                                // textShadow: `1px 1px 0 rgba(255,255,255, .5)`
                                            }}
                                        >{w.label}
                                        </h6>
                                    )
                                )
                            }
                    </div>
                </div>

                <QuadreSessio

                    sessio={v}


                    {...this.props}
                />
            </div>
        );
    }
}
