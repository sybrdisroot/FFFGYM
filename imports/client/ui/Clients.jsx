import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import ClientsForm from './ClientsForm.jsx';
import ClientSingle from './ClientSingle.jsx';

import { check, Match } from 'meteor/check';

import { CSSTransitionGroup } from 'react-transition-group';

import ModalNouClient from 'react-modal';
import sanitizeHtml from 'sanitize-html-react';


class TaulaClients extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            rows: props.clients,
            modalNouEx: false
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            rows: nextProps.clients
        })
    }
    // getRandomDate = (start, end) => {
    //     return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())).toLocaleDateString();
    // };

    // createRows = (numberOfRows) => {
    //     let rows = [];
    //     for (let i = 1; i < numberOfRows; i++) {
    //         rows.push({
    //             id: i,
    //             task: 'Task ' + i,
    //             complete: Math.min(100, Math.round(Math.random() * 110)),
    //             priority: ['Critical', 'High', 'Medium', 'Low'][Math.floor((Math.random() * 3) + 1)],
    //             issueType: ['Bug', 'Improvement', 'Epic', 'Story'][Math.floor((Math.random() * 3) + 1)],
    //             startDate: this.getRandomDate(new Date(2015, 3, 1), new Date()),
    //             completeDate: this.getRandomDate(new Date(), new Date(2016, 0, 1))
    //         });
    //     }
    //     return rows;
    // };

    rowGetter = (i) => {
        return this.state.rows[i];
    };

    handleGridSort = (sortColumn, sortDirection) => {
        const
            comparer = (a, b) => {
                if (sortDirection === 'ASC') {
                    return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
                } else if (sortDirection === 'DESC') {
                    return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
                }
            },
            rows = sortDirection === 'NONE' ? this.state.originalRows.slice(0) : this.state.rows.sort(comparer)
        ;

        this.setState({ rows });
    };

    handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
        let rows = this.state.rows.slice();

        for (let i = fromRow; i <= toRow; i++) {
            let rowToUpdate = rows[i];
            let updatedRow = update(rowToUpdate, {$merge: updated}); // <<<<<<<<<<<<<<< Meteor.method()
            rows[i] = updatedRow;
        }

        this.setState({ rows });
    };

    handleInsertButtonClick = (onClick) => {
        // Custom your onClick event here,
        // it's not necessary to implement this function if you have no any process before onClick
        console.log('This is my custom function for InserButton click event');
        onClick();
    };

    createCustomInsertButton = (onClick) => {
        return (
          <InsertButton
            btnText='Nou'
            className='my-custom-class'

            onClick={() => {
                //this.handleInsertButtonClick(onClick)
                this.props.toggleForm();
            }}
          />
        );
        // If you want have more power to custom the child of InsertButton,
        // you can do it like following
        // return (
        //   <InsertButton
        //     btnContextual='btn-warning'
        //     className='my-custom-class'
        //      btnContextual='btn-warning'
        //      btnGlyphicon='glyphicon-edit'
        //     onClick={ () => this.handleInsertButtonClick(onClick) }>
        //     { ... }
        //   </InsertButton>
        // );
    };

    handleDeleteButtonClick = (onClick) => {
      // Custom your onClick event here,
      // it's not necessary to implement this function if you have no any process before onClick
      console.log('This is my custom function for DeleteButton click event');
      onClick();
    };

    createCustomDeleteButton = (onClick) => {
        return (
            <DeleteButton
                btnText='Elimina'
                className='my-custom-class'
                onClick={ e => this.handleDeleteButtonClick(onClick) }
            />
      );
      // If you want have more power to custom the child of DeleteButton,
      // you can do it like following
      // return (
      //   <DeleteButton
      //     btnContextual='btn-warning'
      //     className='my-custom-class'
          // btnGlyphicon='glyphicon-edit'
          // btnContextual='btn-success'
      //     onClick={ () => this.handleDeleteButtonClick(onClick) }>
      //     { ... }
      //   </DeleteButton>
      // );
  };

    createCustomSearchField = (props) => {
        return (
            <SearchField
                className='my-custom-class'
                defaultValue={ props.defaultSearch }
                placeholder="Busca els Clients..."
            />
        );
    };

    beforeClose = (e) => {
        alert(`[Custom Event]: Before modal close event triggered!`);
    };

    handleModalClose = (closeModal) => {
        // Custom your onCloseModal event here,
        // it's not necessary to implement this function if you have no any process before modal close
        console.log('This is my custom function for modal close event');
        closeModal();
    };

    createCustomModalHeader = (onClose, onSave) => {
        const headerStyle = {
            fontWeight: 'bold',
            fontSize: 'large',
            textAlign: 'center',
            backgroundColor: '#eeeeee'
        };
        return (
            <div className='modal-header' style={ headerStyle }>
                <h3>Nou Client</h3>
                <button className='btn btn-info' onClick={onClose}>&times;</button>
            </div>
        );
    };

    createCustomModalFooter = (onClose, onSave) => {
        const style = {
            backgroundColor: '#ffffff'
        };
        return (
            <div className='modal-footer' style={ style }>
                <button className='btn btn-xs ' onClick={ onSave }>Guardar</button>
            </div>
        );
    };

    objecteIDs = (keyVal) => {
        console.log("keyVal: ", keyVal);
        console.dir("thisState: ", this.state);
        const
            idx = this.state.rows.findIndex((row) => row.clientiNomComplet === keyVal),
            oId = this.state.rows[idx]._id,
            uId = this.state.rows[idx].user
        ;
        //console.log("idx: ", idx);

        return {
            idx,
            _id: oId,
            user: uId
        };
    }

    onDeleteRow = (rows) => {
         //alert("Esborrant...");
         console.dir("rows: ", rows);
         console.dir("thisROW: ", this.state.row);
         console.dir("thisROWS: ", this.state.rows);

         rows.map(
             (v,i,a) => {
                // let
                //     objIDs = this.objecteIDs(v)
                // ;
                // console.dir("objIDs: ", objIDs);
                // console.log("MUser: ", Meteor.user());

                let
                    eliminador = {_id: v}
                ;

              //  console.dir("eliminador: ", eliminador);

                Meteor.call('clients.delete', eliminador);
             }
         )
    }

    handleClose = () => this.setState({modalNouEx: false})

    imprimeixLlista = () => {
    //    PHE.printElement(document.querySelector(".olLlistaGMs"));
        //alert('Després de PHE');
        $(".ulClientsLlista").printThis();
    }

    render() {
        let
            onAfterInsertRow = (row)  => {
                let newRowStr = '';

                for (const prop in row) {
                    newRowStr += prop + ': ' + row[prop] + ' \n';
                }
                alert('The new row is:\n ' + newRowStr);
            },

            customConfirm = (next, dropRowKeys)  => {
                const dropRowKeysStr = dropRowKeys.join('\n ⇝ ');
                if (
                    confirm(`Vas a eliminar per complet ${dropRowKeys.length} element${dropRowKeys.length>1?"s":""}: \n ⇝ ${dropRowKeysStr} \n Estàs segur# de continuar?`)
                ) {
                    // If the confirmation is true, call the function that
                    // continues the deletion of the record.
                    next();
                }
            },

            afterDeleteRow = (rowKeys)  => {
                console.dir("rowKeys: ", rowKeys);
            },

            afterSearch = (searchText, result)  => {
                console.log('Your search text is ' + searchText);
                console.log('Result is:');
                for (let i = 0; i < result.length; i++) {
                    console.dir('Result: ', result[i]);
                }
            },

            //    dangerouslySetInnerHTML={{__html: sanitizeHtml(`${row.clientCognoms}, ${row.clientNom}`)}}
            clientLinkFormatter = (cell, row) => (

                <a
                    href={`/client/${row._id}`}
                    style={{
                        color: `navy`,
                        fontVariant: `small-caps`,
                        fontSize: `1.2em`,
                        textShadow: `1px 1px 0 rgba(255,255,255,.5)`
                    }}
                >{`${row.clientCognoms}, ${row.clientNom}`}
                </a>
            ),

            descripFormatter = (cell, row)  => (
                <div
                    dangerouslySetInnerHTML={{__html: sanitizeHtml(row.clientObservacions)}}
                    style={{
                        overflow: 'hidden'
                    }}>
                </div>
            )
            // gmFormatter = (cell, row) => {
            //     const
            //         exs = this.props.exercicis,
            //         gms = this.props.grups_musculars,
            //         exercici = this.props.exercicis.filter(ex => ex._id === row._id),
            //         gm = exercici[0].exerciciGrupMuscular
            //     ;
            //
            //     console.dir("cell: ", cell);
            //     console.dir("row: ", row);
            //
            //     // return (<ExerciciGrupMuscularCampEditable
            //     //     grups_musculars={gms}
            //     //     exercici={exercici}
            //     //     onChange={() => {
            //     //         Meteor.call('exercicis.gm.update', exercici[0], gm);
            //     //         console.log("userId: ", Meteor.userId());
            //     //         console.log("ex.uId: ", exercici[0].user);
            //     //         console.dir("Ex: ", exercici[0]);
            //     //         console.dir("gm: ", gm);
            //     //
            //     //     }}
            //     return (
            //         <div
            //             children={cell}
            //         />
            //     );
            // },
            //
            // customGMField = (cell, row) => {
            //     const
            //         exs = this.props.exercicis,
            //         gms = this.props.grups_musculars,
            //         exercici = this.props.exercicis.filter(ex => ex._id === row._id),
            //         gm = exercici.exerciciGrupMuscular
            //     ;
            //
            //     return (
            //         <ExerciciGrupMuscularCampEditable
            //             grups_musculars={gms}
            //             onChange={() => {
            //                 Meteor.call('exercicis.gm.update', exercici, gm);
            //             }}
            //         >{cell}
            //         </ExerciciGrupMuscularCampEditable>
            //     );
            // }
        ;

        return  (
            <div>
                <BootstrapTable
                    data={this.state.rows}
                    tableContainerClass="ulClientsLlista"

                    headerContainerClass='my-custom-class'
                    headerStyle={{
                        background: 'rgba(255,255,255,.5)',
                        fontVariant: 'small-caps',
                        fontSize: '1.3em',
                        textShadow: '1px 1px 0px white'
                    }}
                    options={{
                        sortName: `_id`,
                        sortOrder: `asc`,
                        afterInsertRow: onAfterInsertRow,
                        handleConfirmDeleteRow: customConfirm,
                        onDeleteRow: this.onDeleteRow,
                        afterDeleteRow,
                        afterSearch: afterSearch,
                        insertBtn: this.createCustomInsertButton,
                        deleteBtn: this.createCustomDeleteButton,
                        clearSearch: false,
                        searchField: this.createCustomSearchField,
                        insertModalHeader: this.createCustomModalHeader,
                        insertModalFooter: this.createCustomModalFooter,
                        sortIndicator: false,
                        noDataText: "Carregant dades...",
                        toolbarPosition: 'both'
                    }}
                    striped
                    hover
                    search
                    insertRow
                    deleteRow
                    selectRow={{
                        mode: `checkbox`
                    }}
                    version='4'
                >
                    
                    <TableHeaderColumn
                        isKey
                        hidden
                        dataField='_id'
                        dataFormat={clientLinkFormatter}
                    >Client
                    </TableHeaderColumn>
                    
                    <TableHeaderColumn
                        dataSort
                        searchable
                        dataField='clientLabel'
                        dataFormat={clientLinkFormatter}
                    >Client
                    </TableHeaderColumn>


                    <TableHeaderColumn
                        dataSort
                        searchable
                        dataField='clientObservacions'
                        dataFormat={descripFormatter}
                    >Observacions
                    </TableHeaderColumn>

                </BootstrapTable>

            {//}    <div className="divPrintDeliverer">
                    //<button className="btAddNew" onClick= this.activateForm}>Nou</button>*/
                    // <button
                    //     className="btPrintList"
                    //     onClick={this.imprimeixLlista}
                    // >Imprimir
                    // </button>

            //    </div>
            }

            </div>
        );
    }
}

class ClientsNoData extends Component{
  constructor(props) {
    super(props);

    this.state = {
    //   subscription: {
    //     clients: Meteor.subscribe("userClients"),
    //     grups_musculars: Meteor.subscribe("userGrupsMusculars")
    //   }
        mostraForm: false
    };
  }

  componentDidMount() {

  }

  componentWillUnmount() {
    //this.props.subscription.clients.stop();
  }

  /*renderResolutions(){
    return this.props.resolutions.map((resolution)=>(
      <ResolutionSingle key={resolution._id} resolution={resolution} />
    ));
  }*/

  handleClose = () => this.setState({
    mostraForm: false
  })

  toggleForm = () => {
      this.setState({
          mostraForm: !this.state.mostraForm
      });
  }

  render() {
  //  let resol = this.props.resolutions;
    //console.log(resol);

    return (
        <div>
            <div
                id="imprimible"
            >
                <TaulaClients
                    clients={this.props.clients}
                    toggleForm={this.toggleForm}
                />
            </div>

            <div
                style={{
                    textAlign: `right`
                }}
            >
                <button
                    onClick={() => $(".ulClientsLlista").printThis()}
                >Imprimeix
                </button>
            </div>
            {

                // <div
                //     style={{
                //         display: this.state.mostraForm ? `block` : `none`
                //     }}
                // >
            }
                <ModalNouClient
                    isOpen={this.state.mostraForm}
                >
                    <div
                        style={{
                            textAlign: `right`,
                            fontSize: `2em`,
                            color: `grey`,
                            marginTop: `-2em`,
                            marginRight: `-1.5em`,
                            cursor: `pointer`
                        }}
                        onClick={(ev) => {
                            this.setState({
                                mostraForm: false
                            });
                        }}
                    >&times;
                    </div>

                    <ClientsForm
                        onRequestClose={this.handleClose}
                    />
                </ModalNouClient>
        </div>
    );
  }
}

// Clients.propTypes = {
// //  clients: PropTypes.array.isRequired
// };

export default createContainer(() => {
    const
        subscription = {
            clientsSubscription: Meteor.subscribe("userClients"),
            grups_muscularsSubscription: Meteor.subscribe("userGrupsMusculars"),
        //    imatgesSubscription: Meteor.subscribe("userImatges"),
            exercicisSubscription: Meteor.subscribe("userExercicis")
        };

    return {
        clients: Clients.find().fetch(),
        grups_musculars: GrupsMusculars.find().fetch()
        // ,
        // imatges: Imatges.find().fetch()
    }
}, ClientsNoData);
