import React, { Component } from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

export default class SelectExercicisJEstyle extends Component {
        // PROPS: 
        //  
    constructor(props) {
        super(props);

        this.state = {
            selectGMsValues: null,
            selExercicis: []
        };
    }

    selectGMsUpdateValues = (selectGMsValues) => {
        this.setState({
            selectGMsValues,
            selExercicis: []
        });
    }

    render() {
        return (
            <div
                style={{
                    display: `grid`,
                    gridTemplateAreas: `"gms exs"
                                        "but but"`
                }}
            >
                <div
                    style={{
                        zIndex: 300,
                        position: `relative`,
                        gridArea: `gms`
                    }}
                >
                    <Select
                        id="selGMs"
                        placeholder="Grup Muscular"
                        options={this.props.grups_musculars.map(
                            (v,i,a) => ({
                                label: v.grupMuscularNom,
                                value: v._id
                            })
                        )}
                        value={this.state.selectGMsValues}
                        onChange={this.selectGMsUpdateValues}
                    />
                </div>

                <div
                    style={{
                        zIndex: 400,
                        position: `relative`,
                        gridArea: `exs`
                    }}
                >
                    <Select
                        id="selExercicis"
                        placeholder="Exercicis"
                        ref={selEx => this.selEx = selEx}
                        options={
                            (() => {
                               // let exs = new Set();
                                let 
                                    filteredExs = [],
                                    filteredExsFormatted = []
                                ;
                            //
                            // this.state.selectGMsValue.forEach(
                            //     (v,i,a) => {
                            //         gms.add(v.label);
                            //     }
                            // );thi

                                                                            // this.props.exercicis.reduce(
                                                                            //     (t,v,i,a) => {

                                                                            //         let gms = new Set();
                                                                            //         //
                                                                            //         if (this.state.selectGMsValue !== null) {
                                                                            //             this.state.selectGMsValue.forEach(
                                                                            //                 (v,i,a) => {
                                                                            //                     gms.add(v.label);
                                                                            //                 }
                                                                            //             );
                                                                            //         }

                                                                            //         if (
                                                                            //             gms.has(v.exerciciGrupMuscular)
                                                                            //         ) {
                                                                            //             t.push({
                                                                            //                 label: v.exerciciNom,
                                                                            //                 value: v._id
                                                                            //             });
                                                                            //         }

                                                                            //         return t;
                                                                            //     },
                                                                            //     []
                                                                            // )

                                // this.props.exercicis.reduce(
                                //     (t,v,i,a) => {
                                //         let exs = new Set();
        

                                //     },
                                //     []
                                // );

                                filteredExs = this.props.exercicis.filter(
                                    (v,i,a) => {
                                       // exs.add(v.label);
                                        if (this.state.selectGMsValues !== null) {
                                            console.dir("return: ", v.exerciciGrupMuscular === this.state.selectGMsValues.label);
                                            return v.exerciciGrupMuscular === this.state.selectGMsValues.label;
                                        }
                                    }
                                );

                                filteredExs.forEach(
                                    (v,i,a) => filteredExsFormatted.push({
                                        label: v.exerciciNom,
                                        value: v._id
                                    })
                                );

                                console.dir("filteredExsFormatted: ", filteredExsFormatted);

                                return filteredExsFormatted;
                                
                            })()
                        }

                        multi
                        value={this.state.selExercicis}
                        onChange={(selectExercicisValues) => {

                            //let
                                //selExercicisModificat = { ...this.state.selExercicis },
                            //     rutinaModificada = { ...this.props.rutina }
                            //;

                            // selExercicisModificat.exercicis = selectExercicisValues;
                            this.setState((prevState, props) => {
                                return {
                                    selExercicis: selectExercicisValues
                                };
                            });

                        

                            // rutinaModificada.sessions[this.props.indexSessio].exercicis = selectExercicisValues;
                            // Meteor.call(
                            //     'rutines.update',
                            //     this.props.rutinaId,
                            //     rutinaModificada
                            // );
                        }}
                    />
                </div>

                <div
                    style={{
                        gridArea: `but`
                    }}
                >
                    <button
                        onClick={ev => {
                            this.props.updateExercicisIGMsSelects(this.state.selExercicis);
                            
                            this.selectGMsUpdateValues(null); // Resetejar els valors dels selectors JE.

                            console.dir("selExercicis: ", this.state.selExercicis);
                        }}
                    >Inclou els exercicis a la llista
                    </button>
                </div>
            </div>
        );
    }
}