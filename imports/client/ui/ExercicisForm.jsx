import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
//import ReactDOM from 'react-dom';
import Bert from 'meteor/themeteorchef:bert';
import PujaArxiusAmbText from './PujaArxiusAmbText.jsx';
//import GrupMuscularOpt from './GrupMuscularOpt.jsx';

//import '../../api/collections/GrupsMusculars.js';

export default class ExercicisForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            arrImatgesPujades: [],
            seriesDefault: "4",
            repeticionsDefault: "10",
            descansMinutsDefault: "1",
            descansSegonsDefault: "0",
            carregaDefault: "0"
        };

        this.addExercici = this.addExercici.bind(this);
        this.handleImatgesPujades = this.handleImatgesPujades.bind(this);
    }

    addExercici(event) {
        event.preventDefault();
        let
            arrImatgesPujadesAmbText = [],
            exerciciNom = this.exerciciNom.value.trim(),
            exerciciGrupMuscular = this.selGrupMuscular.selectedOptions[0].value,
    //        exerciciGrupMuscularNom = this.selGrupMuscular.selectedOptions[0].value,
            exerciciDescripcio = this.exerciciDescripcio.value.trim()
            ,
                // exerciciSeriesDefault = this.exerciciSeriesDefault.value.trim(),
                // exerciciRepeticionsDefault = this.exerciciRepeticionsDefault.value.trim(),
                // exerciciDescansMinuts = this.exerciciDescansMinuts.value.trim(),
                // exerciciDescansSegons = this.exerciciDescansSegons.value.trim(),
                // exerciciCarrega = this.exerciciCarrega.value.trim()
            exerciciSeriesDefault = this.state.seriesDefault,
            exerciciRepeticionsDefault = this.state.repeticionsDefault,
            exerciciDescansMinuts = this.state.descansMinutsDefault,
            exerciciDescansSegons = this.state.descansSegonsDefault,
            exerciciCarrega = this.state.carregaDefault
        ;

        if (exerciciNom) {

            this.state.arrImatgesPujades.forEach(
                (v, i, a) => {
                    let
                        imgTaIterantValue = document.querySelector(`#taArx_${i}`).value,
                        arrImatgesPujadesAmbTextIterant = v
                    ;

                    arrImatgesPujadesAmbTextIterant.imgText = imgTaIterantValue;
                    arrImatgesPujadesAmbText.push(arrImatgesPujadesAmbTextIterant);

                    console.dir(arrImatgesPujadesAmbText);
                }
            );

            Meteor.call(
                'exercicis.insert',
                exerciciNom,
                exerciciGrupMuscular,
            //    exerciciGrupMuscularNom,
                exerciciDescripcio,
                exerciciSeriesDefault,
                exerciciRepeticionsDefault,
                exerciciDescansMinuts,
                exerciciDescansSegons,
                exerciciCarrega,
                this.state.arrImatgesPujades,
                (error, data) => {
                    if (error) {
                      Bert.alert(
                          "Logueja't abans d'introduir dades.",
                          "danger",
                          "fixed-top",
                          "fa-frown-o"
                      );
                    } else {
                        this.exerciciNom.value = "";
                        this.exerciciDescripcio.value = "";
                        this.exerciciSeriesDefault.value = "";
                        this.exerciciRepeticionsDefault.value = "";
                        this.exerciciDescansMinuts.value = "";
                        this.exerciciDescansSegons.value = "";
                        this.exerciciCarrega.value = "";
                    }
            });
            // Netegem els valors del formulari.
            this.exerciciNom.value = "";
            //this.selGrupMuscular.selectedOptions[0].value,
            this.exerciciDescripcio.value = "";
            this.exerciciSeriesDefault.value = "";
            this.exerciciRepeticionsDefault.value = "";
            this.exerciciDescansMinuts.value = "";
            this.exerciciDescansSegons.value = "";
            this.exerciciCarrega.value = "";
            // [...document.querySelectorAll(".taArx")].map(
            //     (v,i,a)=>v.value = ""
            // );
            document.querySelector("#divPreview").innerHTML = "";
        }

        this.props.cb();
    }

    handleImatgesPujades(arrImatgesPujades) {
        this.setState({
            arrImatgesPujades
        });
    }

    render() {
        return (
            <div id="divExercicisForm">
                <h2>Nou Exercici</h2>
                <form className="nouexercici" onSubmit={this.addExercici}>
                    <input
                        type="text"
                        ref={input => this.exerciciNom = input}
                        placeholder="Nom"
                    />
                    <textarea
                        ref={ta => this.exerciciDescripcio = ta}
                        placeholder="Descripció de l'exercici"
                    />
                    <select ref={sel => this.selGrupMuscular = sel}>
                        {
                            this.props.grups_musculars.map(grup_muscular =>
                                <option
                                    key={grup_muscular.grupMuscularNom}
                                    value={grup_muscular.grupMuscularNom}
                                    gmid={grup_muscular._id}
                                >
                                    { grup_muscular.grupMuscularNom }
                                </option>
                            )
                        }
                    </select>
                    <fieldset>
                        <legend>Valors per defecte: </legend>
                        <label>Sèries:</label>
                        <input
                            type="number"
                            min="1"
                            ref={input => this.exerciciSeriesDefault = input}
                            placeholder="Sèries"
                            value={this.state.seriesDefault}
                            title="Sèries"
                            onChange={()=>{
                                this.setState({
                                    seriesDefault: this.exerciciSeriesDefault.value.trim()
                                });
                            }}
                            style={{
                                width: `10em`,
                                margin: `0 auto`,
                                textAlign: `center`
                            }}
                        />
                        <label>Repeticions: </label>
                        <input
                            type="number"
                            min="1"
                            ref={input => this.exerciciRepeticionsDefault = input}
                            placeholder="Repeticions"
                            value={this.state.repeticionsDefault}
                            title="Repeticions"
                            onChange={()=>{
                                this.setState({
                                    repeticionsDefault: this.exerciciRepeticionsDefault.value.trim()
                                });
                            }}

                            style={{
                                width: `10em`,
                                margin: `0 auto`,
                                textAlign: `center`
                            }}
                        />
                        <label>Descans Minuts: </label>
                        <input
                            type="number"
                            min="0"
                            ref={input => this.exerciciDescansMinuts = input}
                            placeholder="Descans Minuts"
                            value={this.state.descansMinutsDefault}
                            title="Descans Minuts"
                            onChange={()=>{
                                this.setState({
                                    descansMinutsDefault: this.exerciciDescansMinuts.value.trim()
                                });
                            }}
                            style={{
                                width: `10em`,
                                margin: `0 auto`,
                                textAlign: `center`
                            }}
                        />
                        <label>Descans Segons: </label>
                        <input
                            type="number"
                            min="0"
                            max="60"
                            step="10"
                            ref={input => this.exerciciDescansSegons = input}
                            placeholder="Descans Segons"
                            onChange={()=>{
                                this.setState({
                                    descansSegonsDefault: this.exerciciDescansSegons.value.trim()
                                });
                            }}
                            value={this.state.descansSegonsDefault}
                            title="Descans Segons"

                            style={{
                                width: `10em`,
                                margin: `0 auto`,
                                textAlign: `center`
                            }}
                        />
                        <label>Càrrega: </label>
                        <input
                            type="number"
                            min="0"
                            step="5"
                            ref={input => this.exerciciCarrega = input}
                            placeholder="Càrrega"
                            value={this.state.carregaDefault}
                            title="Càrrega"
                            onChange={()=>{
                                this.setState({
                                    carregaDefault: this.exerciciCarrega.value.trim()
                                });
                            }}

                            style={{
                                width: `10em`,
                                margin: `0 auto`,
                                textAlign: `center`
                            }}
                        />
                    </fieldset>

                    {/*// Introduir arxius i imatges. Cal fer un bon component que puga ser reutilitzat.*/}
                    <PujaArxiusAmbText onImatgesPujades={this.handleImatgesPujades} />

                    <input
                        type="submit"
                        ref={input => this.exerciciSubmit = input}
                        value="Introduir"
                    />
                </form>
            </div>
        );
    }
};
