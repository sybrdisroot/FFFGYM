import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
//import jQuery from 'meteor/jquery';

export default class RutinaSingle extends Component{
  toggleChecked(){
    Meteor.call('rutines.update', this.props.rutina);
  }
  deleteRutina(){
    Meteor.call('rutines.delete', this.props.rutina);
  }
  render(){
    const rutinaClass = this.props.rutina.completed ? "checked" : "";
    //const status = this.props.client.completed ? <span className="spnCompleted">Completed</span> : "";

    return (
         <li
            className={rutinaClass}
            style={
                (this.props.rutinaId === this.props.rutina._id)
                    ?  {
                        background:`lime`
                    }
                    : {}
            }
        >
            <a className="aSingleRutina" href={"/rutina/"+this.props.rutina._id}>{this.props.rutina.nomRutina }</a>
            <button
              className="btDeleteRutina"
              onClick={() => {
                  if (confirm(`Vas a esborrar per complet la rutina ${this.props.rutina.nomRutina}. Estàs segur#?`)) {
                      this.deleteRutina.bind(this)
                  }
              }}
            >
              &times;
            </button>
      </li>
    );
  }
}
