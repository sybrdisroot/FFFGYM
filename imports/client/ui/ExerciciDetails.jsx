import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
//import ReactDOM from 'react-dom';
//import Bert from 'meteor/themeteorchef:bert';
import { createContainer } from 'meteor/react-meteor-data';
import './ExerciciDetails.scss';
import Tappable from 'react-tappable';

import sanitizeHtml from 'sanitize-html-react';
import Select from 'react-select';
//import renderHTML from 'react-render-html';
import ImatgeModificableExercici from './ImatgeModificableExercici.jsx';
import PujaArxiusAmbTextRFRExercici from './PujaArxiusAmbTextRFRExercici.jsx';


class ValorPD extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div
                style={
                    Object.assign(
                        {},
                        this.props.fStyle,
                        {
                            display: `grid`,
                            gridTemplate: `
                                "fTitle fTitle fTitle"
                                "numVal unitChar upArrow"
                                "numVal unitChar downArrow"
                            `,
                            gridTemplateColumns: `${
                                this.props.unitChar
                                ?   "40% 20% 40%"
                                :   "60% 0 40%"
                            }`,
                            border: `solid 1px black`,
                            borderRadius: `2em`,
                            //background: `rgba(255,255,255,.3)`
                            background: `linear-gradient(135deg, rgba(0,0,0,0) 0%,rgba(0,0,0,0.65) 100%)`,
                            margin: `.5px`
                        }
                )}

            >
                <div
                    style={{
                        //gridArea: `fTitle`,
                        //justifySelf: `center`,
                        fontVariant: `small-caps`,
                        fontSize: `.7em`,
                        textShadow: `1px 1px 0 rgba(255,255,255,.5)`,
                        color: `rgba(0,0,0,.5)`,
                        position: `absolute`,
                        marginLeft: `3em`
                    }}
                >{this.props.fTitle}
                </div>

                <span
                    style={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `numVal`,
                                justifySelf: this.props.unitChar ? `end` : `center`,
                                alignSelf: `center`,
                                fontWeight: `bold`,
                                textShadow: `1px 1px 0 rgba(255,255,255,.5)`
                                //fontSize: `4em`
                            }
                        )
                    }
                >{this.props.fVal}
                </span>

                <span
                    style={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `unitChar`,
                                justifySelf: `start`,
                                alignSelf: `center`,
                                textShadow: `1px 1px 0 rgba(255,255,255,.5)`
                                //fontWeight: `bold`,
                                //fontSize: `3.3em`
                            }
                        )
                    }
                >{this.props.unitChar}
                </span>

                <button
                    style={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `upArrow`,
                                justifySelf: `center`,
                                alignSelf: `end`,
                                fontSize: `1em`,
                                width: `80%`,
                                marginTop: `.3em`,
                                marginRight: `.5em`,
                                background: `linear-gradient(to bottom, rgba(184,225,252,1) 0%,rgba(169,210,243,1) 10%,rgba(144,186,228,1) 25%,rgba(144,188,234,1) 37%,rgba(144,191,240,.5) 50%,rgba(107,168,229,.5) 51%,rgba(162,218,245,.5) 83%,rgba(189,243,253,.5) 100%)`
                            }
                        )
                    }
                    onClick={() => {
                        switch (this.props.field) {
                            case "series": {
                                Meteor.call(
                                    'exercicis.series.update',
                                    this.props.exercici[0],
                                    Number(this.props.exercici[0].exerciciSeriesDefault) + this.props.step
                                )
                                break;
                            }
                            case "repeticions": {
                                Meteor.call(
                                    'exercicis.repeticions.update',
                                    this.props.exercici[0],
                                    Number(this.props.exercici[0].exerciciRepeticionsDefault) + this.props.step
                                )
                                break;
                            }
                            case "descansMinuts": {
                                Meteor.call(
                                    'exercicis.minuts.update',
                                    this.props.exercici[0],
                                    Number(this.props.exercici[0].exerciciDescansMinuts) + this.props.step
                                )
                                break;
                            }
                            case "descansSegons": {
                                Meteor.call(
                                    'exercicis.segons.update',
                                    this.props.exercici[0],
                                    Number(this.props.exercici[0].exerciciDescansSegons) + this.props.step
                                )
                                break;
                            }
                            case "carrega": {
                                Meteor.call(
                                    'exercicis.carrega.update',
                                    this.props.exercici[0],
                                    Number(this.props.exercici[0].exerciciCarrega) + this.props.step
                                )
                                break;
                            }
                        }
                    }}
                > ⯅
                </button>
                <button
                    style={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `downArrow`,
                                justifySelf: `center`,
                                alignSelf: `start`,
                                fontSize: `1em`,
                                width: `80%`,
                                marginBottom: `.15em`,
                                marginRight: `.5em`,
                                background: `linear-gradient(to bottom, rgba(184,225,252,1) 0%,rgba(169,210,243,1) 10%,rgba(144,186,228,1) 25%,rgba(144,188,234,1) 37%,rgba(144,191,240,.5) 50%,rgba(107,168,229,.5) 51%,rgba(162,218,245,.5) 83%,rgba(189,243,253,.5) 100%)`
                            }
                        )
                    }
                    onClick={() => {
                        switch (this.props.field) {
                            case "series": {
                                Meteor.call(
                                    'exercicis.series.update',
                                    this.props.exercici[0],
                                    this.props.exercici[0].exerciciSeriesDefault - this.props.step
                                )
                                break;
                            }
                            case "repeticions": {
                                Meteor.call(
                                    'exercicis.repeticions.update',
                                    this.props.exercici[0],
                                    this.props.exercici[0].exerciciRepeticionsDefault - this.props.step
                                )
                                break;
                            }
                            case "descansMinuts": {
                                Meteor.call(
                                    'exercicis.minuts.update',
                                    this.props.exercici[0],
                                    this.props.exercici[0].exerciciDescansMinuts - this.props.step
                                )
                                break;
                            }
                            case "descansSegons": {
                                Meteor.call(
                                    'exercicis.segons.update',
                                    this.props.exercici[0],
                                    this.props.exercici[0].exerciciDescansSegons - this.props.step
                                )
                                break;
                            }
                            case "carrega": {
                                Meteor.call(
                                    'exercicis.carrega.update',
                                    this.props.exercici[0],
                                    this.props.exercici[0].exerciciCarrega - this.props.step
                                )
                                break;
                            }
                        }
                    }}
                > ⯆
                </button>
            </div>
        );
    }
}

class ValorsPerDefecte extends Component {
    constructor(props) {
        super(props);


    }

    render() {
        return (
            // <div
            //     id="divVPD"
            //     style={{
            //         display: `grid`,
            //         borderRadius: `.5em`,
            //         border: `2px steelblue solid`
            //     }}
            // >
            //     <span id="spnSeries" className="spanDefaults">Sèries: {series.get()}</span>
            //     <span id="spnRepeticions"className="spanDefaults"> Repeticions: {repeticions.get()}</span>
            //     <div className="divDescans">
            //         <span id="spnDescansMinuts"className="spanDefaults">
            //             Descans: {descansMinuts.get()}
            //             <span style={{
            //                 fontSize: `3.5em`
            //
            //             }}>'
            //             </span>
            //         </span>
            //         <span id="spnDescansSegons" className="spanDefaults">
            //             {descansSegons.get()}
            //             <span style={{
            //                 fontSize: `3em`
            //             }}>"
            //             </span>
            //         </span>
            //     </div>
            //     <span id="spnCarrega" className="spanDefaults">Càrrega: {carrega.get()}%</span>
            // </div>
            <div
                style={{
                    display: `grid`,
                    gridTemplateAreas: `
                        "s r"
                        "dm ds"
                        "c c"
                    `,
                    margin: `1em`
                }}
            >
                <ValorPD
                    {...this.props}
                    field="series"
                    fTitle="Sèries"
                    fVal={series.get()}
                    unitChar=""
                    step={1}
                    fStyle={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `s`
                            }
                        )
                    }
                />
                <ValorPD
                    {...this.props}
                    field="repeticions"
                    fTitle="Reps"
                    fVal={repeticions.get()}
                    unitChar=""
                    step={1}
                    fStyle={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `r`
                            }
                        )
                    }
                />
                <ValorPD
                    {...this.props}
                    field="descansMinuts"
                    fTitle="Descans Minuts"
                    fVal={descansMinuts.get()}
                    unitChar="'"
                    step={1}
                    fStyle={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `dm`
                            }
                        )
                    }
                />
                <ValorPD
                    {...this.props}
                    field="descansSegons"
                    fTitle="Descans Segons"
                    fVal={descansSegons.get()}
                    unitChar='"'
                    step={10}
                    fStyle={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `ds`
                            }
                        )
                    }
                />
                <ValorPD
                    {...this.props}
                    field="carrega"
                    fTitle="Càrrega"
                    fVal={carrega.get()}
                    unitChar="%"
                    step={5}
                    fStyle={
                        Object.assign(
                            {},
                            this.props.fStyle,
                            {
                                gridArea: `c`
                            }
                        )
                    }
                />
            </div>
        );
    }
}

class ExerciciGrupMuscularCampEditable extends Component {
    constructor(props) {
        super(props);

        let
            gmAct =
                props.exercici
                ?   props.grups_musculars
                    .filter(
                        gm => gm._id === props.exercici[0].exerciciGrupMuscular
                    )
                :'',
            selectValue = {
                label: gmAct[0] ? gmAct[0].grupMuscularNom : "",
                value: gmAct[0] ? gmAct[0]._id : ""
            }
        ;
        //debugger;
        console.dir(`gmAct: `, gmAct[0]);

        this.state = {
            editant: false,
            selectValue
        };
    }

    updateValue = (nouVal) => {
        this.setState({
            selectValue: nouVal,
            editant: false
        });

        this.props.onChange();
    }

    render() {
        let
            arrGrupsMusculars = []
            ,
            gmArr = this.props.exercici?this.props.grups_musculars.filter(gm => gm._id === this.props.exercici[0].exerciciGrupMuscular):'',
            gmNom = this.props.exercici?gmArr.grupMuscularNom:''
        ;

        //console.dir(gmNom);
        this.props.grups_musculars.map(
            (v,i,a) => {
                //console.log(JSON.stringify(client));
                arrGrupsMusculars.push({
                    value: v._id,
                    label: v.grupMuscularNom
                });
            }
        );

        return (
            <div>
                {
                    this.props.exercici && !this.state.editant
                    ?
                        <div onClick={() => {
                            this.setState({
                                editant: true
                            });
                        }}>{this.state.selectValue.label}
                        </div>
                    :
                        <Select
                            value={this.state.selectValue}
                            onChange={this.updateValue}
                            options={arrGrupsMusculars}
                        />
                }
            </div>
        );
    }
}


class ExerciciDetailsNoData extends Component {
    constructor(props) {
        super(props);

        this.state = {
        //   subscription: {
        //     clients: Meteor.subscribe("userClients")
        //   }

            editantNom: false,
            editantImatge: NaN,
            editantDescrip: false,
            editantSeries: false,
            editantRepeticions: false,
            editantMinuts: false,
            editantSegons: false,
            editantCarrega: false
        };

        this.handleTapEventNom = this.handleTapEventNom.bind(this);
        this.handleEditantNom = this.handleEditantNom.bind(this);
        this.handleEstableixNom = this.handleEstableixNom.bind(this);

        this.handleTapEventDescrip = this.handleTapEventDescrip.bind(this);
        this.handleEditantDescrip = this.handleEditantDescrip.bind(this);
        this.handleEstableixDescrip = this.handleEstableixDescrip.bind(this);

        this.handleTapEventSeries = this.handleTapEventSeries.bind(this);
        this.handleEditantSeries = this.handleEditantSeries.bind(this);
        this.handleEstableixSeries = this.handleEstableixSeries.bind(this);

        this.handleTapEventRepeticions = this.handleTapEventRepeticions.bind(this);
        this.handleEditantRepeticions = this.handleEditantRepeticions.bind(this);
        this.handleEstableixRepeticions = this.handleEstableixRepeticions.bind(this);

        this.handleTapEventMinuts = this.handleTapEventMinuts.bind(this);
        this.handleEditantMinuts = this.handleEditantMinuts.bind(this);
        this.handleEstableixMinuts = this.handleEstableixMinuts.bind(this);

        this.handleTapEventSegons = this.handleTapEventSegons.bind(this);
        this.handleEditantSegons = this.handleEditantSegons.bind(this);
        this.handleEstableixSegons = this.handleEstableixSegons.bind(this);

        this.handleTapEventCarrega = this.handleTapEventCarrega.bind(this);
        this.handleEditantCarrega = this.handleEditantCarrega.bind(this);
        this.handleEstableixCarrega = this.handleEstableixCarrega.bind(this);
    }

    handleTapEventNom() {
        this.nom.setAttribute("contenteditable", "true");
    }
    handleEditantNom() {
        this.setState({
            editantNom: true
        });
    }
    handleEstableixNom() {
        Meteor.call(
            'exercicis.nom.update',
            this.props.exercici[0],
            this.nom.innerHTML
        );
        this.nom.setAttribute("contenteditable", "false");
        this.setState({
            editantNom: false
        });
    }

    // handlePressEventTitol() {
    //     this.titol.setAttribute("contenteditable", "true")
    // }

    handleEditantImatge(clau) {
        this.setState({
            editantImatge: clau
        })
    }

    handleTapEventDescrip() {
        this.descrip.setAttribute("contenteditable", "true");
    }
    handleEditantDescrip() {
        this.setState({
            editantDescrip: true
        });
    }
    handleEstableixDescrip() {
        Meteor.call(
            'exercicis.descrip.update',
            this.props.exercici[0],
            this.descrip.innerHTML
        );
        this.descrip.setAttribute("contenteditable", "false");
        this.setState({
            editantDescrip: false
        });
    }

    handleTapEventSeries() {
        this.series.setAttribute("contenteditable", "true");
    }
    handleEditantSeries() {
        this.setState({
            editantSeries: true
        });
    }
    handleEstableixSeries() {
        Meteor.call(
            'exercicis.series.update',
            this.props.exercici[0],
            this.series.innerHTML
        );
        this.series.setAttribute("contenteditable", "false");
        this.setState({
            editantSeries: false
        });
    }

    handleTapEventRepeticions() {
        this.repeticions.setAttribute("contenteditable", "true");
    }
    handleEditantRepeticions() {
        this.setState({
            editantRepeticions: true
        });
    }
    handleEstableixRepeticions() {
        Meteor.call(
            'exercicis.repeticions.update',
            this.props.exercici[0],
            this.repeticions.innerHTML
        );
        this.repeticions.setAttribute("contenteditable", "false");
        this.setState({
            editantRepeticions: false
        });
    }

    handleTapEventMinuts() {
        this.minuts.setAttribute("contenteditable", "true");
    }
    handleEditantMinuts() {
        this.setState({
            editantMinuts: true
        });
    }
    handleEstableixMinuts() {
        Meteor.call(
            'exercicis.minuts.update',
            this.props.exercici[0],
            this.minuts.innerHTML
        );
        this.minuts.setAttribute("contenteditable", "false");
        this.setState({
            editantMinuts: false
        });
    }

    handleTapEventSegons() {
        this.segons.setAttribute("contenteditable", "true");
    }
    handleEditantSegons() {
        this.setState({
            editantSegons: true
        });
    }
    handleEstableixSegons() {
        Meteor.call(
            'exercicis.segons.update',
            this.props.exercici[0],
            this.segons.innerHTML
        );
        this.segons.setAttribute("contenteditable", "false");
        this.setState({
            editantSegons: false
        });
    }

    handleTapEventCarrega() {
        this.carrega.setAttribute("contenteditable", "true");
    }
    handleEditantCarrega() {
        this.setState({
            editantCarrega: true
        });
    }
    handleEstableixCarrega() {
        Meteor.call(
            'exercicis.carrega.update',
            this.props.exercici[0],
            this.carrega.innerHTML
        );
        this.carrega.setAttribute("contenteditable", "false");
        this.setState({
            editantCarrega: false
        });
    }

    render() {
        let
            titol = new ReactiveVar(
                this.props.exercici[0]
                ?   <div>
                        <Tappable
                            onTap={this.handleTapEventNom}
                        >
                            <span
                                id="spnNom"
                                ref={nom => this.nom = nom}
                                onInput={this.handleEditantNom}
                            >
                                {this.props.exercici[0].exerciciNom}
                            </span>
                        </Tappable>
                        <button
                            style={{
                                visibility: this.state.editantNom ? `visible` : `hidden`,
                                display: this.state.editantNom ? `inline-block` : `none`
                            }}
                            onClick={this.handleEstableixNom}
                        >Estableix el nom
                        </button>
                    </div>
                :
                    `Carregant...`
            ),

            descripcio = new ReactiveVar(
                this.props.exercici[0]
                    ?   this.props.exercici[0].exerciciDescripcio
                    :   `Carregant...`
            ),

            imatgePrincipal = new ReactiveVar(
                (this.props.exercici[0] && this.props.exercici[0].arrImatges.length > 0)
                ?
                    // <div key={0} className="divExerciciImatgePrincipal">
                    //     <img
                    //         className="imgPrincipalExercici"
                    //         src={this.props.exercici[0].arrImatges[0].imgArx.buffer}
                    //         alt={this.props.exercici[0].arrImatges[0].imgArx.name}
                    //     />
                    //     <div className="divExerciciImgText divExerciciImgPrincipalText">
                    //         {this.props.exercici[0].arrImatges[0].imgText}
                    //     </div>
                    // </div>
                    //<<<<<<<<<<<<<<
                    <div key={0} className="divExerciciImatgePrincipal">
                    {
                        this.props.exercici[0].arrImatges
                        ?   <ImatgeModificableExercici
                                clau={0}
                                className="imgPrincipalExercici"
                                src={this.props.exercici[0].arrImatges[0].imgArx.buffer}
                                alt={this.props.exercici[0].arrImatges[0].imgArx.name}
                                peu={this.props.exercici[0].arrImatges[0].imgText}
                                exercici={this.props.exercici[0]}
                                imatge_amb_text_original={this.props.exercici[0].arrImatges[0]}
                                handleEditantImatge={this.handleEditantImatge}
                                rota={this.props.exercici[0].arrImatges[0].imgArx.rota}
                            />
                        :   null
                    }
                    </div>

                : `Carregant...`
            ),

            imatgesAdicionals = new ReactiveVar(
                (this.props.exercici[0] && this.props.exercici[0].arrImatges)
                ?
                    this.props.exercici[0].arrImatges.map(
                        (v, i, a) => {
                            if (i === 0) { return; }
                            return (
                                <div key={i} className="divExerciciImatges">
                                    {// <img className="imgExercici" src={v.imgArx.buffer} alt={v.imgArx.name} />
                                    // {
                                    //     (v.imgText.length > 0)
                                    //     ?   <div className="divExerciciImgText">
                                    //             {v.imgText}
                                    //         </div>
                                    //     :   null
                                    // }
                                    }
                                    {
                                        this.props.exercici[0].arrImatges
                                        ?   <ImatgeModificableExercici
                                                clau={i}
                                                className="imgExercici"
                                                src={this.props.exercici[0].arrImatges[i].imgArx.buffer}
                                                alt={this.props.exercici[0].arrImatges[i].imgArx.name}
                                                peu={this.props.exercici[0].arrImatges[i].imgText}
                                                exercici={this.props.exercici[0]}
                                                imatge_amb_text_original={this.props.exercici[0].arrImatges[i]}
                                                handleEditantImatge={this.handleEditantImatge}
                                                rota={this.props.exercici[0].arrImatges[i].imgArx.rota}
                                            />
                                        :   null
                                    }
                                </div>
                            );
                        }
                    )
                :
                    `Carregant...`
            )

            series = new ReactiveVar(
                this.props.exercici[0] && this.props.exercici[0].exerciciSeriesDefault
                    ? <div
                        style={{
                            display: `inline-block`
                        }}
                      >
                            <Tappable
                                onTap={this.handleTapEventSeries}
                            >
                                <span
                                    id="spnSeries"
                                    ref={ser => this.series = ser}
                                    onInput={this.handleEditantSeries}
                                    style={{
                                    //    fontSize: `3em`
                                    }}
                                  >
                                    { this.props.exercici[0].exerciciSeriesDefault }
                                  </span>
                            </Tappable>
                            <button
                                style={{
                                    visibility: this.state.editantSeries ? `visible` : `hidden`,
                                    display: this.state.editantSeries ? `inline-block` : `none`
                                }}
                                onClick={this.handleEstableixSeries}
                            >Estableix les sèries
                            </button>
                        </div>
                    : `--`
            ),
            repeticions = new ReactiveVar(
                this.props.exercici[0] && this.props.exercici[0].exerciciRepeticionsDefault
                    ? <div
                        style={{
                            display: `inline-block`
                        }}
                      >
                            <Tappable
                                onTap={this.handleTapEventRepeticions}
                            >
                                <span
                                    id="spnRepeticions"
                                    ref={rep => this.repeticions = rep}
                                    onInput={this.handleEditantRepeticions}
                                    style={{
                                        // fontSize: `3em`
                                    }}
                                  >
                                    { this.props.exercici[0].exerciciRepeticionsDefault }
                                  </span>
                            </Tappable>
                            <button
                                style={{
                                    visibility: this.state.editantRepeticions ? `visible` : `hidden`,
                                    display: this.state.editantRepeticions ? `inline-block` : `none`
                                }}
                                onClick={this.handleEstableixRepeticions}
                            >Estableix les repeticions
                            </button>
                        </div>
                    : `--`
            ),
            descansMinuts = new ReactiveVar(
                this.props.exercici[0] && this.props.exercici[0].exerciciDescansMinuts
                    ? <div
                        style={{
                            display: `inline-block`
                        }}
                      >
                            <Tappable
                                onTap={this.handleTapEventMinuts}
                            >
                                <span
                                    id="spnMinuts"
                                    ref={min => this.minuts = min}
                                    onInput={this.handleEditantMinuts}
                                    style={{
                                        // fontSize: `3em`
                                    }}
                                  >
                                    { this.props.exercici[0].exerciciDescansMinuts }
                                  </span>
                            </Tappable>
                            <button
                                style={{
                                    visibility: this.state.editantMinuts ? `visible` : `hidden`,
                                    display: this.state.editantMinuts ? `inline-block` : `none`
                                }}
                                onClick={this.handleEstableixMinuts}
                            >Estableix els minuts
                            </button>
                        </div>
                    : `--`
            ),
            descansSegons = new ReactiveVar(
                this.props.exercici[0] && this.props.exercici[0].exerciciDescansSegons
                    ? <div
                        style={{
                            display: `inline-block`
                        }}
                      >
                            <Tappable
                                onTap={this.handleTapEventSegons}
                            >
                                <span
                                    id="spnSegons"
                                    ref={seg => this.segons = seg}
                                    onInput={this.handleEditantSegons}
                                    style={{
                                        // fontSize: `3em`
                                    }}
                                  >
                                    { this.props.exercici[0].exerciciDescansSegons.length === 1
                                        ? `0${this.props.exercici[0].exerciciDescansSegons}`
                                        : this.props.exercici[0].exerciciDescansSegons
                                    }
                                  </span>
                            </Tappable>
                            <button
                                style={{
                                    visibility: this.state.editantSegons ? `visible` : `hidden`,
                                    display: this.state.editantSegons ? `inline-block` : `none`
                                }}
                                onClick={this.handleEstableixSegons}
                            >Estableix els segons
                            </button>
                        </div>
                    : `--`
            ),
            carrega = new ReactiveVar(
                (this.props.exercici[0] && this.props.exercici[0].exerciciCarrega)
                    ? <div
                        style={{
                            display: `inline-block`
                        }}
                      >
                            <Tappable
                                onTap={this.handleTapEventCarrega}
                            >
                                <span
                                    id="spnCarrega"
                                    ref={car => this.carrega = car}
                                    onInput={this.handleEditantCarrega}
                                    style={{
                                        // fontSize: "3em"
                                    }}
                                >
                                    { this.props.exercici[0].exerciciCarrega }
                                </span>
                            </Tappable>
                            <button
                                style={{
                                    visibility: this.state.editantCarrega ? `visible` : `hidden`,
                                    display: this.state.editantCarrega ? `inline-block` : `none`
                                }}
                                onClick={this.handleEstableixCarrega}
                            >Estableix la carrega
                            </button>
                        </div>
                    : `--`
            )

            ,
            grupMuscular = new ReactiveVar(
                (this.props.exercici[0] && this.props.exercici[0].exerciciGrupMuscular)
                    ? <div
                        style={{
                            display: `inline-block`
                        }}
                      >
                            <Tappable
                                //onTap={this.handleTapEventGrupMuscular}
                            >
                                <span
                                    id="spnGrupMuscular"
                                    ref={gm => this.grupMuscular = gm}

                                    style={{
                                        // fontSize: "3em"
                                    }}
                                >
                                    { this.props.exercici[0].exerciciGrupMuscular }
                                </span>
                            </Tappable>
                            {// <button
                            //     style={{
                            //         visibility: this.state.handleEditantGrupMuscular ? `visible` : `hidden`,
                            //         display: this.state.editantGrupMuscular ? `inline-block` : `none`
                            //     }}
                            //     onClick={this.handleEstableixGrupMuscular}
                            // >Estableix la carrega
                            // </button>

                            // De l'span de dalt:
                            //onInput={this.handleEditantGrupMuscular}

                        }
                        </div>
                    : `--`
            )

            ,
                exs = this.props.exercicis,
                gms = this.props.grups_musculars,
                exercici = new ReactiveVar(
                    this.props.exercici[0]
                    ?   this.props.exercici[0]
                    :   `--`
                ),
                gm = grupMuscular
        ;

        return (
            <div>
                
                <div 
                    id="divExercici"
                    style={{
                        fontSize: `1.2em`,
                        border: `1px solid black`,
                        padding: `3em`,
                        background: `rgba(255,255,255,.5)`
                    }}
                >
                    <div
                        style={{
                            display: `grid`,
                            gridTemplateAreas: `"h3 gm"`
                        }}
                    >
                        <h3
                            style={{
                                gridArea: `h3`,
                                display: `inline-block`,
                                color: `rgba(100,100,100,.5)`,
                                fontStyle: `italic`,
                                fontVariant: `small-caps`,
                                textDecoration: `underline`

                            }}
                        >Exercici:
                        </h3>
                        <div
                            style={{
                                gridArea: `gm`,
                                justifySelf: `center`,
                                fontStyle: `italic`,
                                color: `steelblue`
                            }}
                        >{
                            grupMuscular.get()

                            // this.props.exercici[0]
                            // ?   this.props.exercici[0].
                            // :   `--`

                            // <ExerciciGrupMuscularCampEditable
                            //     grups_musculars={gms}
                            //     exercici={exercici.get()}
                            //     onChange={() => {
                            //         Meteor.call('exercicis.gm.update', exercici.get(), gm.get());
                            //         // console.log("userId: ", Meteor.userId());
                            //         // console.log("ex.uId: ", exercici[0].user);
                            //         // console.dir("Ex: ", exercici[0]);
                            //         // console.dir("gm: ", gm);
                            //
                            //     }}
                            // />
                        }
                        </div>
                    </div>

                    <h2
                        style={{
                            textAlign: `center`,
                            fontVariant: `small-caps`,

                            //background: `rgba(255, 255, 255, 0.5)`,
                            color: `steelblue`,
                            textShadow: `1px 1px 3px grey`
                        }}
                        ref={t => this.titol = t}
                    >{titol.get()}
                    </h2>

                    <div className="divGridExerciciImgs">
                        {imatgePrincipal.get()}
                    </div>

                    {//   descripcio.get()
                    //     ?   <div>
                    //             <Tappable
                    //                 onTap={this.handleTapEventDescrip}
                    //             >
                    //                 <div
                    //                     className="divExerciciDescripcio"
                    //                     ref={d => this.descrip = d}
                    //                     onInput={this.handleEditantDescrip}
                    //                 >
                    //                     {descripcio.get()}
                    //                 </div>
                    //             </Tappable>
                    //             <button
                    //                 style={{
                    //                     visibility: this.state.editantDescrip ? `visible` : `hidden`,
                    //                     display: this.state.editantDescrip ? `inline-block` : `none`
                    //                 }}
                    //                 onClick={this.handleEstableixDescrip}
                    //             >Estableix la descripció
                    //             </button>
                    //         </div>
                    //     :   <button
                    //             style={{
                    //                 display: `block`
                    //             }}
                    //             onClick={() => this.setState({
                    //                 edtiantDescrip: true
                    //             })}
                    //         >Descripció de l'exercici
                    //         </button>
                    //
                    }

                    <div className="divGridExerciciImgs">
                        {imatgesAdicionals.get()}
                    </div>
                    <ValorsPerDefecte
                        exercici={this.props.exercici}
                        //fTitle="Un titolet"
                        fStyle={{
                            //gridArea: `numVal`,
                            fontSize: `2em`
                        }}

                        // unitChar={}
                        // step={}
                        // gArea={}
                    />
                    <div
                        style={{
                            display:
                            this.state.afegitFet
                            ? `none`
                            : `grid`
                        }}
                    >
                        <AfegeixImatges
                            exercici={this.props.exercici[0]}
                            afegitFet={this.afegitFet}
                        />
                    </div>
                </div>

                <div
                    style={{
                        textAlign: `right`
                    }}
                >
                    <button
                        onClick={() => $("#divExercici").printThis()}
                    >Imprimeix
                    </button>
                </div>
            </div>
          );
    }
}

class AfegeixImatges extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imatgesPujades: []
        }
    }

    render() {
        return (
            <div
                style={{
                    display: `grid`
                }}
            >
                <PujaArxiusAmbTextRFRExercici
                    exercici={this.props.exercici}
                    afegitFet={this.props.afegitFet}
                />

            </div>
        );
    }
}

// ResolutionDetails.propTypes = {
//   res: PropTypes.array.isRequired
// };
//
export default createContainer(({ params }) => {
    const
        { id } = params,

        subscription = {
            clientsSubscription: Meteor.subscribe("userClients"),
            grups_muscularsSubscription: Meteor.subscribe("userGrupsMusculars"),
        //    imatgesSubscription: Meteor.subscribe("userImatges"),
            exercicisSubscription: Meteor.subscribe("userExercicis")
        };

    let
        aquestExercici = Exercicis.find({_id: id}).fetch();//,
        //arrImatges = aquestGM.arrImatges || [];



  //console.log(props);
    return {
        clients: Clients.find().fetch(),
        exercici: Exercicis.find({_id: id}).fetch(),
        exercicis: Exercicis.find().fetch(),
        rutines: Rutines.find().fetch(),
        //imatges: Imatges.find({user: Meteor.userId()}).fetch(),
        grups_musculars: GrupsMusculars.find().fetch()
    }
}, ExerciciDetailsNoData);

//res: Resolutions.find({_id: this.props.id}).fetch()
//res: "Algo per dir algo..."

// imatges: Imatges.find({
//     _id: { $in: this.props.grups_musculars[0].arrImatges }
// }).fetch()
