import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
//import ReactDOM from 'react-dom';
//import Bert from 'meteor/themeteorchef:bert';
import { createContainer } from 'meteor/react-meteor-data';

import './ClientDetails.scss';

import moment from 'moment';
import Tappable from 'react-tappable';

import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';

import subYears from 'date-fns/sub_years';

import ImatgeModificableClient from './ImatgeModificableClient.jsx';



class ClientDetailsNoData extends Component {
    constructor(props) {
        super(props);

        this.state = {
        //   subscription: {
        //     clients: Meteor.subscribe("userClients")
        //   }
            editantNom: false,
            editantCognoms: false,
            editantNaixement: false,
            dateOfBirth: null,
            editantTelefon: false
        };
    }

    handleTapEventCognoms = () => {
        this.cognoms.setAttribute("contenteditable", "true");
    }

    handleEditaCognoms = () => {
        this.setState({
            editantCognoms: true
        });
    }

    handleEstableixCognoms = () => {
        Meteor.call(
            'clients.cognoms.update',
            this.props.client[0],
            this.cognoms.innerHTML
        );
        this.cognoms.setAttribute("contenteditable", "false");
        this.setState({
            editantCognoms: false
        });
    }

    handleTapEventNom = () => {
        this.nom.setAttribute("contenteditable", "true");
    }

    handleEditaNom = () => {
        this.setState({
            editantNom: true
        });
    }

    handleEstableixNom = () => {
        Meteor.call(
            'clients.nom.update',
            this.props.client[0],
            this.nom.innerHTML
        );
        this.nom.setAttribute("contenteditable", "false");
        this.setState({
            editantNom: false
        });
    }


    handleDateOfBirth = (date) => {
        this.setState({
            dateOfBirth: date
        });
        // Açò no és més que un miserable hack per fer funcionar adequadament la funció de seleccionar l'any.
        setTimeout(() => {
            document
                .querySelectorAll("span.Cal__Header__date")[1]
                .click();

            Meteor.call(
                'clients.naixement.update',
                this.props.client[0],
                date,
                (error, result) => {
                    this.setState({
                        editantNaixement: false
                    });
                }
            );
        }, 1200);
        
    }

    handleTapEventNaixement = () => {
        
    }

    handleEditaNaixement = () => {
        this.setState({
            editantNaixement: true
        });
    }

    handleEstableixNaixement = (date) => {
        
        //this.n.setAttribute("contenteditable", "false");
        
    }

    handleTapEventTelefon = () => {
        this.telefon.setAttribute("contenteditable", "true");
    }

    handleEditaTelefon = () => {
        this.setState({
            editantTelefon: true
        });
    }
    
    handleEstableixTelefon = () => {
        Meteor.call(
            'clients.telefon.update',
            this.props.client[0],
            this.telefon.innerHTML
        );
        this.telefon.setAttribute("contenteditable", "false");
        this.setState({
            editantTelefon: false
        });
    }


    handleTapEventEmail = () => {
        this.email.setAttribute("contenteditable", "true");
    }

    handleEditaEmail = () => {
        this.setState({
            editantEmail: true
        });
    }
    
    handleEstableixEmail = () => {
        Meteor.call(
            'clients.email.update',
            this.props.client[0],
            this.email.innerHTML
        );
        this.email.setAttribute("contenteditable", "false");
        this.setState({
            editantEmail: false
        });
    }


    handleTapEventAddress = () => {
        this.address.setAttribute("contenteditable", "true");
    }

    handleEditaAddress = () => {
        this.setState({
            editantAddress: true
        });
    }
    
    handleEstableixAddress = () => {
        Meteor.call(
            'clients.address.update',
            this.props.client[0],
            this.address.innerHTML
        );
        this.address.setAttribute("contenteditable", "false");
        this.setState({
            editantAddress: false
        });
    }


    handleTapEventObservacions = () => {
        this.observacions.setAttribute("contenteditable", "true");
    }

    handleEditaObservacions = () => {
        this.setState({
            editantObservacions: true
        });
    }
    
    handleEstableixObservacions = () => {
        Meteor.call(
            'clients.observacions.update',
            this.props.client[0],
            this.observacions.innerHTML
        );
        this.address.setAttribute("contenteditable", "false");
        this.setState({
            editantObservacions: false
        });
    }



    componentWillUnmount() {
    //this.props.clients.stop();
    }

    render() {
    //console.log(this.props.res);

    /*if (this.props.clients.ready()) {*/
        return (
            <div>

            
                <div 
                    id="divClient"
                    style={{
                        fontSize: `1.2em`,
                        border: `1px solid black`,
                        padding: `3em`,
                        background: `rgba(255,255,255,.5)`,
                        display: `grid`,
                        gridTemplateAreas: `
                            "pagina  foto"
                            "nom  foto"
                            "alta  foto"
                            "naixement  foto"
                            "telefon  foto"
                            "email  foto"
                            "adreça  foto"
                            "observacionsLbl  ."
                            "observacions . "
                        `
                    }}
                >
                    <h2
                        style={{
                            gridArea: `pagina`,
                            fontVariant: `small-caps`,
                            color: `grey`,
                            textDecoration: `underline`
                        }}
                    >Client: </h2>

                
                        {
                            this.props.client[0] ?
                            <div>
                                <Tappable
                                    onTap={this.handleTapEventCognoms}
                                >
                                    <h1
                                        ref={cognoms => this.cognoms = cognoms}
                                        style={{
                                            display: `inline`
                                        }}
                                        onInput={this.handleEditaCognoms}
                                    >
                                        {/* <span id="spnCognom">
                                            {this.props.client[0].clientCognoms}
                                        </span>, 
                                        <span id="spnNom">
                                            {this.props.client[0].clientNom}
                                        </span> */
                                            `${this.props.client[0].clientCognoms}`
                                        }
                                    </h1>
                                </Tappable>
                                {`, `} 
                                <Tappable
                                    onTap={this.handleTapEventNom}
                                >
                                    <h1
                                        ref={nom => this.nom = nom}
                                        style={{
                                            display: `inline`
                                        }}
                                        onInput={this.handleEditaNom}
                                    >
                                        {`${this.props.client[0].clientNom}`}
                                    </h1>
                                </Tappable>

                            </div> :
                            `Carregant...`
                        }
                    
                    <button
                        style={{
                            visibility: this.state.editantCognoms ? `visible` : `hidden`,
                            display: this.state.editantCognoms ? `inline-block` : `none`
                        }}
                        onClick={this.handleEstableixCognoms}
                    >Estableix els cognoms
                    </button>

                    <button
                        style={{
                            visibility: this.state.editantNom ? `visible` : `hidden`,
                            display: this.state.editantNom ? `inline-block` : `none`
                        }}
                        onClick={this.handleEstableixNom}
                    >Estableix el nom
                    </button>

                    <div
                        style={{
                            gridArea: `naixement`
                        }}
                    >
                        <label
                            style={{
                                fontWeight: `bold`,
                                marginRight: `1em`
                            }}
                        >
                            Data de naixement: 
                        </label>
                        <Tappable
                            onTap={this.handleEditaNaixement}
                        >
                            <span
                                style={{
                                    display: `inline`
                                }}
                            >
                                {
                                    this.props.client[0] 
                                    ? moment(this.props.client[0].clientDateOfBirth).format(`l`)
                                    : `Carregant...`
                                }
                            </span>
                        </Tappable>
                        <div
                            style={{
                                visibility: this.state.editantNaixement ? `visible` : `hidden`,
                                display: this.state.editantNaixement ? `inline-block` : `none`
                            }}
                        >
                            <InfiniteCalendar
                                display="years"
                                selected={null}
                                min={new Date(1940, 0, 1)}
                                minDate={new Date(1940, 0, 1)}
                                max={subYears(Date(), 10)}
                                maxDate={subYears(Date(), 10)}

                                locale={{
                                    locale: require('date-fns/locale/ca'),
                                    headerFormat: 'dddd, D MMM',
                                    weekdays: ["Dmg","Dll","Dm","Dcs","Djs","Dvs","Dss"],
                                    weekStartsOn: 1,
                                    blank: 'Data de naixement: ',
                                    todayLabel: {
                                        long: 'Anar a avui',
                                        short: 'Avui.'
                                    }
                                }}
                                onSelect={this.handleDateOfBirth}
                                ref={infCal => this.infCal = infCal}
                            />
                        </div>
                    </div>
                    
                    <div
                        style={{
                            gridArea: `alta`
                        }}
                    >
                        <label
                            style={{
                                fontWeight: `bold`,
                                marginRight: `1em`
                            }}
                        >
                            Alta: 
                        </label>
                        <span>{
                            this.props.client[0] 
                            ? moment(this.props.client[0].createdAt).format(`l`)
                            : `Carregant...`}
                        </span>
                    </div>

                    <div
                        style={{
                            gridArea: `telefon`
                        }}
                    >
                        <label
                            style={{
                                fontWeight: `bold`,
                                marginRight: `1em`
                            }}
                        >Telèfon: 
                        </label>
                        <Tappable
                            onTap={this.handleTapEventTelefon}
                        >
                            <span
                                ref={telefon => this.telefon = telefon}
                                style={{
                                    display: `inline`
                                }}
                                onInput={this.handleEditaTelefon}
                            >{
                                this.props.client[0] 
                                ? this.props.client[0].clientMobil 
                                : `Carregant...`}
                            </span>
                        </Tappable>

                        <button
                            style={{
                                visibility: this.state.editantTelefon ? `visible` : `hidden`,
                                display: this.state.editantTelefon ? `inline-block` : `none`
                            }}
                            onClick={this.handleEstableixTelefon}
                        >Estableix el telèfon
                        </button>
                    </div>

                    
                    <div
                        style={{
                            gridArea: `email`
                        }}
                    >
                        <label
                            style={{
                                fontWeight: `bold`,
                                marginRight: `1em`
                            }}
                        >Email: 
                        </label>
                        <Tappable
                            onTap={this.handleTapEventEmail}
                        >
                            <span
                                ref={email => this.email = email}
                                style={{
                                    display: `inline`
                                }}
                                onInput={this.handleEditaEmail}
                            >{
                                this.props.client[0] 
                                ? this.props.client[0].clientEmail 
                                : `Carregant...`}
                            </span>
                        </Tappable>

                        <button
                            style={{
                                visibility: this.state.editantEmail ? `visible` : `hidden`,
                                display: this.state.editantEmail ? `inline-block` : `none`
                            }}
                            onClick={this.handleEstableixEmail}
                        >Estableix el correu electronic
                        </button>
                    </div>

                    <div
                        style={{
                            gridArea: `adreça`
                        }}
                    >
                        <label
                            style={{
                                fontWeight: `bold`,
                                marginRight: `1em`
                            }}
                        >Adreça: 
                        </label>
                        <Tappable
                            onTap={this.handleTapEventAddress}
                        >
                            <span
                                ref={address => this.address = address}
                                style={{
                                    display: `inline`
                                }}
                                onInput={this.handleEditaAddress}
                            >{
                                this.props.client[0] 
                                ? this.props.client[0].clientAddress 
                                : `Carregant...`}
                            </span>
                        </Tappable>

                        <button
                            style={{
                                visibility: this.state.editantAddress ? `visible` : `hidden`,
                                display: this.state.editantAddress ? `inline-block` : `none`
                            }}
                            onClick={this.handleEstableixAddress}
                        >Estableix l'adreça
                        </button>
                    </div>

                    <label
                        style={{
                            fontWeight: `bold`,
                            marginRight: `1em`,
                            gridArea: `observacionsLbl`
                        }}
                    >Observacions: 
                    </label>
                    <Tappable
                        onTap={this.handleTapEventObservacions}
                        style={{
                            gridArea: `observacions`
                        }}
                    >
                        <div
                            className="divClientObservacions"
                            ref={observacions => this.observacions = observacions}
                            style={{
                                display: `block`
                            }}
                            onInput={this.handleEditaObservacions}
                        >
                            {
                                this.props.client[0] ?
                                this.props.client[0].clientObservacions :
                                `Carregant...`
                            }
                        </div>
                    </Tappable>
                    <button
                        style={{
                            visibility: this.state.editantObservacions ? `visible` : `hidden`,
                            display: this.state.editantObservacions ? `inline-block` : `none`
                        }}
                        onClick={this.handleEstableixObservacions}
                    >Estableix les observacions
                    </button>

                    <div 
                        className="divGridClientImgs"
                        style={{
                            gridArea: `foto`
                        }}
                    >
                        {   this.props.client[0] && this.props.client[0].arrImatges ?
                            this.props.client[0].arrImatges.map(
                                (v, i, a) => {
                                    return (
                                        <div>
                                            {/* <div key={i} className="divClientImatges">
                                                <img className="imgClient" src={v.imgArx.buffer} alt={v.imgArx.name} />
                                                <div className="divClientImgText">
                                                    {v.imgText}
                                                </div>
                                            </div> */}

                                            { 
                                                this.props.client[0].arrImatges
                                                ?   <ImatgeModificableClient
                                                        key={i}
                                                        clau={i}
                                                        src={v.imgArx.buffer}
                                                        alt={v.imgArx.name}
                                                        peu={v.imgText}
                                                        client={this.props.client[0]}
                                                        imatge_amb_text_original={v}
                                                        handleEditantImatge={this.handleEditantImatge}
                                                    />
                                                :   null
                                            }
                                        </div>
                                    );
                                }
                            ) :
                            `Carregant...`
                        }
                    </div>

                </div>
                
                <div
                    style={{
                        textAlign: `right`
                    }}
                >
                    <button
                        onClick={() => $("#divClient").printThis()}
                    >Imprimeix
                    </button>
                </div>
            </div>
          );
        /*}*/

//this.props.grup_muscular[0]._id
        // <h6>Created at: {this.props.grups_musculars[0].createdAt.toString()}</h6>
        // <h4>Nom: {this.props.grups_musculars[0].grupMuscularNom}</h4>
        // <h4>Descripció: {this.props.grups_musculars[0].grupMuscularDescripcio}</h4>
        // <div className="divGMImatges">
        //     { this.props.imatges.map((el, index) =>
        //         <img key={index} className="imgGM" src={el.data} />
        //     )}
        // </div>

    /*return (
      <span>Loading...</span>
  );*/
    }
}

// ResolutionDetails.propTypes = {
//   res: PropTypes.array.isRequired
// };
//
export default createContainer(({ params }) => {
    const
        { id } = params,

        subscription = {
            clientsSubscription: Meteor.subscribe("userClients"),
            grups_muscularsSubscription: Meteor.subscribe("userGrupsMusculars"),
        //    imatgesSubscription: Meteor.subscribe("userImatges"),
            exercicisSubscription: Meteor.subscribe("userExercicis")
        };

    let
        aquestClient = Clients.find({_id: id}).fetch();//,
        //arrImatges = aquestGM.arrImatges || [];



  //console.log(props);
    return {
        clients: Clients.find().fetch(),
        client: Clients.find({_id: id}).fetch(),
        exercicis: Exercicis.find().fetch(),
        rutines: Rutines.find().fetch(),
    //    imatges: Imatges.find({user: Meteor.userId()}).fetch()
    }
}, ClientDetailsNoData);

//res: Resolutions.find({_id: this.props.id}).fetch()
//res: "Algo per dir algo..."

// imatges: Imatges.find({
//     _id: { $in: this.props.grups_musculars[0].arrImatges }
// }).fetch()
