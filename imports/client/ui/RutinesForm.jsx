import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import Bert from 'meteor/themeteorchef:bert';
import InfiniteCalendar, { withRange, Calendar } from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';
import {
    format,
    getISOWeek,
    startOfWeek,
    compareDesc,
    endOfWeek,
    addDays
} from 'date-fns';
import caLocale from 'date-fns/locale/ca';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Modal from 'react-modal';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';
import moment from 'moment';
import 'moment/locale/ca';
import capitalize from 'lodash/capitalize';
import QuadreSessio from './QuadreSessio.jsx';
import SessioOut from './SessioOut.jsx';
import randomString from '/imports/utils/randomString.js';

import {
    BootstrapTable,
    TableHeaderColumn
} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import './bootstrap.css';
import sanitizeHtml from 'sanitize-html-react';

import styled from 'styled-components';

import { ReactiveVar } from 'meteor/reactive-var';
//-import { Session } from 'meteor/session';
import flatMap from 'lodash/flatMap';
import isEqual from 'lodash/isEqual';

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

// var dateArray = getDates(new Date(), (new Date()).addDays(2));
// for (i = 0; i < dateArray.length; i ++ ) {
//   alert (dateArray[i]);

import * as d3 from "d3";

import mergeABP from '/imports/utils/mergeABP.js';
import SelectExercicisJEstyle from './SelectExercicisJEstyle.jsx';
import { prependOnceListener } from 'cluster';
// }

import './RutinesForm.scss';
//import {SortableContainer, SortableElement} from 'react-sortable-hoc';
//import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


const
    today = new Date(),
    lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7),
    lastYear = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate()),
    nextYear = new Date(today.getFullYear() + 1, today.getMonth(), today.getDate()),
    Avatar = (props) => (
        <img
            src={
                props.client ? (
                    props.client.value.arrImatges[0] ?
                        props.client.value.arrImatges[0].imgArx.buffer :
                        `anon0.jpg`
                ) : `anon0.jpg`
            }
            alt="Avatar del client"
            style={{
                borderRadius: `10em`,
                maxWidth: `12em`,
                maxHeight: `12em`,
                display: `inline-block`,
                boxShadow: `0 .2em .1em black`
            }}
        />
    )
    ;

class Rutina extends Component {
    constructor(props) {
        super(props);

        let
            rutinaId = props.rutinaId,
            rutina = {
                id: props.rutinaId || null,
                client: props.rutinaId || null,
                sessions: props.sessions || []
            }
        ;

        this.periode = {
            click: () => { }
        };

        this.state = {
            rangeOfDates: {},
            selectClientsValue: null,

            selectGMsValue: ``,
            maxIndexExercicis: 0,
            setmanes: {},
            chkFotos: true,
            nSetmanes: 0,
            extremsSetmanes: {},
            rutinaCreada: !!props.rutinaId || false,
            rutina,
            rutinaId: props.rutinaId,
            periode: `${capitalize(moment(rutina.dataIni).format("dddd, ll"))} → ${capitalize(moment(rutina.dataFi).format("dddd, ll"))}`,

            dataMoments: {},
            /////////////// Opcions d'impressio
            impNomRutina: true,
            impNomClient: true,
            impPeriode: true,
            impObsGen: true,
            impAvatarCli: false,
            impEntrenador: true,


            impOpcionsVisibles: false,
            diesVisibles: true,

            editantSessio: false,
            editantRutina: false,

            entrenador: "",
            sessions: [],
            opcionsImpressio: {},

            opcionsImpressioActuals: []

            // ,

            // impNomSessio: true,
            // impObsSessio: true,
            // impImatgesExs: true,
            // impDescExs: true,
            // impEscalf: true,
            // impTorCalma: true
        }

        this.rutinaNom = {
            value: this.state.rutina.nomRutina || ""
        };

        this.entrenador = "";
        this.taObservacionsRutina = "";

    }

    componentWillReceiveProps(nextProps) {
        let
            rutina = Rutines.findOne({
                _id: nextProps.rutinaId
            })
            ,

            opcionsImpressioActuals = rutina && rutina.sessions.map(
                (v, i, a) => v.opcionsImpressio
            )
        ;

        //if(this.rutinaNom !== null) {
        this.rutinaNom = {
            value: rutina ? rutina.nomRutina : ""
        };
        //} else {}
        this.entrenador = {
            value: rutina ? rutina.entrenador : ""
        };

        this.taObservacionsRutina = {
            value: rutina ? rutina.observacions : ""
        };

        if (rutina) {
            let
                dataMoments = {
                    eventType: 3,
                    start: moment(rutina.dataIni)._d,
                    end: moment(rutina.dataFi)._d
                },
                sessions = []
            ;
            //      console.dir("DataMoments: ", dataMoments);

            let
                data = dataMoments,
                setmanes = {},
                rangeOfDates = data,
                nSetmanes = getISOWeek(data.end) - getISOWeek(data.start) + 1,
                dataIniSetmana,
                dataFiSetmana,
                nSetmanaISO,
                nSetmanaRutina,
                setmana,
                extremsSetmanes,
                finalPrimeraSetmana,
                iniciUltimaSetmana
            ;


            if (data.eventType === 3) {

                if (compareDesc(rangeOfDates.end, endOfWeek(rangeOfDates.start, { weekStartsOn: 1 })) === 1) {
                    finalPrimeraSetmana = rangeOfDates.end;
                } else {
                    finalPrimeraSetmana = endOfWeek(rangeOfDates.start, { weekStartsOn: 1 });
                }

                //----

                if (nSetmanes === 1) {
                    iniciUltimaSetmana = rangeOfDates.start;
                } else {
                    iniciUltimaSetmana = startOfWeek(rangeOfDates.end, { weekStartsOn: 1 });
                }

                extremsSetmanes = Object.assign(
                    {},

                    {
                        1: {
                            start: rangeOfDates.start,
                            end: finalPrimeraSetmana
                        }
                    },

                    {
                        [nSetmanes]: {
                            start: iniciUltimaSetmana,
                            end: rangeOfDates.end
                        }
                    }
                );

                for (let i = 1; i < nSetmanes; i += 1) {

                    if (i === 1) {
                        dataIniSetmana = extremsSetmanes[1].start;
                        dataFiSetmana = extremsSetmanes[1].end;
                    } else {
                        dataIniSetmana = new Date(extremsSetmanes[i - 1].end.getFullYear(), extremsSetmanes[i - 1].end.getMonth(), extremsSetmanes[i - 1].end.getDate() + 1);
                        dataFiSetmana = endOfWeek(dataIniSetmana, { weekStartsOn: 1 });
                    }

                    Object.assign(
                        extremsSetmanes,

                        {
                            [i]: {
                                start: dataIniSetmana,
                                end: dataFiSetmana
                            }
                        }
                    );
                }

                let
                    iniciPeriode = new Date(extremsSetmanes[1].start),
                    finalPeriode = new Date(extremsSetmanes[Object.keys(extremsSetmanes).length].end)
                ;

                moment.locale('ca');

                this.periode.value = `${capitalize(moment(iniciPeriode).format("dddd, ll"))} → ${capitalize(moment(finalPeriode).format("dddd, ll"))}`;
                this.periode.click();

                this.setState({
                    rangeOfDates,
                    nSetmanes,
                    setmanes,
                    extremsSetmanes,
                    rutina: {
                        ...this.state.rutina,
                        dataIni: iniciPeriode,
                        dataFi: finalPeriode
                    }
                });

            }

            this.setState({
                selectClientsValue: rutina ? rutina.client : ({
                    value: {
                        arrImatges: [{
                            imgArx: {
                                buffer: "/anon0.jpg"
                            }
                        }]
                    }
                }),
                rutina,
                rutinaId: nextProps.rutinaId,

                opcionsImpressioActuals
            });
        }
    }

    addRutina = (event) => {
        event.preventDefault();
        let
            rutinaNom = this.rutinaNom.value.trim(),
            rutinaClient = this.selClient.selectedOptions[0].value,
            //rutinaGrupMuscular = this.selGrupMuscular.selectedOptions[0].value,
            rutinaDescripcio = this.rutinaDescripcio.value.trim(),

            rutinaDataInici = this.state.rangeOfDates.start,
            rutinaDataFi = this.state.rangeOfDates.end,

            rutinaEntrenador = this.state.entrenador,
            rutinaSessions = this.state.sessions,
            rutinaOpcionsImpressio = this.state.opcionsImpressio
            ;

        if (rutinaNom) {
            Meteor.call(
                'rutines.insert',
                rutinaNom,
                rutinaClient,
                rutinaDataInici,
                rutinaDataFi,
                rutinaEntrenador,
                rutinaDescripcio,
                rutinaSessions,
                rutinaOpcionsImpressio,
                (error, data) => {
                    if (error) {
                        Bert.alert("Logueja't abans d'introduir dades.", "danger", "fixed-top", "fa-frown-o");
                    } else {
                        this.rutinaNom.value = "";
                        this.rutinaDescripcio.value = "";
                    }
                }
            );
        }
    };

    afegeixSetmanaAlStateDeLaRutina = (nSetmana, setmana) => {

        this.setState(
            (previousState, props) => {
                let
                    setmanes = previousState.setmanes;

                //setmanes.push(setmana);
                Object.assign(setmanes, setmana);
                return ({
                    setmanes
                });
            }
        );
    };

    handleDates = (
        data,
        //nomRutina,
        //client,
        //dataIni,
        //dataFi,
        //entrenador,
        sessions
    ) => {
        let
            setmanes = {},
            rangeOfDates = data,
            nSetmanes = (getISOWeek(data.end) - getISOWeek(data.start) + 1) >= 1 ? (getISOWeek(data.end) - getISOWeek(data.start) + 1) : 52 + (getISOWeek(data.end) - getISOWeek(data.start) + 1),
            dataIniSetmana,
            dataFiSetmana,
            nSetmanaISO,
            nSetmanaRutina,
            setmana,
            extremsSetmanes,
            finalPrimeraSetmana,
            iniciUltimaSetmana
        ;

        if (data.eventType === 3) {

            // console.dir("Data: ", data);
            // console.dir("nSetmanes: ", nSetmanes);
            // console.dir("ISOweek end: ", getISOWeek(data.end));
            // console.dir("ISOweek start: ", getISOWeek(data.start));
            // console.dir("ISOweeks: ", getISOWeek(data.start));

            if (this.props.rutinaId) {
                if (confirm("Vas a canviar el periode de la rutina. Les sessions s'assignaran ordenada i consecutivament als primers dies del nou periode. Seguir?")) {// Se vol editar el periode d'una rutina. Per salvar les sessions les assignarem respectivament als primers dies del nou periode
                    // Haurem de modificar les dates a les sessions de la rutina. Per fer-ho veurem d'obtindre la rutina a una variable i després reassignar la modificació
                    let
                        idRutina = this.props.rutinaId,
                        rutinaOriginal = { ...this.state.rutina },
                        arrSessionsOriginals = [...this.state.rutina.sessions],
                        dataNovaInicial = data.start,
                        dataNovaFinal = data.end,
                        arrSessionsModificades = arrSessionsOriginals.map(
                            (sessio, idx) => {
                                let
                                    sessioAmbDataNova = {
                                        ...sessio,
                                        periodeSessio: {},
                                        datesSessio: [],
                                        dataSessio: new Date(moment(dataNovaInicial).add(idx, "days").format())
                                    }
                                ;
                                return sessioAmbDataNova;
                            }
                        ),

                        rutinaModificada = {
                            ...rutinaOriginal,
                            dataIni: dataNovaInicial,
                            dataFi: dataNovaFinal,
                            sessions: [...arrSessionsModificades]
                        }
                    ;

                    this.setState({
                        rutina: rutinaModificada//,
                        //       editantRutina: false
                    });

                    Meteor.call(
                        'rutines.update',
                        idRutina,
                        rutinaModificada
                    );
                    // console.dir("RUTINAORIGINAL: ", rutinaOriginal);
                    // console.dir("arrSessionsOriginals: ", arrSessionsOriginals);
                    // console.dir("dataNovaInicial: ", data.start);
                    // console.dir("dataNovaFinal: ", data.end);
                    // console.dir("arrSessionsModificades: ", arrSessionsModificades);
                }
            }
            //
            // console.dir("MomentDate: ", moment(new Date())._d);
            // this.infCal.props.rangeOfDates.start = data.start;
            // this.infCal.props.rangeOfDates.end = data.end;

            // El final de la primera setmana sempre serà endOfWeek(rangeOfDates.start) si aquesta és posterior a rangeOfDates.end.

            if (compareDesc(rangeOfDates.end, endOfWeek(rangeOfDates.start, { weekStartsOn: 1 })) === 1) {
                finalPrimeraSetmana = rangeOfDates.end;
            } else {
                finalPrimeraSetmana = endOfWeek(rangeOfDates.start, { weekStartsOn: 1 });
            }

            //----

            if (nSetmanes === 1) {
                iniciUltimaSetmana = rangeOfDates.start;
            } else {
                iniciUltimaSetmana = startOfWeek(rangeOfDates.end, { weekStartsOn: 1 });
            }

            extremsSetmanes = Object.assign(
                {},

                {
                    1: {
                        start: rangeOfDates.start,
                        end: finalPrimeraSetmana
                    }
                },

                {
                    [nSetmanes]: {
                        start: iniciUltimaSetmana,
                        end: rangeOfDates.end
                    }
                }
            );

            for (let i = 1; i < nSetmanes; i += 1) {

                if (i === 1) {
                    dataIniSetmana = extremsSetmanes[1].start;
                    dataFiSetmana = extremsSetmanes[1].end;
                } else {
                    dataIniSetmana = new Date(
                        extremsSetmanes[i - 1].end.getFullYear(),
                        extremsSetmanes[i - 1].end.getMonth(),
                        extremsSetmanes[i - 1].end.getDate() + 1
                    );
                    dataFiSetmana = endOfWeek(dataIniSetmana, { weekStartsOn: 1 });
                }

                Object.assign(
                    extremsSetmanes,

                    {
                        [i]: {
                            start: dataIniSetmana,
                            end: dataFiSetmana
                        }
                    }
                );
            }

            let
                iniciPeriode = new Date(extremsSetmanes[1].start),
                finalPeriode = new Date(extremsSetmanes[Object.keys(extremsSetmanes).length].end)
                ;

            moment.locale('ca');

            this.periode.value = `${capitalize(moment(iniciPeriode).format("dddd, ll"))} → ${capitalize(moment(finalPeriode).format("dddd, ll"))}`;

            this.periode.click(); // Amagar el calendari

            this.setState({
                rangeOfDates,
                nSetmanes,
                setmanes,
                extremsSetmanes,
                rutina: {
                    ...this.state.rutina,
                    dataIni: iniciPeriode,
                    dataFi: finalPeriode
                }
            });
        }
    };

    actualitzaSetmanesAmbNovaSessioARutinaState = (setmanes) => {
        this.setState({
            setmanes
        });
    };

    selectClientsUpdateValue = (selectClientsValue) => {

        //this.selClientRef

        this.setState({
            selectClientsValue,
            rutina: {
                ...this.state.rutina,
                client: selectClientsValue
            }
        });
    };

    selectGMsUpdateValue = (selectGMsValues) => {
        this.setState({ selectGMsValues });
    };

    setRutinaCreada = (bool) => {
        this.setState({
            rutinaCreada: bool
        });
    };

    editantSessioVal = (bool) => {
        this.setState({ editantSessio: bool });
    };

    opcionsDImpressioEditades = (
        impNomSessio,
        impObsSessio,
        impImatgesExs,
        impDescExs,
        impEscalf,
        impTorCalma,
        impTABULAR
    ) => {
        this.setState({
            impNomSessio,
            impObsSessio,
            impImatgesExs,
            impDescExs,
            impEscalf,
            impTorCalma,
            impTABULAR
        });
    };

    toggleOpcionsVisibles = () => {
        this.setState((prevState) => {
            return {
                impOpcionsVisibles: !prevState.impOpcionsVisibles
            };
        });
    };

    toggleDiesVisibles = () => {
        this.setState((prevState) => {
            return {
                diesVisibles: !prevState.diesVisibles
            };
        });  
    };

    toggleEditantRutina = () => {
        this.setState((prevState) => {
            return {
                editantRutina: !prevState.editantRutina
            };
        });
    };

    render() {
        class Ocultador extends Component {
            constructor(props) {
                super(props);

                this.state = {
                    mostraBotons: false
                };
            }

            render() {
                return (
                    <div
                        style={{
                            minWidth: `30em`,
                            minHeight: `3em`
                        }}
                        onMouseEnter={(ev) => {
                            this.setState({
                                mostraBotons: true
                            });
                            // ev.target.style.background = `rgba(255,20,147,.2)`;
                        }}
                        onMouseLeave={(ev) => {
                            this.setState({
                                mostraBotons: false
                            })
                            // ev.target.style.background = `rgba(255,20,147,0)`;
                        }}
                    >
                        <button
                            style={{
                                display: this.state.mostraBotons ? `inline-block` : `none`
                            }}
                          //  title="Opcions d'Impressió"
                            onClick={(ev) => {
                                this.props.toggleOpcionsVisibles();
                            }}
                        >
                            Opcions d'Impressió
                        </button>

                        <button
                            style={{
                                display: this.state.mostraBotons ? `inline-block` : `none`
                            }}
                           // title="Taula de dies"
                            onClick={(ev) => {
                                this.props.toggleDiesVisibles();
                            }}
                        >
                            Taula de dies
                        </button>

                    </div>
                )
            }
        }

        let
            arrNomsClients = [],
            BotoOpcionsImpressio = styled.button`
                opacity: 0;
                
                :hover {
                    opacity: 100;
                }

            `
        ;

        this.props.clients.map(
            (v, i, a) => {
                //console.log(JSON.stringify(client));
                arrNomsClients.push({
                    value: v,
                    label: `${v.clientCognoms}, ${v.clientNom}`,
                    className: `autoCompleteSelectOption`
                });
            }
        );

        return (
            <div>
                <div
                    style={{
                        display: this.state.mostraForm || this.props.rutinaId ? `block` : `none`,
                        textAlign: `right`,
                        position: `absolute`,
                        right: `10px`
                    }}
                >
                    {/* <BotoOpcionsImpressio
                        title="Opcions d'impressió"
                    >
                    🚀🚀🚀
                    </BotoOpcionsImpressio> */}
                    
                    <Ocultador 
                        toggleOpcionsVisibles={this.toggleOpcionsVisibles}
                        toggleDiesVisibles={this.toggleDiesVisibles}
                    />

                    {/* <i
                        style={{
                            padding: `2px`
                        }}
                        className="fa fa-cog"
                        onClick={() => {
                            this.setState({
                                impOpcionsVisibles: !this.state.impOpcionsVisibles
                            })
                        }}
                        title="Opcions d'Impressió"
                    /> */}

                    {/* <i
                        style={{
                            padding: `2px`
                        }}
                        className="fa fa-calendar"
                        onClick={() => {
                            this.setState({
                                diesVisibles: !this.state.diesVisibles
                            })
                        }}
                        title="Taula de dies"
                    /> */}
                </div>

                <div
                    style={{
                        paddingTop: `1em`,
                        display: this.state.impOpcionsVisibles ? `grid` : `none`,
                        gridTemplateAreas: `
                            "tab  nomRut  avatarCli  obsGen"
                            "dia  entrena    nomCli    periode"
                        `,
                        justifyContent: `start`,
                        zIndex: `100`,

                        bordeRadius: `.3em`,

                        border: `solid 1px gray`,

                        padding: `0.5em 1em`,

                        gridGap: `0.5em`
                    }}
                >
                    <div
                        style={{
                            gridArea: `tab`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkTABULARS"
                            checked={this.state.impTABULARS}
                            onChange={() => {
                                this.setState({
                                    impTABULARS: !this.state.impTABULARS
                                });
                            }}
                        />
                        <label htmlFor="chkTABULARS">TABULARS</label>
                    </div>
                    <div
                        style={{
                            gridArea: `dia`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkDIA"
                            checked={this.state.impDIA}
                            onChange={() => {
                                this.setState({
                                    impDIA: !this.state.impDIA
                                });
                            }}
                        />
                        <label htmlFor="chkDIA">Dia setmana</label>
                    </div>
                    <div
                        style={{
                            gridArea: `nomRut`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkNomRutina"
                            checked={this.state.impNomRutina}
                            onChange={() => {
                                this.setState({
                                    impNomRutina: !this.state.impNomRutina
                                });
                            }}
                        />
                        <label htmlFor="chkNomRutina">Nom de la rutina</label>
                    </div>
                    <div
                        style={{
                            gridArea: `nomCli`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkNomClient"
                            checked={this.state.impNomClient}
                            onChange={() => {
                                this.setState({
                                    impNomClient: !this.state.impNomClient
                                });
                            }}
                        />
                        <label htmlFor="chkNomClient">Nom del client</label>
                    </div>
                    <div
                        style={{
                            gridArea: `periode`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkPeriode"
                            checked={this.state.impPeriode}
                            onChange={() => {
                                this.setState({
                                    impPeriode: !this.state.impPeriode
                                });
                            }}
                        />
                        <label htmlFor="chkPeriode">Periode</label>
                    </div>
                    <div
                        style={{
                            gridArea: `obsGen`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkObservacionsGenerals"
                            checked={this.state.impObsGen}
                            onChange={() => {
                                this.setState({
                                    impObsGen: !this.state.impObsGen
                                });
                            }}
                        />
                        <label htmlFor="chkObservacionsGenerals">Observacions Generals</label>
                    </div>

                    <div
                        style={{
                            gridArea: `avatarCli`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkAvatar"
                            checked={this.state.impAvatarCli}
                            onChange={() => {
                                this.setState({
                                    impAvatarCli: !this.state.impAvatarCli
                                });
                            }}
                        />
                        <label htmlFor="chkAvatar">Avatar del client</label>
                    </div>

                    <div
                        style={{
                            gridArea: `entrena`,
                            display: `grid`,
                            justifyContent: `start`,
                            gridAutoFlow: `column`,
                            alignContent: `center`
                        }}
                    >
                        <input
                            type="checkbox"
                            id="chkEntrenador"
                            checked={this.state.impEntrenador}
                            onChange={() => {
                                this.setState({
                                    impEntrenador: !this.state.impEntrenador
                                });
                            }}
                        />
                        <label htmlFor="chkEntrenador">Entrenador</label>
                    </div>
                </div>

                {
                    (() => {
                        if (this.state.editantRutina) {
                            return (
                                <div>
                                    <div
                                        style={{
                                            display: this.state.impNomRutina ? `block` : `none`
                                        }}
                                    >
                                        <label
                                            htmlFor="inNomRutina"
                                        >Nom de la Rutina:
                                        </label>
                                        <input
                                            placeholder="Nom de la Rutina"
                                            type="text"
                                            ref={inp => this.rutinaNom = inp}
                                            id="inNomRutina"
                                            value={this.state.rutina.nomRutina}
                                            onChange={() => {
                                                this.setState({
                                                    rutina: {
                                                        ...this.state.rutina,
                                                        nomRutina: this.rutinaNom.value
                                                    }
                                                });
                                            }}
                                        />
                                    </div>

                                    <div
                                        style={{
                                            display: this.state.impAvatarCli ? `grid` : `none`,
                                            // gridTemplateAreas: `
                                            //     "nomRut nomCli"
                                            //     "periode obsGen"
                                            // `,
                                            justifyContent: `center`
                                        }}
                                    >
                                        {
                                            this.state.rutina.client
                                                ?
                                                <div
                                                    style={{
                                                        display: `block`
                                                    }}
                                                >
                                                    <Avatar
                                                        client={this.state.selectClientsValue}
                                                    />
                                                </div>
                                                : null
                                        }
                                    </div>

                                    <div
                                        style={{
                                            display: this.state.impNomClient ? `block` : `none`
                                        }}
                                    >
                                        <label
                                            htmlFor="selClient"
                                        >Client:
                                        </label>
                                        <div
                                            style={{
                                                zIndex: 200,
                                                position: `relative`
                                            }}
                                        >
                                            {//
                                                //value={this.state.selectClientsValue}
                                            }
                                            <Select
                                                id="selClient"
                                                value={this.state.rutina.client && this.state.selectClientsValue}
                                                options={arrNomsClients}
                                                onChange={this.selectClientsUpdateValue}
                                            />
                                        </div>
                                    </div>

                                    <label
                                        htmlFor="inEntrenador"
                                    >Entrenador:
                                    </label>
                                    <input
                                        placeholder="Entrenador"
                                        type="text"
                                        ref={inp => this.entrenador = inp}
                                        id="inEntrenador"
                                        value={this.state.rutina.entrenador}
                                        onChange={() => {
                                            this.setState({
                                                rutina: {
                                                    ...this.state.rutina,
                                                    entrenador: this.entrenador.value
                                                }
                                            });
                                        }}
                                    />

                                    <div
                                        style={{
                                            display: this.state.impObsGen ? `block` : `none`
                                        }}
                                    >
                                        <label
                                            htmlFor="taObservacions"
                                        >Observacions generals:
                                        </label>
                                        <textarea
                                            ref={ta => this.taObservacionsRutina = ta}
                                            className="taObservacions"
                                            id="taObservacionsRutina"
                                            value={this.state.rutina.observacions}
                                            onChange={() => {
                                                this.setState({
                                                    rutina: {
                                                        ...this.state.rutina,
                                                        observacions: this.taObservacionsRutina.value
                                                    }
                                                });
                                            }}
                                        />
                                    </div>

                                    <div
                                        style={{
                                            textAlign: `right`,
                                            //  Amaga el botó en la creació d'una rutina nova.
                                            display: typeof (this.props.rutinaId) !== "undefined" ? `block` : `none`
                                        }}
                                    >
                                        <button
                                            style={{
                                                display: `inline-block`,
                                                textAlign: `right`
                                            }}
                                            onClick={() => {

                                                let
                                                    idRutina = this.state.rutina._id,
                                                    rutinaNova = { ...this.state.rutina }
                                                    ;

                                                this.setState({
                                                    editantRutina: false
                                                });

                                                Meteor.call(
                                                    'rutines.update',
                                                    idRutina,
                                                    rutinaNova
                                                );
                                            }}
                                        >ESTABLEIX
                                        </button>
                                    </div>
                                </div>
                            );
                        } else {
                            if (typeof (this.props.rutinaId) === "undefined") {
                                // alert("NOVA?");
                                this.setState({
                                    editantRutina: true
                                });
                            }

                            class OcultadorEdita extends Component {
                                constructor(props) {
                                    super(props);
                    
                                    this.state = {
                                        mostraBoto: false
                                    };
                                }
                    
                                render() {
                                    return (
                                        <div
                                            style={{
                                                minWidth: `10em`,
                                                minHeight: `3em`
                                            }}
                                            onMouseEnter={(ev) => {
                                                this.setState({
                                                    mostraBoto: true
                                                });
                                                // ev.target.style.background = `rgba(255,20,147,.2)`;
                                            }}
                                            onMouseLeave={(ev) => {
                                                this.setState({
                                                    mostraBoto: false
                                                });
                                                // ev.target.style.background = `rgba(255,20,147,0)`;
                                            }}
                                        >
                                            <button
                                                style={{
                                                    display: this.state.mostraBoto ? `inline-block` : `none`
                                                }}
                                              //  title="Opcions d'Impressió"
                                                onClick={(ev) => {
                                                    this.props.toggleEditantRutina();
                                                }}
                                            >
                                                Edita les dades
                                            </button>
                    
                                            <button
                                                style={{
                                                    display: this.state.mostraBotons ? `inline-block` : `none`
                                                }}
                                               // title="Taula de dies"
                                                onClick={(ev) => {
                                                    this.props.toggleDiesVisibles();
                                                }}
                                            >
                                                Taula de dies
                                            </button>
                    
                                        </div>
                                    )
                                }
                            }
                            const
                                Button = styled.button`
                                    display: inline-block;
                                    text-align: right;

                                    @media screen {
                                        background: palevioletred;
                                    }

                                    @media print {
                                        display: none;
                                        text-align: center;
                                        background: red;
                                    }
                                `
                            ;
                            return (
                                <div>
                                    <span
                                        style={{
                                            display: this.state.impNomRutina ? `block` : `none`
                                        }}
                                    >
                                        <label
                                            style={{
                                                fontSize: `1.1em`,
                                                fontWeight: `bolder`,
                                                marginRight: `1em`
                                            }}
                                            htmlFor="inNomRutina"
                                        >Nom de la Rutina:
                                        </label>
                                        <span>{
                                            this.state.rutina.nomRutina !== null && typeof (this.state.rutina.nomRutina) !== "undefined"
                                                ? this.state.rutina.nomRutina
                                                : ""
                                        }
                                        </span>
                                    </span>

                                    <span
                                        style={{
                                            display: this.state.impAvatarCli ? `grid` : `none`,
                                            // gridTemplateAreas: `
                                            //     "nomRut nomCli"
                                            //     "periode obsGen"
                                            // `,
                                            justifyContent: `center`
                                        }}
                                    >
                                        {
                                            this.state.rutina.client
                                                ?
                                                <div
                                                    style={{
                                                        display: `block`
                                                    }}
                                                >
                                                    <Avatar
                                                        client={this.state.selectClientsValue}
                                                    />
                                                </div>
                                                : null
                                        }
                                    </span>

                                    <div
                                        style={{
                                            display: this.state.impNomClient ? `block` : `none`
                                        }}
                                    >
                                        <label
                                            style={{
                                                fontSize: `1.1em`,
                                                fontWeight: `bolder`,
                                                marginRight: `1em`
                                            }}
                                            htmlFor="selClient"
                                        >Client:
                                        </label>
                                        <span
                                            style={{
                                                zIndex: 200,
                                                position: `relative`
                                            }}
                                        >
                                            <span>{
                                                this.state.rutina.client !== null && typeof (this.state.rutina.client.label) !== "undefined"
                                                    ? this.state.rutina.client.label
                                                    : ""
                                            }
                                            </span>
                                        </span>
                                    </div>

                                    <label
                                        style={{
                                            fontSize: `1.1em`,
                                            fontWeight: `bolder`,
                                            marginRight: `1em`
                                        }}
                                        htmlFor="inEntrenador"
                                    >Entrenador:
                                    </label>
                                    <span>{
                                        this.state.rutina.entrenador !== null && typeof (this.state.rutina.entrenador) !== "undefined"
                                            ? this.state.rutina.entrenador
                                            : ""
                                    }
                                    </span>

                                    <div
                                        style={{
                                            display: this.state.impObsGen ? `block` : `none`
                                        }}
                                    >
                                        <label
                                            htmlFor="taObservacions"
                                            style={{
                                                fontSize: `1.1em`,
                                                fontWeight: `bolder`,
                                                marginRight: `1em`,
                                                display: `block`
                                            }}
                                        >Observacions generals:
                                        </label>
                                        <div>{
                                            this.state.rutina.observacions !== null && typeof (this.state.rutina.observacions) !== "undefined"
                                                ? this.state.rutina.observacions
                                                : ""
                                        }
                                        </div>
                                    </div>

                                    <div
                                        style={{
                                            textAlign: `right`
                                        }}
                                    >
                                        {/* <i
                                            className="fa fa-edit"
                                            onClick={() => {
                                                this.setState({
                                                    editantRutina: true
                                                })
                                            }}
                                            title="EDITA"
                                        /> */}

                                        <OcultadorEdita
                                            toggleEditantRutina={this.toggleEditantRutina}
                                        />
                                    </div>
                                </div>
                            );
                        }
                    })()

                }

                {
                    //value={this.state.periode}
                }

                <div
                    style={{
                        display: this.state.impPeriode ? `grid` : `none`,
                        // gridTemplateAreas: `
                        //     "nomRut nomCli"
                        //     "periode obsGen"
                        // `,
                        justifyContent: `stretch`
                    }}
                >
                    <label
                        style={{
                            fontSize: `1.1em`,
                            fontWeight: `bolder`
                        }}
                        htmlFor="inPeriode"
                    >Periode:
                    </label>
                    <input
                        placeholder="Periode"
                        type="text"
                        ref={inPer => this.periode = inPer}
                        id="inPeriode"
                        value={
                            this.props.rutinaId || this.state.rutina
                                ?
                                `${capitalize(moment(this.state.rutina.dataIni).format("dddd, ll"))} → ${capitalize(moment(this.state.rutina.dataFi).format("dddd, ll"))}`
                                : ""
                        }
                        onClick={() => {
                            this.periodeCal.style.display === "block"
                                ? this.periodeCal.style.display = "none"
                                : this.periodeCal.style.display = "block"
                        }}
                        readOnly
                    />
                    <div
                        ref={ical => this.periodeCal = ical}
                        style={{
                            display: `none`
                        }}
                    >
                        <InfiniteCalendar
                            Component={withRange(Calendar)}
                            selected={null}
                            min={lastYear}
                            minDate={lastYear}
                            max={nextYear}
                            maxDate={nextYear}
                            displayOptions={{}}
                            locale={{
                                locale: caLocale,
                                headerFormat: 'ddd, D MMM',
                                weekdays: ["Dmg", "Dll", "Dm", "Dcs", "Djs", "Dvs", "Dss"],
                                weekStartsOn: 1,
                                blank: 'Selecciona un periode',
                                todayLabel: {
                                    long: 'Anar a avui',
                                    short: 'Avui.'
                                }
                            }}
                            ref={infCal => this.infCal = infCal}
                            onSelect={(data) => {
                                this.handleDates(
                                    data,
                                    // this.rutinaNom.value,
                                    // this.state.selectClientsValue,
                                    // moment(data.start).add(1, 'days'),
                                    // moment(data.end).add(1, 'days'),
                                    // this.entrenador.value,
                                    []
                                )
                            }}
                        />
                    </div>

                </div>

                {
                    this.props.rutinaId || this.state.rutina
                        ?
                        (() => {
                            return (
                                <div>
                                    <div
                                        style={{
                                            display: this.state.diesVisibles ? `block` : `none`
                                        }}
                                    >
                                        <BotonsCreaSessio
                                            rangeOfDates={this.state.rangeOfDates}
                                            nSetmanes={this.state.nSetmanes}
                                            afegeixSetmanaAlStateDeLaRutina={this.afegeixSetmanaAlStateDeLaRutina}
                                            setmanes={this.state.setmanes}
                                            extremsSetmanes={this.state.extremsSetmanes}
                                            actualitzaSetmanesAmbNovaSessioARutinaState={this.actualitzaSetmanesAmbNovaSessioARutinaState}
                                            objSetmanes={this.state.setmanes}
                                            rutinaCreada={this.state.rutinaCreada}
                                            rutina={this.state.rutina}
                                            setRutinaCreada={this.setRutinaCreada}
                                            editantSessioVal={this.editantSessioVal}
                                            editantSessio={this.state.editantSessio}
                                            opcionsDImpressioEditades={this.opcionsDImpressioEditades}
                                            rutinaRefresh={this.rutinaRefresh}

                                            {...this.props}
                                        />

                                        {/* <TaulaBotonsDies
                                            rangeOfDates={this.state.rangeOfDates}
                                            nSetmanes={this.state.nSetmanes}
                                            afegeixSetmanaAlStateDeLaRutina={this.afegeixSetmanaAlStateDeLaRutina}
                                            setmanes={this.state.setmanes}
                                            extremsSetmanes={this.state.extremsSetmanes}
                                            actualitzaSetmanesAmbNovaSessioARutinaState={this.actualitzaSetmanesAmbNovaSessioARutinaState}
                                            objSetmanes={this.state.setmanes}
                                            rutinaCreada={this.state.rutinaCreada}
                                            rutina={this.state.rutina}
                                            setRutinaCreada={this.setRutinaCreada}
                                            editantSessioVal={this.editantSessioVal}
                                            editantSessio={this.state.editantSessio}
                                            opcionsDImpressioEditades={this.opcionsDImpressioEditades}
                                            //chgOptsImp={this.chgOptsImp}
                                            rutinaRefresh={this.rutinaRefresh}
                                            {...this.props}
                                        /> */}
                                    </div>

                                    <div className="sessions">
                                        {
                                            this.state.rutina.sessions
                                                ?
                                                this.state.rutina.sessions
                                                    .sort(
                                                        (a, b) => {
                                                            function primeraData(sessio) {
                                                                if (typeof (sessio.periodeSessio) !== "undefined" && sessio.periodeSessio !== null && Object.keys({ ...sessio.periodeSessio }).length !== 0) {
                                                                    console.dir("P: ", sessio.periodeSessio);
                                                                    return new Date(sessio.periodeSessio.start);
                                                                } else {
                                                                    if (typeof (sessio.datesSessio) !== "undefined" && sessio.datesSessio.length > 0) {
                                                                        console.dir("M: ", sessio.datesSessio);
                                                                        return new Date(sessio.datesSessio[0]);
                                                                    } else {

                                                                        if (typeof (sessio.dataSessio) !== "undefined") {
                                                                            console.dir("S: ", sessio.dataSessio);
                                                                            return new Date(sessio.dataSessio);
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (a !== null && typeof (a) !== "undefined") {
                                                                a = primeraData(a);
                                                            }
                                                            if (b !== null && typeof (b) !== "undefined") {
                                                                b = primeraData(b);
                                                            }

                                                            return a < b ? -1 : a > b ? 1 : 0;
                                                            //return a > b;
                                                        }
                                                    )
                                                    .map(
                                                        (v, i, a) => {
                                                            if (this.state.rutina.sessions[i] !== null && typeof (this.state.rutina.sessions[i].opcionsImpressio) !== "undefined" && typeof (this.state.rutina.sessions[i].createdAt) !== "undefined") {
                                                                return (
                                                                    <SessioOut
                                                                        key={i}
                                                                        i={i}
                                                                        v={v}

                                                                        opcionsImpressioActuals={this.state.opcionsImpressioActuals}

                                                                        impOpcionsVisibles={this.state.impOpcionsVisibles}
                                                                        impNomSessio={this.state.rutina.sessions[i].opcionsImpressio.impNomSessio}
                                                                        impObsSessio={this.state.rutina.sessions[i].opcionsImpressio.impObsSessio}
                                                                        impImatgesExs={this.state.rutina.sessions[i].opcionsImpressio.impImatgesExs}
                                                                        impDescExs={this.state.rutina.sessions[i].opcionsImpressio.impDescExs}
                                                                        impEscalf={this.state.rutina.sessions[i].opcionsImpressio.impEscalf}
                                                                        impTorCalma={this.state.rutina.sessions[i].opcionsImpressio.impTorCalma}
                                                                        impTABULAR={this.state.impTABULARS ? true : this.state.rutina.sessions[i].opcionsImpressio.impTABULAR}
                                                                        impDIA={this.state.impDIA}

                                                                        rutina={this.state.rutina}
                                                                        index={i}
                                                                    />
                                                                );
                                                            }
                                                        }
                                                    )
                                                : null
                                        }
                                    </div>
                                </div>
                            );
                        })()
                        : null
                }
            </div>
        );
    }
}

class BotonsEditaDatesSessio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mostraTaulaDies: false,
            mostraNouPeriode: false,
            //    mostraInputPeriodeSessio: false,
            mostraCalPeriodeSessio: false,
            objPeriodeSessio: {},
            showModal: false,
            rangeOfDatesRutina: props.rangeOfDates,
            objDiaSessio: null,
            ambSessio: null,
            datesSessions: [],
            sessionsDia: [],
            indexSessio: null,
            editantSessio: false,
            periodeSessio: {}
        };
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            rangeOfDatesRutina: nextProps.rangeOfDates
        });
    }
    handleCloseModal = () => {
        this.setState({
            showModal: false
        });
    };
    showModal = (
        objDiaSessio,
        ambSessio,
        datesSessions,
        sessionsDia,
        indexSessio
    ) => {
        this.setState({
            showModal: true,
            objDiaSessio,
            datesSessions,
            ambSessio,
            sessionsDia,
            indexSessio
        });
    };
    editantSessioVal = (bool) => {
        this.setState({ editantSessio: bool });
    };
    render() {
        if (Object.keys(this.state.rangeOfDatesRutina).length !== 0) {
            return (
                <div
                    style={{
                        textAlign: `center`
                    }}
                >
                    <button
                        onClick={() => {
                            this.setState((prevState, props) => {
                                return {
                                    mostraTaulaDies: !prevState.mostraTaulaDies,
                                    mostraNouPeriode: false,
                                    mostraCalPeriodeSessio: false
                                }
                            });
                        }}
                    >Taula de dies solts</button>
                    <button
                        onClick={() => {
                            this.setState((prevState, props) => {
                                return {
                                    mostraTaulaDies: false,
                                    mostraNouPeriode: true,
                                    mostraCalPeriodeSessio: !prevState.mostraCalPeriodeSessio
                                }
                            });
                        }}
                    >Subperiode
                    </button>
                    {(() => {
                        if (this.state.mostraTaulaDies) {
                            return (
                                <TaulaBotonsDies
                                    editaDatesSessio
                                    {...this.props}
                                />
                            );
                        }
                        if (this.state.mostraNouPeriode) {
                            return (
                                <div
                                    style={{
                                        display: `inline-block`,
                                        margin: `0 auto`
                                    }}
                                >
                                    {/* <input
                                            style={{
                                                display: this.state.mostraInputPeriodeSessio ? `block` : `none`
                                            }}
                                            type="text"
                                            ref={periodeSessio => this.periodeSessio = periodeSessio}
                                             value={
                                                `${capitalize(moment(this.state.objPeriodeSessio.inici).format("dddd, ll"))} → ${capitalize(moment(this.state.objPeriodeSessio.fi).format("dddd, ll"))}`
                                            }
                                        /> */}
                                    <div
                                        style={{
                                            display: this.state.mostraCalPeriodeSessio ? `block` : `none`
                                        }}
                                    >
                                        <InfiniteCalendar
                                            Component={withRange(Calendar)}
                                            selected={false}
                                            min={this.state.rangeOfDatesRutina.start}
                                            minDate={this.state.rangeOfDatesRutina.start}
                                            max={this.state.rangeOfDatesRutina.end}
                                            maxDate={this.state.rangeOfDatesRutina.end}
                                            displayOptions={{}}
                                            locale={{
                                                locale: caLocale,
                                                headerFormat: 'ddd, D MMM',
                                                weekdays: ["Dmg", "Dll", "Dm", "Dcs", "Djs", "Dvs", "Dss"],
                                                weekStartsOn: 1,
                                                blank: 'Selecciona un periode',
                                                todayLabel: {
                                                    long: 'Anar a avui',
                                                    short: 'Avui.'
                                                }
                                            }}
                                            ref={infCalSes => this.infCalSes = infCalSes}
                                            onSelect={(data) => {
                                                //
                                                if (data.eventType === 3) {
                                                    this.setState(
                                                        {
                                                            showModal: true,
                                                            mostraCalPeriodeSessio: false,
                                                            periodeSessio: data
                                                        },
                                                        () => {
                                                            if (confirm(`Vas a canviar la data de la sessio pel periode ${capitalize(moment(this.state.periodeSessio.start).format("dddd L"))} → ${capitalize(moment(this.state.periodeSessio.end).format("dddd L"))}`)) {
                                                                let
                                                                    //       dataBoto = ev.target.dataset.diaData,

                                                                    truIndexSessio = this.props.truIndexSessio,
                                                                    sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                                                    sessioModificada = { ...sessioAbans },
                                                                    rutinaAbans = this.props.rutina,
                                                                    rutinaModificada = { ...this.props.rutina },

                                                                    rutinaSessionsAbans = rutinaAbans.sessions,
                                                                    rutinaSessionsModificades = [...rutinaSessionsAbans],

                                                                    trobaSessioEnCollection = (sessio) => Sessions.find({
                                                                        nom: sessio.nom
                                                                    }).count() > 0
                                                                    ;

                                                                //         if (confirm(`Vas a canviar la data de la sessió pel ${capitalize(moment(dataBoto).format("dddd L"))}.\n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {
                                                                //alert(`Pos ara ja fariem el canvi.`);
                                                                //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);

                                                                console.dir("LA BONA: SESSIÓ ABANS: ", sessioAbans);
                                                                console.dir("LA BONA: SESSIÓmodificada ABANS: ", sessioModificada);
                                                                //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                                                console.dir("LA BONA: RUTINA ABANS: ", rutinaAbans);
                                                                console.dir("LA BONA: RUTINAmodificada ABANS: ", rutinaModificada);

                                                                sessioModificada.datesSessio = [];

                                                                sessioModificada.periodeSessio = { ...this.state.periodeSessio };
                                                                console.dir("LA BONA: SESSIÓ DESPRÉS: ", sessioAbans);
                                                                console.dir("LA BONA: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                                                console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                                                if (trobaSessioEnCollection(sessioModificada)) {
                                                                    Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                                                    alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                                                }

                                                                console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                                                console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                                                // mergeABP(
                                                                //     rutinaSessionsModificades


                                                                // Meteor.call(``)

                                                                console.dir(
                                                                    "mergeArrayDObjectes: ",
                                                                    mergeABP(
                                                                        rutinaSessionsModificades
                                                                        ,
                                                                        [sessioModificada],
                                                                        "nom"
                                                                    )
                                                                );

                                                                rutinaSessionsModificades = mergeABP(
                                                                    rutinaSessionsModificades
                                                                    ,
                                                                    [sessioModificada],
                                                                    "nom"
                                                                )
                                                                    ;

                                                                console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                                                console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                                                console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                                                rutinaModificada = {
                                                                    ...rutinaAbans,

                                                                    sessions:
                                                                        mergeABP(
                                                                            rutinaAbans.sessions
                                                                            ,
                                                                            [sessioModificada],
                                                                            "nom"
                                                                        )
                                                                };

                                                                console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                                                console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                                                Meteor.call(
                                                                    "rutines.update",
                                                                    rutinaModificada._id,
                                                                    rutinaModificada
                                                                    // ,
                                                                    // (error, result) => {
                                                                    // this.setState({
                                                                    //     showModal: false
                                                                    // });
                                                                    //      location.reload();
                                                                    // }
                                                                );

                                                                ReactDOM.render(
                                                                    <div
                                                                        style={{
                                                                            zIndex: `200000`,
                                                                            background: `rgba(0,0,0,.7)`,
                                                                            width: `100%`,
                                                                            height: `100%`,
                                                                            position: `absolute`
                                                                        }}
                                                                    ></div>,
                                                                    document.body
                                                                );

                                                                location.reload();

                                                                //   }
                                                            }
                                                        }
                                                    );
                                                }
                                            }}
                                        />
                                    </div>
                                </div>
                            );
                        }
                    })()
                    }
                    <Modal
                        shouldCloseOnOverlayClick
                        isOpen={this.state.showModal}
                        contentLabel="Modal"
                        ariaHideApp
                    >
                        <button
                            id="btTancaSessioModal2"
                            onClick={() => {
                                this.setState({
                                    showModal: false
                                });
                            }}
                        >&times;
                        </button>
                        {/* <h1>Algo</h1> */}
                        {
                            // (() => {
                            // //    console.dir("sDProp: ", this.state.selectedDates);
                            // })()
                        }
                        <SessioFormContent
                            objDia={this.state.objDiaSessio}
                            actualitzaSetmanesAmbNovaSessioARutinaState={this.props.actualitzaSetmanesAmbNovaSessioARutinaState}
                            objSetmanes={this.props.objSetmanes}
                            tancaModal={this.handleCloseModal}
                            index={this.state.indexSessio}
                            selectedDates={[]}
                            periodeSessio={this.state.periodeSessio}
                            {...this.props}
                        />
                        {
                            // (() =>{
                            //     let
                            //         valReturn
                            //     ;
                            //
                            //     // this.selectedDatesPromise
                            //     //     .then(
                            //             (result) => {
                            //                 valReturn =
                            //                     <SessioFormContent
                            //                         objDia={this.state.objDiaSessio}
                            //                         actualitzaSetmanesAmbNovaSessioARutinaState={this.props.actualitzaSetmanesAmbNovaSessioARutinaState}
                            //                         objSetmanes={this.props.objSetmanes}
                            //                         tancaModal={this.handleCloseModal}
                            //                         index={this.state.indexSessio}
                            //                         selectedDates={
                            //                         //    result
                            //                             this.selectedDates.get()
                            //                         }
                            //                         {...this.props}
                            //                     />
                            //                 ;
                            //             }
                            //     //    )
                            //     return valReturn;
                            // })()
                        }
                    </Modal>
                </div>
            );
        } else {
            return null;
        }
    }
}

class BotonsCreaSessio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mostraTaulaDies: false,
            mostraNouPeriode: false,
            //    mostraInputPeriodeSessio: false,
            mostraCalPeriodeSessio: false,
            objPeriodeSessio: {},
            showModal: false,
            rangeOfDatesRutina: props.rangeOfDates,

            objDiaSessio: null,
            ambSessio: null,
            datesSessions: [],
            sessionsDia: [],
            indexSessio: null,

            editantSessio: false,

            periodeSessio: {}
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            rangeOfDatesRutina: nextProps.rangeOfDates
        });
    }

    handleCloseModal = () => {
        this.setState({
            showModal: false
        });
    };

    showModal = (
        objDiaSessio,
        ambSessio,
        datesSessions,
        sessionsDia,
        indexSessio
    ) => {
        this.setState({
            showModal: true,
            objDiaSessio,
            datesSessions,
            ambSessio,
            sessionsDia,
            indexSessio
        });
    };

    editantSessioVal = (bool) => {
        this.setState({ editantSessio: bool });
    };

    render() {
        if (Object.keys(this.state.rangeOfDatesRutina).length !== 0) {
            return (
                <div
                    style={{
                        textAlign: `center`
                    }}
                >
                    <button
                        onClick={() => {
                            this.setState((prevState, props) => {
                                return {
                                    mostraTaulaDies: !prevState.mostraTaulaDies,
                                    mostraNouPeriode: false,
                                    mostraCalPeriodeSessio: false
                                }
                            });
                        }}
                    >Taula de dies solts</button>
                    <button
                        onClick={() => {
                            this.setState((prevState, props) => {
                                return {
                                    mostraTaulaDies: false,
                                    mostraNouPeriode: true,
                                    mostraCalPeriodeSessio: !prevState.mostraCalPeriodeSessio
                                }
                            });
                        }}
                    >Subperiode</button>

                    {(() => {
                        if (this.state.mostraTaulaDies) {
                            return (
                                <TaulaBotonsDies
                                    {...this.props}
                                />
                            );
                        }

                        if (this.state.mostraNouPeriode) {
                            return (
                                <div
                                    style={{
                                        display: `inline-block`,
                                        margin: `0 auto`
                                    }}
                                >
                                    {/* <input
                                            style={{
                                                display: this.state.mostraInputPeriodeSessio ? `block` : `none`
                                            }}
                                            type="text"
                                            ref={periodeSessio => this.periodeSessio = periodeSessio}

                                            value={
                                                `${capitalize(moment(this.state.objPeriodeSessio.inici).format("dddd, ll"))} → ${capitalize(moment(this.state.objPeriodeSessio.fi).format("dddd, ll"))}`
                                            }
                                        /> */}
                                    <div
                                        style={{
                                            display: this.state.mostraCalPeriodeSessio ? `block` : `none`
                                        }}
                                    >
                                        <InfiniteCalendar
                                            Component={withRange(Calendar)}
                                            selected={false}
                                            min={this.state.rangeOfDatesRutina.start}
                                            minDate={this.state.rangeOfDatesRutina.start}
                                            max={this.state.rangeOfDatesRutina.end}
                                            maxDate={this.state.rangeOfDatesRutina.end}
                                            displayOptions={{}}
                                            locale={{
                                                locale: caLocale,
                                                headerFormat: 'ddd, D MMM',
                                                weekdays: ["Dmg", "Dll", "Dm", "Dcs", "Djs", "Dvs", "Dss"],
                                                weekStartsOn: 1,
                                                blank: 'Selecciona un periode',
                                                todayLabel: {
                                                    long: 'Anar a avui',
                                                    short: 'Avui.'
                                                }
                                            }}
                                            ref={infCalSes => this.infCalSes = infCalSes}
                                            onSelect={(data) => {
                                                //
                                                if (data.eventType === 3) {
                                                    this.setState({
                                                        showModal: true,
                                                        mostraCalPeriodeSessio: false,
                                                        periodeSessio: data
                                                    });


                                                }

                                            }}
                                        />
                                    </div>

                                </div>
                            );
                        }
                    })()
                    }
                    <Modal
                        shouldCloseOnOverlayClick
                        isOpen={this.state.showModal}
                        contentLabel="Modal"
                        ariaHideApp
                    >
                        <button
                            id="btTancaSessioModal2"
                            onClick={() => {
                                this.setState({
                                    showModal: false
                                });
                            }}
                        >&times;
                        </button>
                        {/* <h1>Algo</h1> */}
                        {
                            // (() => {
                            // //    console.dir("sDProp: ", this.state.selectedDates);
                            // })()
                        }
                        <SessioFormContent
                            objDia={this.state.objDiaSessio}
                            actualitzaSetmanesAmbNovaSessioARutinaState={this.props.actualitzaSetmanesAmbNovaSessioARutinaState}
                            objSetmanes={this.props.objSetmanes}
                            tancaModal={this.handleCloseModal}
                            index={this.state.indexSessio}
                            selectedDates={[]}
                            periodeSessio={this.state.periodeSessio}

                            {...this.props}
                        />

                        {
                            // (() =>{
                            //     let
                            //         valReturn
                            //     ;
                            //
                            //     // this.selectedDatesPromise
                            //     //     .then(
                            //             (result) => {
                            //                 valReturn =
                            //                     <SessioFormContent
                            //                         objDia={this.state.objDiaSessio}
                            //                         actualitzaSetmanesAmbNovaSessioARutinaState={this.props.actualitzaSetmanesAmbNovaSessioARutinaState}
                            //                         objSetmanes={this.props.objSetmanes}
                            //                         tancaModal={this.handleCloseModal}
                            //                         index={this.state.indexSessio}
                            //                         selectedDates={
                            //                         //    result
                            //                             this.selectedDates.get()
                            //                         }
                            //                         {...this.props}
                            //                     />
                            //                 ;
                            //             }
                            //     //    )
                            //     return valReturn;
                            // })()
                        }
                    </Modal>
                </div>
            );
        } else {
            return null;
        }
    }
}

class TaulaBotonsDies extends Component {
    constructor(props) {
        super(props);

        this.selectedDates = new ReactiveVar([]);

        this.state = {
            showModal: false,
            editant: null,
            nSetmanaRutina: null,
            setmana: null,
            objDiaSessio: null,
            ambSessio: null,
            datesSessions: [],
            sessionsDia: [],
            indexSessio: this.props.indexSessio
            ,
            rangeOfDates: this.props.rangeOfDates,
            selectedDates: [],
            rutina: this.props.rutina,

            rutinaEditada: this.props.rutinaEditada || null
        }
    }

    setRutinaEditada = (rutinaEditada) => {
        this.setState({
            rutinaEditada,
            rutina: rutinaEditada
        },
            console.dir("RutinaEditada paràmetre: ", rutinaEditada)
        );

        console.dir("RutinaEditada al State de TaulaBotonsDies: ", this.state.rutinaEditada);
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            rangeOfDates: nextProps.rangeOfDates,
            rutinaEditada: nextProps.rutinaEditada,
            rutina: nextProps.rutinaEditada
        });
    }

    showModal = (
        objDiaSessio,
        ambSessio,
        datesSessions,
        sessionsDia,
        indexSessio
    ) => {
        this.setState({
            showModal: true,
            objDiaSessio,
            datesSessions,
            ambSessio,
            sessionsDia,
            indexSessio
        });
    };

    handleCloseModal = () => {
        this.setState({
            showModal: false,
            nSetmanaRutina: null
        });
        this.props.editantSessioVal(false);

        document.querySelectorAll(".chkSelectDates").forEach((v, i, a) => v.checked = false);
        this.sDModificat = [];

        Meteor.call(
            'rutines.update',
            this.props.rutina._id,
            this.props.rutina
        );
        //  alert(JSON.stringify(this.state.rutinaEditada));
    };

    actualitzaSelectedDates = (selectedDates) => {
        this.setState({
            selectedDates
        });
    };

    construeixArrSetmanes = () => {
        let
            diaDelMesIniciSetmana,
            diaDelMesFiSetmana,
            diaDeLaSetmanaIniciSetmana,
            diaDeLaSetmanaFiSetmana
        ;

        function diaDeLaSetmanaACa(cod) {
            switch (cod) {
                case 0: {
                    return `Diumenge`;
                    break;
                }
                case 1: {
                    return `Dilluns`;
                    break;
                }
                case 2: {
                    return `Dimarts`;
                    break;
                }
                case 3: {
                    return `Dimecres`;
                    break;
                }
                case 4: {
                    return `Dijous`;
                    break;
                }
                case 5: {
                    return `Divendres`;
                    break;
                }
                case 6: {
                    return `Dissabte`;
                    break;
                }
                default: {
                    return `Dia no vàlid!!!`;
                }
            }
        }

        function muntaArrDiesSetmana(setmana) {
            let
                arrDiesSetmana = [],
                punterData = new Date(setmana.start.getFullYear(), setmana.start.getMonth(), setmana.start.getDate()),
                punterDataFi = addDays(new Date(setmana.end.getFullYear(), setmana.end.getMonth(), setmana.end.getDate()), 1),
                nDies
                ;

            while (compareDesc(punterData, punterDataFi) !== 0) {
                arrDiesSetmana.push({
                    diaMes: punterData.getDate(),
                    diaSet: diaDeLaSetmanaACa(punterData.getDay()),
                    diaData: punterData
                });
                punterData = addDays(new Date(punterData), 1);
            }
            nDies = arrDiesSetmana.length;
            return arrDiesSetmana;
        }

        let
            arrSetmanes = [],
            rangeOfDates = this.state.rangeOfDates,
            nSetmanes = this.props.nSetmanes,
            dataIniSetmana,
            dataFiSetmana,
            nSetmanaRutina,
            nSetmanaISO,
            setmana,
            setmanes = this.props.setmanes,
            extremsSetmanes = this.props.extremsSetmanes,
            finalPrimeraSetmana,
            sessions = {},
            iniciUltimaSetmana,
            indexUltimaSessio = null,
            indexSessio = null
            ,
            rangeDatesEndTime = new Date(this.state.rangeOfDates.end).getTime(),
            rangeDatesStartTime = new Date(this.state.rangeOfDates.start).getTime(),

            rangeDies = Math.abs(rangeDatesEndTime - rangeDatesStartTime) / 24 / 60 / 60 / 1000 + 1,

            selectedDates = new Array(nSetmanes)
        ;

        this.selectedDates = { ...selectedDates };

        for (let i = 1; i <= this.props.nSetmanes; i += 1) {

            nSetmanaRutina = i;

            nSetmanaISO = getISOWeek(extremsSetmanes[nSetmanaRutina].start);

            setmana = {
                [i]: {
                    start: extremsSetmanes[nSetmanaRutina].start,
                    end: extremsSetmanes[nSetmanaRutina].end,
                    nSetmanaRutina,
                    nSetmanaISO,
                    sessions
                }
            };

            diaDelMesIniciSetmana = setmana[i].start.getDate();
            diaDelMesFiSetmana = setmana[i].end.getDate();
            diaDeLaSetmanaIniciSetmana = setmana[i].start.getDay();
            diaDeLaSetmanaFiSetmana = setmana[i].end.getDay();
            arrDiesSetmana = muntaArrDiesSetmana(setmana[i]);

            selectedDates[i] = new Array(arrDiesSetmana.length)
                .fill({
                    checked: false,
                    diaData: undefined
                })
                ;

            this.selectedDates = selectedDates;

            arrSetmanes.push(

                <div
                    key={i}
                    style={{
                        borderRadius: `.5em`
                    }}
                >

                    <span
                        style={{
                            display: `block`
                        }}
                    >
                        {`Setmana ${setmana[i].nSetmanaRutina}`}
                    </span>
                    <span
                        style={{
                            display: `block`,
                            justifySelf: `stretch`,
                            fontSize: `.8em`
                        }}
                    >
                        {`${format(setmana[i].start, `D/M/YY`, { locale: caLocale })} ~ ${format(setmana[i].end, `D/M/YY`, { locale: caLocale })}`}
                    </span>
                    <div
                        style={{
                            display: `grid`
                        }}
                    >
                        {
                            (() => {
                                let
                                    totesDates = [],
                                    arrBgColorsBotons = [],
                                    datesSessions =
                                        typeof (this.props.rutina.sessions) !== "undefined" && this.props.rutina.sessions.length > 0
                                            ? this.props.rutina.sessions.map(
                                                (v, i, a) => {

                                                    arrBgColorsBotons[i] = `${d3.interpolateSinebow(Math.random())}`;

                                                    if (v !== null && typeof (v.periodeSessio) !== "undefined" && v.periodeSessio !== null && Object.keys({ ...v.periodeSessio }).length !== 0) {
                                                        this.periodeSessio = v.periodeSessio;

                                                        console.dir("PERIODESESSIO: ", this.periodeSessio);
                                                        return getDates(v.periodeSessio.start, v.periodeSessio.end);
                                                    } else {
                                                        if (v !== null && typeof (v.datesSessio) !== "undefined" && v.datesSessio.length > 0) {
                                                            let
                                                                auxDates = v.datesSessio.map(
                                                                    (w, j, b) => new Date(w)
                                                                )
                                                                ;
                                                            return totesDates.concat(auxDates)
                                                        } else {
                                                            if (v !== null) {
                                                                return totesDates.concat(v.dataSessio);
                                                            }
                                                        }
                                                    }
                                                }
                                            )
                                            : [],
                                    sessionsDia,
                                    ambSessio = (vDiaData) => totesDates.find(
                                        (v, i, a) => {
                                            //console.dir("Vvvvvvvvv: ", v);
                                            if (typeof (v) !== "undefined" && v !== null && v.getTime() === vDiaData.getTime()) {
                                                sessionsDia = v;
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        }
                                    )
                                    ;

                                totesDates = flatMap(datesSessions);

                                // console.dir("totesDates: ", totesDates);
                                // console.dir("datesSessions: ", datesSessions);

                                return arrDiesSetmana.map(
                                    (w, j, b) => {

                                        if (ambSessio(w.diaData)) {
                                            indexUltimaSessio = Number(indexUltimaSessio) + 1;
                                            indexSessio = indexUltimaSessio - 1;
                                        } else {
                                            indexSessio = null;
                                        }

                                        return (

                                            <div
                                                key={j}
                                                style={{
                                                    display: `grid`,
                                                    gridTemplateAreas: `
                                                           "check   boto"
                                                       `,
                                                    justifyContent: `stretch`,
                                                }}
                                            >
                                                <input
                                                    className="chkSelectDates"
                                                    index={i}
                                                    jndex={j}
                                                    type="checkbox"
                                                    style={{
                                                        gridArea: `check`
                                                    }}
                                                    data-dia-data={w.diaData}
                                                    onClick={(ev) => {
                                                        let
                                                            sDAnterior = { ...this.selectedDates },
                                                            sDModificat = sDAnterior
                                                            ;

                                                        // console.dir("EXAMINANT sDAnterior: ", sDAnterior);

                                                        sDModificat[i][j] = {
                                                            checked: !sDAnterior[i][j].checked,
                                                            diaData: ev.target.dataset.diaData
                                                        };





                                                        //   this.selectedDates.set(sDModificat);


                                                        this.selectedDates = sDModificat;

                                                        this.sDModificat = new ReactiveVar(sDModificat);

                                                        // console.dir("EXAMINANT THIS.SELECTEDDATES: ", this.selectedDates);
                                                        // console.dir("EXAMINANT THIS.SDMODIFICAT: ", this.sDModificat);
                                                        //console.dir("EXAMINANT TOTESDATES: ", totesDates);

                                                        //    this.setState({
                                                        //        truSelDates: this.sDModificat.get()
                                                        //    });    

                                                        //     this.actualitzaSelectedDates(this.sDModificat.get());

                                                    }}
                                                />



                                                <button
                                                    key={j}
                                                    data-dia-data={w.diaData}
                                                    indexSessio={indexSessio}
                                                    data-index-sessio={indexSessio}
                                                    data-state-selected-dates={this.state.selectedDates}
                                                    data-seldats={JSON.stringify(this.sDModificat)}
                                                    ref={but => this[`but_${i}_${j}`] = but}
                                                    style={{
                                                        gridArea: `boto`,
                                                        display: `block`,
                                                        justifySelf: `stretch`,
                                                        margin: `0 .3em`,
                                                        background: (ambSessio(w.diaData) && !this.props.editaDatesSessio) ? `rgba(0,255,0,.6)` : `auto`
                                                        //background: (ambSessio(w.diaData)) ? `rgba(0,255,0,.6)` : `auto`
                                                        // background: (ambSessio(w.diaData)) ? arrBgColorsBotons[indexSessio] : `auto`
                                                    }}
                                                    onClick={(ev) => {
                                                        if (!this.props.editaDatesSessio) {

                                                            if (ambSessio(w.diaData)) {
                                                                this.props.editantSessioVal(true);
                                                            }

                                                            // console.dir("W: ", w);

                                                            this.showModal(
                                                                w,
                                                                ambSessio,
                                                                totesDates,
                                                                sessionsDia,
                                                                ev.target.dataset.indexSessio
                                                            );
                                                        } else {


                                                            ///////////////////////////////////////////////////////////////
                                                            let
                                                                arrSelectDates,
                                                                momentDia,
                                                                dataDia,
                                                                dataSessioProp,
                                                                aux,
                                                                resultat,

                                                                seldats
                                                            ;

                                                            if (this.selectedDates !== null && (typeof (this.selectedDates) === "undefined" || this.selectedDates.length === 0)) {
                                                                arrSelectDates = [];
                                                            } else {
                                                                // console.dir("MULTIEDIT notUndefined: ", this.selectedDates);
                                                                arrSelectDates =
                                                                    this.selectedDates === null || typeof (this.selectedDates) === "undefined"
                                                                        ? []
                                                                        : Object.values(this.selectedDates)
                                                                    ;
                                                            }

                                                            //        console.dir("arrSelDats_selectedDates: ", this.props.selectedDates.get() );
                                                            // console.dir("MULTIEDIT arrSelDats: ", arrSelectDates);

                                                            // console.dir("MULTIEDIT momentDia: ", momentDia);
                                                            // console.dir("MULTIEDIT dataDia: ", dataDia);
                                                            // console.dir("MULTIEDIT dataSessioProp: ", dataSessioProp);
                                                            // console.dir("MULTIEDIT this.props.objDia: ", this.props.objDia);
                                                            // console.dir("MULTIEDIT typeof(this.props.objDia): ", typeof (this.props.objDia));

                                                            // console.dir("MULTIEDIT this.state.objPeriodeSessio: ", this.state.objPeriodeSessio);


                                                            aux = arrSelectDates.map(
                                                                (v, i, a) => v.filter(
                                                                    (w, j, b) => w.checked === true
                                                                )
                                                            );

                                                            // console.dir("MULTIEDIT AUX: ", aux);

                                                            resultat = aux.map(
                                                                (v, i, a) => {
                                                                    if (v.length > 0) {
                                                                        return v;
                                                                    }
                                                                }
                                                            )

                                                            resultat = flatMap(resultat).filter(
                                                                (v, i, a) => typeof (v) !== "undefined"
                                                            );

                                                            //console.dir("arrSelectDates: ", arrSelectDates);

                                                            // console.dir("MULTIEDIT resultat: ", resultat);


                                                            // seldats = resultat;

                                                            // console.dir("MULTIEDIT DATA-SELDATS: ", seldats);



                                                            /////////////////////////////////////////////////////////////////////////////

                                                            if (this.props.dateType === "periode") {

                                                                // PERIODE
                                                                //              let
                                                                // //              dataBoto = ev.target.dataset.diaData,

                                                                //                 truIndexSessio = this.props.truIndexSessio,
                                                                //                 sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                                                //                 sessioModificada = {...sessioAbans},
                                                                //                 rutinaAbans = this.props.rutina,
                                                                //                 rutinaModificada = {...this.props.rutina},

                                                                //                 rutinaSessionsAbans = rutinaAbans.sessions,
                                                                //                 rutinaSessionsModificades = [...rutinaSessionsAbans],

                                                                //                 trobaSessioEnCollection = (sessio) => Sessions.find({
                                                                //                     nom: sessio.nom
                                                                //                 }).count() > 0
                                                                //             ;





                                                                if (resultat.length > 0) {
                                                                    // MULTIPLE
                                                                    let
                                                                        //              dataBoto = ev.target.dataset.diaData,

                                                                        truIndexSessio = this.props.truIndexSessio,
                                                                        sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                                                        sessioModificada = { ...sessioAbans },
                                                                        rutinaAbans = this.props.rutina,
                                                                        rutinaModificada = { ...this.props.rutina },

                                                                        rutinaSessionsAbans = rutinaAbans.sessions,
                                                                        rutinaSessionsModificades = [...rutinaSessionsAbans],

                                                                        trobaSessioEnCollection = (sessio) => Sessions.find({
                                                                            nom: sessio.nom
                                                                        }).count() > 0
                                                                        ;

                                                                    if (confirm(`Vas a canviar la data de la sessió per les dates 
                                                                                ${resultat.map((v, i, a) => `\n→ ${capitalize(moment(v.diaData).format("dddd, l"))}`)}.
                                                                            \n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {

                                                                        //alert(`Pos ara ja fariem el canvi.`);
                                                                        //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);

                                                                        console.dir("LA BONA: SESSIÓ ABANS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada ABANS: ", sessioModificada);
                                                                        //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                                                        console.dir("LA BONA: RUTINA ABANS: ", rutinaAbans);
                                                                        console.dir("LA BONA: RUTINAmodificada ABANS: ", rutinaModificada);

                                                                        // if ()

                                                                        sessioModificada.periodeSessio = {};
                                                                        sessioModificada.datesSessio = resultat.map((v, i, a) => v.diaData);

                                                                        console.dir("LA BONA: SESSIÓ DESPRÉS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                                                        console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                                                        if (trobaSessioEnCollection(sessioModificada)) {
                                                                            Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                                                            alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                                                        }

                                                                        console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                                                        console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                                                        // mergeABP(
                                                                        //     rutinaSessionsModificades


                                                                        // Meteor.call(``)

                                                                        console.dir(
                                                                            "mergeArrayDObjectes: ",
                                                                            mergeABP(
                                                                                rutinaSessionsModificades
                                                                                ,
                                                                                [sessioModificada],
                                                                                "nom"
                                                                            )
                                                                        );

                                                                        rutinaSessionsModificades = mergeABP(
                                                                            rutinaSessionsModificades
                                                                            ,
                                                                            [sessioModificada],
                                                                            "nom"
                                                                        )
                                                                            ;

                                                                        console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                                                        console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                                                        rutinaModificada = {
                                                                            ...rutinaAbans,

                                                                            sessions:
                                                                                mergeABP(
                                                                                    rutinaAbans.sessions
                                                                                    ,
                                                                                    [sessioModificada],
                                                                                    "nom"
                                                                                )
                                                                        };

                                                                        console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                                                        Meteor.call(
                                                                            "rutines.update",
                                                                            rutinaModificada._id,
                                                                            rutinaModificada
                                                                            // ,
                                                                            // (error, result) => {
                                                                            // this.setState({
                                                                            //     showModal: false
                                                                            // });
                                                                            //      location.reload();
                                                                            // }
                                                                        );

                                                                        ReactDOM.render(
                                                                            <div
                                                                                style={{
                                                                                    zIndex: `200000`,
                                                                                    background: `rgba(0,0,0,.7)`,
                                                                                    width: `100%`,
                                                                                    height: `100%`,
                                                                                    position: `absolute`
                                                                                }}
                                                                            ></div>,
                                                                            document.body
                                                                        );

                                                                        location.reload();

                                                                    }
                                                                } else {

                                                                    // SIMPLE

                                                                    let
                                                                        dataBoto = ev.target.dataset.diaData,

                                                                        truIndexSessio = this.props.truIndexSessio,
                                                                        sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                                                        sessioModificada = { ...sessioAbans },
                                                                        rutinaAbans = this.props.rutina,
                                                                        rutinaModificada = { ...this.props.rutina },

                                                                        rutinaSessionsAbans = rutinaAbans.sessions,
                                                                        rutinaSessionsModificades = [...rutinaSessionsAbans],

                                                                        trobaSessioEnCollection = (sessio) => Sessions.find({
                                                                            nom: sessio.nom
                                                                        }).count() > 0
                                                                        ;

                                                                    if (confirm(`Vas a canviar la data de la sessió pel \n\n→ ${capitalize(moment(dataBoto).format("dddd, l"))}.\n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {
                                                                        //alert(`Pos ara ja fariem el canvi.`);
                                                                        //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);

                                                                        console.dir("LA BONA: SESSIÓ ABANS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada ABANS: ", sessioModificada);
                                                                        //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                                                        console.dir("LA BONA: RUTINA ABANS: ", rutinaAbans);
                                                                        console.dir("LA BONA: RUTINAmodificada ABANS: ", rutinaModificada);

                                                                        // if ()


                                                                        sessioModificada.periodeSessio = {};
                                                                        sessioModificada.dataSessio = new Date(dataBoto);

                                                                        console.dir("LA BONA: SESSIÓ DESPRÉS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                                                        console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                                                        if (trobaSessioEnCollection(sessioModificada)) {
                                                                            Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                                                            alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                                                        }

                                                                        console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                                                        console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                                                        // mergeABP(
                                                                        //     rutinaSessionsModificades


                                                                        // Meteor.call(``)

                                                                        console.dir(
                                                                            "mergeArrayDObjectes: ",
                                                                            mergeABP(
                                                                                rutinaSessionsModificades
                                                                                ,
                                                                                [sessioModificada],
                                                                                "nom"
                                                                            )
                                                                        );

                                                                        rutinaSessionsModificades = mergeABP(
                                                                            rutinaSessionsModificades
                                                                            ,
                                                                            [sessioModificada],
                                                                            "nom"
                                                                        )
                                                                            ;

                                                                        console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                                                        console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                                                        rutinaModificada = {
                                                                            ...rutinaAbans,

                                                                            sessions:
                                                                                mergeABP(
                                                                                    rutinaAbans.sessions
                                                                                    ,
                                                                                    [sessioModificada],
                                                                                    "nom"
                                                                                )
                                                                        };

                                                                        console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                                                        Meteor.call(
                                                                            "rutines.update",
                                                                            rutinaModificada._id,
                                                                            rutinaModificada
                                                                            // ,
                                                                            // (error, result) => {
                                                                            // this.setState({
                                                                            //     showModal: false
                                                                            // });
                                                                            //      location.reload();
                                                                            // }
                                                                        );

                                                                        ReactDOM.render(
                                                                            <div
                                                                                style={{
                                                                                    zIndex: `200000`,
                                                                                    background: `rgba(0,0,0,.7)`,
                                                                                    width: `100%`,
                                                                                    height: `100%`,
                                                                                    position: `absolute`
                                                                                }}
                                                                            ></div>,
                                                                            document.body
                                                                        );

                                                                        location.reload();

                                                                    }
                                                                }






                                                                if (confirm(`Vas a canviar la data de la sessió per les dates 
                                                                            ${resultat.map((v, i, a) => `\n→ ${capitalize(moment(v.diaData).format("dddd, l"))}`)}.
                                                                        \n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {

                                                                    //alert(`Pos ara ja fariem el canvi.`);
                                                                    //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);

                                                                    console.dir("LA BONA: SESSIÓ ABANS: ", sessioAbans);
                                                                    console.dir("LA BONA: SESSIÓmodificada ABANS: ", sessioModificada);
                                                                    //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                                                    console.dir("LA BONA: RUTINA ABANS: ", rutinaAbans);
                                                                    console.dir("LA BONA: RUTINAmodificada ABANS: ", rutinaModificada);

                                                                    // if ()

                                                                    sessioModificada.datesSessio = resultat.map((v, i, a) => v.diaData);
                                                                    console.dir("LA BONA: SESSIÓ DESPRÉS: ", sessioAbans);
                                                                    console.dir("LA BONA: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                                                    console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                                                    if (trobaSessioEnCollection(sessioModificada)) {
                                                                        Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                                                        alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                                                    }

                                                                    console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                                                    console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                                                    // mergeABP(
                                                                    //     rutinaSessionsModificades


                                                                    // Meteor.call(``)

                                                                    console.dir(
                                                                        "mergeArrayDObjectes: ",
                                                                        mergeABP(
                                                                            rutinaSessionsModificades
                                                                            ,
                                                                            [sessioModificada],
                                                                            "nom"
                                                                        )
                                                                    );

                                                                    rutinaSessionsModificades = mergeABP(
                                                                        rutinaSessionsModificades
                                                                        ,
                                                                        [sessioModificada],
                                                                        "nom"
                                                                    )
                                                                        ;

                                                                    console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                                                    console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                                                    console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                                                    rutinaModificada = {
                                                                        ...rutinaAbans,

                                                                        sessions:
                                                                            mergeABP(
                                                                                rutinaAbans.sessions
                                                                                ,
                                                                                [sessioModificada],
                                                                                "nom"
                                                                            )
                                                                    };

                                                                    console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                                                    console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                                                    Meteor.call(
                                                                        "rutines.update",
                                                                        rutinaModificada._id,
                                                                        rutinaModificada
                                                                        // ,
                                                                        // (error, result) => {
                                                                        // this.setState({
                                                                        //     showModal: false
                                                                        // });
                                                                        //      location.reload();
                                                                        // }
                                                                    );

                                                                    ReactDOM.render(
                                                                        <div
                                                                            style={{
                                                                                zIndex: `200000`,
                                                                                background: `rgba(0,0,0,.7)`,
                                                                                width: `100%`,
                                                                                height: `100%`,
                                                                                position: `absolute`
                                                                            }}
                                                                        ></div>,
                                                                        document.body
                                                                    );

                                                                    location.reload();
                                                                }

                                                            } else {

                                                                if (resultat.length > 0) {
                                                                    // MULTIPLE
                                                                    let
                                                                        //              dataBoto = ev.target.dataset.diaData,

                                                                        truIndexSessio = this.props.truIndexSessio,
                                                                        sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                                                        sessioModificada = { ...sessioAbans },
                                                                        rutinaAbans = this.props.rutina,
                                                                        rutinaModificada = { ...this.props.rutina },

                                                                        rutinaSessionsAbans = rutinaAbans.sessions,
                                                                        rutinaSessionsModificades = [...rutinaSessionsAbans],

                                                                        trobaSessioEnCollection = (sessio) => Sessions.find({
                                                                            nom: sessio.nom
                                                                        }).count() > 0
                                                                        ;

                                                                    if (confirm(`Vas a canviar la data de la sessió per les dates 
                                                                                ${resultat.map((v, i, a) => `\n→ ${capitalize(moment(v.diaData).format("dddd, l"))}`)}.
                                                                            \n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {

                                                                        //alert(`Pos ara ja fariem el canvi.`);
                                                                        //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);

                                                                        console.dir("LA BONA: SESSIÓ ABANS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada ABANS: ", sessioModificada);
                                                                        //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                                                        console.dir("LA BONA: RUTINA ABANS: ", rutinaAbans);
                                                                        console.dir("LA BONA: RUTINAmodificada ABANS: ", rutinaModificada);

                                                                        // if ()

                                                                        sessioModificada.datesSessio = resultat.map((v, i, a) => v.diaData);
                                                                        console.dir("LA BONA: SESSIÓ DESPRÉS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                                                        console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                                                        if (trobaSessioEnCollection(sessioModificada)) {
                                                                            Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                                                            alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                                                        }

                                                                        console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                                                        console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                                                        // mergeABP(
                                                                        //     rutinaSessionsModificades


                                                                        // Meteor.call(``)

                                                                        console.dir(
                                                                            "mergeArrayDObjectes: ",
                                                                            mergeABP(
                                                                                rutinaSessionsModificades
                                                                                ,
                                                                                [sessioModificada],
                                                                                "nom"
                                                                            )
                                                                        );

                                                                        rutinaSessionsModificades = mergeABP(
                                                                            rutinaSessionsModificades
                                                                            ,
                                                                            [sessioModificada],
                                                                            "nom"
                                                                        )
                                                                            ;

                                                                        console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                                                        console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                                                        rutinaModificada = {
                                                                            ...rutinaAbans,

                                                                            sessions:
                                                                                mergeABP(
                                                                                    rutinaAbans.sessions
                                                                                    ,
                                                                                    [sessioModificada],
                                                                                    "nom"
                                                                                )
                                                                        };

                                                                        console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                                                        Meteor.call(
                                                                            "rutines.update",
                                                                            rutinaModificada._id,
                                                                            rutinaModificada
                                                                            // ,
                                                                            // (error, result) => {
                                                                            // this.setState({
                                                                            //     showModal: false
                                                                            // });
                                                                            //      location.reload();
                                                                            // }
                                                                        );

                                                                        ReactDOM.render(
                                                                            <div
                                                                                style={{
                                                                                    zIndex: `200000`,
                                                                                    background: `rgba(0,0,0,.7)`,
                                                                                    width: `100%`,
                                                                                    height: `100%`,
                                                                                    position: `absolute`
                                                                                }}
                                                                            ></div>,
                                                                            document.body
                                                                        );

                                                                        location.reload();

                                                                    }
                                                                } else {

                                                                    // SIMPLE

                                                                    let
                                                                        dataBoto = ev.target.dataset.diaData,

                                                                        truIndexSessio = this.props.truIndexSessio,
                                                                        sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                                                        sessioModificada = { ...sessioAbans },
                                                                        rutinaAbans = this.props.rutina,
                                                                        rutinaModificada = { ...this.props.rutina },

                                                                        rutinaSessionsAbans = rutinaAbans.sessions,
                                                                        rutinaSessionsModificades = [...rutinaSessionsAbans],

                                                                        trobaSessioEnCollection = (sessio) => Sessions.find({
                                                                            nom: sessio.nom
                                                                        }).count() > 0
                                                                        ;

                                                                    if (confirm(`Vas a canviar la data de la sessió pel \n\n→ ${capitalize(moment(dataBoto).format("dddd, l"))}.\n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {
                                                                        //alert(`Pos ara ja fariem el canvi.`);
                                                                        //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);

                                                                        console.dir("LA BONA: SESSIÓ ABANS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada ABANS: ", sessioModificada);
                                                                        //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                                                        console.dir("LA BONA: RUTINA ABANS: ", rutinaAbans);
                                                                        console.dir("LA BONA: RUTINAmodificada ABANS: ", rutinaModificada);

                                                                        // if ()

                                                                        sessioModificada.datesSessio = [];
                                                                        sessioModificada.dataSessio = new Date(dataBoto);

                                                                        console.dir("LA BONA: SESSIÓ DESPRÉS: ", sessioAbans);
                                                                        console.dir("LA BONA: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                                                        console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                                                        if (trobaSessioEnCollection(sessioModificada)) {
                                                                            Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                                                            alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                                                        }

                                                                        console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                                                        console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                                                        // mergeABP(
                                                                        //     rutinaSessionsModificades


                                                                        // Meteor.call(``)

                                                                        console.dir(
                                                                            "mergeArrayDObjectes: ",
                                                                            mergeABP(
                                                                                rutinaSessionsModificades
                                                                                ,
                                                                                [sessioModificada],
                                                                                "nom"
                                                                            )
                                                                        );

                                                                        rutinaSessionsModificades = mergeABP(
                                                                            rutinaSessionsModificades
                                                                            ,
                                                                            [sessioModificada],
                                                                            "nom"
                                                                        )
                                                                            ;

                                                                        console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                                                        console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                                                        rutinaModificada = {
                                                                            ...rutinaAbans,

                                                                            sessions:
                                                                                mergeABP(
                                                                                    rutinaAbans.sessions
                                                                                    ,
                                                                                    [sessioModificada],
                                                                                    "nom"
                                                                                )
                                                                        };

                                                                        console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                                                        console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                                                        Meteor.call(
                                                                            "rutines.update",
                                                                            rutinaModificada._id,
                                                                            rutinaModificada
                                                                            // ,
                                                                            // (error, result) => {
                                                                            // this.setState({
                                                                            //     showModal: false
                                                                            // });
                                                                            //      location.reload();
                                                                            // }
                                                                        );

                                                                        ReactDOM.render(
                                                                            <div
                                                                                style={{
                                                                                    zIndex: `200000`,
                                                                                    background: `rgba(0,0,0,.7)`,
                                                                                    width: `100%`,
                                                                                    height: `100%`,
                                                                                    position: `absolute`
                                                                                }}
                                                                            ></div>,
                                                                            document.body
                                                                        );

                                                                        location.reload();

                                                                    }
                                                                }

                                                            }
                                                            // console.dir("diaDataBoto: ", dataBoto);
                                                        }
                                                    }}
                                                >
                                                    <div
                                                        style={{
                                                            display: `grid`,
                                                            gridTemplateAreas: `
                                                                       "diaSet diaMes"`,
                                                            justifyContent: `center`,
                                                            gridGap: `.5rem`
                                                        }}
                                                    >

                                                        <span
                                                            style={{
                                                                gridArea: `diaSet`
                                                            }}
                                                        >
                                                            {`${w.diaSet} `}
                                                        </span>
                                                        <span
                                                            style={{
                                                                gridArea: `diaMes`,
                                                                fontWeight: `bolder`,
                                                                fontSize: `1.1em`
                                                                // background: `red`
                                                            }}
                                                        >
                                                            {w.diaMes}
                                                        </span>
                                                    </div>
                                                </button>
                                            </div>
                                        )
                                    }
                                )
                            })()
                        }
                    </div>
                </div>
            );
        }
        return arrSetmanes;
    };

    render() {
        if (!!this.props.nSetmanes) {
            let
                arrSetmanes = this.construeixArrSetmanes()
                ;

            return (
                <div
                    style={{
                        display: `grid`,
                        gridTemplateColumns: `repeat( auto-fit, minmax(100px, 1fr) )`,
                        textAlign: `center`
                    }}
                >
                    {(() => {
                        return arrSetmanes;
                    })()
                    }
                    <Modal
                        shouldCloseOnOverlayClick
                        isOpen={this.state.showModal}
                        contentLabel="Modal"
                        ariaHideApp
                    >
                        <button
                            id="btTancaSessioModal"
                            onClick={this.handleCloseModal}
                        >&times;
                        </button>

                        {
                            (() => {
                                //    console.dir("sDProp: ", this.state.selectedDates);
                            })()
                        }

                        <SessioFormContent
                            objDia={this.state.objDiaSessio}
                            actualitzaSetmanesAmbNovaSessioARutinaState={this.props.actualitzaSetmanesAmbNovaSessioARutinaState}
                            objSetmanes={this.props.objSetmanes}
                            tancaModal={this.handleCloseModal}
                            index={this.state.indexSessio}
                            selectedDates={
                                // //    result:
                                typeof (this.sDModificat) !== "undefined" ? this.sDModificat : null
                            }
                            // selectedPeriode={this.state.selectedPeriode}

                            rutinaEditada={this.state.rutinaEditada}
                            setRutinaEditada={this.setRutinaEditada}

                            {...this.props}
                        />
                        {
                            // (() =>{
                            //     let
                            //         valReturn
                            //     ;
                            //
                            //     // this.selectedDatesPromise
                            //     //     .then(
                            //             (result) => {
                            //                 valReturn =
                            //                     <SessioFormContent
                            //                         objDia={this.state.objDiaSessio}
                            //                         actualitzaSetmanesAmbNovaSessioARutinaState={this.props.actualitzaSetmanesAmbNovaSessioARutinaState}
                            //                         objSetmanes={this.props.objSetmanes}
                            //                         tancaModal={this.handleCloseModal}
                            //                         index={this.state.indexSessio}
                            //                         selectedDates={
                            //                         //    result
                            //                             this.selectedDates.get()
                            //                         }
                            //                         {...this.props}
                            //                     />
                            //                 ;
                            //             }
                            //     //    )
                            //     return valReturn;
                            // })()
                        }
                    </Modal>
                </div>
            );
        } else {
            return null;
        }
    }
}

class SessioFormContent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            parts: {},
            showModal: false,
            showCanviaDiaSessioModal: false,
            // selectGMsValue: [],
            chkFotos: true,
            enSessio: true,
            taEscalfament: "",
            selectExercicisValues: [],
            selectGMsValues: [],
            taCalma: "",
            taObsSessio: "",
            /////////Opcions d'impressió
            impNomSessio: false,
            impObsSessio: true,
            impImatgesExs: true,
            impDescExs: true,
            impEscalf: true,
            impTorCalma: true,

            editantSessio: this.props.editantSessio, //THIS??
            nomSessio: "",

            showConegudes: false,
            sesIndex: null,

            selectedDates: props.selectedDates,
            periodeSessio: props.periodeSessio,

            truIndexSessio: null,

            rutinaEditada: null
        };

        this.truIndexSessio = null;
    }

    // shouldComponentUpdate() {
    //     //return false;
    //     if (this.props.editantSessio) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    componentWillReceiveProps(nextProps) {
        this.setState({
            editantSessio: nextProps.editantSessio,
            selectedDates: nextProps.selectedDates,
            periodeSessio: nextProps.periodeSessio,
            rutinaEditada: nextProps.rutinaEditada
        });
    }

    showModal = (part) => {
        this.setState({
            showModal: true,
            editant: part
        });
    };

    showCanviaDiaSessioModal() {
        this.setState({
            showCanviaDiaSessioModal: true
        });
    }

    hideCanviaDiaSessioModal = () => {
        this.setState({
            showCanviaDiaSessioModal: false
        });
    };

    handleCloseModal = () => {
        this.setState({
            showModal: false,
            nSetmanaRutina: null
        });
    };

    handleEstableixSessio = (
        nom,
        data,
        dates,
        periodeSessio,
        incImatges,
        gms,
        escalfament,
        exercicis,
        calma,
        observacionsSessio,
        opcionsImpressio
    ) => {
        Meteor.call(
            'sessions.insert',
            nom,
            data,
            dates,
            periodeSessio,
            incImatges,
            gms,
            escalfament,
            exercicis,
            calma,
            observacionsSessio,
            opcionsImpressio
        );
    };

    selectGMsUpdateValue = (selectGMsValues) => {
        this.setState({ selectGMsValues });
    };

    tancaSessionsConegudes = () => {
        this.setState({
            showConegudes: false
        });
    };

    updateExercicisIGMsSelects = (selectExercicisValues) => {
        this.setState((prevState, props) => {
            let
                exercicisAnteriors = prevState.selectExercicisValues || [],
                exercicisNous = selectExercicisValues,

                grupsMuscularsTrobats = []
                ;

            exercicisAnteriors.concat(exercicisNous)
                .forEach(
                    exerciciAnteriorONou => {
                        let
                            exerciciTrobat = props.exercicis.find(
                                exercici => exercici._id === exerciciAnteriorONou.value
                            ),

                            gmTrobat = props.grups_musculars.find(
                                grupMuscular => grupMuscular.grupMuscularNom === exerciciTrobat.exerciciGrupMuscular
                            ),

                            gmAmbFormat = {
                                label: gmTrobat.grupMuscularNom,
                                value: gmTrobat._id
                            }
                            ;

                        if (grupsMuscularsTrobats.find(
                            grupMuscularTrobat => grupMuscularTrobat.value === gmAmbFormat.value
                        ) === undefined) {
                            grupsMuscularsTrobats.push(gmAmbFormat);
                        }
                    }
                )
                ;

            console.dir("grupsMuscularsTrobats: ", grupsMuscularsTrobats);

            return {
                selectExercicisValues: exercicisAnteriors.concat(exercicisNous),
                selectGMsValues: grupsMuscularsTrobats
            };
        });
    };

    render() {
        let
            dataSessioProp =
                typeof (this.props.objDia) !== "undefined" && this.props.objDia !== null
                    ? addDays(this.props.objDia.diaData, 1)
                    : null
            ,
            dataDia =
                typeof (this.props.objDia) !== "undefined" && this.props.objDia !== null
                    ? new Date(
                        dataSessioProp.getFullYear(),
                        dataSessioProp.getMonth(),
                        dataSessioProp.getDate()
                    )
                    : null
            ,
            momentDia =
                typeof (this.props.objDia) !== "undefined" && this.props.objDia !== null
                    ? moment(dataDia).locale("ca").subtract(1, 'days')
                    : null
            ,
            // exec = (() => console.dir("TYPOFarrSelDats_selectedDates: ", typeof(this.props.selectedDates) === "undefined" ) )(),
            // exec2 = (() => console.dir("arrSelDats_selectedDates: ", this.props.selectedDates ))(),
            // arrSelectDates = []
            arrSelectDates,
            aux,

            totesDates = []
            ,

            datesSessionsBIS =
                this.props.rutina.sessions
                    ? this.props.rutina.sessions.map(
                        (v, i, a) => {

                            //         console.dir("VVVVVVVVVVeee: ", v);

                            if (v !== null && typeof (v.periodeSessio) !== "undefined" && v.periodeSessio !== null && Object.keys({ ...v.periodeSessio }).length !== 0) {
                                return getDates(v.periodeSessio.start, v.periodeSessio.end);
                            } else {

                                if (v !== null && typeof (v.datesSessio) !== "undefined" && v.datesSessio !== null && v.datesSessio.length > 0) {
                                    let
                                        auxDates = v.datesSessio.map(
                                            (w, j, b) => new Date(w)
                                        )
                                    ;
                                    return totesDates.concat(auxDates)
                                }
                                //return totesDates.concat(v.dataSessio);
                                //    console.dir("vDatessSessio.dataSessio: ", v.dataSessio);
                                if (v !== null) {
                                    return totesDates.concat(v.dataSessio);
                                }
                            }

                        }
                    )
                    : []
            ,
            //rSessions = this.props.rutina.sessions || [],
            auxDates,
            sessionsAMostrar = [],
            d =
                typeof (this.props.objDia) !== "undefined" && this.props.objDia !== null
                    ? this.props.objDia.diaData
                    : null
            ,
            pivotIndex,
            indexSessio,

            dateType
        ;

        // console.dir(">>>>>>>>datesSessionsBIS: ", datesSessionsBIS);

        datesSessionsBIS.forEach(
            (v, i, a) => {

                // console.dir("VE: ", v);
                // console.dir("D: ", d);
                // console.dir("INDEX: ", i);

                if (v !== null && typeof (v) !== "undefined") {
                    pivotIndex = v.filter(
                        (w, j, b) =>
                            typeof (w) !== "undefined" && w !== null && d !== null
                                ? w.getTime() === d.getTime()
                                : false
                    );
                }

                // console.dir("PIVOTINDEX", pivotIndex);

                if (typeof (pivotIndex) !== "undefined" && pivotIndex.length > 0) {
                    indexSessio = i;
                }
            }
        );

        // console.dir("DATESSESSIONS: ", datesSessionsBIS);

        // console.dir("INDEXSESSIO: ", indexSessio);

        // this.setState({
        //     truIndexSessio: indexSessio
        // });

        this.truIndexSessio = indexSessio;


        // console.dir(`TRUINDEXSESSIO: `, this.truIndexSessio);

        dateType =
            typeof (indexSessio) !== "undefined"
                ?
                typeof (this.props.rutina.sessions[indexSessio]) !== "undefined"
                    && typeof (this.props.rutina.sessions[indexSessio].periodeSessio) !== "undefined"
                    && this.props.rutina.sessions[indexSessio].periodeSessio !== null
                    && Object.keys({ ...this.props.rutina.sessions[indexSessio].periodeSessio }).length > 0
                    ? "periode"
                    : typeof (this.props.rutina.sessions[indexSessio]) !== "undefined"
                        && typeof (this.props.rutina.sessions[indexSessio].datesSessio) !== "undefined"
                        && this.props.rutina.sessions[indexSessio].datesSessio.length > 0
                        ? "multiple"
                        : typeof (this.props.rutina.sessions[indexSessio]) !== "undefined"
                            && typeof (this.props.rutina.sessions[indexSessio].dataSessio) !== "undefined"
                            ? "simple"
                            : "error"
                : "noIndexSessio"
            ;

        // console.dir("DATETYPE: ", dateType);

        if (typeof (indexSessio) !== "undefined") {
            /*INICI DEL BLOC ON LA SESSIÓ ÉS CONEGUDA. Falta editar correctament triant les dates que seran afectades.*/
            sessionsAMostrar.push(this.props.rutina.sessions[indexSessio]);

            // console.dir("SESSIONSAMOSTRAR: ", sessionsAMostrar);

            // ///////////////////////////////////////////////////////////////////////////--------------------

            // console.dir("DATASESSIOPROP: ", dataSessioProp);
            // console.dir("DATADIA: ", dataDia);
            // console.dir("MOMENTDIA: ", momentDia);


            if (this.props.selectedDates !== null && (typeof (this.props.selectedDates) === "undefined" || this.props.selectedDates.length === 0)) {
                arrSelectDates = [];
            } else {
                // console.dir("notUndefined: ", this.props.selectedDates);
                arrSelectDates =
                    this.props.selectedDates === null || typeof (this.props.selectedDates) === "undefined"
                        ? []
                        : Object.values(this.props.selectedDates.get())
                    ;
            }

            // console.dir("arrSelDats_selectedDates: ", this.props.selectedDates);
            // console.dir("arrSelDats: ", arrSelectDates);

            // console.dir("momentDia: ", momentDia);
            // console.dir("dataDia: ", dataDia);
            // console.dir("dataSessioProp: ", dataSessioProp);
            // console.dir("this.props.objDia: ", this.props.objDia);
            // console.dir("typeof(this.props.objDia): ", typeof (this.props.objDia));

            // console.dir("this.state.objPeriodeSessio: ", this.state.objPeriodeSessio);


            aux = arrSelectDates.map(
                (v, i, a) => v.filter(
                    (w, j, b) => w.checked === true
                )
            );

            // console.dir("AUX: ", aux);

            resultat = aux.map(
                (v, i, a) => {
                    if (v.length > 0) {
                        return v;
                    }
                }
            )

            resultat = flatMap(resultat).filter(
                (v, i, a) => typeof (v) !== "undefined"
            );

            this.resultat = resultat;

            // console.dir("arrSelectDates: ", arrSelectDates);

            // console.dir("resultat: ", resultat);

            // console.dir("MOMENTDIA: ", momentDia);

            // console.dir("SESSIODATES: ", this.props.rutina.sessions[indexSessio].datesSessio);

            //this.dataDia = momentDia;

            return (
                <div>
                    {
                        (() => {
                            switch (dateType) {

                                case "periode": {
                                    return <h1>{`${capitalize(moment(this.props.rutina.sessions[indexSessio].periodeSessio.start).format("dddd, l"))}→${capitalize(moment(this.props.rutina.sessions[indexSessio].periodeSessio.end).format("dddd, l"))}`}</h1>;
                                    //alert("PERIODE");
                                    break;
                                }

                                case "multiple": {
                                    return this.props.rutina.sessions[indexSessio].datesSessio.map(
                                        (v, i, a) => <h2 key={i}>{capitalize(moment(v).format("dddd l"))}</h2>
                                    );
                                    //alert("multiple");
                                    break;
                                }

                                case "simple": {
                                    return <h1>{capitalize(momentDia.format("dddd l"))}</h1>;
                                    //alert("SIMPLE");
                                    break;
                                }

                                default: {
                                    alert("ERRORRR AMB DATETYPE!!!!");
                                    break;
                                }
                            }

                            // resultat.length > 0
                            // ?   resultat.map(
                            //         (v,i,a) => <h2 key={i}>{capitalize(moment(v.diaData).format("dddd L"))}</h2>
                            //     )
                            // :   typeof(this.props.objDia) !== "undefined" && this.props.objDia !== null
                            //     ?   <h1>{capitalize(momentDia.format("dddd L"))}</h1>
                            //     :   <h1>{`${capitalize(moment(this.props.periodeSessio.start).format("dddd, L"))}→${capitalize(moment(this.props.periodeSessio.end).format("dddd, L"))}`}</h1>
                        })()
                    }

                    <div>
                        <button
                            onClick={(ev) => {
                                ReactDOM.render(
                                    <BotonsEditaDatesSessio
                                        truIndexSessio={this.truIndexSessio}

                                        dateType={dateType}
                                        {...this.props}
                                    />
                                    ,
                                    ev.target.parentNode
                                );
                            }}
                        >
                            Canviar dates per noves
                        </button>
                        {
                            /*    (() => {
                                   if (dateType !== "simple") {
                                       return (
                                           <button>
                                               Modificar les dates respectant les anteriors
                                           </button>
                                       );
                                   } else {
                                       return null;
                                   }
                               })() */
                        }
                    </div>



                    <button
                        style={{
                            background: `rgba(255,0,255, .5)`
                        }}
                        onClick={() => {
                            // let
                            //     rutinaAbans = this.props.rutina,
                            //     sessionsAbans = this.props.rutina.sessions,
                            //     sessioActual =
                            //         this.props.rutina.sessions
                            //         ?   this.props.rutina.sessions[this.truIndexSessio]
                            //         :   [],
                            //     sessionsSenseActual =
                            //         this.props.rutina.sessions
                            //         ?  (() => {
                            //                 sessionsAbans.splice(this.truIndexSessio, 1);
                            //                 return sessionsAbans;
                            //             })()

                            //         // this.props.rutina.sessions.filter(
                            //         //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                            //         //     )
                            //         :   [],
                            //     rutinaDespres = {...rutinaAbans}
                            // ;


                            // rutinaDespres.sessions = sessionsSenseActual;

                            this.setState({
                                showConegudes: !this.state.showConegudes,
                                tancaConegudes: this.tancaSessionsConegudes,
                                sesIndex: this.props.index || null
                            });

                            // if (confirm(`Vas a guardar una nova sessió amb el nom "${sessioActual[0].nom}". Confirmes l'operació?`)) {
                            //                         // Meteor.call(
                            //                         //     'rutines.update',
                            //                         //     this.props.rutinaId,
                            //                         //     rutinaDespres
                            //                         // );
                            //     console.dir("sessioActual: ", sessioActual);
                            //
                            //     // Meteor.call(
                            //     //     'sessions.insert',
                            //     //     ...sessioActual
                            //     //     // nom,
                            //     //     // data,
                            //     //     // incImatges,
                            //     //     // gms,
                            //     //     // escalfament,
                            //     //     // exercicis,
                            //     //     // calma,
                            //     //     // observacionsSessio,
                            //     //     // opcionsImpressio
                            //     // );
                            // }
                        }}
                    >Sessions conegudes
                    </button>

                    <button
                        style={{
                            background: `aqua`
                            // ,
                            // display: this.props.editantSessio ? `inline-block` : `none`
                        }}
                        onClick={() => {
                            let
                                rutinaAbans = this.props.rutina,
                                sessionsAbans = this.props.rutina.sessions,
                                sessioActual =
                                    this.props.rutina.sessions
                                        ? this.props.rutina.sessions[this.truIndexSessio]
                                        : [],
                                sessionsSenseActual =
                                    this.props.rutina.sessions
                                        ? (() => {
                                            sessionsAbans.splice(this.truIndexSessio, 1);
                                            return sessionsAbans;
                                        })()

                                        // this.props.rutina.sessions.filter(
                                        //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                                        //     )
                                        : [],
                                rutinaDespres = { ...rutinaAbans }
                            ;

                            rutinaDespres.sessions = sessionsSenseActual;

                            // if (sessioActual.nom.length === 7 && confirm(`Potser no li hages posat un nom a la sessió que et permeta identificar-la després d'entre la resta de les guardades. Vols donar-li nom ara?`)) {
                            //     this.setState({
                            //         impNomSessio: true
                            //     });

                                
                            // }

                            if (confirm(`Vas a guardar una nova sessió amb el nom "${sessioActual.nom}". Confirmes l'operació?`)) {
                                // Meteor.call(
                                //     'rutines.update',
                                //     this.props.rutinaId,
                                //     rutinaDespres
                                // );
                                console.dir("sessioActual: ", sessioActual);

                                Meteor.call(
                                    'sessions.insert',

                                    sessioActual.nom,
                                    sessioActual.dataSessio,
                                    sessioActual.datesSessio,
                                    sessioActual.periodeSessio,
                                    sessioActual.incImatges,
                                    sessioActual.gms,
                                    sessioActual.escalfament,
                                    sessioActual.exercicis,
                                    sessioActual.calma,
                                    sessioActual.observacionsSessio,
                                    sessioActual.opcionsImpressio
                                );
                            }
                        }}
                    >Guarda a la taula
                    </button>



                    <button
                        style={{
                            background: `red`
                        }}
                        onClick={() => {
                            //  alert(`TRUINDEXSESSIO: ${this.truIndexSessio}\n\nDATETYPE: ${dateType}`);
                            switch (dateType) {
                                case "periode": {
                                    //       alert("En periode.");
                                    let
                                        rutinaAbans = this.props.rutina,
                                        sessionsAbans = this.props.rutina.sessions,
                                        sessioActual =
                                            this.props.rutina.sessions
                                                ? this.props.rutina.sessions[this.truIndexSessio]
                                                : [],
                                        sessionsSenseActual =
                                            this.props.rutina.sessions
                                                ? (() => {
                                                    sessionsAbans.splice(this.truIndexSessio, 1);
                                                    return sessionsAbans;
                                                })()

                                                // this.props.rutina.sessions.filter(
                                                //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                                                //     )
                                                : [],
                                        rutinaDespres = { ...rutinaAbans }
                                        ;

                                    rutinaDespres.sessions = sessionsSenseActual;

                                    console.dir("SESSIOACTUAL: ", sessioActual);
                                    console.dir("DATETYPE ELIMINATORI: ", dateType);
                                    console.dir("SESSIONSSENSEACTUAL: ", sessionsSenseActual);


                                    if (confirm(`Vas a esborrar la sessió "${sessioActual.nom}". Aquesta operació no se pot recuperar. \n\nConfirmes l'operació?`)) {
                                        Meteor.call(
                                            'rutines.update',
                                            this.props.rutinaId,
                                            { ...rutinaDespres }

                                        );
                                        //console.dir("rutinaDespres: ", rutinaDespres);


                                        ReactDOM.render(
                                            <div
                                                style={{
                                                    zIndex: `200000`,
                                                    background: `rgba(0,0,0,.7)`,
                                                    width: `100%`,
                                                    height: `100%`,
                                                    position: `absolute`
                                                }}
                                            ></div>,
                                            document.body
                                        );

                                        location.reload();
                                    }
                                    break;
                                }
                                case "multiple": {
                                    //  alert("En múltiple.");
                                    let
                                        rutinaAbans = this.props.rutina,
                                        sessionsAbans = this.props.rutina.sessions,
                                        sessioActual =
                                            this.props.rutina.sessions
                                                ? this.props.rutina.sessions[this.truIndexSessio]
                                                : [],
                                        sessionsSenseActual =
                                            this.props.rutina.sessions
                                                ? (() => {
                                                    sessionsAbans.splice(this.truIndexSessio, 1);
                                                    return sessionsAbans;
                                                })()

                                                // this.props.rutina.sessions.filter(
                                                //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                                                //     )
                                                : [],
                                        rutinaDespres = { ...rutinaAbans }
                                        ;

                                    rutinaDespres.sessions = sessionsSenseActual;

                                    console.dir("SESSIOACTUAL: ", sessioActual);
                                    console.dir("DATETYPE ELIMINATORI: ", dateType);
                                    console.dir("SESSIONSSENSEACTUAL: ", sessionsSenseActual);


                                    if (confirm(`Vas a esborrar la sessió "${sessioActual.nom}". Aquesta operació no se pot recuperar. \n\nConfirmes l'operació?`)) {
                                        Meteor.call(
                                            'rutines.update',
                                            this.props.rutinaId,
                                            { ...rutinaDespres }

                                        );
                                        //console.dir("rutinaDespres: ", rutinaDespres);


                                        ReactDOM.render(
                                            <div
                                                style={{
                                                    zIndex: `200000`,
                                                    background: `rgba(0,0,0,.7)`,
                                                    width: `100%`,
                                                    height: `100%`,
                                                    position: `absolute`
                                                }}
                                            ></div>,
                                            document.body
                                        );

                                        location.reload();
                                    }
                                    break;
                                }
                                case "simple": {

                                    let
                                        rutinaAbans = this.props.rutina,
                                        sessionsAbans = this.props.rutina.sessions,
                                        sessioActual =
                                            this.props.rutina.sessions
                                                ? this.props.rutina.sessions[this.truIndexSessio]
                                                : [],
                                        sessionsSenseActual =
                                            this.props.rutina.sessions
                                                ? (() => {
                                                    sessionsAbans.splice(this.truIndexSessio, 1);
                                                    return sessionsAbans;
                                                })()

                                                // this.props.rutina.sessions.filter(
                                                //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                                                //     )
                                                : [],
                                        rutinaDespres = { ...rutinaAbans }
                                        ;


                                    rutinaDespres.sessions = sessionsSenseActual;

                                    console.dir("SESSIOACTUAL: ", sessioActual);
                                    console.dir("DATETYPE ELIMINATORI: ", dateType);


                                    if (confirm(`Vas a esborrar la sessió "${sessioActual.nom}". Aquesta operació no se pot recuperar. \n\nConfirmes l'operació?`)) {
                                        Meteor.call(
                                            'rutines.update',
                                            this.props.rutinaId,
                                            { ...rutinaDespres }

                                        );
                                        //console.dir("rutinaDespres: ", rutinaDespres);


                                        ReactDOM.render(
                                            <div
                                                style={{
                                                    zIndex: `200000`,
                                                    background: `rgba(0,0,0,.7)`,
                                                    width: `100%`,
                                                    height: `100%`,
                                                    position: `absolute`
                                                }}
                                            ></div>,
                                            document.body
                                        );

                                        location.reload();
                                    }

                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                        }}
                    >Elimina la sessió
                    </button>

                    {
                        (() => {
                            class SessioIndividual extends Component {
                                constructor(props) {
                                    super(props);

                                    console.dir("propsVALUE ", props.value);

                                    this.state = {
                                        vValue: props.value,
                                        selectGMsValues: props.value.gms,
                                        selectExercicisValues: props.value.exercicis,
                                        impOpcionsVisibles: true,
                                        impNomSessio: props.value.opcionsImpressio.impNomSessio,
                                        impObsSessio: props.value.opcionsImpressio.impObsSessio,
                                        impImatgesExs: props.value.opcionsImpressio.impImatgesExs,
                                        impDescExs: props.value.opcionsImpressio.impDescExs,
                                        impEscalf: props.value.opcionsImpressio.impEscalf,
                                        impTorCalma: props.value.opcionsImpressio.impTorCalma,
                                        impTABULAR: props.value.opcionsImpressio.impTABULAR,
                                        showModalReordena: false,
                                        sortedExs: props.value.exercicis
                                    };
                                }

                                selectGMsUpdateValues = (selectGMsValues) => {
                                    let
                                        rutinaAnterior = this.props.rutina,
                                        rutinaModificada = { ...rutinaAnterior }
                                        ;

                                    rutinaModificada.sessions[this.props.indexSessio].gms = selectGMsValues;

                                    this.setState({ selectGMsValues });

                                    Meteor.call(
                                        'rutines.update',
                                        this.props.rutinaId,
                                        rutinaModificada
                                    );
                                }

                                opcionsDImpressioEditades = (
                                    impNomSessio,
                                    impObsSessio,
                                    impImatgesExs,
                                    impDescExs,
                                    impEscalf,
                                    impTorCalma,
                                    impTABULAR
                                ) => {
                                    this.setState({
                                        impNomSessio,
                                        impObsSessio,
                                        impImatgesExs,
                                        impDescExs,
                                        impEscalf,
                                        impTorCalma,
                                        impTABULAR
                                    });
                                };

                                updateExercicisIGMsSelects = (selectExercicisValues) => {
                                    this.setState((prevState, props) => {
                                        let
                                            exercicisAnteriors = prevState.selectExercicisValues || [],
                                            exercicisNous = selectExercicisValues,

                                            grupsMuscularsTrobats = [],
                                            vValueNou = prevState.vValue
                                            ;

                                        exercicisAnteriors.concat(exercicisNous)
                                            .forEach(
                                                exerciciAnteriorONou => {
                                                    let
                                                        exerciciTrobat = props.exercicis.find(
                                                            exercici => exercici._id === exerciciAnteriorONou.value
                                                        ),

                                                        gmTrobat = props.grups_musculars.find(
                                                            grupMuscular => grupMuscular.grupMuscularNom === exerciciTrobat.exerciciGrupMuscular
                                                        ),

                                                        gmAmbFormat = {
                                                            label: gmTrobat.grupMuscularNom,
                                                            value: gmTrobat._id
                                                        }
                                                        ;

                                                    if (grupsMuscularsTrobats.find(
                                                        grupMuscularTrobat => grupMuscularTrobat.value === gmAmbFormat.value
                                                    ) === undefined) {
                                                        grupsMuscularsTrobats.push(gmAmbFormat);
                                                    }
                                                }
                                            )
                                            ;

                                        console.dir("grupsMuscularsTrobats: ", grupsMuscularsTrobats);

                                        vValueNou.exercicis = exercicisAnteriors.concat(exercicisNous);

                                        return {
                                            selectExercicisValues: exercicisAnteriors.concat(exercicisNous),
                                            vValue: vValueNou,
                                            selectGMsValues: grupsMuscularsTrobats
                                        };
                                    });
                                };

                                handleCloseModalReordena = () => {
                                    this.setState({
                                        showModalReordena: false
                                    });
                                };

                                sortedExs = (newValues) => {
                                    this.setState({
                                        selectExercicisValues: [...newValues]
                                    });
                                };

                                render() {

                                    let
                                        dateType
                                        ;

                                    const ListElement = styled.li`
                                        background-color: white;
                                        user-select: none;
                                        moz-user-select: none;
                                        &.dragging-helper-class {
                                            box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.25);
                                        }
                                    `;

                                    const SortableItem = SortableElement(({ value }) =>
                                        <ListElement
                                            style={{
                                                // border: `1px`,
                                                // padding: `1em`,
                                                // background: `beige`,
                                                // margin: `.2em`,
                                                // width: `auto`,
                                                userSelect: `none`,
                                                MozUserSelect: `none`
                                            }}
                                            className="SortableItem"
                                        >{value}</ListElement>
                                    );

                                    const SortableList = SortableContainer(({ items }) => {
                                        return (
                                            <ol
                                                className="SortableList"
                                            >
                                                {items.map((value, index) => (
                                                    <SortableItem key={`item-${index}`} index={index} value={value.label} />
                                                ))}
                                            </ol>
                                        );
                                    });

                                    class SortableComponent extends Component {
                                        state = {
                                            items: this.props.items,
                                        };
                                        onSortEnd = ({ oldIndex, newIndex }) => {
                                            this.setState({
                                                items: arrayMove(this.state.items, oldIndex, newIndex)
                                            });
                                            this.props.sortedExs(this.state.items);
                                        };
                                        render() {
                                            return <SortableList items={this.state.items} onSortEnd={this.onSortEnd} helperClass="dragging-helper-class" />;
                                        }
                                    }

                                    //////////




                                    return (
                                        <div>

                                            <Modal
                                                shouldCloseOnOverlayClick
                                                isOpen={this.state.showModalReordena}
                                                contentLabel="ModalReordena"
                                                ariaHideApp
                                            >
                                                <button
                                                    id="btTancaModalReordena"
                                                    onClick={this.handleCloseModalReordena}
                                                >&times;
                                                </button>

                                                {/* <ol
                                                        style={{
                                                        
                                                        }}
                                                    >
                                                {
                                                    typeof(this.state.selectExercicisValues) !== "undefined" && this.state.selectExercicisValues.map(
                                                        (v,i,a) => <li>{v.label}</li>
                                                    )
                                                        
                                                }
                                                </ol> */}

                                                <SortableComponent items={this.state.selectExercicisValues} sortedExs={this.sortedExs} />

                                                <button
                                                    items={this.state.selectExercicisValues}
                                                    onClick={(e) => {
                                                        console.log("~~~~~~~~~~~\n");
                                                        console.dir("E: ", e.target);
                                                        console.dir("vValue: ", this.state.vValue);
                                                        console.dir("rutina: ", this.props.rutina);
                                                        console.dir("STATE: ", this.state);
                                                        console.dir("PROPS: ", this.props);
                                                        console.log("~~~~~~~~~~~\n");

                                                        //                  this.updateExercicisIGMsSelects(this.state.sortedExs);

                                                        ((sortedExs) => {
                                                            this.setState((prevState, props) => {
                                                                let
                                                                    vValueNou = prevState.vValue
                                                                    ;

                                                                vValueNou.exercicis = sortedExs;

                                                                return {
                                                                    selectExercicisValues: sortedExs,
                                                                    vValue: vValueNou
                                                                    // ,
                                                                    // selectGMsValues: grupsMuscularsTrobats
                                                                };
                                                            });

                                                        })(this.state.selectExercicisValues);


                                                        ((selectExercicisValues) => {
                                                            let
                                                                vValueModificat = { ...this.state.vValue },
                                                                rutinaModificada = { ...this.props.rutina }
                                                                ;

                                                            vValueModificat.exercicis = selectExercicisValues;
                                                            this.setState({
                                                                vValue: vValueModificat
                                                            });

                                                            rutinaModificada.sessions[this.props.indexSessio].exercicis = selectExercicisValues;

                                                            //       this.updateExercicisIGMsSelects();

                                                            Meteor.call(
                                                                'rutines.update',
                                                                this.props.rutinaId,
                                                                rutinaModificada
                                                            );
                                                        })(this.state.selectExercicisValues)

                                                    }}
                                                >
                                                    ESTABLEIX
                                                </button>


                                            </Modal>

                                            <SelectExercicisJEstyle
                                                updateExercicisIGMsSelects={this.updateExercicisIGMsSelects}
                                                {...this.props}
                                            />


                                            <label
                                                htmlFor="selGMs"
                                                style={{
                                                    display: `block`
                                                }}
                                            >Grups Musculars:
                                            </label>
                                            <div
                                                style={{
                                                    zIndex: 200,
                                                    position: `relative`
                                                }}
                                            >

                                                <Select
                                                    id="selGMs"
                                                    options={this.props.grups_musculars.map(
                                                        (v, i, a) => ({
                                                            label: v.grupMuscularNom,
                                                            value: v._id
                                                        })
                                                    )}
                                                    multi
                                                    value={this.state.selectGMsValues}
                                                    onChange={this.selectGMsUpdateValues}
                                                />
                                            </div>

                                            <label
                                                htmlFor="selExercicis"
                                            >Exercicis:
                                            </label>
                                            <div
                                                style={{
                                                    zIndex: 100,
                                                    position: `relative`
                                                }}
                                            >
                                                <Select
                                                    id="selExercicis"
                                                    options={
                                                        //(() => {
                                                        // let gms = new Set();
                                                        //
                                                        // this.state.selectGMsValue.forEach(
                                                        //     (v,i,a) => {
                                                        //         gms.add(v.label);
                                                        //     }
                                                        // );

                                                        this.props.exercicis.reduce(
                                                            (t, v, i, a) => {

                                                                let gms = new Set();
                                                                //
                                                                this.state.selectGMsValues.forEach(
                                                                    (v, i, a) => {
                                                                        gms.add(v.label);
                                                                    }
                                                                );

                                                                if (
                                                                    gms.has(v.exerciciGrupMuscular)
                                                                ) {
                                                                    t.push({
                                                                        label: v.exerciciNom,
                                                                        value: v._id
                                                                    });
                                                                }

                                                                return t;
                                                            },
                                                            []
                                                        )
                                                        //    })()
                                                    }

                                                    multi
                                                    value={this.state.selectExercicisValues}
                                                    onChange={(selectExercicisValues) => {
                                                        let
                                                            vValueModificat = { ...this.state.vValue },
                                                            rutinaModificada = { ...this.props.rutina }
                                                            ;

                                                        vValueModificat.exercicis = selectExercicisValues;
                                                        this.setState({
                                                            vValue: vValueModificat
                                                        });

                                                        rutinaModificada.sessions[this.props.indexSessio].exercicis = selectExercicisValues;

                                                        //       this.updateExercicisIGMsSelects();

                                                        Meteor.call(
                                                            'rutines.update',
                                                            this.props.rutinaId,
                                                            rutinaModificada
                                                        );
                                                    }}
                                                />
                                                <button
                                                    onClick={() => {
                                                        this.setState({
                                                            showModalReordena: !this.state.showModalReordena
                                                        });
                                                    }}
                                                >
                                                    Reordena
                                                </button>
                                            </div>

                                            {
                                                (() => {
                                                    //     console.log("Pos ací anava el DaType...");
                                                    // console.dir("DATYPE: ", typeof(this.props.rutina[this.props.indexSessio]) !== "undefined"
                                                    //     && typeof(this.props.rutina[this.props.indexSessio].periodeSessio) !== "undefined"
                                                    // ?   "periode"
                                                    // :   typeof(this.props.rutina[this.props.indexSessio]) !== "undefined"
                                                    //         && typeof(this.props.rutina[this.props.indexSessio].datesSessio) !== "undefined"
                                                    //         && this.props.rutina[this.props.indexSessio].datesSessio.length > 0
                                                    //     ?   "multiple"
                                                    //     :   typeof(this.props.rutina[this.props.indexSessio]) !== "undefined"
                                                    //             && typeof(this.props.rutina[this.props.indexSessio].dataSessio) !== "undefined"
                                                    //         ?   "simple"
                                                    //         :   "error");
                                                    dateType =
                                                        typeof (this.props.rutina.sessions[this.props.indexSessio]) !== "undefined"
                                                            && typeof (this.props.rutina.sessions[this.props.indexSessio].periodeSessio) !== "undefined"
                                                            && this.props.rutina.sessions[this.props.indexSessio].periodeSessio !== null
                                                            && Object.keys({ ...this.props.rutina.sessions[this.props.indexSessio].periodeSessio }).length !== 0
                                                            ? "periode"
                                                            : typeof (this.props.rutina.sessions[this.props.indexSessio]) !== "undefined"
                                                                && typeof (this.props.rutina.sessions[this.props.indexSessio].datesSessio) !== "undefined"
                                                                && this.props.rutina.sessions[this.props.indexSessio].datesSessio.length > 0
                                                                ? "multiple"
                                                                : typeof (this.props.rutina.sessions[this.props.indexSessio]) !== "undefined"
                                                                    && typeof (this.props.rutina.sessions[this.props.indexSessio].dataSessio) !== "undefined"
                                                                    ? "simple"
                                                                    : "error"
                                                        ;
                                                })()
                                            }

                                            <QuadreSessio
                                                inputMode={dateType}
                                                sessio={this.state.vValue}
                                                impOpcionsVisibles
                                                impNomSessio={this.state.impNomSessio}
                                                impObsSessio={this.state.impObsSessio}
                                                impImatgesExs={this.state.impImatgesExs}
                                                impDescExs={this.state.impDescExs}
                                                impEscalf={this.state.impEscalf}
                                                impTorCalma={this.state.impTorCalma}
                                                impTABULAR={this.state.impTABULAR}
                                                //chgOptsImp={this.props.chgOptsImp}
                                                //            index={0}
                                                dateType={dateType}

                                                {...this.props}
                                            />
                                        </div>
                                    );
                                }
                            }

                            class SessioAMostrar extends Component {
                                constructor(props) {
                                    super(props);

                                    this.state = {
                                        valorModificat: props.valor,
                                        editantSessio: props.editantSessio
                                    };
                                }

                                render() {
                                    let
                                        dateType
                                        ;

                                    return (
                                        <div>
                                            {
                                                this.props.valor.map(
                                                    (v, i, a) => {
                                                        console.dir("SessioIndividualV: ", v);
                                                        return (
                                                            <div
                                                                key={i}
                                                            >
                                                                <SessioIndividual
                                                                    index={i}
                                                                    indexSessio={indexSessio}
                                                                    value={v}

                                                                    {...this.props}
                                                                />
                                                            </div>
                                                        )
                                                    }
                                                )
                                            }
                                        </div>
                                    );
                                }
                            }

                            //    console.dir("SessionsAMostrar: ", sessionsAMostrar);

                            if (sessionsAMostrar.filter(
                                (v, i, a) => typeof (v) !== "undefined"
                            ).length > 0) {
                                // console.log("sessionsAMostrar.length > 0!!!!");
                                // console.dir("SESSIONSAMOSTRAR: ", sessionsAMostrar);
                                return (
                                    <SessioAMostrar
                                        valor={sessionsAMostrar}
                                        editantSessio={this.state.editantSessio}
                                        //   chgOptsImp={this.chgOptsImp}

                                        {...this.props}
                                    />
                                );
                            } else {
                                return (
                                    <div
                                        ref={dv => this.novaSessio = dv}
                                        style={{
                                            display: `block`
                                        }}
                                    >
                                        <div>Opcions d'impressió per defecte de la sessió:
                                    </div>
                                        <div
                                            style={{
                                                display: `grid`,
                                                gridTemplateAreas: `
                                                "sessioNom  tabular  sessioObservacions"
                                                "imatgesExs   tabular   descripcioExs"
                                                "sessioEscalf   tabular    sessioTorCalma"
                                            `
                                            }}
                                        >

                                            <div
                                                style={{
                                                    gridArea: `tabular`,
                                                    alignSelf: `center`
                                                }}
                                            >
                                                <label htmlFor="chkTabular">
                                                    TABULAR
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkTabular"
                                                    checked={this.state.impTABULAR}
                                                    onChange={() => {
                                                        this.setState({
                                                            impTABULAR: !this.state.impTABULAR
                                                        });
                                                    }}
                                                />
                                            </div>

                                            <div
                                                style={{
                                                    gridArea: `sessioNom`
                                                }}
                                            >
                                                <label htmlFor="chkSessioNom">
                                                    Nom de la sessió
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkSessioNom"
                                                    checked={this.state.impNomSessio}
                                                    onChange={() => {
                                                        this.setState({
                                                            impNomSessio: !this.state.impNomSessio
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioObservacions`
                                                }}
                                            >
                                                <label htmlFor="chkSessioObservacions">
                                                    Observacions de la sessió
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkSessioObservacions"
                                                    checked={this.state.impObsSessio}
                                                    onChange={() => {
                                                        this.setState({
                                                            impObsSessio: !this.state.impObsSessio
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `imatgesExs`
                                                }}
                                            >
                                                <label htmlFor="chkImatgesExs">
                                                    Imatges dels exercicis
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkImatgesExs"
                                                    checked={this.state.impImatgesExs}
                                                    onChange={() => {
                                                        this.setState({
                                                            impImatgesExs: !this.state.impImatgesExs
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `descripcioExs`
                                                }}
                                            >
                                                <label htmlFor="chkDescripcioExs">
                                                    Descripcions dels exercicis
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkDescripcioExs"
                                                    checked={this.state.impDescExs}
                                                    onChange={() => {
                                                        this.setState({
                                                            impDescExs: !this.state.impDescExs
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioEscalf`
                                                }}
                                            >
                                                <label htmlFor="chkSessioEscalf">
                                                    Escalfament
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkSessioEscalf"
                                                    checked={this.state.impEscalf}
                                                    onChange={() => {
                                                        this.setState({
                                                            impEscalf: !this.state.impEscalf
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioTorCalma`
                                                }}
                                            >
                                                <label htmlFor="chkTorCalma">
                                                    Tornada a la calma
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkTorCalma"
                                                    checked={this.state.impTorCalma}
                                                    onChange={() => {
                                                        this.setState({
                                                            impTorCalma: !this.state.impTorCalma
                                                        });
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div
                                            style={{
                                                display: `grid`,
                                                gridTemplateAreas: `
                                                "imatges"
                                                "nom"
                                                ""
                                            `
                                            }}
                                        >
                                        </div>

                                        <label
                                            htmlFor="inNomSessio"
                                        >Nom de la sessió (per reutilitzar-la):
                                        </label>
                                        <input
                                            id="inNomSessio"
                                            type="text"
                                            ref={inp => this.nomSessio = inp}
                                            onChange={ev => this.setState({
                                                nomSessio: ev.target.value
                                            })}
                                        />

                                        <label
                                            htmlFor="selGMs"
                                            style={{
                                                display: `block`
                                            }}
                                        >Grups Musculars:
                                        </label>
                                        <div
                                            style={{
                                                zIndex: 200,
                                                position: `relative`
                                            }}
                                        >
                                            <Select
                                                id="selGMs"
                                                options={this.props.grups_musculars.map(
                                                    (v, i, a) => ({
                                                        label: v.grupMuscularNom,
                                                        value: v._id
                                                    })
                                                )}
                                                multi
                                                value={this.state.selectGMsValues}
                                                onChange={this.selectGMsUpdateValue}
                                            />
                                        </div>

                                        {
                                            //     <label
                                            //         htmlFor="chkFotos"
                                            //     >Inclou imatges:
                                            //     </label>
                                            //     <input
                                            //         id="chkFotos"
                                            //         type="checkbox"
                                            //         checked={this.state.chkFotos}
                                            //         onChange={v => this.setState({chkFotos: v.target.checked})}
                                            //         style={{
                                            //             display: `inline`,
                                            //             width: `auto`
                                            //         }}
                                            //     />
                                        }

                                        <label
                                            htmlFor="taEscalfament"
                                            style={{
                                                display: `block`
                                            }}
                                        >Escalfament:
                                        </label>
                                        <textarea
                                            id="taEscalfament"
                                            ref={ta => this.escalfament = ta}
                                            onChange={() => this.setState({ taEscalfament: this.escalfament.value })}
                                            style={{
                                                // display: `inline`,
                                                // width: `auto`
                                            }}
                                        />

                                        <label
                                            htmlFor="selExercicis"
                                        >Exercicis:
                                        </label>
                                        <div
                                            style={{
                                                zIndex: 100,
                                                position: `relative`
                                            }}
                                        >
                                            <Select
                                                id="selExercicis"
                                                options={
                                                    //(() => {
                                                    // let gms = new Set();
                                                    //
                                                    // this.state.selectGMsValue.forEach(
                                                    //     (v,i,a) => {
                                                    //         gms.add(v.label);
                                                    //     }
                                                    // );

                                                    this.props.exercicis.reduce(
                                                        (t, v, i, a) => {

                                                            let gms = new Set();
                                                            //
                                                            this.state.selectGMsValue.forEach(
                                                                (v, i, a) => {
                                                                    gms.add(v.label);
                                                                }
                                                            );

                                                            if (
                                                                gms.has(v.exerciciGrupMuscular)
                                                            ) {
                                                                t.push({
                                                                    label: v.exerciciNom,
                                                                    value: v._id
                                                                });
                                                            }

                                                            return t;
                                                        },
                                                        []
                                                    )
                                                    //    })()
                                                }

                                                multi
                                                value={this.state.selectExercicisValue}
                                                onChange={(selectExercicisValue) => {
                                                    this.setState({
                                                        selectExercicisValue
                                                    });
                                                    //   this.updateExercicisIGMsSelects();
                                                }}
                                            />
                                        </div>

                                        <label
                                            htmlFor="taCalma"
                                        >Tornada a la calma:
                                        </label>
                                        <textarea
                                            id="taCalma"
                                            ref={ta => this.calma = ta}
                                            onChange={() => this.setState({ taCalma: this.calma.value })}
                                            style={{
                                                // display: `inline`,
                                                // width: `auto`
                                            }}
                                        />

                                        <label
                                            htmlFor="taObservacionsSessio"
                                        >Observacions de la sessió:
                                        </label>
                                        <textarea
                                            id="taObservacionsSessio"
                                            ref={ta => this.obsSessio = ta}
                                            onChange={() => this.setState({ taObsSessio: this.obsSessio.value })}
                                            style={{
                                                // display: `inline`,
                                                // width: `auto`
                                            }}
                                        />
                                    </div>
                                );
                            }

                        })()
                    }

                    <button
                        onClick={() => {
                            let
                                datesSessio = resultat.map(
                                    (v, i, a) => v.diaData
                                )
                            ;

                            this.nomSessio.value = this.nomSessio.value || randomString(7);


                            if (this.state.selectExercicisValues.length > 0) {

                                if (!this.props.rutinaCreada) {

                                    Meteor.apply(
                                        'rutines.insert',
                                        [
                                            this.props.rutina.nomRutina || randomString(7),
                                            this.props.rutina.client,
                                            this.props.rutina.dataIni,
                                            this.props.rutina.dataFi,
                                            this.props.rutina.entrenador,
                                            this.props.rutina.observacions,
                                            [
                                                {
                                                    nom: this.nomSessio.value,
                                                    createdAt: new Date(),
                                                    user: Meteor.userId(),
                                                    dataSessio: typeof (this.props.objDia) !== "undefined" && this.props.objDia !== null ? this.props.objDia.diaData : null,
                                                    datesSessio,
                                                    periodeSessio: this.state.periodeSessio,
                                                    incImatges: this.state.chkFotos,
                                                    gms: this.state.selectGMsValue,
                                                    escalfament: this.escalfament.value,
                                                    exercicis: this.state.selectExercicisValues,
                                                    calma: this.calma.value,
                                                    observacionsSessio: this.state.taObsSessio,
                                                    opcionsImpressio: {
                                                        impNomSessio: this.state.impNomSessio,
                                                        impObsSessio: this.state.impObsSessio,
                                                        impImatgesExs: this.state.impImatgesExs,
                                                        impDescExs: this.state.impDescExs,
                                                        impEscalf: this.state.impEscalf,
                                                        impTorCalma: this.state.impTorCalma,
                                                        impTABULAR: this.state.impTABULAR
                                                    }
                                                }
                                            ]
                                        ],
                                        {
                                            impNomRutina: this.props.rutina.impNomRutina,
                                            impNomClient: this.props.rutina.impNomClient,
                                            impPeriode: this.props.rutina.impPeriode,
                                            impObsGen: this.props.rutina.impObsGen
                                        }
                                        // ,
                                        // {
                                        //     onResultReceived: (error, result) => {
                                        //         rutinaId = result;
                                        //         console.dir("rutinaId: ", rutinaId);
                                        //     }
                                        // }
                                        ,
                                        (error, result) => {
                                            rutinaId = result;
                                            console.dir("rutinaId: ", rutinaId);
                                        }
                                    );

                                    this.props.setRutinaCreada(true);
                                    alert(`S\'ha creat la sessió: \"${this.nomSessio.value}\"`);
                                    //this.props.tancaModal();
                                    this.setState({
                                        showModal: false
                                    });

                                    FlowRouter.go(`/rutina/${rutinaId}`);

                                } else {
                                    // this.nomSessio.value = this.nomSessio.value || randomString(7);

                                    Meteor.call(
                                        'rutines.afegeixSessio',
                                        this.props.rutinaId,
                                        {
                                            nom: this.nomSessio.value,
                                            createdAt: new Date(),
                                            user: Meteor.userId(),
                                            dataSessio: this.props.objDia !== null ? this.props.objDia.diaData : null,
                                            datesSessio,
                                            periodeSessio: this.state.periodeSessio,
                                            incImatges: this.state.chkFotos,
                                            gms: this.state.selectGMsValue,
                                            escalfament: this.escalfament.value,
                                            exercicis: this.state.selectExercicisValues,
                                            calma: this.calma.value,
                                            observacionsSessio: this.state.taObsSessio,
                                            opcionsImpressio: {
                                                impNomSessio: this.state.impNomSessio,
                                                impObsSessio: this.state.impObsSessio,
                                                impImatgesExs: this.state.impImatgesExs,
                                                impDescExs: this.state.impDescExs,
                                                impEscalf: this.state.impEscalf,
                                                impTorCalma: this.state.impTorCalma,
                                                impTABULAR: this.state.impTABULAR
                                            }
                                        }
                                    );

                                    alert(`S\'ha afegit la sessió: \"${this.nomSessio.value}\" a la rutina`);

                                    this.handleCloseModal();

                                   // this.props.tancaModal();
                                    // this.setState({
                                    //     showModal: false
                                    // });
                                    // FlowRouter.go(`/rutina/${this.props.rutinaId}`);
                                }
                            } else {
                                alert(`Per afegir la sessió: \"${this.nomSessio.value}\" a la rutina, selecciona algun exercici.`);
                            }
                        }}
                        style={{
                            display: this.state.enSessio && !this.state.editantSessio ? `block` : `none`,
                            textAlign: `center`
                        }}
                    >Estableix la sessió
                    </button>

                    <Modal
                        shouldCloseOnOverlayClick
                        isOpen={this.state.showConegudes}
                        contentLabel="Modal"
                        ariaHideApp
                    >
                        <button
                            onClick={() => {
                                this.setState({
                                    showConegudes: false
                                });
                            }}
                        >&times;
                        </button>
                        <h1>CONEGUDES</h1>

                        {
                            (() => {
                                console.dir("THISTRUINDEXSESSIO PA LA TAULASESSIONS -CONEGUDA-: ", this.truIndexSessio);
                            })()
                        }
                        <TaulaSessionsWithData
                            sesIndex={this.truIndexSessio}
                            tancaConegudes={this.tancaSessionsConegudes}

                            periodeSessio={this.state.periodeSessio}
                            selectedDates={this.resultat}

                            dateType={dateType}
                            {...this.props}
                        />

                    </Modal>
                </div>
            );
            /*FINAL DEL BLOC ON LA SESSIÓ ÉS CONEGUDA*/
        } else {
            /*INICI DEL BLOC ON LA SESSIÓ HA DE SER CREADA*/

            //return <h1>A   // console.dir("resultat: ", resultat);

            // console.dir("MOMcí alguna cosa.</h1>

            //sessionsAMostrar.push(this.props.rutina.sessions[this.props.rutina.sessions.length-1]);

            // console.dir("SESSIONSAMOSTRAR: ", sessionsAMostrar);

            // ///////////////////////////////////////////////////////////////////////////--------------------

            // console.dir("DATASESSIOPROP: ", dataSessioProp);
            // console.dir("DATADIA: ", dataDia);
            // console.dir("MOMENTDIA: ", momentDia);


            if (this.props.selectedDates !== null && (typeof (this.props.selectedDates) === "undefined" || this.props.selectedDates.length === 0)) {
                arrSelectDates = [];
            } else {
                // console.dir("notUndefined: ", this.props.selectedDates);
                arrSelectDates =
                    this.props.selectedDates === null || typeof (this.props.selectedDates) === "undefined"
                        ? []
                        : Object.values(this.props.selectedDates.get())
                    ;
            }

            // //        console.dir("arrSelDats_selectedDates: ", this.props.selectedDates.get() );
            // console.dir("arrSelDats: ", arrSelectDates);

            // console.dir("momentDia: ", momentDia);
            // console.dir("dataDia: ", dataDia);
            // console.dir("dataSessioProp: ", dataSessioProp);
            // console.dir("this.props.objDia: ", this.props.objDia);
            // console.dir("typeof(this.props.objDia): ", typeof (this.props.objDia));

            // console.dir("this.state.objPeriodeSessio: ", this.state.objPeriodeSessio);


            aux = arrSelectDates.map(
                (v, i, a) => v.filter(
                    (w, j, b) => w.checked === true
                )
            );

            // console.dir("AUX: ", aux);

            resultat = aux.map(
                (v, i, a) => {
                    if (v.length > 0) {
                        return v;
                    }
                }
            )

            resultat = flatMap(resultat).filter(
                (v, i, a) => typeof (v) !== "undefined"
            );

            //console.dir("arrSelectDates: ", arrSelectDates);

            // console.dir("resultat: ", resultat);

            // console.dir("MOMENTDIA: ", momentDia);

            //     console.dir("SESSIODATES: ", this.props.selectedDates);

            this.dataDia = momentDia;

            return (
                <div>
                    {
                        (() => {

                            if (typeof (this.state.periodeSessio) !== "undefined"
                                && this.state.periodeSessio !== null
                                && Object.keys({ ...this.state.periodeSessio }).length > 0
                            ) {
                                return <h1>{`${capitalize(moment(this.state.periodeSessio.start).format("dddd, L"))}→${capitalize(moment(this.state.periodeSessio.end).format("dddd, L"))}`}</h1>;
                            } else {
                                if (this.state.selectedDates !== null && resultat.length > 0
                                    //    && typeof(this.state.selectedDates.get) === "function" ? this.state.selectedDates.get() !== null
                                ) {
                                    //console.dir("arrSelectDates: ", this.state.selectedDates.get());
                                    return resultat.map(
                                        (v, i, a) => <h2 key={i}>{capitalize(moment(v.diaData).format("dddd L"))}</h2>
                                    );
                                } else {
                                    return <h1>{capitalize(momentDia.format("dddd L"))}</h1>;
                                }
                            }

                            /*   switch (dateType) {
  
                                  case "periode": {
                                       return <h1>{`${capitalize(moment(this.props.rutina.sessions[indexSessio].periodeSessio.start).format("dddd, L"))}→${capitalize(moment(this.props.rutina.sessions[indexSessio].periodeSessio.end).format("dddd, L"))}`}</h1>;
                                      //alert("PERIODE");
                                      break;
                                  }
  
                                  case "multiple": {
                                      return this.props.rutina.sessions[indexSessio].datesSessio.map(
                                          (v,i,a) => <h2 key={i}>{capitalize(moment(v).format("dddd L"))}</h2>
                                      );
                                      //alert("multiple");
                                      break;
                                  }
  
                                  case "simple": {
                                       return <h1>{capitalize(momentDia.format("dddd L"))}</h1>;
                                      //alert("SIMPLE");
                                      break;
                                  }
  
                                  default: {
                                //      alert("ERRORRR AMB DATETYPE!!!!");
                                      break;
                                  }
                              } */

                            // resultat.length > 0
                            // ?   resultat.map(
                            //         (v,i,a) => <h2 key={i}>{capitalize(moment(v.diaData).format("dddd L"))}</h2>
                            //     )
                            // :   typeof(this.props.objDia) !== "undefined" && this.props.objDia !== null
                            //     ?   <h1>{capitalize(momentDia.format("dddd L"))}</h1>
                            //     :   <h1>{`${capitalize(moment(this.props.periodeSessio.start).format("dddd, L"))}→${capitalize(moment(this.props.periodeSessio.end).format("dddd, L"))}`}</h1>
                        })()
                    }


                    <button
                        style={{
                            background: `rgba(255,0,255, .5)`
                        }}
                        onClick={() => {
                            // let
                            //     rutinaAbans = this.props.rutina,
                            //     sessionsAbans = this.props.rutina.sessions,
                            //     sessioActual =
                            //         this.props.rutina.sessions
                            //         ?   this.props.rutina.sessions[this.truIndexSessio]
                            //         :   [],
                            //     sessionsSenseActual =
                            //         this.props.rutina.sessions
                            //         ?  (() => {
                            //                 sessionsAbans.splice(this.truIndexSessio, 1);
                            //                 return sessionsAbans;
                            //             })()

                            //         // this.props.rutina.sessions.filter(
                            //         //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                            //         //     )
                            //         :   [],
                            //     rutinaDespres = {...rutinaAbans}
                            // ;


                            // rutinaDespres.sessions = sessionsSenseActual;

                            this.setState({
                                showConegudes: !this.state.showConegudes,
                                tancaConegudes: this.tancaSessionsConegudes,
                                sesIndex: this.props.index || null
                            });

                            // if (confirm(`Vas a guardar una nova sessió amb el nom "${sessioActual[0].nom}". Confirmes l'operació?`)) {
                            //                         // Meteor.call(
                            //                         //     'rutines.update',
                            //                         //     this.props.rutinaId,
                            //                         //     rutinaDespres
                            //                         // );
                            //     console.dir("sessioActual: ", sessioActual);
                            //
                            //     // Meteor.call(
                            //     //     'sessions.insert',
                            //     //     ...sessioActual
                            //     //     // nom,
                            //     //     // data,
                            //     //     // incImatges,
                            //     //     // gms,
                            //     //     // escalfament,
                            //     //     // exercicis,
                            //     //     // calma,
                            //     //     // observacionsSessio,
                            //     //     // opcionsImpressio
                            //     // );
                            // }
                        }}
                    >Sessions conegudes
                    </button>

                    <button
                        style={{
                            background: `aqua`,
                            display: this.props.editantSessio ? `inline-block` : `none`
                        }}
                        onClick={() => {
                            let
                                rutinaAbans = this.props.rutina,
                                sessionsAbans = this.props.rutina.sessions,
                                sessioActual =
                                    this.props.rutina.sessions
                                        ? this.props.rutina.sessions[this.truIndexSessio]
                                        : [],
                                sessionsSenseActual =
                                    this.props.rutina.sessions
                                        ? (() => {
                                            sessionsAbans.splice(this.truIndexSessio, 1);
                                            return sessionsAbans;
                                        })()

                                        // this.props.rutina.sessions.filter(
                                        //         (v,i,a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                                        //     )
                                        : [],
                                rutinaDespres = { ...rutinaAbans }
                            ;

                            rutinaDespres.sessions = sessionsSenseActual;


                            if (confirm(`Vas a guardar una nova sessió amb el nom "${sessioActual[0].nom}". Confirmes l'operació?`)) {
                                // Meteor.call(
                                //     'rutines.update',
                                //     this.props.rutinaId,
                                //     rutinaDespres
                                // );
                                console.dir("sessioActual: ", sessioActual);

                                Meteor.call(
                                    'sessions.insert',

                                    sessioActual.nom,
                                    sessioActual.dataSessio,
                                    sessioActual.incImatges,
                                    sessioActual.gms,
                                    sessioActual.escalfament,
                                    sessioActual.exercicis,
                                    sessioActual.calma,
                                    sessioActual.observacionsSessio,
                                    sessioActual.opcionsImpressio
                                );
                            }
                        }}
                    >Guarda a la taula
                    </button>


                    <button
                        style={{
                            background: `red`,
                            display: this.props.editantSessio ? `inline-block` : `none`
                        }}
                        onClick={() => {
                            let
                                rutinaAbans = this.props.rutina,
                                sessionsAbans = this.props.rutina.sessions,
                                sessioActual =
                                    this.props.rutina.sessions
                                        ? this.props.rutina.sessions.filter(
                                            (v, i, a) => v.dataSessio.getTime() === this.props.objDia.diaData.getTime()
                                        )
                                        : [],
                                sessionsSenseActual =
                                    this.props.rutina.sessions
                                        ? this.props.rutina.sessions.filter(
                                            (v, i, a) => v.dataSessio.getTime() !== this.props.objDia.diaData.getTime()
                                        )
                                        : [],
                                rutinaDespres = { ...rutinaAbans }
                                ;

                            rutinaDespres.sessions = sessionsSenseActual;


                            if (confirm(`Vas a esborrar la sessió "${sessioActual[0].nom}". Confirmes l'operació?`)) {
                                Meteor.call(
                                    'rutines.update',
                                    this.props.rutinaId,
                                    rutinaDespres
                                );
                                //console.dir("rutinaDespres: ", rutinaDespres);
                            }
                        }}
                    >Elimina la sessió
                    </button>

                    {
                        (() => {
                            class SessioIndividual extends Component {
                                constructor(props) {
                                    super(props);

                                    console.dir("propsVALUE ", props.value);

                                    this.state = {
                                        vValue: props.value,
                                        selectGMsValue: props.value.gms,
                                        impOpcionsVisibles: true,
                                        impNomSessio: props.value.opcionsImpressio.impNomSessio,
                                        impObsSessio: props.value.opcionsImpressio.impObsSessio,
                                        impImatgesExs: props.value.opcionsImpressio.impImatgesExs,
                                        impDescExs: props.value.opcionsImpressio.impDescExs,
                                        impEscalf: props.value.opcionsImpressio.impEscalf,
                                        impTorCalma: props.value.opcionsImpressio.impTorCalma,
                                        impTABULAR: props.value.opcionsImpressio.impTABULAR
                                    };
                                }

                                selectGMsUpdateValue = (selectGMsValues) => {
                                    let
                                        rutinaAnterior = this.props.rutina,
                                        rutinaModificada = { ...rutinaAnterior }
                                        ;

                                    rutinaModificada.sessions[this.props.indexSessio].gms = selectGMsValue;

                                    this.setState({ selectGMsValues });

                                    Meteor.call(
                                        'rutines.update',
                                        this.props.rutinaId,
                                        rutinaModificada
                                    );
                                }

                                opcionsDImpressioEditades = (
                                    impNomSessio,
                                    impObsSessio,
                                    impImatgesExs,
                                    impDescExs,
                                    impEscalf,
                                    impTorCalma,
                                    impTABULAR
                                ) => {
                                    this.setState({
                                        impNomSessio,
                                        impObsSessio,
                                        impImatgesExs,
                                        impDescExs,
                                        impEscalf,
                                        impTorCalma,
                                        impTABULAR
                                    });
                                };

                                render() {

                                    let
                                        dateType
                                        ;

                                    return (
                                        <div>
                                            <label
                                                htmlFor="selGMs"
                                                style={{
                                                    display: `block`
                                                }}
                                            >Grups Musculars:
                                        </label>
                                            <div
                                                style={{
                                                    zIndex: 200,
                                                    position: `relative`
                                                }}
                                            >

                                                <Select
                                                    id="selGMs"
                                                    options={this.props.grups_musculars.map(
                                                        (v, i, a) => ({
                                                            label: v.grupMuscularNom,
                                                            value: v._id
                                                        })
                                                    )}
                                                    multi
                                                    value={this.state.selectGMsValues}
                                                    onChange={this.selectGMsUpdateValue}
                                                />
                                            </div>

                                            <label
                                                htmlFor="selExercicis"
                                            >Exercicis:
                                        </label>
                                            <div
                                                style={{
                                                    zIndex: 100,
                                                    position: `relative`
                                                }}
                                            >
                                                <Select
                                                    id="selExercicis"
                                                    options={
                                                        //(() => {
                                                        // let gms = new Set();
                                                        //
                                                        // this.state.selectGMsValue.forEach(
                                                        //     (v,i,a) => {
                                                        //         gms.add(v.label);
                                                        //     }
                                                        // );

                                                        this.props.exercicis.reduce(
                                                            (t, v, i, a) => {

                                                                let gms = new Set();
                                                                //
                                                                this.state.selectGMsValue.forEach(
                                                                    (v, i, a) => {
                                                                        gms.add(v.label);
                                                                    }
                                                                );

                                                                if (
                                                                    gms.has(v.exerciciGrupMuscular)
                                                                ) {
                                                                    t.push({
                                                                        label: v.exerciciNom,
                                                                        value: v._id
                                                                    });
                                                                }

                                                                return t;
                                                            },
                                                            []
                                                        )
                                                        //    })()
                                                    }

                                                    multi
                                                    value={this.state.vValue.exercicis}
                                                    onChange={(selectExercicisValues) => {
                                                        let
                                                            vValueModificat = { ...this.state.vValue },
                                                            rutinaModificada = { ...this.props.rutina }
                                                            ;

                                                        vValueModificat.exercicis = selectExercicisValues;
                                                        this.setState({
                                                            vValue: vValueModificat
                                                        });

                                                        rutinaModificada.sessions[this.props.indexSessio].exercicis = selectExercicisValues;

                                                        //       this.updateExercicisIGMsSelects();
                                                        Meteor.call(
                                                            'rutines.update',
                                                            this.props.rutinaId,
                                                            rutinaModificada
                                                        );
                                                    }}
                                                />
                                            </div>

                                            {
                                                (() => {
                                                    console.log("Pos ací anava el DaType...");
                                                    // console.dir("DATYPE: ", typeof(this.props.rutina[this.props.indexSessio]) !== "undefined"
                                                    //     && typeof(this.props.rutina[this.props.indexSessio].periodeSessio) !== "undefined"
                                                    // ?   "periode"
                                                    // :   typeof(this.props.rutina[this.props.indexSessio]) !== "undefined"
                                                    //         && typeof(this.props.rutina[this.props.indexSessio].datesSessio) !== "undefined"
                                                    //         && this.props.rutina[this.props.indexSessio].datesSessio.length > 0
                                                    //     ?   "multiple"
                                                    //     :   typeof(this.props.rutina[this.props.indexSessio]) !== "undefined"
                                                    //             && typeof(this.props.rutina[this.props.indexSessio].dataSessio) !== "undefined"
                                                    //         ?   "simple"
                                                    //         :   "error");
                                                    dateType =
                                                        typeof (this.props.rutina.sessions[this.props.indexSessio]) !== "undefined"
                                                            && typeof (this.props.rutina.sessions[this.props.indexSessio].periodeSessio) !== "undefined"
                                                            && this.props.rutina.sessions[this.props.indexSessio].periodeSessio !== null
                                                            && Object.keys({ ...this.props.rutina.sessions[this.props.indexSessio].periodeSessio }).length !== 0
                                                            ? "periode"
                                                            : typeof (this.props.rutina.sessions[this.props.indexSessio]) !== "undefined"
                                                                && typeof (this.props.rutina.sessions[this.props.indexSessio].datesSessio) !== "undefined"
                                                                && this.props.rutina.sessions[this.props.indexSessio].datesSessio.length > 0
                                                                ? "multiple"
                                                                : typeof (this.props.rutina.sessions[this.props.indexSessio]) !== "undefined"
                                                                    && typeof (this.props.rutina.sessions[this.props.indexSessio].dataSessio) !== "undefined"
                                                                    ? "simple"
                                                                    : "error"
                                                        ;
                                                })()
                                            }

                                            <QuadreSessio
                                                inputMode={dateType}
                                                sessio={this.state.vValue}
                                                impOpcionsVisibles
                                                // impNomSessio={this.state.impNomSessio}
                                                // impObsSessio={this.state.impObsSessio}
                                                // impImatgesExs={this.state.impImatgesExs}
                                                // impDescExs={this.state.impDescExs}
                                                // impEscalf={this.state.impEscalf}
                                                // impTorCalma={this.state.impTorCalma}
                                                //chgOptsImp={this.props.chgOptsImp}
                                                //            index={0}
                                                dateType={dateType}

                                                {...this.props}
                                            />
                                        </div>
                                    );
                                }
                            }

                            class SessioAMostrar extends Component {
                                constructor(props) {
                                    super(props);

                                    this.state = {
                                        valorModificat: props.valor,
                                        editantSessio: props.editantSessio
                                    };
                                }

                                render() {
                                    let
                                        dateType
                                        ;

                                    return (
                                        <div>
                                            {
                                                this.props.valor.map(
                                                    (v, i, a) => {
                                                        console.dir("SessioIndividualV: ", v);
                                                        return (
                                                            <div
                                                                key={i}
                                                            >
                                                                <SessioIndividual
                                                                    index={i}
                                                                    indexSessio={indexSessio}
                                                                    value={v}

                                                                    {...this.props}
                                                                />
                                                            </div>
                                                        )
                                                    }
                                                )
                                            }
                                        </div>
                                    );
                                }
                            }

                            //    console.dir("SessionsAMostrar: ", sessionsAMostrar);

                            if (sessionsAMostrar.filter(
                                (v, i, a) => typeof (v) !== "undefined"
                            ).length > 0) {
                                // console.log("sessionsAMostrar.length > 0!!!!");
                                // console.dir("SESSIONSAMOSTRAR: ", sessionsAMostrar);
                                return (
                                    <SessioAMostrar
                                        valor={sessionsAMostrar}
                                        editantSessio={this.state.editantSessio}
                                        //   chgOptsImp={this.chgOptsImp}

                                        {...this.props}
                                    />
                                );
                            } else {
                                return (
                                    <div
                                        ref={dv => this.novaSessio = dv}
                                        style={{
                                            display: `block`
                                        }}
                                    >
                                        <div>Opcions d'impressió per defecte de la sessió:
                                    </div>
                                        <div
                                            style={{
                                                display: `grid`,
                                                gridTemplateAreas: `
                                                "sessioNom     tabular  sessioObservacions"
                                                "imatgesExs    tabular  descripcioExs"
                                                "sessioEscalf  tabular  sessioTorCalma"
                                            `
                                            }}
                                        >
                                            <div
                                                style={{
                                                    gridArea: `tabular`,
                                                    alignSelf: `center`
                                                }}
                                            >
                                                <label htmlFor="chkTabular">
                                                    TABULAR
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkTabular"
                                                    checked={this.state.impTABULAR}
                                                    onChange={() => {
                                                        this.setState({
                                                            impTABULAR: !this.state.impTABULAR
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioNom`
                                                }}
                                            >
                                                <label htmlFor="chkSessioNom">
                                                    Nom de la sessió
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkSessioNom"
                                                    checked={this.state.impNomSessio}
                                                    onChange={() => {
                                                        this.setState({
                                                            impNomSessio: !this.state.impNomSessio
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioObservacions`
                                                }}
                                            >
                                                <label htmlFor="chkSessioObservacions">
                                                    Observacions de la sessió
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkSessioObservacions"
                                                    checked={this.state.impObsSessio}
                                                    onChange={() => {
                                                        this.setState({
                                                            impObsSessio: !this.state.impObsSessio
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `imatgesExs`
                                                }}
                                            >
                                                <label htmlFor="chkImatgesExs">
                                                    Imatges dels exercicis
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkImatgesExs"
                                                    checked={this.state.impImatgesExs}
                                                    onChange={() => {
                                                        this.setState({
                                                            impImatgesExs: !this.state.impImatgesExs
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `descripcioExs`
                                                }}
                                            >
                                                <label htmlFor="chkDescripcioExs">
                                                    Descripcions dels exercicis
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkDescripcioExs"
                                                    checked={this.state.impDescExs}
                                                    onChange={() => {
                                                        this.setState({
                                                            impDescExs: !this.state.impDescExs
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioEscalf`
                                                }}
                                            >
                                                <label htmlFor="chkSessioEscalf">
                                                    Escalfament
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkSessioEscalf"
                                                    checked={this.state.impEscalf}
                                                    onChange={() => {
                                                        this.setState({
                                                            impEscalf: !this.state.impEscalf
                                                        });
                                                    }}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    gridArea: `sessioTorCalma`
                                                }}
                                            >
                                                <label htmlFor="chkTorCalma">
                                                    Tornada a la calma
                                            </label>
                                                <input
                                                    type="checkbox"
                                                    id="chkTorCalma"
                                                    checked={this.state.impTorCalma}
                                                    onChange={() => {
                                                        this.setState({
                                                            impTorCalma: !this.state.impTorCalma
                                                        });
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div
                                            style={{
                                                display: `grid`,
                                                gridTemplateAreas: `
                                                "imatges"
                                                "nom"
                                                ""
                                            `
                                            }}
                                        >
                                        </div>

                                        <SelectExercicisJEstyle
                                            updateExercicisIGMsSelects={this.updateExercicisIGMsSelects}
                                            {...this.props}
                                        />

                                        <label
                                            htmlFor="inNomSessio"
                                        >Nom de la sessió (per reutilitzar-la):
                                    </label>
                                        <input
                                            id="inNomSessio"
                                            type="text"
                                            ref={inp => this.nomSessio = inp}
                                            onChange={ev => this.setState({
                                                nomSessio: ev.target.value
                                            })}
                                        />

                                        <label
                                            htmlFor="selGMs"
                                            style={{
                                                display: `block`
                                            }}
                                        >Grups Musculars:
                                    </label>
                                        <div
                                            style={{
                                                zIndex: 200,
                                                position: `relative`
                                            }}
                                        >
                                            <Select
                                                id="selGMs"
                                                options={this.props.grups_musculars.map(
                                                    (v, i, a) => ({
                                                        label: v.grupMuscularNom,
                                                        value: v._id
                                                    })
                                                )}
                                                multi
                                                value={this.state.selectGMsValues}
                                                onChange={this.selectGMsUpdateValues}
                                            />
                                        </div>

                                        {
                                            //     <label
                                            //         htmlFor="chkFotos"
                                            //     >Inclou imatges:
                                            //     </label>
                                            //     <input
                                            //         id="chkFotos"
                                            //         type="checkbox"
                                            //         checked={this.state.chkFotos}
                                            //         onChange={v => this.setState({chkFotos: v.target.checked})}
                                            //         style={{
                                            //             display: `inline`,
                                            //             width: `auto`
                                            //         }}
                                            //     />
                                        }

                                        <label
                                            htmlFor="taEscalfament"
                                            style={{
                                                display: `block`
                                            }}
                                        >Escalfament:
                                    </label>
                                        <textarea
                                            id="taEscalfament"
                                            ref={ta => this.escalfament = ta}
                                            onChange={() => this.setState({ taEscalfament: this.escalfament.value })}
                                            style={{
                                                // display: `inline`,
                                                // width: `auto`
                                            }}
                                        />

                                        <label
                                            htmlFor="selExercicis"
                                        >Exercicis:
                                    </label>
                                        <div
                                            style={{
                                                zIndex: 100,
                                                position: `relative`
                                            }}
                                        >
                                            <Select
                                                id="selExercicis"
                                                options={
                                                    //(() => {
                                                    // let gms = new Set();
                                                    //
                                                    // this.state.selectGMsValue.forEach(
                                                    //     (v,i,a) => {
                                                    //         gms.add(v.label);
                                                    //     }
                                                    // );

                                                    this.props.exercicis.reduce(
                                                        (t, v, i, a) => {

                                                            let gms = new Set();
                                                            //
                                                            this.state.selectGMsValues.forEach(
                                                                (v, i, a) => {
                                                                    gms.add(v.label);
                                                                }
                                                            );

                                                            if (
                                                                gms.has(v.exerciciGrupMuscular)
                                                            ) {
                                                                t.push({
                                                                    label: v.exerciciNom,
                                                                    value: v._id
                                                                });
                                                            }

                                                            return t;
                                                        },
                                                        []
                                                    )
                                                    //    })()
                                                }

                                                multi
                                                value={this.state.selectExercicisValues}
                                                onChange={(selectExercicisValues) => {

                                                    // this.updateExercicisIGMsSelects();
                                                    let grupsMuscularsTrobats = [];



                                                    selectExercicisValues.forEach(
                                                        exerciciDeLlista => {
                                                            let
                                                                exerciciTrobat = this.props.exercicis.find(
                                                                    exercici => exercici._id === exerciciDeLlista.value
                                                                ),

                                                                gmTrobat = this.props.grups_musculars.find(
                                                                    grupMuscular => grupMuscular.grupMuscularNom === exerciciTrobat.exerciciGrupMuscular
                                                                ),

                                                                gmAmbFormat = {
                                                                    label: gmTrobat.grupMuscularNom,
                                                                    value: gmTrobat._id
                                                                }
                                                                ;

                                                            if (grupsMuscularsTrobats.find(
                                                                grupMuscularTrobat => grupMuscularTrobat.value === gmAmbFormat.value
                                                            ) === undefined) {
                                                                grupsMuscularsTrobats.push(gmAmbFormat);
                                                            }
                                                        }
                                                    );

                                                    this.setState({
                                                        selectExercicisValues,
                                                        selectGMsValues: grupsMuscularsTrobats
                                                    });

                                                }}
                                            />
                                        </div>

                                        <label
                                            htmlFor="taCalma"
                                        >Tornada a la calma:
                                        </label>
                                        <textarea
                                            id="taCalma"
                                            ref={ta => this.calma = ta}
                                            onChange={() => this.setState({ taCalma: this.calma.value })}
                                            style={{
                                                // display: `inline`,
                                                // width: `auto`
                                            }}
                                        />

                                        <label
                                            htmlFor="taObservacionsSessio"
                                        >Observacions de la sessió:
                                        </label>
                                        <textarea
                                            id="taObservacionsSessio"
                                            ref={ta => this.obsSessio = ta}
                                            onChange={() => this.setState({ taObsSessio: this.obsSessio.value })}
                                            style={{
                                                // display: `inline`,
                                                // width: `auto`
                                            }}
                                        />
                                    </div>
                                );
                            }

                        })()
                    }

                    <button
                        onClick={() => {
                            let
                                datesSessio = resultat.map(
                                    (v, i, a) => v.diaData
                                )
                            ;

                            this.nomSessio.value = this.nomSessio.value || randomString(7);


                            if (this.state.selectExercicisValues.length > 0) {

                                if (!this.props.rutinaCreada) {


                                    Meteor.apply(
                                        'rutines.insert',
                                        [
                                            this.props.rutina.nomRutina || randomString(7),
                                            this.props.rutina.client,
                                            this.props.rutina.dataIni,
                                            this.props.rutina.dataFi,
                                            this.props.rutina.entrenador,
                                            this.props.rutina.observacions,
                                            [
                                                {
                                                    nom: this.nomSessio.value,
                                                    createdAt: new Date(),
                                                    user: Meteor.userId(),
                                                    dataSessio: typeof (this.props.objDia) !== "undefined" && this.props.objDia !== null ? this.props.objDia.diaData : null,
                                                    datesSessio,
                                                    periodeSessio: this.state.periodeSessio,
                                                    incImatges: this.state.chkFotos,
                                                    gms: this.state.selectGMsValues,
                                                    escalfament: this.escalfament.value,
                                                    exercicis: this.state.selectExercicisValues,
                                                    calma: this.calma.value,
                                                    observacionsSessio: this.state.taObsSessio,
                                                    opcionsImpressio: {
                                                        impNomSessio: this.state.impNomSessio,
                                                        impObsSessio: this.state.impObsSessio,
                                                        impImatgesExs: this.state.impImatgesExs,
                                                        impDescExs: this.state.impDescExs,
                                                        impEscalf: this.state.impEscalf,
                                                        impTorCalma: this.state.impTorCalma,
                                                        impTABULAR: this.state.impTABULAR
                                                    }
                                                }
                                            ]
                                        ],
                                        {
                                            impNomRutina: this.props.rutina.impNomRutina,
                                            impNomClient: this.props.rutina.impNomClient,
                                            impPeriode: this.props.rutina.impPeriode,
                                            impObsGen: this.props.rutina.impObsGen
                                        }
                                        // ,
                                        // {
                                        //     onResultReceived: (error, result) => {
                                        //         rutinaId = result;
                                        //         console.dir("rutinaId: ", rutinaId);
                                        //     }
                                        // }
                                        ,
                                        (error, result) => {
                                            rutinaId = result;
                                            console.dir("rutinaId: ", rutinaId);
                                        }
                                    );

                                    this.props.setRutinaCreada(true);
                                    alert(`S\'ha creat la sessió: \"${this.nomSessio.value}\"`);
                                    //this.props.tancaModal();
                                    this.setState({
                                        showModal: false
                                    });

                                    FlowRouter.go(`/rutina/${rutinaId}`);

                                } else {
                                    // this.nomSessio.value = this.nomSessio.value || randomString(7);

                             //       alert(`Afegim la sessio ${this.nomSessio.value} a la rutina ${this.props.rutinaId}.`);

                                    let 
                                        sessioAAfegir = {
                                            nom: this.nomSessio.value,
                                            createdAt: new Date(),
                                            user: Meteor.userId(),
                                            dataSessio: this.props.objDia !== null ? this.props.objDia.diaData : null,
                                            datesSessio,
                                            periodeSessio: this.state.periodeSessio,
                                            incImatges: this.state.chkFotos,
                                            gms: this.state.selectGMsValues,
                                            escalfament: this.escalfament.value,
                                            exercicis: this.state.selectExercicisValues,
                                            calma: this.calma.value,
                                            observacionsSessio: this.state.taObsSessio,
                                            opcionsImpressio: {
                                                impNomSessio: this.state.impNomSessio,
                                                impObsSessio: this.state.impObsSessio,
                                                impImatgesExs: this.state.impImatgesExs,
                                                impDescExs: this.state.impDescExs,
                                                impEscalf: this.state.impEscalf,
                                                impTorCalma: this.state.impTorCalma,
                                                impTABULAR: this.state.impTABULAR
                                            }
                                        }
                                    ;

                                    Meteor.call(
                                        'rutines.afegeixSessio',
                                        this.props.rutinaId,
                                        sessioAAfegir,
                                        (err, res) => {
                                    //        console.log(`Després d'afegeixSessio a la rutina: ${this.props.rutinaId}.\nErr: ${err}\nRes: ${res}`);
                                            //FlowRouter.go(`/rutina/${rutinaId}`);
                                        }
                                    );
                                    
                              //      alert(`S\'ha afegit la sessió: \"${this.nomSessio.value}\" a la rutina`);
                                    setTimeout(
                                        this.props.tancaModal,
                                        5000
                                    );
                                    
                                    // this.setState({
                                    //     showModal: false
                                    // });

                                    
                                  //  this.props.tancaModal();
                                }
                            } else {
                                alert(`Per afegir la sessió: \"${this.nomSessio.value}\" a la rutina, selecciona algun exercici.`);
                            }
                        }}
                        style={{
                            display: this.state.enSessio && !this.state.editantSessio ? `block` : `none`,
                            textAlign: `center`
                        }}
                    >Estableix la sessió
                    </button>

                    <Modal
                        shouldCloseOnOverlayClick
                        isOpen={this.state.showConegudes}
                        contentLabel="Modal"
                        ariaHideApp
                    >
                        <button
                            onClick={() => {
                                this.setState({
                                    showConegudes: false
                                });
                            }}
                        >&times;
                        </button>
                        <h1>CONEGUDES</h1>



                        <TaulaSessionsWithData
                            sesIndex={this.truIndexSessio}
                            tancaConegudes={this.tancaSessionsConegudes}

                            periodeSessio={this.state.periodeSessio}
                            selectedDates={this.resultat}

                            dateType={dateType}
                            {...this.props}
                        />

                    </Modal>
                </div>
            );
            /*FINAL DEL BLOC ON LA SESSIO HA DE SER CREADA*/
        }
    }
}

class TaulaSessions extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            rows: props.sessions,
            modalNovaSes: false
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            rows: nextProps.sessions
        })
    }

    rowGetter = (i) => {
        return this.state.rows[i];
    };

    handleGridSort = (sortColumn, sortDirection) => {
        const
            comparer = (a, b) => {
                if (sortDirection === 'ASC') {
                    return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
                } else if (sortDirection === 'DESC') {
                    return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
                }
            },
            rows = sortDirection === 'NONE' ? this.state.originalRows.slice(0) : this.state.rows.sort(comparer)
            ;

        this.setState({ rows });
    };

    handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
        let rows = this.state.rows.slice();

        for (let i = fromRow; i <= toRow; i++) {
            let rowToUpdate = rows[i];
            let updatedRow = update(rowToUpdate, { $merge: updated }); // <<<<<<<<<<<<<<< Meteor.method()
            rows[i] = updatedRow;
        }

        this.setState({ rows });
    };

    handleInsertButtonClick = (onClick) => {
        // Custom your onClick event here,
        // it's not necessary to implement this function if you have no any process before onClick
        console.log('This is my custom function for InserButton click event');
        onClick();
    };

    createCustomInsertButton = (onClick) => {
        return (
            <InsertButton
                btnText='Nou'
                className='my-custom-class'

                onClick={() => {
                    //this.handleInsertButtonClick(onClick)
                    this.props.toggleForm();
                }}
            />
        );
    };

    handleDeleteButtonClick = (onClick) => {
        // Custom your onClick event here,
        // it's not necessary to implement this function if you have no any process before onClick
        console.log('This is my custom function for DeleteButton click event');
        onClick();
    };

    createCustomDeleteButton = (onClick) => {
        return (
            <DeleteButton
                btnText='Elimina'
                className='my-custom-class'
                onClick={e => this.handleDeleteButtonClick(onClick)}
            />
        );
    };

    createCustomSearchField = (props) => {
        return (
            <SearchField
                className='my-custom-class'
                defaultValue={props.defaultSearch}
                placeholder="Busca sessions..."
            />
        );
    };

    beforeClose = (e) => {
        alert(`[Custom Event]: Before modal close event triggered!`);
    };

    handleModalClose = (closeModal) => {
        // Custom your onCloseModal event here,
        // it's not necessary to implement this function if you have no any process before modal close
        console.log('This is my custom function for modal close event');
        closeModal();
    };

    createCustomModalHeader = (onClose, onSave) => {
        const headerStyle = {
            fontWeight: 'bolder',
            fontSize: 'large',
            textAlign: 'center',
            backgroundColor: '#eeeeee'
        };
        return (
            <div className='modal-header' style={headerStyle}>
                <h3>Nova Rutina</h3>
                <button className='btn btn-info' onClick={onClose}>&times;</button>
            </div>
        );
    };

    createCustomModalFooter = (onClose, onSave) => {
        const style = {
            backgroundColor: '#ffffff'
        };
        return (
            <div className='modal-footer' style={style}>
                <button className='btn btn-xs ' onClick={onSave}>Guardar</button>
            </div>
        );
    };

    objecteIDs = (keyVal) => {
        console.log("keyVal: ", keyVal);
        console.dir("thisState: ", this.state);
        const
            idx = this.state.rows.findIndex((row) => row.clientiNomComplet === keyVal),
            oId = this.state.rows[idx]._id,
            uId = this.state.rows[idx].user
            ;
        //console.log("idx: ", idx);

        return {
            idx,
            _id: oId,
            user: uId
        };
    };

    onDeleteRow = (rows) => {
        //alert("Esborrant...");
        console.dir("rows: ", rows);

        rows.map(
            (v, i, a) => {
                let
                    obj = Sessions.findOne({ nom: v })
                    ;
                // let
                //     objSess = Sessions.findOne(
                //         {nom: v[0]}
                //     ),
                //     objID = objSess._id
                // ;
                // console.dir("objID: ", objID);
                // console.log("MUser: ", Meteor.user());
                Meteor.call('sessions.delete', obj._id);
                //        console.log("v: ", v);
                //    console.dir("obj: ", obj);

            }
        );
    };

    handleClose = () => this.setState({ modalNovaSes: false });

    imprimeixLlista = () => {
        //    PHE.printElement(document.querySelector(".olLlistaGMs"));
        //alert('Després de PHE');
        $(".ulRutineslista").printThis();
    };

    render() {
        let
            onAfterInsertRow = (row) => {
                let newRowStr = '';

                for (const prop in row) {
                    newRowStr += prop + ': ' + row[prop] + ' \n';
                }
                alert('The new row is:\n ' + newRowStr);
            },

            customConfirm = (next, dropRowKeys) => {
                const dropRowKeysStr = dropRowKeys.join('\n ⇝ ');
                if (
                    confirm(`Vas a eliminar per complet ${dropRowKeys.length} element${dropRowKeys.length > 1 ? "s" : ""}: \n ⇝ ${dropRowKeysStr} \n Estàs segur# de continuar?`)
                ) {
                    // If the confirmation is true, call the function that
                    // continues the deletion of the record.
                    next();
                }
            },

            afterDeleteRow = (rowKeys) => {
                console.dir("rowKeys: ", rowKeys);
            },

            afterSearch = (searchText, result) => {
                console.log('Your search text is ' + searchText);
                console.log('Result is:');
                for (let i = 0; i < result.length; i++) {
                    console.dir('Result: ', result[i]);
                }
            },

            sessioLinkFormatter = (cell, row) => (
                <a
                    href={`/sessio/${row._id}`}
                    target="_self"
                    style={{
                        color: `navy`,
                        fontVariant: `small-caps`,
                        fontSize: `1.2em`,
                        textShadow: `1px 1px 0 rgba(255,255,255,.5)`
                    }}
                    onClick={
                        (ev) => {
                            ev.preventDefault();
                            ev.stopPropagation();

                            let
                                rutinaAnterior = { ...this.props.rutina },
                                rutinaModificada = {},

                                arrSelectDates,
                                aux,
                                resultat
                            ;

                            if (typeof (this.props.sesIndex) === "undefined" || this.props.sesIndex === null) {
                                ///////////////// << SESSIÓ A CREAR
                                //               alert("Sessió a crear");
                                if (typeof this.props.periodeSessio !== "undefined") {
                                    //                   alert("PERIODE");

                                    Meteor.call(
                                        'rutines.afegeixSessio',
                                        this.props.rutinaId,
                                        {
                                            nom: randomString(7),
                                            createdAt: new Date(),
                                            user: Meteor.userId(),
                                            //  dataSessio: this.props.objDia ? this.props.objDia.diaData : "",
                                            periodeSessio: this.props.periodeSessio,
                                            incImatges: null,
                                            gms: row.gms,
                                            escalfament: row.escalfament,
                                            exercicis: row.exercicis,
                                            calma: row.calma,
                                            observacionsSessio: row.observacionsSessio,
                                            opcionsImpressio: row.opcionsImpressio
                                            // {
                                            //     impNomSessio: this.state.impNomSessio,
                                            //     impObsSessio: this.state.impObsSessio,
                                            //     impImatgesExs: this.state.impImatgesExs,
                                            //     impDescExs: this.state.impDescExs,
                                            //     impEscalf: this.state.impEscalf,
                                            //     impTorCalma: this.state.impTorCalma
                                            // }
                                        }
                                    );

                                    this.props.tancaConegudes();
                                    this.props.tancaModal();

                                } else {
                                    ////////////////////

                                    if (this.props.selectedDates !== null && (typeof (this.props.selectedDates) === "undefined" || typeof this.props.selectedDates.get !== "function" || this.props.selectedDates.get().length === 0)) {
                                        arrSelectDates = [];
                                    } else {
                                        console.dir("MULTICREA notUndefined: ", this.props.selectedDates !== null ? this.props.selectedDates.get() : "null");
                                        arrSelectDates =
                                            this.props.selectedDates === null || typeof (this.props.selectedDates) === "undefined"
                                                ? []
                                                : Object.values(this.props.selectedDates.get())
                                            ;
                                    }

                                    //        console.dir("arrSelDats_selectedDates: ", this.props.selectedDates.get() );
                                    console.dir("MULTICREA arrSelDats: ", arrSelectDates);

                                    //     console.dir("MULTICREA momentDia: ", momentDia);
                                    //      console.dir("MULTICREA dataDia: ", dataDia);
                                    //      console.dir("MULTICREA dataSessioProp: ", dataSessioProp);
                                    console.dir("MULTICREA this.props.objDia: ", this.props.objDia);
                                    console.dir("MULTICREA typeof(this.props.objDia): ", typeof (this.props.objDia));

                                    console.dir("MULTICREA this.state.objPeriodeSessio: ", this.state.objPeriodeSessio);


                                    aux = arrSelectDates.map(
                                        (v, i, a) => v.filter(
                                            (w, j, b) => w.checked === true
                                        )
                                    );

                                    console.dir("MULTICREA AUX: ", aux);

                                    resultat = aux.map(
                                        (v, i, a) => {
                                            if (v.length > 0) {
                                                return v;
                                            }
                                        }
                                    )

                                    resultat = flatMap(resultat).filter(
                                        (v, i, a) => typeof (v) !== "undefined"
                                    );

                                    //console.dir("arrSelectDates: ", arrSelectDates);

                                    console.dir("MULTICREA resultat: ", resultat);

                                    ///////////////////////////

                                    if (typeof this.props.rutina.sessions === "undefined" || this.props.rutina.sessions.length === 0) {
                                        alert(`Se crearà una sessió nova a partir de les dades de la sessió ${ev.target.innerHTML}.`);

                                        if (this.props.rutina._id === null || typeof this.props.rutina._id === "undefined") {
                                            //             alert(`Crearem una nova rutina.`);

                                            let
                                                dataSessio = this.props.objDia.diaData,
                                                novesSessions = [row]
                                                ;
                                            //
                                            novesSessions[0].dataSessio = dataSessio;
                                            novesSessions[0].nom = randomString(7);


                                            Meteor.call(
                                                'rutines.insert',
                                                this.props.rutina.nomRutina,
                                                this.props.rutina.client,
                                                this.props.rutina.dataIni,
                                                this.props.rutina.dataFi,
                                                this.props.rutina.entrenador,
                                                this.props.rutina.observacions,
                                                novesSessions,
                                                this.props.opcionsImpressio,
                                                (error, result) => {
                                                    ReactDOM.render(
                                                        <div
                                                            style={{
                                                                zIndex: `200000`,
                                                                background: `rgba(0,0,0,.7)`,
                                                                width: `100%`,
                                                                height: `100%`,
                                                                position: `absolute`
                                                            }}
                                                        ></div>,
                                                        document.body
                                                    );

                                                    location.replace(`/rutina/${result}`);

                                                }
                                            );




                                        } else {
                                            alert("Sessió nova en Rutina amb sessions ");



                                        }



                                    } else {




                                        if (resultat.length > 0) {
                                            //                    alert("ASSIGNACIÓ MULTIPLE");

                                            let
                                                datesSessio = resultat.map((v, i, a) => v.diaData)
                                            ;

                                            console.dir("ASSIGNAMULTI DATESSESSIO: ", datesSessio);

                                            console.dir("ASSIGNAMULTI RUTINAANTERIOR: ", rutinaAnterior);


                                            if (confirm(`Vas a afegir una sessió de múltiples dates basada en els valors de la sessió "${cell}" a la rutina "${this.props.rutina.nomRutina}". \n\nEstàs segur# de l'operació?`)) {

                                                //alert("ehmmmm provant...");

                                                // console.dir("ROW: ", row);
                                                // console.log("sesIndex: ", this.props.sesIndex);





                                                //


                                                if (typeof (this.props.sesIndex) !== "undefined" && this.props.sesIndex !== null) {

                                                    //                    alert("sesIndex definit. Sessió preexistent");

                                                    rutinaAnterior.sessions[this.props.sesIndex] = row;
                                                    rutinaAnterior.sessions[this.props.sesIndex].dataSessio = undefined;
                                                    rutinaAnterior.sessions[this.props.sesIndex].datesSessio = datesSessio;

                                                    Object.assign(
                                                        rutinaModificada,
                                                        rutinaAnterior
                                                    );

                                                    console.dir("ASSIGNAMULTI rutinaAnterior: ", rutinaAnterior);
                                                    console.dir("ASSIGNAMULTI rutinaModificada: ", rutinaModificada);

                                                    this.props.tancaConegudes();

                                                } else {
                                                    //    alert("sesIndex INDEFINIT");
                                                    // MÚLTIPLE NOVA - OK


                                                    Meteor.call(
                                                        'rutines.afegeixSessio',
                                                        this.props.rutinaId,
                                                        {
                                                            nom: randomString(7),
                                                            createdAt: new Date(),
                                                            user: Meteor.userId(),
                                                            //  dataSessio: this.props.objDia ? this.props.objDia.diaData : "",
                                                            datesSessio,
                                                            incImatges: null,
                                                            gms: row.gms,
                                                            escalfament: row.escalfament,
                                                            exercicis: row.exercicis,
                                                            calma: row.calma,
                                                            observacionsSessio: row.observacionsSessio,
                                                            opcionsImpressio: row.opcionsImpressio
                                                            // {
                                                            //     impNomSessio: this.state.impNomSessio,
                                                            //     impObsSessio: this.state.impObsSessio,
                                                            //     impImatgesExs: this.state.impImatgesExs,
                                                            //     impDescExs: this.state.impDescExs,
                                                            //     impEscalf: this.state.impEscalf,
                                                            //     impTorCalma: this.state.impTorCalma
                                                            // }
                                                        },

                                                        () => {
                                                            this.props.tancaConegudes();
                                                            this.props.tancaModal();
                                                        }
                                                    );



                                                }
                                            }

                                        } else {

                                            if (confirm(`Vas a substituir a la rutina "${this.props.rutina.nomRutina}" els valors de la sessió actual pels valors de la sessió seleccionada: "${cell}". \n\nEstàs segur# de l'operació?`)) {



                                                //alert("ehmmmm provant...");

                                                // console.dir("ROW: ", row);
                                                // console.log("sesIndex: ", this.props.sesIndex);

                                                if (typeof (this.props.sesIndex) !== "undefined" && this.props.sesIndex !== null) {

                                                    let
                                                        dataSessio = rutinaAnterior.sessions[this.props.sesIndex].dataSessio
                                                        ;

                                                    //
                                                    rutinaAnterior.sessions[this.props.sesIndex] = row;
                                                    rutinaAnterior.sessions[this.props.sesIndex].dataSessio = dataSessio;

                                                    Object.assign(
                                                        rutinaModificada,
                                                        rutinaAnterior
                                                    );



                                                    console.dir("rutinaAnterior: ", rutinaAnterior);
                                                    console.dir("rutinaModificada: ", rutinaModificada);

                                                    this.props.tancaConegudes();


                                                } else {
                                                    //    alert("sesIndex INDEFINIT");

                                                    if (this.props.selectedDates !== null && (typeof (this.props.selectedDates) === "undefined" || this.props.selectedDates.length === 0)) {
                                                        arrSelectDates = [];
                                                    } else {
                                                        console.dir("notUndefined: ", this.props.selectedDates);
                                                        arrSelectDates =
                                                            this.props.selectedDates === null || typeof (this.props.selectedDates) === "undefined"
                                                                ? []
                                                                : Object.values(this.props.selectedDates.get())
                                                            ;
                                                    }



                                                    aux = arrSelectDates.map(
                                                        (v, i, a) => v.filter(
                                                            (w, j, b) => w.checked === true
                                                        )
                                                    );

                                                    console.dir("AUX: ", aux);

                                                    resultat = aux.map(
                                                        (v, i, a) => {
                                                            if (v.length > 0) {
                                                                return v;
                                                            }
                                                        }
                                                    )

                                                    resultat = flatMap(resultat).filter(
                                                        (v, i, a) => typeof (v) !== "undefined"
                                                    );

                                                    // let 
                                                    //     datesSessio = 
                                                    // ;

                                                    console.dir("-->> arrSelectDates: ", arrSelectDates);
                                                    console.dir("-->> aux: ", aux);
                                                    console.dir("-->> resultat: ", resultat);

                                                    Meteor.call(
                                                        'rutines.afegeixSessio',
                                                        this.props.rutinaId,
                                                        {
                                                            nom: randomString(7),
                                                            createdAt: new Date(),
                                                            user: Meteor.userId(),
                                                            dataSessio: this.props.objDia ? this.props.objDia.diaData : "",
                                                            // datesSessio: this.props.
                                                            incImatges: null,
                                                            gms: row.gms,
                                                            escalfament: row.escalfament,
                                                            exercicis: row.exercicis,
                                                            calma: row.calma,
                                                            observacionsSessio: row.observacionsSessio,
                                                            opcionsImpressio: row.opcionsImpressio
                                                            // {
                                                            //     impNomSessio: this.state.impNomSessio,
                                                            //     impObsSessio: this.state.impObsSessio,
                                                            //     impImatgesExs: this.state.impImatgesExs,
                                                            //     impDescExs: this.state.impDescExs,
                                                            //     impEscalf: this.state.impEscalf,
                                                            //     impTorCalma: this.state.impTorCalma
                                                            // }
                                                        },

                                                        () => {
                                                            this.props.tancaConegudes();
                                                            this.props.tancaModal();
                                                        }
                                                    );



                                                }
                                            }
                                        }
                                    }
                                }
                                ///////////////// SESSIÓ A CREAR >>
                            } else {
                                /////////////////// << SESSIÓ PREEXISTENT   
                                //                 alert(`Sessió preexistent amb index ${this.props.sesIndex}`);

                                let
                                    sessioPreexistent = this.props.rutina.sessions[this.props.sesIndex],
                                    dataS =
                                        typeof sessioPreexistent.dataSessio !== "undefined"
                                            && sessioPreexistent.dataSessio !== null
                                            ? sessioPreexistent.dataSessio
                                            : ""
                                    ,
                                    datesS =
                                        typeof sessioPreexistent.datesSessio !== "undefined"
                                            && sessioPreexistent.datesSessio !== null
                                            && sessioPreexistent.datesSessio.length > 0
                                            ? sessioPreexistent.datesSessio
                                            : []
                                    ,
                                    periodeS =
                                        typeof sessioPreexistent.periodeSessio !== "undefined"
                                            && sessioPreexistent.periodeSessio !== null
                                            && Object.keys(sessioPreexistent.periodeSessio).length > 0
                                            ? sessioPreexistent.periodeSessio
                                            : {}
                                    ,
                                    truIndexSessio = this.props.truIndexSessio,
                                    sessioAbans = this.props.rutina.sessions[truIndexSessio],
                                    sessioModificada = { ...sessioAbans },
                                    rutinaAbans = this.props.rutina,
                                    rutinaModificada = { ...this.props.rutina },

                                    rutinaSessionsAbans = rutinaAbans.sessions,
                                    rutinaSessionsModificades = [...rutinaSessionsAbans],

                                    trobaSessioEnCollection = (sessio) => Sessions.find({
                                        nom: sessio.nom
                                    }).count() > 0
                                ;



                                console.dir(`PREEX dataS:`, dataS);
                                console.dir(`PREEX datesS:`, datesS);
                                console.dir(`PREEX periodeS:`, periodeS);

                                console.dir(`PREEX sessió preexistent: `, sessioPreexistent);


                                //         if (confirm(`Vas a canviar la data de la sessió pel ${capitalize(moment(dataBoto).format("dddd L"))}.\n\n**El procés tarda uns segons i recarregarà la pantalla.`)) {
                                //alert(`Pos ara ja fariem el canvi.`);
                                //console.dir(`LA BONA: INDEXSESSIO >>> `, truIndexSessio);


                                sessioModificada = { ...sessioPreexistent };

                                console.dir("LA PREEX: SESSIÓ ABANS: ", sessioAbans);
                                console.dir("LA PREEX: SESSIÓmodificada ABANS: ", sessioModificada);
                                //console.dir(`Veges si són el mateix objecte: ${sessioAbans === sessioModificada}`);
                                console.dir("LA PREEX: RUTINA ABANS: ", rutinaAbans);
                                console.dir("LA PREEX: RUTINAmodificada ABANS: ", rutinaModificada);
                                /////////////////
                                console.dir("LA PREEX: RUTINAsessionsAbans ABANS: ", rutinaSessionsAbans);
                                console.dir("LA PREEX: RUTINAsessionsModificades ABANS: ", rutinaSessionsModificades);

                                console.dir("PREEX Valor dels nous valors de la sessió: ", row);

                                /////////// A CONSERVAR DE LA SESSIÓ PREEXISTENT: 
                                /*   CLIENT,
                                    ENTRENADOR,
                                    DATA,
                                    NOM
                                 */

                                if (confirm(`Vas a substituir els valors de la sessió pels guardats a ${row.nom}. Confirmes el procés?`)) {


                                    sessioModificada.calma = row.calma;
                                    sessioModificada.escalfament = row.escalfament;
                                    sessioModificada.exercicis = row.exercicis;
                                    sessioModificada.gms = row.gms;
                                    sessioModificada.observacionsSessio = row.observacionsSessio;



                                    //       sessioModificada.datesSessio = [];

                                    //     sessioModificada.periodeSessio = {...this.state.periodeSessio};
                                    console.dir("LA PREEX: SESSIÓ DESPRÉS: ", sessioAbans);
                                    console.dir("LA PREEX: SESSIÓmodificada DESPRÉS: ", sessioModificada);

                                    console.dir("TROBASESSIOENCOLLECTION: ", trobaSessioEnCollection(sessioModificada));

                                    if (trobaSessioEnCollection(sessioModificada)) {
                                        Meteor.call(`sessions.update`, sessioAbans, sessioModificada);
                                        alert("La sessio ha sigut modificada a la llista de Sessions guardades.");
                                    }

                                    console.dir("RUTINASESSIONS ABANS: ", rutinaSessionsAbans);
                                    console.dir("RUTINASESSIONSmodificades ABANS: ", rutinaSessionsModificades);

                                    // mergeABP(
                                    //     rutinaSessionsModificades


                                    // Meteor.call(``)

                                    //     alert("STOPPING");

                                    console.dir(
                                        "mergeArrayDObjectes: ",
                                        mergeABP(
                                            rutinaSessionsModificades
                                            ,
                                            [sessioModificada],
                                            "nom"
                                        )
                                    );

                                    rutinaSessionsModificades = mergeABP(
                                        rutinaSessionsModificades
                                        ,
                                        [sessioModificada],
                                        "nom"
                                    )
                                        ;

                                    console.dir("rutinaSessionsModificades després de mergeABP: ", rutinaSessionsModificades);

                                    console.dir("RutinaAntiga ABANS: ", rutinaAbans);
                                    console.dir("RutinaModificada ABANS: ", rutinaModificada);

                                    rutinaModificada = {
                                        ...rutinaAbans,

                                        sessions:
                                            mergeABP(
                                                rutinaAbans.sessions
                                                ,
                                                [sessioModificada],
                                                "nom"
                                            )
                                    };

                                    console.dir("RutinaAbans DESPRÉS: ", rutinaAbans);
                                    console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                                    Meteor.call(
                                        "rutines.update",
                                        rutinaModificada._id,
                                        rutinaModificada
                                        // ,
                                        // (error, result) => {
                                        // this.setState({
                                        //     showModal: false
                                        // });
                                        //      location.reload();
                                        // }
                                    );

                                    ReactDOM.render(
                                        <div
                                            style={{
                                                zIndex: `200000`,
                                                background: `rgba(0,0,0,.7)`,
                                                width: `100%`,
                                                height: `100%`,
                                                position: `absolute`
                                            }}
                                        ></div>,
                                        document.body
                                    );

                                    location.reload();

                                }


                                //   }



                                if (typeof this.props.periodeSessio !== "undefined") {
                                    //                 alert("PERIODE");

                                    Meteor.call(
                                        'rutines.afegeixSessio',
                                        this.props.rutinaId,
                                        {
                                            nom: randomString(7),
                                            createdAt: new Date(),
                                            user: Meteor.userId(),
                                            //  dataSessio: this.props.objDia ? this.props.objDia.diaData : "",
                                            periodeSessio: this.props.periodeSessio,
                                            incImatges: null,
                                            gms: row.gms,
                                            escalfament: row.escalfament,
                                            exercicis: row.exercicis,
                                            calma: row.calma,
                                            observacionsSessio: row.observacionsSessio,
                                            opcionsImpressio: row.opcionsImpressio
                                            // {
                                            //     impNomSessio: this.state.impNomSessio,
                                            //     impObsSessio: this.state.impObsSessio,
                                            //     impImatgesExs: this.state.impImatgesExs,
                                            //     impDescExs: this.state.impDescExs,
                                            //     impEscalf: this.state.impEscalf,
                                            //     impTorCalma: this.state.impTorCalma
                                            // }
                                        }
                                    );

                                    this.props.tancaConegudes();
                                    this.props.tancaModal();

                                } else {
                                    ////////////////////

                                    if (this.props.selectedDates !== null && (typeof (this.props.selectedDates) === "undefined" || typeof this.props.selectedDates.get !== "function" || this.props.selectedDates.get().length === 0)) {
                                        arrSelectDates = [];
                                    } else {
                                        console.dir("MULTICREA notUndefined: ", this.props.selectedDates !== null ? this.props.selectedDates.get() : "null");
                                        arrSelectDates =
                                            this.props.selectedDates === null || typeof (this.props.selectedDates) === "undefined"
                                                ? []
                                                : Object.values(this.props.selectedDates.get())
                                            ;
                                    }

                                    //        console.dir("arrSelDats_selectedDates: ", this.props.selectedDates.get() );
                                    console.dir("MULTICREA arrSelDats: ", arrSelectDates);

                                    //     console.dir("MULTICREA momentDia: ", momentDia);
                                    //      console.dir("MULTICREA dataDia: ", dataDia);
                                    //      console.dir("MULTICREA dataSessioProp: ", dataSessioProp);
                                    console.dir("MULTICREA this.props.objDia: ", this.props.objDia);
                                    console.dir("MULTICREA typeof(this.props.objDia): ", typeof (this.props.objDia));

                                    console.dir("MULTICREA this.state.objPeriodeSessio: ", this.state.objPeriodeSessio);


                                    aux = arrSelectDates.map(
                                        (v, i, a) => v.filter(
                                            (w, j, b) => w.checked === true
                                        )
                                    );

                                    console.dir("MULTICREA AUX: ", aux);

                                    resultat = aux.map(
                                        (v, i, a) => {
                                            if (v.length > 0) {
                                                return v;
                                            }
                                        }
                                    )

                                    resultat = flatMap(resultat).filter(
                                        (v, i, a) => typeof (v) !== "undefined"
                                    );

                                    //console.dir("arrSelectDates: ", arrSelectDates);

                                    console.dir("MULTICREA resultat: ", resultat);

                                    ///////////////////////////

                                    if (typeof this.props.rutina.sessions === "undefined" || this.props.rutina.sessions.length === 0) {
                                        alert(`Se crearà una sessió nova a partir de les dades de la sessió ${ev.target.innerHTML}.`);

                                        if (this.props.rutina._id === null || typeof this.props.rutina._id === "undefined") {
                                            alert(`Crearem una nova rutina.`);

                                            let
                                                dataSessio = this.props.objDia.diaData,
                                                novesSessions = [row]
                                                ;
                                            //
                                            novesSessions[0].dataSessio = dataSessio;
                                            novesSessions[0].nom = randomString(7);


                                            Meteor.call(
                                                'rutines.insert',
                                                this.props.rutina.nomRutina,
                                                this.props.rutina.client,
                                                this.props.rutina.dataIni,
                                                this.props.rutina.dataFi,
                                                this.props.rutina.entrenador,
                                                this.props.rutina.observacions,
                                                novesSessions,
                                                this.props.opcionsImpressio,
                                                (error, result) => {
                                                    ReactDOM.render(
                                                        <div
                                                            style={{
                                                                zIndex: `200000`,
                                                                background: `rgba(0,0,0,.7)`,
                                                                width: `100%`,
                                                                height: `100%`,
                                                                position: `absolute`
                                                            }}
                                                        ></div>,
                                                        document.body
                                                    );

                                                    location.replace(`/rutina/${result}`);

                                                }
                                            );




                                        } else {
                                            alert("Sessió nova en Rutina amb sessions ");



                                        }



                                    } else {




                                        if (resultat.length > 0) {
                                            //               alert("ASSIGNACIÓ MULTIPLE");

                                            let
                                                datesSessio = resultat.map((v, i, a) => v.diaData)
                                                ;

                                            console.dir("ASSIGNAMULTI DATESSESSIO: ", datesSessio);

                                            console.dir("ASSIGNAMULTI RUTINAANTERIOR: ", rutinaAnterior);


                                            if (confirm(`Vas a afegir una sessió de múltiples dates basada en els valors de la sessió "${cell}" a la rutina "${this.props.rutina.nomRutina}". \n\nEstàs segur# de l'operació?`)) {

                                                //alert("ehmmmm provant...");

                                                // console.dir("ROW: ", row);
                                                // console.log("sesIndex: ", this.props.sesIndex);





                                                //


                                                if (typeof (this.props.sesIndex) !== "undefined" && this.props.sesIndex !== null) {

                                                    //                        alert("sesIndex definit. Sessió preexistent");

                                                    rutinaAnterior.sessions[this.props.sesIndex] = row;
                                                    rutinaAnterior.sessions[this.props.sesIndex].dataSessio = undefined;
                                                    rutinaAnterior.sessions[this.props.sesIndex].datesSessio = datesSessio;

                                                    Object.assign(
                                                        rutinaModificada,
                                                        rutinaAnterior
                                                    );

                                                    console.dir("ASSIGNAMULTI rutinaAnterior: ", rutinaAnterior);
                                                    console.dir("ASSIGNAMULTI rutinaModificada: ", rutinaModificada);

                                                    this.props.tancaConegudes();

                                                } else {
                                                    //    alert("sesIndex INDEFINIT");
                                                    // MÚLTIPLE NOVA - OK


                                                    Meteor.call(
                                                        'rutines.afegeixSessio',
                                                        this.props.rutinaId,
                                                        {
                                                            nom: randomString(7),
                                                            createdAt: new Date(),
                                                            user: Meteor.userId(),
                                                            //  dataSessio: this.props.objDia ? this.props.objDia.diaData : "",
                                                            datesSessio,
                                                            incImatges: null,
                                                            gms: row.gms,
                                                            escalfament: row.escalfament,
                                                            exercicis: row.exercicis,
                                                            calma: row.calma,
                                                            observacionsSessio: row.observacionsSessio,
                                                            opcionsImpressio: row.opcionsImpressio
                                                            // {
                                                            //     impNomSessio: this.state.impNomSessio,
                                                            //     impObsSessio: this.state.impObsSessio,
                                                            //     impImatgesExs: this.state.impImatgesExs,
                                                            //     impDescExs: this.state.impDescExs,
                                                            //     impEscalf: this.state.impEscalf,
                                                            //     impTorCalma: this.state.impTorCalma
                                                            // }
                                                        }
                                                    );

                                                    this.props.tancaConegudes();
                                                    this.props.tancaModal();


                                                }
                                            }

                                        } else {

                                            if (confirm(`Vas a substituir a la rutina "${this.props.rutina.nomRutina}" els valors de la sessió actual pels valors de la sessió seleccionada: "${cell}". \n\nEstàs segur# de l'operació?`)) {



                                                //alert("ehmmmm provant...");

                                                // console.dir("ROW: ", row);
                                                // console.log("sesIndex: ", this.props.sesIndex);

                                                if (typeof (this.props.sesIndex) !== "undefined" && this.props.sesIndex !== null) {

                                                    let
                                                        dataSessio = rutinaAnterior.sessions[this.props.sesIndex].dataSessio
                                                        ;

                                                    //
                                                    rutinaAnterior.sessions[this.props.sesIndex] = row;
                                                    rutinaAnterior.sessions[this.props.sesIndex].dataSessio = dataSessio;

                                                    Object.assign(
                                                        rutinaModificada,
                                                        rutinaAnterior
                                                    );



                                                    console.dir("rutinaAnterior: ", rutinaAnterior);
                                                    console.dir("rutinaModificada: ", rutinaModificada);

                                                    this.props.tancaConegudes();


                                                } else {
                                                    //    alert("sesIndex INDEFINIT");

                                                    if (this.props.selectedDates !== null && (typeof (this.props.selectedDates) === "undefined" || this.props.selectedDates.length === 0)) {
                                                        arrSelectDates = [];
                                                    } else {
                                                        console.dir("notUndefined: ", this.props.selectedDates);
                                                        arrSelectDates =
                                                            this.props.selectedDates === null || typeof (this.props.selectedDates) === "undefined"
                                                                ? []
                                                                : Object.values(this.props.selectedDates.get())
                                                            ;
                                                    }



                                                    aux = arrSelectDates.map(
                                                        (v, i, a) => v.filter(
                                                            (w, j, b) => w.checked === true
                                                        )
                                                    );

                                                    console.dir("AUX: ", aux);

                                                    resultat = aux.map(
                                                        (v, i, a) => {
                                                            if (v.length > 0) {
                                                                return v;
                                                            }
                                                        }
                                                    )

                                                    resultat = flatMap(resultat).filter(
                                                        (v, i, a) => typeof (v) !== "undefined"
                                                    );

                                                    // let 
                                                    //     datesSessio = 
                                                    // ;

                                                    console.dir("-->> arrSelectDates: ", arrSelectDates);
                                                    console.dir("-->> aux: ", aux);
                                                    console.dir("-->> resultat: ", resultat);

                                                    Meteor.call(
                                                        'rutines.afegeixSessio',
                                                        this.props.rutinaId,
                                                        {
                                                            nom: randomString(7),
                                                            createdAt: new Date(),
                                                            user: Meteor.userId(),
                                                            dataSessio: this.props.objDia ? this.props.objDia.diaData : "",
                                                            // datesSessio: this.props.
                                                            incImatges: null,
                                                            gms: row.gms,
                                                            escalfament: row.escalfament,
                                                            exercicis: row.exercicis,
                                                            calma: row.calma,
                                                            observacionsSessio: row.observacionsSessio,
                                                            opcionsImpressio: row.opcionsImpressio
                                                            // {
                                                            //     impNomSessio: this.state.impNomSessio,
                                                            //     impObsSessio: this.state.impObsSessio,
                                                            //     impImatgesExs: this.state.impImatgesExs,
                                                            //     impDescExs: this.state.impDescExs,
                                                            //     impEscalf: this.state.impEscalf,
                                                            //     impTorCalma: this.state.impTorCalma
                                                            // }
                                                        }
                                                    );

                                                    this.props.tancaConegudes();
                                                    this.props.tancaModal();


                                                }
                                            }
                                        }
                                    }
                                }
                                ////////////////// SESSIÓ PREEXISTENT >>
                            }
                        }
                    }
                >{cell}
                </a>
            ),

            descripFormatter = (cell, row) => (
                <div
                    dangerouslySetInnerHTML={{
                        __html:
                            typeof (row.observacions) !== "undefined"
                                ? sanitizeHtml(row.observacions)
                                : ""
                    }}
                    style={{
                        overflow: 'hidden'
                    }}>
                </div>
            )

        rutinaCreatedAtFormatter = (cell, row) => (
            <div
                dangerouslySetInnerHTML={{ __html: sanitizeHtml(moment(row.createdAt).format("D/MM/YYYY")) }}
                style={{
                    overflow: 'hidden'
                }}>
            </div>
        )
            ;

        return (
            <div>
                <BootstrapTable
                    data={this.state.rows}
                    tableContainerClass="ulSessionsLlista"
                    headerContainerClass='my-custom-class'

                    striped
                    hover
                    search
                    deleteRow

                    version='4'

                    headerStyle={{
                        background: 'rgba(255,255,255,.5)',
                        fontVariant: 'small-caps',
                        fontSize: '1.3em',
                        textShadow: '1px 1px 0px white'
                    }}

                    options={{
                        sortName: `createdAt`,
                        sortOrder: `desc`,
                        afterInsertRow: onAfterInsertRow,
                        handleConfirmDeleteRow: customConfirm,
                        onDeleteRow: this.onDeleteRow,
                        afterDeleteRow,
                        afterSearch: afterSearch,
                        deleteBtn: this.createCustomDeleteButton,
                        clearSearch: false,
                        searchField: this.createCustomSearchField,
                        insertModalHeader: this.createCustomModalHeader,
                        insertModalFooter: this.createCustomModalFooter,
                        sortIndicator: false,
                        noDataText: "Carregant dades...",
                        toolbarPosition: 'both'
                    }}

                    selectRow={{
                        mode: `checkbox`
                    }}

                >
                    <TableHeaderColumn
                        isKey
                        dataSort
                        searchable
                        dataField="nom"
                        dataFormat={sessioLinkFormatter}
                    >Sessio
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataSort
                        searchable
                        dataField='observacions'
                        dataFormat={descripFormatter}
                    >Observacions
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataSort
                        dataField='createdAt'
                        dataFormat={rutinaCreatedAtFormatter}
                    >Creada
                    </TableHeaderColumn>

                </BootstrapTable>

            </div>
        );
    }
}

class SessionsNoData extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sessio: {},
            mostraForm: false
        };
    }

    handleClose = () => this.setState({ mostraForm: false });

    toggleForm = () => {
        this.setState({
            mostraForm: !this.state.mostraForm
        });
    };

    render() {

        return (
            <div>
                <TaulaSessions
                    sessions={this.props.sessions}
                    toggleForm={this.toggleForm}
                    {...this.props}
                />
            </div>
        );
    }
}

// App.propTypes = {
// //  clients: PropTypes.array.isRequired
// };

const TaulaSessionsWithData = createContainer(() => {
    const
        subscription = {
            clientsSubscription: Meteor.subscribe("userClients"),
            grups_muscularsSubscription: Meteor.subscribe("userGrupsMusculars"),
            //imatgesSubscription: Meteor.subscribe("userImatges"),
            exercicisSubscription: Meteor.subscribe("userExercicis"),
            sessionsSubscription: Meteor.subscribe("userSessions"),
            rutinesSubscription: Meteor.subscribe("userRutines")
        },

        loading = !(subscription.clientsSubscription.ready() &&
            subscription.grups_muscularsSubscription.ready() &&
            subscription.exercicisSubscription.ready() &&
            subscription.sessionsSubscription.ready() &&
            subscription.rutinesSubscription.ready()
        ),

        clients = subscription.clientsSubscription.ready() ? Clients.find().fetch() : [],
        grups_musculars = subscription.grups_muscularsSubscription.ready() ? GrupsMusculars.find().fetch() : [],
        exercicis = subscription.exercicisSubscription.ready() ? Exercicis.find().fetch() : [],
        sessions = subscription.sessionsSubscription.ready() ? Sessions.find().fetch() : [],
        rutines = subscription.rutinesSubscription.ready() ? Rutines.find().fetch() : []
        ;

    return {
        clients,
        grups_musculars,
        exercicis,
        sessions,
        rutines,
        loading
    }
}, SessionsNoData);

export default Rutina;
