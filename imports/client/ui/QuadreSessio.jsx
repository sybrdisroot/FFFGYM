import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import capitalize from 'lodash/capitalize';
import ContentEditable from 'react-contenteditable';
import sanitizeHtml from 'sanitize-html-react';

import mergeABP from '/imports/utils/mergeABP.js';


class CampTextModificable extends Component {
    constructor(props) {
        super(props);

        const sanitizeConf = {
            allowedTags: []
        };

        this.state = {
            html: typeof (this.props.children) !== "undefined" && (sanitizeHtml(this.props.children, sanitizeConf) || "<br />"),
            disabled: !this.props.inputMode
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            html: nextProps.children,
            disabled: !nextProps.inputMode
        })
    }

    // componentShouldUpdate() {
    //     return false;
    // }

    render() {
        return (
            <ContentEditable
                html={this.state.html}
                disabled={
                    !this.props.inputMode
                }
            //     onClick={(ev) => {
            //         // this.setState({
            //         //   //  disabled: false
                      
            //         // })
            //         ev.target.select();
            // // 
            //     }}
                onChange={(ev) => {
                    this.setState({
                        html: ev.target.innerHTML ? ev.target.innerHTML : this.state.html
                    });
                }}

                onBlur={(ev) => {
                    let
                        rutinaAnterior = this.props.rutina,
                        rutinaModificada = { ...rutinaAnterior }
                    ;

                    if (typeof (this.props.exerField) !== "undefined" && this.props.exerField) {
                        rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i][this.props.field] = ev.target.innerHTML;
                    } else {
                        rutinaModificada.sessions[this.props.indexSessio][this.props.field] = ev.target.innerHTML;
                    }

                    console.dir("rutinaModificada: ", rutinaModificada);

                    // Meteor.call(
                    //     'rutines.update',
                    //     this.props.rutinaId,
                    //     rutinaModificada
                    // );

                    this.props.setRutinaEditada(rutinaModificada);   // Aquesta línia va emparellada amb el Meteor.call de "handleCloseModal" a RutinesForm:TaulaBotonsDies

                    //alert(`Pos ara s'ha canviat!: ${ev.target.value}`);
                }}
            />
        );
    }
}


class NumericEditable3 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            html: typeof (this.props.children) !== "undefined" && (this.props.children || "<br />"),
            disabled: !this.props.inputMode
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            html: nextProps.children,
            disabled: !nextProps.inputMode
        })
    }

    // sanitizeConf = {
    //     allowedTags: [],
    //     allowedAttributes: {}
    // };

    // sanitize = () => {
    //     this.setState({ html: sanitizeHtml(this.state.html, this.sanitizeConf) });
    // };

    render() {
        return (
            <ContentEditable
                html={this.state.html}
                disabled={
                    !this.props.inputMode
                }
                // onClick={() => {
                //     this.setState({
                //         disabled: false
                //     })
                // }}
                onKeyUp={(ev) => {
                    console.dir("EVENTONKEYUP - ENTERkey? ", ev.target.keyCode);
                }}

                onChange={(ev) => {
                    console.dir("EVENTONCHANGE-ENTERkey? ", ev.target);

                    this.setState({
                        html: ev.target.innerHTML ? ev.target.innerHTML : this.state.html
                    });
                }}

                onBlur={(ev) => {
                    let
                        rutinaAnterior = this.props.rutina,
                        rutinaModificada = { ...rutinaAnterior }
                        ;

                    //this.sanitize();

                    if (typeof (this.props.exerField) !== "undefined" && this.props.exerField) {
                        rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i][this.props.field] = ev.target.innerHTML;
                    } else {
                        rutinaModificada.sessions[this.props.indexSessio][this.props.field] = ev.target.innerHTML;
                    }

                    console.dir("rutinaModificada: ", rutinaModificada);

                    Meteor.call(
                        'rutines.update',
                        this.props.rutinaId,
                        rutinaModificada
                    );

                    //alert(`Pos ara s'ha canviat!: ${ev.target.value}`);
                }}
            />
        );
    }
}

class NumericEditable2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            displayValue: props.children,
            ex: props.ex,
            field: props.field
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            displayValue: nextProps.children,
            ex: nextProps.ex,
            field: nextProps.field
        });
    }


    render() {

        // let
        //     asyncRutinaUpdate = async (rutinaId, rutinaMod) => {
        //         let resMeteorCall = Meteor.call(
        //             'rutines.update',
        //             rutinaId,
        //             rutinaMod
        //         );
        //         console.dir("MeteorCall: ", resMeteorCall);
        //     }
        // ;

        if (this.props.inputMode) {
            return (
                <div
                    style={{
                        display: `grid`,
                        gridArea: `${this.props.area}`,
                        gridTemplateAreas: `"zona"`,

                        fontWeight: `bold`,
                        fontSize: `1.2em`,
                        textAlign: `center`,
                        fontVariant: `small-caps`,
                        background: `gold`,
                        borderRadius: `2em`,
                        alignContent: `stretch`,
                        transform: `skewX(-12deg)`,
                        justifyContent: `stretch`,
                        alignSelf: `stretch`,
                        justifySelf: `stretch`
                    }}
                >
                    <input
                        type="text"
                        style={{
                            // display: `grid`,
                            // justifySelf: `center`,
                            // alignSelf: `center`,
                            textAlign: `center`,
                            gridArea: `zona`,
                            //${this.props.area}
                            zIndex: `5000`
                        }}
                        onClick={(ev) => {
                            ev.target.select();
                        }}
                        contentEditable
                        onChange={(e) => {
                            this.setState({
                                displayValue: e.target.value
                            });
                        }}
                        onBlur={(ev) => {
                            let
                                rutinaAnterior = this.props.rutina,
                                rutinaModificada = { ...rutinaAnterior }
                                ;

                            //this.sanitize();
                            // this.setState({
                            //     displayValue: ev.target.value
                            // });

                            if (typeof (this.props.field) !== "undefined" && this.props.field) {
                                if (this.props.field === "exerciciDescansSegons") {
                                    const segonsZeroLead = String(this.state.displayValue).length === 1 ? `0${this.state.displayValue}` : `${this.state.displayValue}`;

                                    rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i][this.props.field] = segonsZeroLead;
                                } else {
                                    rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i][this.props.field] = this.state.displayValue;
                                }
                            } else {
                                rutinaModificada.sessions[this.props.indexSessio][this.props.field] = this.state.displayValue;
                            }

                            console.dir("rutinaModificada: ", rutinaModificada);

                            // Meteor.call(
                            //     'rutines.update',
                            //     this.props.rutinaId,
                            //     rutinaModificada
                            // );

                            //           asyncRutinaUpdate(this.props.rutinaId, rutinaModificada);

                            //    this.props.setRutinaEditada(rutinaModificada); // Establim la rutina modificada a l'estat del component d'ordre superior necessari amb aquest mètode passat fins ací per props.
                        }}
                        value={this.state.displayValue}
                    />
                </div>
            );
        } else {
            return (
                <div
                    style={{
                        textAlign: `center`,
                        gridArea: `${this.props.area}`,
                        zIndex: `5000`
                    }}
                >
                    {this.state.displayValue}
                </div>
            );
        }
    }
}

class NumericEditable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            displayValue: props.children,
            ex: props.ex,
            field: props.field
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            displayValue: nextProps.children,
            ex: nextProps.ex,
            field: nextProps.field
        });
    }

    modificaCamp = (ev, operacio, pas = 1) => {
        let
            ctrlKey = ev.ctrlKey,
            shiftKey = ev.shiftKey,
            //metaKey = ev.metaKey,
            pasAmbKey =
                ctrlKey
                    ? shiftKey
                        ? 25
                        : 5
                    : shiftKey
                        ? 10
                        : pas
            ,

            rutinaAnterior = this.props.rutina,
            rutinaModificada = { ...rutinaAnterior },

            exerciciValor = Number(this.props.ex[this.props.field]),
            exerciciValorModificat =
                operacio === "RESTA"
                    ? Number(this.props.ex[this.props.field]) - pasAmbKey
                    : operacio === "SUMA"
                        ? Number(this.props.ex[this.props.field]) + pasAmbKey
                        : operacio === "ZERO"
                            ? 0
                            : Number(this.props.ex[this.props.field])
            ;

        if (typeof (rutinaModificada.sessions[this.props.indexSessio].exercicis) !== "undefined") {
            rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i][this.props.field] = exerciciValorModificat;
        }

        // console.dir("rutinaAnterior: ", rutinaAnterior);
        // console.dir("rutinaModificada: ", rutinaModificada);

        // console.dir("exerciciValor: ", exerciciValor);
        //
        // console.dir("exerciciValorModificat: ", exerciciValorModificat);

        Meteor.call(
            'rutines.update',
            this.props.rutinaId,
            rutinaModificada
        );

        this.setState((prevState, nexState) => {
            displayValue: operacio === "RESTA"
                ? Number(prevState.displayValue) - pasAmbKey
                : operacio === "SUMA"
                    ? Number(prevState.displayValue) + pasAmbKey
                    : operacio === "ZERO"
                        ? 0
                        : Number(prevState.displayValue)
        });
    };

    render() {
        let pas;
        switch (this.state.field) {
            case ("exerciciSeriesDefault"):
            case ("exerciciDescansMinuts"):
            case ("exerciciRepeticionsDefault"): {
                pas = 1;
                break;
            }
            case ("exerciciDescansSegons"): {
                pas = 10;
                break;
            }
            case ("exerciciCarrega"): {
                pas = 5;
                break;
            }
        }

        if (this.props.inputMode) {
            return (
                <div
                    style={{
                        display: `grid`,
                        gridArea: `${this.props.area}`,
                        gridTemplateAreas: `"zona"`,

                        fontWeight: `bold`,
                        fontSize: `1.2em`,
                        textAlign: `center`,
                        fontVariant: `small-caps`,
                        //     background: `gold`,
                        borderRadius: `2em`,
                        alignContent: `stretch`,
                        transform: `skewX(-12deg)`,
                        justifyContent: `stretch`,
                        alignSelf: `stretch`,
                        justifySelf: `stretch`
                    }}
                >
                    <div
                        style={{
                            display: `grid`,
                            gridArea: `zona`,
                            //          background: `lime`,
                            gridTemplateAreas: `"less more"`
                        }}
                    >
                        <div
                            title={`Redueix ${this.props.campNomAmbArticle}`}
                            ref={d => this.less = d}

                            onClick={(ev) => {
                                this.modificaCamp(ev, "RESTA", pas);
                            }}

                            onMouseEnter={(ev) => {
                                ev.target.style.background = `rgba(25,123,255,.8)`;
                                ev.target.style.boxShadow = `0 0 1em rgba(25,123,255,.8)`;

                                //   alert("Zona Resta");
                            }}
                            onMouseOut={(ev) => {
                                ev.target.style.background = `transparent`;
                                ev.target.style.boxShadow = `none`;
                                //   alert("Zona Resta");
                            }}
                            style={{
                                gridArea: `less`,
                                borderRadius: `2em 0 0 2em`,

                                //background: `rgba(25,123,255,.6)`,
                                justifySelf: `stretch`,
                                alignSelf: `stretch`,
                                alignContent: `stretch`,
                                justifyContent: `stretch`,
                                minHeight: `100%`,
                                minWidth: `100%`,
                                zIndex: `10000`

                            }}
                        >
                            <div
                                title={`Posa a zero`}
                                ref={d => this.zero = d}

                                onClick={ev => {
                                    //ev.preventDefault();
                                    ev.stopPropagation();
                                    this.modificaCamp(ev, "ZERO");
                                }}
                                onMouseEnter={(ev) => {
                                    ev.target.style.background = `rgba(0,0,255,.7)`;
                                    ev.target.style.boxShadow = `0 0 1em rgba(25,123,255,.8)`;

                                    //  alert("Zona Resta");
                                }}
                                onMouseOut={(ev) => {
                                    ev.target.style.background = `transparent`;
                                    ev.target.style.boxShadow = `none`;
                                    //   alert("Zona Resta");
                                }}
                                style={{
                                    gridArea: `less`,
                                    borderRadius: `2em 0 0 2em`,

                                    //background: `rgba(25,123,255,.6)`,
                                    //background: `rgba(0,0,255,.7)`,
                                    justifySelf: `start`,
                                    alignSelf: `end`,
                                    alignContent: `start`,
                                    justifyContent: `start`,
                                    height: `100%`,
                                    width: `30%`,
                                    zIndex: `100000`

                                }}
                            />
                        </div>
                        <div
                            title={`Augmenta ${this.props.campNomAmbArticle}`}

                            onClick={(ev) => {
                                this.modificaCamp(ev, "SUMA", pas);
                            }}

                            onMouseEnter={(ev) => {
                                ev.target.style.background = `rgba(255,0,100,.8)`;
                                ev.target.style.boxShadow = `0 0 1em rgba(255,0,100,.8)`;

                                //   alert("Zona Suma");
                            }}
                            onMouseOut={(ev) => {
                                ev.target.style.background = `transparent`;
                                ev.target.style.boxShadow = `none`;
                                //   alert("Zona Suma");
                            }}
                            style={{
                                gridArea: `more`,
                                borderRadius: `0 2em 2em 0`,
                                // background: `rgba(255,0,0,.7)`,

                                justifySelf: `stretch`,
                                alignSelf: `stretch`,
                                alignContent: `stretch`,
                                justifyContent: `stretch`,
                                zIndex: `10000`
                            }}
                        />

                    </div>
                    <div
                        style={{
                            // display: `grid`,
                            // justifySelf: `center`,
                            // alignSelf: `center`,
                            textAlign: `center`,
                            gridArea: `zona`,
                            //${this.props.area}
                            zIndex: `5000`
                        }}
                    >
                        {typeof (this.props.children) !== "undefined" && this.props.children}
                    </div>
                </div>
            );
        } else {
            return (
                <div
                    style={{
                        textAlign: `center`,
                        gridArea: `${this.props.area}`,
                        zIndex: `5000`
                    }}
                >
                    {this.state.displayValue}
                </div>
            );
        }
    }
}

class ExerciciModificable extends Component {
    constructor(props) {
        super(props);

        if (props.inputMode) {
            this.state = {
                inputMode: props.inputMode,
                impOpcionsVisibles: props.impOpcionsVisibles,
                impNomSessio: props.sessio.opcionsImpressio.impNomSessio,
                impObsSessio: props.sessio.opcionsImpressio.impObsSessio,
                impImatgesExs: props.sessio.opcionsImpressio.impImatgesExs,
                impDescExs: props.sessio.opcionsImpressio.impDescExs,
                impEscalf: props.sessio.opcionsImpressio.impEscalf,
                impTorCalma: props.sessio.opcionsImpressio.impTorCalma,
                impTABULAR: props.sessio.opcionsImpressio.impTABULAR,
                ex: props.ex,
                superSeries: props.ex.superSeries || false,
                tripleSeries: props.ex.tripleSeries || false
            };
        } else {
            this.state = {
                inputMode: props.inputMode,
                impOpcionsVisibles: props.impOpcionsVisibles,
                impNomSessio: props.impNomSessio,
                impObsSessio: props.impObsSessio,
                impImatgesExs: props.impImatgesExs,
                impDescExs: props.impDescExs,
                impEscalf: props.impEscalf,
                impTorCalma: props.impTorCalma,
                impTABULAR: props.impTABULAR,
                ex: props.ex,
                superSeries: props.ex.superSeries || false,
                tripleSeries: props.ex.tripleSeries || false
            };
        }
    }

    componentWillReceiveProps(nextProps) {
        console.dir
        if (this.props.inputMode) {
            this.setState({
                inputMode: nextProps.inputMode,
                impOpcionsVisibles: nextProps.impOpcionsVisibles,
                impNomSessio: nextProps.sessio.opcionsImpressio.impNomSessio,
                impObsSessio: nextProps.sessio.opcionsImpressio.impObsSessio,
                impImatgesExs: nextProps.sessio.opcionsImpressio.impImatgesExs,
                impDescExs: nextProps.sessio.opcionsImpressio.impDescExs,
                impEscalf: nextProps.sessio.opcionsImpressio.impEscalf,
                impTorCalma: nextProps.sessio.opcionsImpressio.impTorCalma,
                impTABULAR: nextProps.sessio.opcionsImpressio.impTABULAR,
                ex: nextProps.ex,
                superSeries: nextProps.ex.superSeries || false,
                tripleSeries: nextProps.ex.tripleSeries || false
            });
        } else {
            this.setState({
                inputMode: nextProps.inputMode,
                impOpcionsVisibles: nextProps.impOpcionsVisibles,
                impNomSessio: nextProps.impNomSessio,
                impObsSessio: nextProps.impObsSessio,
                impImatgesExs: nextProps.impImatgesExs,
                impDescExs: nextProps.impDescExs,
                impEscalf: nextProps.impEscalf,
                impTorCalma: nextProps.impTorCalma,
                ex: nextProps.ex,
                superSeries: nextProps.ex.superSeries || false,
                tripleSeries: nextProps.ex.tripleSeries || false
            });
        }
    }

    render() {
        let
            v = this.props.v,
            i = this.props.i,
            s = this.props.s,
            ex = this.props.ex
            ,
            bgColor =
                this.state.superSeries
                    ? `rgba(255,255,0,.5)`
                    : this.state.tripleSeries
                        ? `rgba(255,0,0,.5)`
                        : `rgba(0,0,0,.3)`
        ;

        // console.dir("Exx: ", ex);
        // ;

        console.dir("Ex: ", ex);


        return (
            <div
                style={{
                    position: `relative`,
                    display: `grid`,
                    gridTemplateAreas: `
                        "imatge"
                        "captions"`
                    ,
                    border: `solid 1px`,
                    borderRadius: `.8em`,
                    background: bgColor,
                    overflow: `hidden`,
                    height: `100%`
                    // outline: `1px solid steelblue`
                }}
            >
                <div
                    style={{
                        gridArea: `imatge`,
                        justifySelf: `center`,
                        alignSelf: `center`
                    }}
                >
                    {
                        this.state.impImatgesExs
                            ? <div>
                                <img
                                    style={{
                                        maxWidth: `150px`,
                                        maxHeight: `150px`,
                                        gridArea: `imatge`,
                                        justifySelf: `center`
                                        // ,
                                        // margin: `1em 0`
                                    }}
                                    src={(typeof (ex.arrImatges) !== "undefined" && !!ex.arrImatges[0]) ? ex.arrImatges[0].imgArx.buffer : ""}
                                />
                            </div>
                            : <div />
                    }
                </div>

                <div
                    style={{
                        gridArea: `captions`,
                        padding: `0 1em`,
                        display: `grid`,
                        gridTemplateAreas: `
                            "titol                  titol"
                            "supertriple supertriple"
                            "series           repeticions"
                            "progressio   progressio"
                            "descans              carrega"
                            "descrip                descrip"`,
                        background: `rgba(255,255,255, .3)`
                    }}
                >
                    <div
                        style={{
                            width: `100%`,
                            // transform: `rotate(90deg)`,
                            // transformOrigin: `right top`,
                            //background: `rgba(255,255,255,.2)`,
                            background: `linear-gradient(to bottom, rgba(0,0,0,1) 0%,rgba(125,185,232,0) 100%)`,
                            padding: `.5em`,
                            fontStyle: `italic`,
                            fontVariant: `small-caps`,
                            color: `rgba(176, 196, 222, .3)`,
                            textShadow: `0 1px 0 grey`,
                            top: `0`,
                            justifySelf: `right`,
                            zIndex: `10`,
                            position: `absolute`,
                            textAlign: `right`,
                            left: 0
                        }}
                    >{ex.exerciciGrupMuscular}
                    </div>
                    <div
                        style={{
                            fontWeight: `bold`,
                            fontSize: `1.2em`,
                            textAlign: `center`,
                            gridArea: `titol`,
                            fontVariant: `small-caps`,
                            background: `rgba(0, 55, 255, .2)`,
                            //borderRadius: `2em`,
                            display: `grid`,
                            alignContent: `center`,
                            //transform: `skewX(-12deg)`
                            margin: `0 -1em`,
                            textShadow: `1px 1px 0 rgba(255,255,255,.5)`

                        }}
                    >{capitalize(this.state.ex.exerciciNom)}
                    </div>

                    {
                        this.props.inputMode
                            ? <div
                                style={{
                                    display: `grid`,
                                    gridArea: `supertriple`,
                                    gridTemplateAreas: `
                                "super    triple"`,
                                    textAlign: `center`
                                }}
                            >
                                <div
                                    style={{
                                        gridArea: `super`
                                    }}
                                >
                                    <input
                                        type="checkbox"
                                        name={`superSeries_${this.props.i}`}
                                        checked={this.state.superSeries}
                                        onChange={() => {
                                            this.setState({
                                                superSeries: !this.state.superSeries,
                                                tripleSeries: false
                                            }, () => {
                                                let
                                                    rutinaAnterior = this.props.rutina,
                                                    rutinaModificada = { ...rutinaAnterior }
                                                    ;

                                                rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i].superSeries = this.state.superSeries;
                                                rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i].tripleSeries = this.state.tripleSeries;

                                                console.dir("rutinaModificada: ", rutinaModificada);

                                                Meteor.call(
                                                    'rutines.update',
                                                    this.props.rutinaId,
                                                    rutinaModificada
                                                );
                                            });
                                        }}
                                    />
                                    <label
                                        htmlFor={`superSeries_${this.props.i}`}
                                    >Super-Sèries
                                </label>
                                </div>
                                <div
                                    style={{
                                        gridArea: `triple`
                                    }}
                                >
                                    <input
                                        type="checkbox"
                                        name={`tripleSeries_${this.props.i}`}
                                        checked={this.state.tripleSeries}
                                        onChange={() => {
                                            this.setState({
                                                tripleSeries: !this.state.tripleSeries,
                                                superSeries: false
                                            }, () => {
                                                let
                                                    rutinaAnterior = this.props.rutina,
                                                    rutinaModificada = { ...rutinaAnterior }
                                                    ;

                                                rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i].superSeries = this.state.superSeries;

                                                rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i].tripleSeries = this.state.tripleSeries;

                                                console.dir("rutinaModificada: ", rutinaModificada);

                                                Meteor.call(
                                                    'rutines.update',
                                                    this.props.rutinaId,
                                                    rutinaModificada
                                                );
                                            });
                                        }}
                                    />
                                    <label
                                        htmlFor={`tripleSeries_${this.props.i}`}
                                    >Triple-Sèries
                                </label>
                                </div>
                            </div>
                            : this.state.superSeries
                                ? <div
                                    style={{
                                        background: `yellow`,
                                        boxShadow: `0 0 1em yellow`,
                                        color: `red`,
                                        gridArea: `supertriple`,
                                        justifySelf: `stretch`,
                                        alignSelf: `center`,
                                        textAlign: `center`,
                                        borderRadius: `.5em`
                                    }}
                                >SUPER
                                </div>
                                : this.state.tripleSeries
                                    ? <div
                                        style={{
                                            background: `red`,
                                            boxShadow: `0 0 1em red`,
                                            color: `lightblue`,
                                            gridArea: `supertriple`,
                                            justifySelf: `stretch`,
                                            alignSelf: `center`,
                                            textAlign: `center`,
                                            borderRadius: `.5em`,
                                            fontWeight: `bold`
                                        }}
                                    >TRIPLE
                                    </div>
                                    : null
                    }

                    <div
                        style={{
                            gridArea: `series`
                        }}
                    >
                        <span>Sèries: </span>
                        <span
                            style={{
                                // fontWeight: `bold`,
                                // marginRight: `1em`
                                display: `grid`,
                                textAlign: `center`,
                                alignContent: `center`,
                                fontWeight: `bold`,
                                fontSize: `1.2em`,
                                gridTemplateAreas: `"tit"`,

                                fontVariant: `small-caps`,
                                background: `rgba(255,255,255, .8)`,
                                borderRadius: `2em`,
                                transform: `skewX(-12deg)`
                            }}
                        >
                            <NumericEditable2
                                ex={this.state.ex}
                                field="exerciciSeriesDefault"
                                area="tit"
                                campNomAmbArticle="les sèries"
                                {...this.props}
                            >
                                {this.state.ex.exerciciSeriesDefault}
                            </NumericEditable2>
                        </span>
                    </div>
                    <div
                        style={{
                            gridArea: `repeticions`
                        }}
                    >
                        <span>Reps.: </span>
                        <span
                            style={{
                                display: `grid`,
                                textAlign: `center`,
                                alignContent: `center`,
                                fontWeight: `bold`,
                                fontSize: `1.2em`,
                                gridTemplateAreas: `"tit"`,

                                fontVariant: `small-caps`,
                                background: `rgba(255,255,255, .8)`,
                                borderRadius: `2em`,
                                transform: `skewX(-12deg)`
                            }}
                        >
                            <NumericEditable2
                                ex={this.state.ex}
                                field="exerciciRepeticionsDefault"
                                area="tit"
                                campNomAmbArticle="les repeticions"
                                {...this.props}
                            >
                                {this.state.ex.exerciciRepeticionsDefault}
                            </NumericEditable2>
                        </span>
                    </div>

                    <div
                        style={{
                            gridArea: `progressio`
                        }}
                    >
                        <OportuCampTextModificable
                            exerField
                            field="exerciciProgressio"
                            title="Progressió: "
                            style={{
                                background: `rgba(0,150,0,.6)`,
                                color: `white`,
                                padding: `.3em 1em`,
                                marginBottom: `0`,
                                borderRadius: `.7em`,
                                textAlign: `center`
                            }}
                            {...this.props}
                            {...this.state}

                            // onClick={(ev) => {
                            //     ev.target.select();
                            // }}

                        >{this.state.ex.exerciciProgressio}
                        </OportuCampTextModificable>
                    </div>

                    <div
                        style={{
                            gridArea: `descans`
                        }}
                    >
                        <span>Descans:</span>
                        <div
                            style={{
                                fontWeight: `bold`,
                                fontSize: `1.2em`,
                                textAlign: `center`,
                                gridArea: `titol`,
                                fontVariant: `small-caps`,
                                background: `rgba(255,255,255, .4)`,
                                borderRadius: `2em`,
                                display: `grid`,
                                alignContent: `center`,
                                transform: `skewX(-12deg)`,
                                gridTemplateAreas: `
                                    "mins segs"
                                `
                            }}
                        >
                            <span
                                style={{
                                    // fontWeight: `bold`,
                                    // marginLeft: `1em`
                                    fontWeight: `bold`,
                                    //      fontSize: `1.2em`,
                                    textAlign: `center`,
                                    gridArea: `mins`,
                                    fontVariant: `small-caps`,
                                    background: `rgba(255,255,255, .6)`,
                                    borderRadius: `2em`,
                                    // display: `grid`,
                                    alignContent: `center`,
                                    //     transform: `skewX(-12deg)`
                                }}
                            >
                                <NumericEditable2
                                    ex={this.state.ex}
                                    field="exerciciDescansMinuts"
                                    area="mins"
                                    campNomAmbArticle="els minuts"
                                    {...this.props}
                                >
                                    {this.state.ex.exerciciDescansMinuts}
                                </NumericEditable2>
                                <span>'</span>
                            </span>
                            <span
                                style={{
                                    // fontWeight: `bold`,
                                    // marginRight: `1em`
                                    fontWeight: `bold`,
                                    //      fontSize: `1.2em`,
                                    textAlign: `center`,
                                    gridArea: `segs`,
                                    fontVariant: `small-caps`,
                                    background: `rgba(255,255,255, .6)`,
                                    borderRadius: `2em`,
                                    // display: `grid`,
                                    alignContent: `center`,
                                    //    transform: `skewX(-12deg)`
                                }}
                            >
                                <NumericEditable2
                                    ex={this.state.ex}
                                    field="exerciciDescansSegons"
                                    area="segs"
                                    campNomAmbArticle="els segons"
                                    {...this.props}
                                >
                                    {String(this.state.ex.exerciciDescansSegons).length === 1 ? `0${this.state.ex.exerciciDescansSegons}` : `${this.state.ex.exerciciDescansSegons}`}
                                </NumericEditable2>
                                <span>"</span>
                            </span>
                        </div>
                    </div>
                    <div
                        style={{
                            gridArea: `carrega`
                        }}
                    >
                        <span>Càrrega: </span>
                        <span
                            style={{
                                // fontWeight: `bold`
                                display: `grid`,
                                gridTemplateAreas: `"tit"`,
                                fontWeight: `bold`,
                                fontSize: `1.2em`,
                                textAlign: `center`,
                                gridArea: `titol`,
                                fontVariant: `small-caps`,
                                background: `rgba(255,255,255, .8)`,
                                borderRadius: `2em`,
                                alignContent: `center`,
                                transform: `skewX(-12deg)`
                            }}
                        >
                            <NumericEditable2
                                ex={this.state.ex}
                                field="exerciciCarrega"
                                area="tit"
                                campNomAmbArticle="la càrrega"
                                {...this.props}
                            >
                                {this.state.ex.exerciciCarrega}
                            </NumericEditable2>
                            <span>%</span>
                        </span>
                    </div>

                    <div
                        style={{
                            gridArea: `descrip`,
                            display: this.state.impDescExs ? `block` : `none`,
                            padding: `0 1em`,
                            margin: `0 -1em`,
                            minHeight: `3em`,
                            background: `pink`
                        }}
                    >
                        <ExerDescripcioModificable
                            {...this.props}
                        >
                            {this.state.ex.exerciciDescripcio}
                        </ExerDescripcioModificable>
                    </div>

                </div>
            </div>
        );
    }
}

class ExerciciModificableSortable extends Component {
}

class ExerDescripcioModificable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            html: typeof (this.props.children) !== "undefined" && this.props.children,
            disabled: !this.props.inputMode
        };
    }

    render() {
        return (
            <ContentEditable
                style={{
                    width: `100%`,
                    height: `100%`
                }}
                html={typeof (this.props.children) !== "undefined" && this.props.children || ""}
                disabled={
                    !this.props.inputMode
                }
                // onClick={() => {
                //     this.setState({
                //         disabled: false
                //     })
                // }}
                // onChange={(ev) => {
                //     // this.setState({
                //     //     html: ev.target.innerHTML //? ev.target.innerHTML : this.state.html
                //     //    //html: ev.target.innerHTML
                //     // });
                // }}
                onBlur={(ev) => {
                    let
                        rutinaAnterior = this.props.rutina,
                        rutinaModificada = { ...rutinaAnterior }
                        ;

                    console.dir("rutinaAnterior-------: ", rutinaAnterior);
                    console.dir("rutinaModificada-------: ", rutinaModificada);

                    //Ori: rutinaModificada.sessions[this.props.index].exercicis[this.props.i].exerciciDescripcio = ev.target.innerHTML;
                    rutinaModificada.sessions[this.props.indexSessio].exercicis[this.props.i].exerciciDescripcio = ev.target.innerHTML;

                    console.dir("rutinaModificada+++++: ", rutinaModificada);

                    Meteor.call(
                        'rutines.update',
                        this.props.rutinaId,
                        rutinaModificada
                    );

                    //alert(`Pos ara s'ha canviat!: ${ev.target.value}`);
                }}
            />
        );
    }
}

class OportuCampTextModificable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            impOpt: props.impOpt,
            field: props.field,
            children: props.children,
            title: props.title,
            style: props.style,
            ...props
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            ...nextProps
        });
    }

    render() {
        if (this.props.impOpt) {
            if (this.state[this.props.impOpt] || this.props[this.props.impOpt]) {
                if (!this.props.inputMode) {
                    if (typeof (this.props.children) !== "undefined" && this.props.children.length > 0) {
                        return (
                            <div
                                style={this.props.style}
                            >
                                <h5
                                    style={{
                                        display: `inline-block`
                                    }}
                                >{this.props.title}</h5>
                                <div
                                    style={{
                                        display: `inline-block`,
                                        marginLeft: `1em`
                                    }}
                                >
                                    <CampTextModificable
                                        field={this.props.field}
                                        {...this.props}
                                    >{typeof (this.props.children) !== "undefined" && this.props.children}
                                    </CampTextModificable>
                                </div>
                            </div>
                        );
                    } else {
                        return null;
                    }
                } else {
                    return (
                        <div
                            style={this.props.style}
                        >
                            <h5
                                style={{
                                    display: `inline-block`
                                }}
                            >{this.props.title}</h5>
                            <div
                                style={{
                                    display: `block`,
                                    marginLeft: `1em`
                                }}
                            >
                                <CampTextModificable
                                    field={this.props.field}
                                    {...this.props}
                                >{typeof (this.props.children) !== "undefined" && this.props.children || ""}
                                </CampTextModificable>
                            </div>
                        </div>
                    );
                }
            } else {
                return null;
            }
        } else {
            if (!this.props.inputMode) {
                if (typeof (this.props.children) !== "undefined" && sanitizeHtml(this.props.children).replace(/^( |<br \/>)*(.*?)( |<br \/>)*$/, "$2").trim().length > 0) {
                    // console.log("Sanitize <br>: ", sanitizeHtml(this.props.children).replace(/^( |<br \/>)*(.*?)( |<br \/>)*$/,"$2").trim());
                    return (
                        <div
                            style={this.props.style}
                        >
                            <h5
                                style={{
                                    display: `inline-block`
                                }}
                            >{this.props.title}</h5>
                            <div
                                style={{
                                    display: `block`,
                                    marginLeft: `1em`
                                }}
                            >
                                <CampTextModificable
                                    field={this.props.field}
                                    {...this.props}
                                >{typeof (this.props.children) !== "undefined" && this.props.children}
                                </CampTextModificable>
                            </div>
                        </div>
                    );
                } else {
                    return null;
                }
            } else {
                return (
                    <div
                        style={this.props.style}
                    >
                        <h5
                            style={{
                                display: `inline-block`
                            }}
                        >{this.props.title}</h5>
                        <div
                            style={{
                                display: `inline-block`,
                                marginLeft: `1em`
                            }}
                        >
                            <CampTextModificable
                                field={this.props.field}
                                {...this.props}
                            >{typeof (this.props.children) !== "undefined" && this.props.children}
                            </CampTextModificable>
                        </div>
                    </div>
                );
            }
        }
    }
}

class SessioTabular extends Component {
    constructor(props) {
        super(props);


    }

    render() {
        return (
            <div>
                <table
                    style={{
                        margin: `0 auto`
                    }}
                >
                    <thead
                        style={{
                            background: `rgba(0,0,0,.2)`,
                            textShadow: `0 1px rgba(255,255,255,.3  )`
                        }}
                    >
                        <tr
                            style={{
                                textAlign: `center`,
                                border: `1px solid`
                            }}
                        >
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Tipus</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Exercici</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Sèries</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Reps</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Prog</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >DMins</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >DSegs</th>
                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Carr</th>

                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >GM</th>

                            <th
                                style={{
                                    textAlign: `center`,
                                    border: `1px solid`
                                }}
                            >Descripció</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            typeof (this.props.sessio.exercicis) !== "undefined" && this.props.sessio.exercicis.map(
                                (v, i, a) => {
                                    let
                                        //   rutinaAct = Rutines.findOne({_id: this.props.rutinaId}),
                                        ex = Object.assign(
                                            {},
                                            Exercicis.findOne({ _id: v.value }),
                                            {
                                                exerciciDescripcio: this.props.sessio.exercicis[i].exerciciDescripcio
                                            },
                                            Number(this.props.sessio.exercicis[i].exerciciSeriesDefault) >= 0
                                                ? {
                                                    exerciciSeriesDefault: this.props.sessio.exercicis[i].exerciciSeriesDefault
                                                }
                                                : null
                                            ,
                                            Number(this.props.sessio.exercicis[i].exerciciRepeticionsDefault) >= 0
                                                ? {
                                                    exerciciRepeticionsDefault: this.props.sessio.exercicis[i].exerciciRepeticionsDefault
                                                }
                                                : null
                                            ,
                                            Number(this.props.sessio.exercicis[i].exerciciDescansMinuts) >= 0
                                                ? {
                                                    exerciciDescansMinuts: this.props.sessio.exercicis[i].exerciciDescansMinuts
                                                }
                                                : null
                                            ,
                                            Number(this.props.sessio.exercicis[i].exerciciDescansSegons) >= 0
                                                ? {
                                                    exerciciDescansSegons: this.props.sessio.exercicis[i].exerciciDescansSegons
                                                }
                                                : null,
                                            Number(this.props.sessio.exercicis[i].exerciciCarrega) >= 0
                                                ? {
                                                    exerciciCarrega: this.props.sessio.exercicis[i].exerciciCarrega
                                                }
                                                : null,
                                            typeof (this.props.sessio.exercicis[i].exerciciProgressio) !== "undefined" && Number(this.props.sessio.exercicis[i].exerciciProgressio.length) > 0
                                                ? {
                                                    exerciciProgressio: this.props.sessio.exercicis[i].exerciciProgressio
                                                }
                                                : null,
                                            {
                                                superSeries: this.props.sessio.exercicis[i].superSeries
                                            },
                                            {
                                                tripleSeries: this.props.sessio.exercicis[i].tripleSeries
                                            },
                                            //     ,
                                            // Number(this.props.sessio.exercicis[i].arrImatges.length) > 0
                                            // ?   {
                                            //     arrImatges: this.props.sessio.exercicis[i].arrImatges
                                            // }
                                            // : []<<<<<<<<

                                        )
                                        ;

                                    return (


                                        <tr
                                            key={i}
                                            style={{
                                                textAlign: `center`,
                                                border: `1px solid`,
                                                background:
                                                    i % 2 === 1
                                                        ? `rgba(255,255,255,.2)`
                                                        : `transparent`
                                            }}
                                        >
                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                            >{
                                                    ex.superSeries
                                                        ? "SS"
                                                        : v.tripleSeries
                                                            ? "TS"
                                                            : ""
                                                }</td>

                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                                contentEditable="true"
                                            >{ex.exerciciNom}
                                            </td>

                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                            >{ex.exerciciSeriesDefault}
                                            </td>

                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                            >{ex.exerciciRepeticionsDefault}
                                            </td>

                                            <td
                                                dangerouslySetInnerHTML={{ __html: typeof (ex.exerciciProgressio) !== "undefined" ? sanitizeHtml(ex.exerciciProgressio) : "" }}
                                                style={{
                                                    border: `1px solid`
                                                }}

                                            />

                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                            >{ex.exerciciDescansMinuts}'
                                    </td>

                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                            >
                                                {String(ex.exerciciDescansSegons).length === 1 ? `0${ex.exerciciDescansSegons}` : `${ex.exerciciDescansSegons}`}"
                                    </td>

                                            <td
                                                style={{
                                                    border: `1px solid`
                                                }}
                                            >{ex.exerciciCarrega}%
                                    </td>

                                            <td>
                                                {ex.exerciciGrupMuscular}
                                            </td>

                                            <td
                                                dangerouslySetInnerHTML={{ __html: typeof (ex.exerciciDescripcio) !== "undefined" ? sanitizeHtml(ex.exerciciDescripcio) : "" }}
                                                style={{
                                                    border: `1px solid`
                                                }}

                                            />

                                        </tr>
                                        /*
                                                                       <div
                                                                           key={i}
                                                                           style={{
                                                                               display: `grid`,
                                                                               gridTemplateAreas: `
                                                                               "tipus nom series reps prog mins segs carrega notes"`,
                                                                               border: `solid 1px rgba(0,0,0,.5)`,
                                                                               justifyContent: `stretch`,
                                                                               textAlign: `center`
                                                                           }}
                                                                       >
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `tipus`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                           >{
                                                                               ex.superSeries
                                                                               ?   "SS"
                                                                               :   v.tripleSeries
                                                                                   ?   "TS"
                                                                                   :   ""
                                                                           }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `nom`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                               contentEditable="true"
                                                                           >{ ex.exerciciNom }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `series`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                           >{ ex.exerciciSeriesDefault }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `reps`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                           >{ ex.exerciciRepeticionsDefault }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `prog`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                               dangerouslySetInnerHTML={{__html: typeof(ex.exerciciProgressio) !== "undefined" ? sanitizeHtml(ex.exerciciProgressio) : ""}}
                                                                           >
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `mins`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                           >{ ex.exerciciDescansMinuts }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `segs`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                           >{ ex.exerciciDescansSegons }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `carrega`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                           >{ ex.exerciciCarrega }
                                                                           </span>
                                                                           <span
                                                                               style={{
                                                                                   gridArea: `notes`,
                                                                                   border: `solid 1px rgba(0,0,0,.5)`
                                                                               }}
                                                                               dangerouslySetInnerHTML={{__html: typeof(ex.exerciciDescripcio) !== "undefined" ? sanitizeHtml(ex.exerciciDescripcio) : ""}}
                                                                           >
                                                                           </span>
                                                                       </div>
                                       
                                                               */
                                    );
                                }
                            )
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default class QuadreSessio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMode: props.inputMode,
            impOpcionsVisibles: props.impOpcionsVisibles,
            impNomSessio: props.sessio.opcionsImpressio.impNomSessio,
            impObsSessio: props.sessio.opcionsImpressio.impObsSessio,
            impImatgesExs: props.sessio.opcionsImpressio.impImatgesExs,
            impDescExs: props.sessio.opcionsImpressio.impDescExs,
            impEscalf: props.sessio.opcionsImpressio.impEscalf,
            impTorCalma: props.sessio.opcionsImpressio.impTorCalma,
            impProgressio: props.sessio.opcionsImpressio.impProgressio,
            rutina: props.rutina,
            rutinaEditada: null,
            sessio: props.sessio,
            impTABULAR: props.impTABULAR
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            impOpcionsVisibles: nextProps.impOpcionsVisibles,
            impNomSessio: nextProps.impNomSessio,
            impObsSessio: nextProps.impObsSessio,
            impImatgesExs: nextProps.impImatgesExs,
            impDescExs: nextProps.impDescExs,
            impEscalf: nextProps.impEscalf,
            impTorCalma: nextProps.impTorCalma,
            impTABULAR: nextProps.impTABULAR,
            rutina: nextProps.rutina,
            sessio: nextProps.sessio
            // ...nextProps
        });
    }

    actualitzaNovesOpcionsDImpressio = () => {

        /* this.state({
            impTABULAR: 
        }); */

        function isIterable(obj) {
            // checks for null and undefined
            if (obj == null) {
                return false;
            }
            return typeof obj[Symbol.iterator] === 'function';
        }



        switch (this.state.inputMode) {
            case "simple": {

                //  alert("Ieee que va! SIMPLE");

                let
                    rutinaAntiga = this.props.rutina,
                    rutinaModificada = { ...rutinaAntiga }
                    ,
                    sessioModificada = { ...this.props.rutina.sessions[this.props.index] }
                    ;

                console.dir("rutinaAntiga: ", rutinaAntiga);
                console.dir("rutinaModificada: ", rutinaModificada);
                console.dir("sessioModificada AB: ", sessioModificada);

                sessioModificada.opcionsImpressio = {
                    impNomSessio: this.state.impNomSessio,
                    impObsSessio: this.state.impObsSessio,
                    impImatgesExs: this.state.impImatgesExs,
                    impDescExs: this.state.impDescExs,
                    impEscalf: this.state.impEscalf,
                    impTorCalma: this.state.impTorCalma,
                    impTABULAR: this.state.impTABULAR
                };


                console.dir("sessioModificada DP: ", sessioModificada);

                rutinaModificada = {
                    ...rutinaAntiga,

                    sessions: [
                        ...rutinaAntiga.sessions
                        // ,
                        //...sessioModificada
                    ]

                };

                rutinaModificada.sessions[this.props.indexSessio].opcionsImpressio = {
                    impNomSessio: this.state.impNomSessio,
                    impObsSessio: this.state.impObsSessio,
                    impImatgesExs: this.state.impImatgesExs,
                    impDescExs: this.state.impDescExs,
                    impEscalf: this.state.impEscalf,
                    impTorCalma: this.state.impTorCalma,
                    impTABULAR: this.state.impTABULAR
                };

                console.dir("RutinaModificada ABANS: ", rutinaModificada);
                console.dir("SessioModificada++: ", sessioModificada);

                //           rutinaModificada.sessions[this.props.index] = sessioModificada;

                console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                Meteor.call(
                    'rutines.update',
                    this.props.rutinaId,
                    rutinaModificada
                );
                break;
            }

            case "multiple": {
                //    alert("Ieee que va! MULTIPLE");

                let
                    rutinaAntiga = this.props.rutina,
                    rutinaModificada = { ...rutinaAntiga }
                    ,
                    sessioModificada = { ...this.props.rutina.sessions[this.props.indexSessio] }
                    ;

                console.dir("rutinaAntiga: ", rutinaAntiga);
                console.dir("rutinaModificada: ", rutinaModificada);
                console.dir("sessioModificada AB: ", sessioModificada);

                sessioModificada.opcionsImpressio = {
                    impNomSessio: this.state.impNomSessio,
                    impObsSessio: this.state.impObsSessio,
                    impImatgesExs: this.state.impImatgesExs,
                    impDescExs: this.state.impDescExs,
                    impEscalf: this.state.impEscalf,
                    impTorCalma: this.state.impTorCalma,
                    impTABULAR: this.state.impTABULAR
                };


                console.dir("sessioModificada DP: ", sessioModificada);

                // function mergeArraysDObjectes(arr1, arr2) {   

                //     console.dir("arr1: ", arr1);
                //     console.dir("arr2: ", arr2);

                //     let i,p,obj={},result=[];
                //     for(i=0;i<arr1.length;i++)obj[arr1[i].name]=arr1[i].value;
                //     for(i=0;i<arr2.length;i++)obj[arr2[i].name]=arr2[i].value;
                //     for(p in obj)if(obj.hasOwnProperty(p))result.push({name:p,value:obj[p]});
                //     return result;
                // }

                // let 
                //     mergeABP = (a, b, p) => a.filter( 
                //         aa => ! b.find( 
                //             bb => aa[p] === bb[p]
                //         )
                //     ).concat(b)
                // ;


                console.dir(
                    "mergeArrayDObjectes: ",
                    mergeABP(
                        rutinaAntiga.sessions
                        ,
                        [sessioModificada],
                        "nom"
                    )
                );

                rutinaModificada = {
                    ...rutinaAntiga,

                    sessions:
                        mergeABP(
                            rutinaAntiga.sessions
                            ,
                            [sessioModificada],
                            "nom"
                        )
                };

                rutinaModificada.sessions[this.props.indexSessio].opcionsImpressio = {
                    impNomSessio: this.state.impNomSessio,
                    impObsSessio: this.state.impObsSessio,
                    impImatgesExs: this.state.impImatgesExs,
                    impDescExs: this.state.impDescExs,
                    impEscalf: this.state.impEscalf,
                    impTorCalma: this.state.impTorCalma,
                    impTABULAR: this.state.impTABULAR
                };

                console.dir("RutinaModificada ABANS: ", rutinaModificada);
                console.dir("SessioModificada++: ", sessioModificada);

                rutinaModificada.sessions[this.props.indexSessio] = sessioModificada;

                console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                Meteor.call(
                    'rutines.update',
                    this.props.rutinaId,
                    rutinaModificada
                );

                break;
            }

            case "periode": {
                let
                    rutinaAntiga = this.props.rutina,
                    rutinaModificada = { ...rutinaAntiga }
                    ,
                    sessioModificada = { ...this.props.rutina.sessions[this.props.indexSessio] }
                    ;

                console.dir("rutinaAntiga: ", rutinaAntiga);
                console.dir("rutinaModificada: ", rutinaModificada);
                console.dir("sessioModificada AB: ", sessioModificada);

                sessioModificada.opcionsImpressio = {
                    impNomSessio: this.state.impNomSessio,
                    impObsSessio: this.state.impObsSessio,
                    impImatgesExs: this.state.impImatgesExs,
                    impDescExs: this.state.impDescExs,
                    impEscalf: this.state.impEscalf,
                    impTorCalma: this.state.impTorCalma,
                    impTABULAR: this.state.impTABULAR
                };


                console.dir("sessioModificada DP: ", sessioModificada);

                // function mergeArraysDObjectes(arr1, arr2) {   

                //     console.dir("arr1: ", arr1);
                //     console.dir("arr2: ", arr2);

                //     let i,p,obj={},result=[];
                //     for(i=0;i<arr1.length;i++)obj[arr1[i].name]=arr1[i].value;
                //     for(i=0;i<arr2.length;i++)obj[arr2[i].name]=arr2[i].value;
                //     for(p in obj)if(obj.hasOwnProperty(p))result.push({name:p,value:obj[p]});
                //     return result;
                // }

                let
                    mergeABP = (a, b, p) => a.filter(
                        aa => !b.find(
                            bb => aa[p] === bb[p]
                        )
                    ).concat(b)
                    ;


                console.dir("mergeArrayDObjectes: ", mergeABP(
                    rutinaAntiga.sessions
                    ,
                    [sessioModificada],
                    "nom"
                ));

                rutinaModificada = {
                    ...rutinaAntiga,

                    sessions:
                        mergeABP(
                            rutinaAntiga.sessions
                            ,
                            [sessioModificada],
                            "nom"
                        )
                };

                rutinaModificada.sessions[this.props.indexSessio].opcionsImpressio = {
                    impNomSessio: this.state.impNomSessio,
                    impObsSessio: this.state.impObsSessio,
                    impImatgesExs: this.state.impImatgesExs,
                    impDescExs: this.state.impDescExs,
                    impEscalf: this.state.impEscalf,
                    impTorCalma: this.state.impTorCalma,
                    impTABULAR: this.state.impTABULAR
                };

                console.dir("RutinaModificada ABANS: ", rutinaModificada);
                console.dir("SessioModificada++: ", sessioModificada);

                rutinaModificada.sessions[this.props.indexSessio] = sessioModificada;

                console.dir("RutinaModificada DESPRÉS: ", rutinaModificada);

                Meteor.call(
                    'rutines.update',
                    this.props.rutinaId,
                    rutinaModificada
                );

                break;
            }

            default:

        }

        if (this.state.inputMode) { //Sols volem modificar el document de la Rutina a la BD quan estem al Formulari d'edició.
            // this.props.chgOptsImp(this.props.index, {
            //     impNomSessio: this.state.impNomSessio,
            //     impObsSessio: this.state.impObsSessio,
            //     impImatgesExs: this.state.impImatgesExs,
            //     impDescExs: this.state.impDescExs,
            //     impEscalf: this.state.impEscalf,
            //     impTorCalma: this.state.impTorCalma
            // });

            //this.props.rutinaRefresh(novaRutina);



            //  chgOptsImp = (index, nouOptsImp) => {
            //     let
            //         nouValorModificat = [
            //             ...this.state.valorModificat
            //         ],
            //         efectua = (nouValorModificat) => {
            //             let
            //                 rutinaFinal = {
            //                     ...this.props.rutina,
            //                     sessions: nouValorModificat
            //                 }
            //             ;

            //             this.opcionsDImpressioEditades(
            //                 this.state.impNomSessio,
            //                 this.state.impObsSessio,
            //                 this.state.impImatgesExs,
            //                 this.state.impDescExs,
            //                 this.state.impEscalf,
            //                 this.state.impTorCalma
            //             );

            //             Meteor.call(
            //                 'rutines.update',
            //                 this.props.rutinaId,
            //                 rutinaFinal
            //             );
            //         }
            //     ;

            //     nouValorModificat[index].opcionsImpressio = {...nouOptsImp};

            //     // this.setState({
            //     //     valorModificat: nouValorModificat
            //     // },
            //     //     () => {
            //         //    efectua(nouValorModificat);
            //             console.dir("valorModificat: ", nouValorModificat);
            //     //    }
            //     //);
            //     efectua(nouValorModificat);
            // };
        }
    };

    // shouldComponentUpdate() {
    //    // if (!this.state.impTABULAR) {
    //         return false;
    //   //  }
    //  //   return true;
    // }

    render() {
        let
            s = this.state.sessio,
            impOptionsShown = {
                display: `grid`,
                gridTemplateAreas: `
                    "impTABULAR   impNomSessio      impObsSessio      impImatgesExs"
                    "impTABULAR   impDescripcioExs      impEscalf          impCalma"
                `,
                justifyContent: `end`
            }
            ;

        return (
            <div>
                {
                    this.state.inputMode
                        ?
                        <div
                            style={this.state.impOpcionsVisibles ? impOptionsShown : { display: `none` }}
                        >
                            <div
                                style={{
                                    display: `grid`,
                                    gridArea: `impTABULAR`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkSessioTABULAR"
                                    checked={this.state.impTABULAR}
                                    onChange={() => {
                                        this.setState({
                                            impTABULAR: !this.state.impTABULAR
                                        },
                                            this.actualitzaNovesOpcionsDImpressio
                                        );
                                    }}
                                />
                                <label
                                    htmlFor="chkSessioTABULAR"
                                >
                                    TABULAR
                                </label>
                            </div>

                            <div
                                style={{
                                    display: `grid`,
                                    gridArea: `impNomSessio`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkSessioNom"
                                    checked={this.state.impNomSessio}
                                    onChange={() => {
                                        this.setState({
                                            impNomSessio: !this.state.impNomSessio
                                        },
                                            () => {
                                                this.actualitzaNovesOpcionsDImpressio();
                                                this.props.setRutinaEditada(this.state.rutinaEditada);
                                            }
                                        );
                                    }}
                                />
                                <label
                                    htmlFor="chkSessioNom"
                                >
                                    Nom de la sessió
                                </label>
                            </div>
                            <div
                                style={{
                                    gridArea: `impObsSessio`,
                                    display: `grid`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkSessioObservacions"
                                    checked={this.state.impObsSessio}
                                    onChange={() => {
                                        this.setState({
                                            impObsSessio: !this.state.impObsSessio
                                        },
                                            this.actualitzaNovesOpcionsDImpressio
                                        );
                                    }}
                                />
                                <label htmlFor="chkSessioObservacions">
                                    Observacions de la sessió
                                </label>
                            </div>
                            <div
                                style={{
                                    gridArea: `impImatgesExs`,
                                    display: `grid`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkImatgesExs"
                                    checked={this.state.impImatgesExs}
                                    onChange={() => {
                                        this.setState({
                                            impImatgesExs: !this.state.impImatgesExs
                                        },
                                            this.actualitzaNovesOpcionsDImpressio
                                        );
                                    }}
                                />
                                <label htmlFor="chkImatgesExs">
                                    Imatges dels exercicis
                                </label>
                            </div>
                            <div
                                style={{
                                    gridArea: `impDescripcioExs`,
                                    display: `grid`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkDescripcioExs"
                                    checked={this.state.impDescExs}
                                    onChange={() => {
                                        this.setState({
                                            impDescExs: !this.state.impDescExs
                                        },
                                            this.actualitzaNovesOpcionsDImpressio
                                        );
                                    }}
                                />
                                <label htmlFor="chkDescripcioExs">
                                    Descripcions dels exercicis
                                </label>
                            </div>
                            <div
                                style={{
                                    gridArea: `impEscalf`,
                                    display: `grid`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkSessioEscalf"
                                    checked={this.state.impEscalf}
                                    onChange={() => {
                                        this.setState({
                                            impEscalf: !this.state.impEscalf
                                        },
                                            this.actualitzaNovesOpcionsDImpressio
                                        );
                                    }}
                                />
                                <label htmlFor="chkSessioEscalf">
                                    Escalfament
                                </label>
                            </div>
                            <div
                                style={{
                                    gridArea: `impCalma`,
                                    display: `grid`,
                                    justifyContent: `start`,
                                    gridAutoFlow: `column`,
                                    alignContent: `center`
                                }}
                            >
                                <input
                                    type="checkbox"
                                    id="chkTorCalma"
                                    checked={this.state.impTorCalma}
                                    onChange={() => {
                                        this.setState({
                                            impTorCalma: !this.state.impTorCalma
                                        },
                                            this.actualitzaNovesOpcionsDImpressio
                                        );
                                    }}
                                />
                                <label htmlFor="chkTorCalma">
                                    Tornada a la calma
                                </label>
                            </div>

                        </div>
                        :
                        null

                }


                {
                    this.state.impTABULAR
                        ?
                        <div
                            style={{
                                margin: `1em`
                            }}
                        >
                            <OportuCampTextModificable
                                impOpt="impEscalf"
                                field="escalfament"
                                title="Escalfament: "
                                style={{
                                    background: `rgba(0,0,0,.6)`,
                                    color: `grey`,
                                    padding: `.5em 1em`,
                                    margin: `-1em`,
                                    marginBottom: `0`,
                                    borderRadius: `.5em .5em 0 0`
                                }}
                                {...this.props}
                                {...this.state}
                            >{s.escalfament}
                            </OportuCampTextModificable>
                            <SessioTabular
                                sessio={this.props.sessio}
                            />
                            <OportuCampTextModificable
                                impOpt="impTorCalma"
                                field="calma"
                                title="Retorn a la calma: "
                                style={{
                                    background: `rgba(0,0,0,.6)`,
                                    color: `grey`,
                                    padding: `.5em 1em`,
                                    margin: `-1em`,
                                    marginTop: `0`,
                                    borderRadius: `0 0 .5em .5em`
                                }}
                                {...this.props}
                                {...this.state}
                            >{s.calma}
                            </OportuCampTextModificable>
                        </div>
                        :
                        <div
                            style={{
                                background: ` rgba(255,255,255,.3)`,
                                margin: `.5em`,
                                padding: `1em`,
                                borderRadius: `.5em`,
                                boxShadow: `0 0 146px inset black`
                            }}
                        >
                            {
                                this.state.impNomSessio
                                    ? <div
                                        style={{
                                            background: `rgba(0,0,0,.6)`,
                                            color: `lightgrey`,
                                            border: `1px solid grey`,
                                            padding: `.5em 1em`,
                                            margin: `-1em`,
                                            marginBottom: `0`,
                                            borderRadius: `.5em .5em 0 0`,
                                            textAlign: `right`
                                        }}
                                    // onClick={
                                    //     this.props.inputMode
                                    //     ? (ev) => {
                                    //         let
                                    //             prevContent = ev.target.innerHTML
                                    //         ;

                                    //         ev.target.innerHTML = `<input
                                    //             type="text"
                                    //             value="${prevContent}"
                                    //             onChange={}
                                    //         />`;
                                    //         ev.target.setAttribute("contentEditable", "true");
                                    //        // alert(`Ieee pos s'ha clicat. prevContent: ${prevContent}`);
                                    //     }
                                    //     : ()=>{}
                                    // }
                                    // onChange={() => {
                                    //     alert("Ieee pos s'ha canviat");
                                    // }}
                                    >
                                        <CampTextModificable
                                            field="nom"
                                            {...this.props}
                                        >{this.state.sessio.nom}
                                        </CampTextModificable>
                                    </div>
                                    : null
                            }

                            <OportuCampTextModificable
                                impOpt="impEscalf"
                                field="escalfament"
                                title="Escalfament: "
                                style={{
                                    background: `rgba(0,0,0,.6)`,
                                    color: `grey`,
                                    padding: `.5em 1em`,
                                    margin: `-1em`,
                                    marginBottom: `0`,
                                    borderRadius: `.5em .5em 0 0`
                                }}
                                {...this.props}
                                {...this.state}
                            >{s.escalfament}
                            </OportuCampTextModificable>

                            <div
                                style={{
                                    display: `grid`,
                                    gridTemplateColumns: `repeat(auto-fit, minmax(150px, 1fr))`,
                                    //    gridTemplateRows: `repeat(auto-fit, minmax(150px, 1fr))`,
                                    // background: ` rgba(0,0,0,.3)`,
                                    margin: `0 -1em`
                                    // ,
                                    // padding: `1em`
                                }}
                            >
                                {
                                    typeof (this.state.sessio.exercicis) !== "undefined" && this.state.sessio.exercicis.map(
                                        (v, i, a) => {
                                            let
                                                //   rutinaAct = Rutines.findOne({_id: this.props.rutinaId}),
                                                ex = Object.assign(
                                                    {},
                                                    Exercicis.findOne({ _id: v.value }),
                                                    {
                                                        exerciciDescripcio: this.props.sessio.exercicis[i].exerciciDescripcio
                                                    },
                                                    Number(this.props.sessio.exercicis[i].exerciciSeriesDefault) >= 0
                                                        ? {
                                                            exerciciSeriesDefault: this.props.sessio.exercicis[i].exerciciSeriesDefault
                                                        }
                                                        : null
                                                    ,
                                                    Number(this.props.sessio.exercicis[i].exerciciRepeticionsDefault) >= 0
                                                        ? {
                                                            exerciciRepeticionsDefault: this.props.sessio.exercicis[i].exerciciRepeticionsDefault
                                                        }
                                                        : null
                                                    ,
                                                    Number(this.props.sessio.exercicis[i].exerciciDescansMinuts) >= 0
                                                        ? {
                                                            exerciciDescansMinuts: this.props.sessio.exercicis[i].exerciciDescansMinuts
                                                        }
                                                        : null
                                                    ,
                                                    Number(this.props.sessio.exercicis[i].exerciciDescansSegons) >= 0
                                                        ? {
                                                            exerciciDescansSegons: this.props.sessio.exercicis[i].exerciciDescansSegons
                                                        }
                                                        : null,
                                                    Number(this.props.sessio.exercicis[i].exerciciCarrega) >= 0
                                                        ? {
                                                            exerciciCarrega: this.props.sessio.exercicis[i].exerciciCarrega
                                                        }
                                                        : null,
                                                    typeof (this.props.sessio.exercicis[i].exerciciProgressio) !== "undefined" && Number(this.props.sessio.exercicis[i].exerciciProgressio.length) > 0
                                                        ? {
                                                            exerciciProgressio: this.props.sessio.exercicis[i].exerciciProgressio
                                                        }
                                                        : null,
                                                    {
                                                        superSeries: this.props.sessio.exercicis[i].superSeries
                                                    },
                                                    {
                                                        tripleSeries: this.props.sessio.exercicis[i].tripleSeries
                                                    },
                                                    //     ,
                                                    // Number(this.props.rutina.sessions[this.props.index].exercicis[i].arrImatges.length) > 0
                                                    // ?   {
                                                    //     arrImatges: this.props.rutina.sessions[this.props.index].exercicis[i].arrImatges
                                                    // }
                                                    // : []<<<<<<<<

                                                )
                                                ;

                                            return (
                                                <ExerciciModificable
                                                    key={i}
                                                    v={v}
                                                    i={i}
                                                    s={s}
                                                    ex={ex}
                                                    {...this.props}
                                                    impNomSessio={this.state.impNomSessio}
                                                    impObsSessio={this.state.impObsSessio}
                                                    impImatgesExs={this.state.impImatgesExs}
                                                    impDescExs={this.state.impDescExs}
                                                    impEscalf={this.state.impEscalf}
                                                    impTorCalma={this.state.impTorCalma}
                                                />
                                            );
                                        }
                                    )
                                }
                            </div>

                            <OportuCampTextModificable
                                impOpt="impTorCalma"
                                field="calma"
                                title="Retorn a la calma: "
                                style={{
                                    background: `rgba(0,0,0,.6)`,
                                    color: `grey`,
                                    padding: `.5em 1em`,
                                    margin: `-1em`,
                                    marginTop: `0`,
                                    borderRadius: `0 0 .5em .5em`
                                }}
                                {...this.props}
                                {...this.state}
                            >{s.calma}
                            </OportuCampTextModificable>
                        </div>
                }

                <OportuCampTextModificable
                    impOpt="impObsSessio"
                    field="observacionsSessio"
                    title="Observacions de la sessió: "
                    style={{
                        background: `rgba(0,0,0,.6)`,
                        color: `grey`,
                        padding: `.5em 1em`,
                        borderRadius: `.8em`
                    }}
                    {...this.props}
                    {...this.state}
                >{s.observacionsSessio}
                </OportuCampTextModificable>

            </div>
        );
    }
}
