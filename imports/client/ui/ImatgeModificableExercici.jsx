import React, { Component } from 'react';
//import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import ReactFileReader from 'react-file-reader';
import Tappable from 'react-tappable';
import sanitizeHtml from 'sanitize-html-react';
//
// import { ScaleModal as CropModal } from 'boron';
//import { Button, Alert, Spinner, Modal as CropModal, ModalBody, ModalHeader, ModalFooter } from 'elemental';
//import { Modal as CropModal } from 'react-bootstrap';
//import {ModalContainer, ModalDialog} from 'react-modal-dialog';

import CropModal from 'react-modal';
//import 'simple-react-modal/dist/modal';

import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

//import Konva from 'konva';
import KR from 'konva-react';
//import _ from 'lodash';
// import {
//     Stage,
//     Layer,
//     Image
// } from 'react-konva';
import { ReactiveVar } from 'meteor/reactive-var';


export default class ImatgeModificableExercici extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editantImatge: false,
            editantImgText: false,
            overImage: false,
            crop: {},
            pixelCrop: {},
            cropping: false,
            prevCrop: ()=>{},
            rota: 0,
            estableixGir: false,
            //girVal: "",
            actGirVal: false,
            auxCount: 0,
            editantPeu: false
        };

        this.handlePressEventImatge = this.handlePressEventImatge.bind(this);

        this.handleTapImgText = this.handleTapImgText.bind(this);
        this.handleEditaImgText = this.handleEditaImgText.bind(this);
        this.handleEstableixImgText = this.handleEstableixImgText.bind(this);

        this.showDeleteImageButton = this.showDeleteImageButton.bind(this);
        this.hideDeleteImageButton = this.hideDeleteImageButton.bind(this);
        this.handleEsborraImatge = this.handleEsborraImatge.bind(this);
    }

    // componentDidMount() {
    //     this.forceUpdate();
    // }

    handlePressEventImatge(ev) {
    //     let clau = ev.target.dataset.clau;
    //
        this.setState({
            editantImatge: true,
            overImage: true
        });
    }

// ImgText:
    handleTapImgText() {
        this.imgText.setAttribute("contenteditable", "true");
    }

    handleEditaImgText() {
        this.setState({
            editantImgText: true
        });
    }

    handleEstableixImgText() {
        // console.log("exercicis.imatgeText.update abans del Mètode: ");
        // console.dir("exercici: ", this.props.exercici);
        // console.log("clau: ", this.props.clau);
        // console.log("innerHTML: ", this.imgText.innerHTML);
        Meteor.call(
            'exercicis.imatgeText.update',
            this.props.exercici,
            this.props.clau,
            this.imgText.innerHTML
        );
        this.imgText.setAttribute("contenteditable", "false");
        this.setState({
            editantImgText: false
        });
    }

// Esborra la imatge:
    showDeleteImageButton() {
        this.setState({
            overImage: true
        })
    }

    hideDeleteImageButton() {
        this.setState({
            overImage: false
        })
    }

    handleEsborraImatge() {
        if (confirm("Vas a esborrar la imatge amb el seu text per complet. Recorda que pots editar tant la imatge com el text per separat. Confirma per procedir.")) {
            Meteor.call(
                'exercicis.imatge.delete',
                this.props.exercici,
                this.props.clau
            );
        }
    }

    handleRetallaImatge = () => {
        this.setState({
            cropping: true
        });
    }

    handleCropChange = (crop, pixelCrop) => {
        this.setState({
            crop,
            pixelCrop
        });

        const canvas = document.createElement('canvas');
        canvas.width = this.state.pixelCrop.width;
        canvas.height = this.state.pixelCrop.height;
        const ctx = canvas.getContext('2d');

        const img = new Image();

        img.src = this.props.src;

        console.log(Math.abs(this.state.rota % 360));
        //
        // switch (Math.abs(this.state.rota % 360)) {
        //     case 0: {
        //         // canvas.width = this.state.pixelCrop.width;
        //         // canvas.height = this.state.pixelCrop.height;
        //         // const ctx = canvas.getContext('2d');
        //
        //         break;
        //     }
        //     case 90: {
        //         ctx.transform(0, 1, -1, 0, canvas.height, 0);
        //         // canvas.width = this.state.pixelCrop.height;
        //         // canvas.height = this.state.pixelCrop.width;
        //         // const ctx = canvas.getContext('2d');
        //         //
        //         // ctx.rotate(90 * Math.PI / 180);
        //         // ctx.translate(0, -canvas.width);
        //         //alert(Math.abs(this.state.rota % 360));
        //         break;
        //     }
        //     case 180: {
        //         ctx.transform(-1, 0, 0, -1, canvas.width, canvas.height);
        //         // canvas.width = this.state.pixelCrop.width;
        //         // canvas.height = this.state.pixelCrop.height;
        //         // const ctx = canvas.getContext('2d');
        //
        //         break;
        //     }
        //     case 270: {
        //         ctx.transform(0, -1, 1, 0, 0, canvas.width);
        //
        //         break;
        //     }
        //     default: {
        //
        //         break;
        //     }
        //
        // }

        ctx.drawImage(
            img,
            this.state.pixelCrop.x,
            this.state.pixelCrop.y,
            this.state.pixelCrop.width,
            this.state.pixelCrop.height,
            0,
            0,
            this.state.pixelCrop.width,
            this.state.pixelCrop.height
        );

        // As Base64 string
        const base64Image = canvas.toDataURL('image/jpeg');

        //
        // function rotateBase64Image90Degree(base64data) {
        //   var canvas = document.getElementById("c");
        //   var ctx = canvas.getContext("2d");
        //   var image = new Image();
        //
        //   image.src = base64data;
        //   image.onload = function() {
        //     canvas.width = image.height;
        //     canvas.height = image.width;
        //     ctx.rotate(90 * Math.PI / 180);
        //     ctx.translate(0, -canvas.width);
        //     ctx.drawImage(image, 0, 0);
        //   };
        // }

        // As a blob
        // return new Promise((resolve, reject) => {
        //     canvas.toBlob(file => {
        //         file.name = fileName;
        //         resolve(file);
        //     }, 'image/jpeg');
        // });

        this.setState({
            prevCrop: <img
                src={base64Image}
                style={{
                    display: `block`,
                    maxWidth: `400px`,
                    margin: `0 auto`
                    // ,
                    // transform: `rotate(${this.state.rota}deg)`
                    // ,
                    // transformOrigin: `50% 50% 0`
                }}
            />
        });

        // , () => {
        //     let
        //         dc = document.querySelector(".cropCont"),
        //         dcw = dc.getBoundingClientRect().width,
        //         dch = dcw < 400 ? dcw : 400
        //     ;
        //
        //     dc.style.height = `${dch}px`;
        // }

    }

    toggleCropModal = () => {
        //this.refs.cropModal.toggle();
        this.setState({
            cropping: true
        });
    }

    handleEstableixCrop = (
        exercici,
        clau,
        imgSrc,
        imgTitle,
        editDate
    ) => {
        try {
            Meteor.call('exercicis.imatge.update',
                exercici,
                clau,
                imgSrc,
                imgTitle,
                editDate,
                this.state.rota
            );
        } catch (err) {
            alert("Error:", err);
        }
    }

    handleClick = () => this.setState({isShowingModal: true})
    handleClose = () => this.setState({cropping: false})

    girVal = (ex, cl, imgSrc, imT, rota) => {
        window.girVal = imgSrc;
        // Meteor.call('exercicis.imatge.update',
        //     ex,
        //     cl,
        //     imgSrc,
        //     imT,
        //     new Date(),
        //     rota
        // );
    }

    actGirVal = () => {
        this.setState({
            actGirVal: true
        })
    }

    // estableixGir = (imgSrc) => {
    //     Meteor.call(
    //         'exercicis.imatge.update',
    //         this.props.exercici,
    //         this.props.clau,
    //         imgSrc,
    //         this.props.exercici.exerciciNom,
    //         new Date(),
    //         this.state.rota
    //     );
    // }

    // --------> Guarda Stage en ImatgeModificableExercici:

    guardaKonvaStage = (stgNode) => {
        this.konvaStage = stgNode;
    }


    render() {

        let crop = {
            x: 20,
            y: 10,
            width: 30,
            height: 10
        };

        // const
        //     Canv = ({props}) => {
        //         return (
        //             <img
        //                 className={props.className}
        //                 src={props.src}
        //                 alt={props.alt}
        //             />
        //         )
        //     }

        class Canvas extends Component {
            constructor(props) {
                super(props);

            //    this.counter = 0;
                this.loaded = false;

                this.image = new Image();
                this.image.src = props.src;
            //     this.image.onload = () => {
            // //        alert(`im: ${props.alt}\n Count: ${this.counter}`);
            //         this.counter += 1;
            //     }

                this.state = {
                    rota: props.rota,
                    src: props.src
                };
            }

            haRotat = (srcVal) => {
                if (this.state.rota) {
                    this.props.girVal(
                        this.props.exercici,
                        this.props.clau,
                        srcVal,
                        this.props.exercici.exerciciNom,
                        this.state.rota
                    );
                //    alert("rotat");
                }
            }

            // componentWillReceiveProps(nextProps) {
            //     this.image.src = nextProps.src;
            //     this.setState({
            //         src: nextProps.src
            //     })
            // }

            componentDidMount() {
    //             const
    //                 stg = this.stage.node
    //             ;
    //
    // //
    // //             //console.dir("stg: ", stg);
    //             if (this.state.rota !== 0) {
    //                 this.haRotat(stg.toDataURL());
    //             }
                // const
                //     stg = this.myStage,
                //     img = new Image()
                // ;
                //
                // img.src = this.props.src;
                // stg.width = img.width;
                // stg.height = img.height;
                // stg.style.maxWidth="300px";
                // stg.style.maxHeight="300px";

        //        setTimeout(() => {
//                 if (this.myCanvas) {
//                     const
//                         canvas = this.myCanvas,
//                         ctx = canvas.getContext('2d'),
//                         img = new Image()
//                     ;
//
//                     img.src = this.props.src;
//                     canvas.width = img.width;
//                     canvas.height = img.height;
//                     canvas.style.maxWidth="300px";
//                     canvas.style.maxHeight="300px";
//
//                     //ctx.save();
//                     //ctx.fillStyle = 'rgba(200,0,250)';
//                     //ctx.fillRect(10, 10, 55, 50);
//
//                     switch (this.state.rota % 360) {
//                         case -90: {
//                         //    alert("-90");
//                             console.log("-90");
//                             canvas.width = img.height;
//                             canvas.height = img.width;
//                             ctx.transform(0, -1, 1, 0, 0, img.width);
//
//                         //    console.dir("toDataURL: ", canvas.toDataURL("image/jpeg"));
//                             // Meteor.call(
//                             //     'exercicis.imatge.update',
//                             //     this.props.exercici,
//                             //     this.props.clau,
//                             //     canvas.toDataURL("image/jpeg"),
//                             //     this.props.exercici.exerciciNom,
//                             //     new Date(),
//                             //     this.state.rota
//                             // );
//                             this.haRotat(canvas.toDataURL("image/jpeg"));
//                             break;
//                         }
//
//                         case -180: {
//                             ctx.transform(-1, 0, 0, -1, img.width, img.height );
//                                 console.log("-180");
//                             // Meteor.call(
//                             //     'exercicis.imatge.update',
//                             //     this.props.exercici,
//                             //     this.props.clau,
//                             //     canvas.toDataURL("image/jpeg"),
//                             //     this.props.exercici.exerciciNom,
//                             //     new Date(),
//                             //     this.state.rota
//                             // );
//
//                             this.haRotat(canvas.toDataURL("image/jpeg"));
//                             break;
//                         }
//
//                         case -270: {
//                                 console.log("-270");
//                                 canvas.width = img.height;
//                                 canvas.height = img.width;
//                                 ctx.transform(0, 1, -1, 0, img.height , 0);
//                                 // Meteor.call(
//                                 //     'exercicis.imatge.update',
//                                 //     this.props.exercici,
//                                 //     this.props.clau,
//                                 //     canvas.toDataURL("image/jpeg"),
//                                 //     this.props.exercici.exerciciNom,
//                                 //     new Date(),
//                                 //     this.state.rota
//                                 // );
//                                 this.haRotat(canvas.toDataURL("image/jpeg"));
//                             break;
//                         }
//
//                         default: {
//                         //    alert("0 o altre");
//                             console.log("0 o ALTRE");
//                             break;
//                         }
//                     }
//
//                     ctx.drawImage(img, 0, 0);
//
// //                    this.props.girVal(canvas.toDataURL("image/jpeg"));
//
//                     // this.setState(
//                     //     (prevState, props) => ({
//                     //         src: canvas.toDataURL("image/jpeg")
//                     //     })
//                     // );
//                     this.setState({
//                         rota: 0
//                     });
//                 }
            }

            render() {
                const
                //    img = new Image(),
                    maxW = 300,
                    maxH = 300
                ;
                let
                    iW,
                    iH,
                    ratioWH
                ;
                //
                // this.image.src = this.props.src;

                iW = this.image.width;
                iH = this.image.height;
                ratioHW = iH / iW;

                if (iW > iH) {
                    iW = maxW;
                    iH = ratioHW * iW;
                } else {
                    iH = maxH;
                    iW = iH / ratioHW;
                }
                oX = iW / 2;
                oY = iH / 2;

                // img.onload = () => {
                //     // this.loaded.set(
                //
                //     console.dir("img: ", img);
                // //    alert("loaded: ", img);
                // };

                return (
                    <div
                        style={{
                            gridArea: `imatgeModificable`
                        }}
                    >
                    {//(() => {
                        //    setTimeout(()=>{
                                //alert(this.loaded);
                                this.state.rota && this.props.src
                                ?
                                    <KR.Stage
                                        height={this.state.rota % 180 === 0 ? iH : iW}
                                        width={this.state.rota % 180 === 0 ? iW : iH}
                                        ref={stage => {
                                            this.stage = stage;
                                            window.stage = stage;
                                        }}
                                    >
                                        <KR.Layer
                                            offset={{
                                                x: this.state.rota % 180 === 0 ? -iW/2 : -iH/2,
                                                y: this.state.rota % 180 === 0 ? -iH/2 : -iW/2
                                            }}
                                        >
                                            <KR.Image
                                                ref={kri => this.kri = kri}
                                                image={this.image}
                                                width={iW}
                                                height={iH}
                                                offset={{
                                                    x: iW/2,
                                                    y: iH/2//                             break;

                                                }}
                                                rotation={this.state.rota}
                                            />
                                        </KR.Layer>
                                    </KR.Stage>
                                    // <canvas
                                    //     ref={c => this.myCanvas = c}
                                    // ></canvas>
                                :   null
                        //    }, 10000);
                //        })()
                    }
                    </div>
                );
            }
        }

        return (
            <div
                className="divExerciciImatges"
                style={{
                    display: `grid`,
                    margin: `.5em`
                }}
                onMouseEnter={this.showDeleteImageButton}
                onMouseLeave={this.hideDeleteImageButton}
            >
                <Tappable
                    component="div"
                    onPress={this.handlePressEventImatge}
                    pressDelay={250}
                    stopPropagation={true}
                >
                    <div style={{
                        display: "grid",
                        gridTemplateAreas: '"imatgeModificable"'
                    }}>

                        <Canvas
                            src={this.props.src}
                            alt={this.props.alt}
                            rota={this.state.rota}
                            clau={this.props.clau}
                            exercici={this.props.exercici}
                            estableixGir={this.estableixGir}
                            girVal={this.girVal}
                            actGirVal={this.state.actGirVal}
                            guardaKonvaStage={this.guardaKonvaStage}
                        />

                        <img
                            ref={imatgeNoCanv => this.imatgeNoCanv = imatgeNoCanv}
                            src={this.props.src}
                            style={{
                                gridArea: `imatgeModificable`,
                                maxHeight: `300px`,
                                maxWidth: `300px`
                            }}
                        />

                        <div
                            style={{
                                gridArea: `imatgeModificable`,
                                position: `relative`,
                                display:
                                    this.state.overImage
                                    ? `inline-block`
                                    : `none`
                            }}
                        >
                            <button
                                className="btDelImage"
                                style={{
                                    gridArea: `imatgeModificable`,
                                    position: `relative`,
                                    float: `left`,
                                    bottom: `0`,
                                    zIndex: `200`
                                }}
                                onClick={() => {
                                    if (this.imatgeNoCanv.width > 0) {
                                        this.iniWidth = this.imatgeNoCanv.width;
                                    }

                            //        alert(this.iniWidth);
                                //    console.dir("imatgeNoCanv: ", this.imatgeNoCanv);

                                    this.imatgeNoCanv.width > 0 && this.rota !== 0
                                    ?   this.imatgeNoCanv.style.width = 0
                                    :   this.imatgeNoCanv.style.width = `${this.iniWidth}px`
                                    ;

                                    this.actGirVal();
                                    this.setState(
                                        (prevState, props) => ({
                                            rota: (prevState.rota - 90) % 360,
                                            estableixGir: ((prevState.rota - 90) % 360) !== 0 ? true : false
                                        })
                                    );
                                    //this.girVal(this.stage.node.toDataURL());
                                    //console.dir(stage.node.toDataURL());
                                }}
                            >Girar
                            </button>
                            <button
                                className="btDelImage"
                                style={{
                                    gridArea: `imatgeModificable`,
                                    position: `relative`,
                                    float: `left`,
                                    bottom: `0`,
                                    zIndex: `200`
                                }}
                                onClick={this.toggleCropModal}
                            >Retallar
                            </button>
                            <button
                                className="btDelImage"
                                style={{
                                    gridArea: `imatgeModificable`,
                                    position: `relative`,
                                    float: `left`,
                                    bottom: `0`,
                                    zIndex: `200`,
                                    display: `${this.state.estableixGir ? "inline" : "none"}`
                                }}
                                onClick={() => {
                                    Meteor.call(
                                        'exercicis.imatge.update',
                                        this.props.exercici,
                                        this.props.clau,
                                        //window.girVal,
                                        window.stage.node.toDataURL(),
                                        this.props.exercici.exerciciNom,
                                        new Date(),
                                        this.state.rota
                                    );
                                    this.setState({rota: 0});
                                    location.reload();
                                }}
                            >Estableix
                            </button>
                        </div>

                        <div
                            style={{
                                gridArea: `imatgeModificable`,
                                position: `relative`,
                                display: this.state.overImage
                                    ? `inline-block`
                                    : `none`
                            }}
                        >
                            <button
                                className="btDelImage"
                                style={{
                                    gridArea: `imatgeModificable`,
                                    position: `relative`,
                                    float: `right`,
                                    zIndex: `200`
                                }}
                                onClick={this.handleEsborraImatge}
                            >x
                            </button>
                        </div>

                        <div
                            style={{
                                gridArea: `imatgeModificable`,
                                alignSelf: `stretch`,
                                justifySelf: `center`,
                                borderRadius: `2em`,
                                overflow: `hidden`,
                                width: `100%`,
                                textAlign: `center`,
                                margin: `0 auto`
                            }}
                        >
                            <SelectorImatge
                                exercici={this.props.exercici}
                                isOpen={this.state.editantImatge}
                                clau={this.props.clau}
                                handleEditantImatge={this.props.handleEditantImatge}
                            />
                        </div>
                    </div>
                </Tappable>

                <div
                    style={{
                        display:
                            this.state.overImage || this.props.peu || this.state.editantPeu
                            ? `inline-block`
                            : `none`
                    }}
                >
                    <Tappable
                        onTap={this.handleTapImgText}
                    >
                        <div
                            contenteditable
                            className="divExerciciImgText"
                            ref={imgText => this.imgText = imgText}
                            onInput={this.handleEditaImgText}
                            dangerouslySetInnerHTML={{
                                __html:
                                    this.props.peu || this.state.editantImgText
                                    ?   sanitizeHtml(this.props.peu)
                                    :   "ESCRIU ACÍ"
                            }}
                            title="Selecciona per editar el text"
                        ></div>
                    </Tappable>
                </div>

                <button
                    style={{
                        visibility: this.state.editantImgText ? `visible` : `hidden`,
                        display: this.state.editantImgText ? `inline-block` : `none`,
                        alignSelf: `center`
                    }}
                    onClick={this.handleEstableixImgText}
                >Estableix</button>

                <CropModal
                    isOpen={this.state.cropping}
                    onRequestClose={this.handleClose}
                >
                    <div>
                        <h2>⇩Selecciona la part a retallar de la imatge⇩</h2>
                        <ReactCrop
                            src={this.props.src}
                            onChange={this.handleCropChange}
                            crop={this.state.crop}
                            rota={this.state.rota}
                        />
                        {
                                this.state.pixelCrop.height > 0
                                ?
                                //     ?   <div>
                                //             <button
                                //                 style={{
                                //                     display: `block`,
                                //                     margin: `0 auto`
                                //                 }}
                                //                 onClick={this.handleEstableixCrop}
                                //             >
                                //                 Retalla
                               //             </button>
                                //
                                //         </div>
                                //     : null
                                    <div>
                                        <h2>⇩Estableix la nova imatge fent clic sobre la imatge previsualitzada⇩</h2>

                                        {/* <BotoGiraCropPrev rota=this.state.rota} />
                                        //
                                        // <button
                                        //     onClick=()=>
                                        //         this.setState(
                                        //             rota: this.state.rota - 90
                                        //         });
                                        //     }}
                                        // >Gira esquerra
                                        // </button>
                                        // <button
                                        //     onClick=()=>
                                        //         this.setState(
                                        //             rota: this.state.rota + 90
                                        //         })
                                        //     }}
                                        // >Gira dreta
                                        // </button> */}
                                        <div
                                            className="cropCont"
                                            onClick={()=>{
                                                if (confirm("Vas a modificar la imatge de manera definitiva. Confirmes l'operació?")) {
                                                    this.handleEstableixCrop(
                                                        this.props.exercici,
                                                        this.props.clau,

                                                        this.state.prevCrop.props.src, //base64data del crop

                                                        "Imatge retallada",
                                                        new Date()
                                                    );
                                                    this.handleClose();
                                                }
                                            }}
                                        >
                                            {this.state.prevCrop}
                                        </div>
                                    </div>
                                :   null
                        }
                    </div>
                </CropModal>
            </div>
        );
    }
}

class SelectorImatge extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imatgeEstablerta: false
        }

        this.handleEstableixImatge = this.handleEstableixImatge.bind(this);
    }

    handleEstableixImatge() {
        this.setState({
            imatgeEstablerta: true
        });
    }

    render() {
        return (
            <div
                className="divOverlay"
                style={{
                    display:
                        this.props.isOpen && !this.state.imatgeEstablerta
                        ?   `grid`
                        :   `none`
                    ,
                    position: `relative`,
                    background: `rgba(50,50,50,.85)`,
                    width: `100%`,
                    height: `100%`,
                    float: `left`,
                    padding: `0 1em`,
                    zIndex: '100',
                    alignContent: `center`
                }}
            >
                <ActualitzaImatge
                    meteor_method={``}
                    exercici={this.props.exercici}
                    imatge_amb_text_original={this.props.imatge_amb_text_original}
                    clau={this.props.clau}
                    handleEditantImatge={this.props.handleEditantImatge}
                    handleEstableixImatge={this.handleEstableixImatge}

                />
            </div>
        );
    }
}

class ActualitzaImatge extends Component {
    constructor(props) {
        super(props);

        //this.fileSelect = this.fileSelect.bind(this);
        //this.sendFiles = this.sendFiles.bind(this);
        this.arrImatgesPujades = [];

        this.state = {
          imatgeTriada: false,
          imatgeEstablerta: false,

          imgSrc: "",
          imgAlt: "",
          imgTitle: ""
        };

        this.handleFiles = this.handleFiles.bind(this);
        this.sendFile = this.sendFile.bind(this);
        //this.selArxiusAmbAnchor = this.selArxiusAmbAnchor.bind(this);
        this.handleEstableixImatge = this.handleEstableixImatge.bind(this);
    }

    handleFiles(files) {
      console.log(files);
      this.setState({
          imatgeTriada: true,

          imgSrc: files.base64,
          imgAlt: files.fileList[0].name,
          imgTitle: files.fileList[0].name
      });
    }

    // fileSelect(ev) {
    //     let clau = this.props.clau;
    //
    //     this.setState({
    //         imatgeTriada: true
    //     });
    //
    //     const
    //         arxiu = ev.target.files[0],
    //         preview = this.preview;
    //
    //     let
    //         arx = arxiu,
    //         imageType = /^image\//;
    //
    //         // if (!imageType.test(arx.type)){
    //         //   continue;
    //         // }
    //     let divArx = document.createElement("div");
    //     divArx.classList.add("divPreview");
    //
    //     //console.dir("Clau: ", this.props.clau);
    //
    //     preview.appendChild(divArx);
    //
    //     let img = document.createElement("img");
    //     img.classList.add("preview");
    //     img.file = arx;
    //     divArx.appendChild(img);
    //
    //         /* 	let prog = document.createElement("progress");
    //             prog.setAttribute("max", "100");
    //             prog.setAttribute("value", "0");
    //             prog.setAttribute("id", `progress_${i}`);
    //             divArx.appendChild(prog);
    //  */
    //         //div.children.push(img).push(prog);
    //         //preview.appendChild(div);
    //
    //     let reader = new FileReader();
    //     reader.onload = ((aImg) => {
    //             //let buffer = new Uint8Array(reader.result);
    //             //Meteor.call('saveFile', buffer);
    //         return (e) => aImg.src = e.target.result;
    //     })(img);
    //     reader.readAsDataURL(arx);
    //         //reader.readAsArrayBuffer(arx);
    //
    //     let funA = function funAlert() {
    //         stopPropagation();
    //         alert("okokok");
    //     }
    //     let btEstableixImatge = document.createElement("button");
    //     btEstableixImatge.classList.add("btEstableixImatge");
    //     btEstableixImatge.innerHTML = "Estableix la imatge";
    //     btEstableixImatge.setAttribute("data-clau", this.props.clau);
    //     btEstableixImatge.setAttribute("ref", bt => this.bt = bt);
    //     btEstableixImatge.setAttribute("onClick", funA);
    //     divArx.appendChild(btEstableixImatge);
    //
    //     console.dir(`Clau: `, this.props.clau);
    //     console.dir(`Imatge: `, arx);
    //     console.dir(`GM: `, this.props.exercici);
    //     //alert("Arxius seleccionats. Missatge a la consola.");
    //
    //     //this.sendFiles2(arxius)
    // //this.sendFile(arx);
    // }

    sendFile(arxiu) {
        //for (let i = 0; i < arxius.length; i++) {

        // let
        //     arx = arxiu,
        //     imageType = /^image\//;
        //
        // if (!imageType.test(arx.type)) {
        // //    continue;
        // }
        //
        // let reader2 = new FileReader();
        // reader2.onload = function (event) {
        //
        //     let buffer = event.target.result;// = new Uint8Array(event.target.result);
        //
        //     Meteor.call('exercicis.imatge.update',
        //           buffer,
        //           "Provisional",
        //           "ID del GM referenciat",
        //           { observacions: "Ací una observació de l'arxiu particular"},
        //           (error, result) => {
        //               that.arrImatgesPujades.push(result);
        //               console.log("ImatgesPujades: ");
        //               console.dir(that.arrImatgesPujades);
        //               // De moment farà els canvis tantes vegades com arxius se passen.
        //               // Se podrà millorar en un futur utilitzant una promesa o un async/await.
        //               that.props.onImatgesPujades(that.arrImatgesPujades);
        //           }
        //       );
        //     //return (e) => aImg.src = e.target.result;
        // };
        // reader2.readAsDataURL(arx);
        // //reader2.readAsArrayBuffer(arx);
    }

    handleEstableixImatge(
        exercici,
        clau,
        imgSrc,
        imgTitle
    ) {
        let editDate = new Date();

        //alert("A punt de cridar el mètode de Meteor");
        // console.dir("GM:", exercici),
        // console.log("clau: ",clau);
        // console.log("imgSrc: ", imgSrc);
        // console.log("imgTitle: ", imgTitle);
        // console.log("editDate: ", editDate);

        Meteor.call('exercicis.imatge.update',
            exercici,
            clau,
            imgSrc,
            imgTitle,
            editDate);

        // console.log("Després de tornar del mètode.");
        this.setState({
            imatgeEstablerta: true
        });

        this.props.handleEstableixImatge();
    }

    render() {
        let
            Preview = (props) => (
                <img
                    src={props.imgSrc}
                    alt={props.imgAlt}
                    title={props.imgTitle}
                    style={{
                        display: this.state.imatgeTriada && !this.state.imatgeEstablerta
                            ? `grid`
                            : `none`,
                        width: `210px`,
                        alignSelf: `center`
                    }}
                />
            ),
            BotoEstableix = (props) =>
                <button
                    onClick={(ev) => {
                        ev.stopPropagation();
                        this.handleEstableixImatge(
                            props.exercici,
                            props.clau,
                            props.imgSrc,
                            props.imgTitle
                        );
                    }}
                    style={{
                        display: this.state.imatgeTriada && !this.state.imatgeEstablerta
                            ? `block`
                            : `none`
                        ,
                        margin: `.5em`
                    }}
                >Estableix la imatge
                </button>
        ;

        return (
            <ReactFileReader
                base64={true}
                handleFiles={this.handleFiles}
            >
                <div>
                    <input type="button"
                        id={`inFile${this.props.clau}`}
                        accept="image/*"
                        style={{ display: `none` }}
                        data-clau={this.props.clau}
                    />
                    {/*  <a href="#" id="aSelArxius" onClick= this.selArxiusAmbAnchor}>Selecciona imatges (anchor)</a> */}
                    <label htmlFor={`inFile${this.props.clau}`}>
                        <div id="divZonaUpload"
                            style={{
                                display: !this.state.imatgeTriada && !this.state.imatgeEstablerta
                                    ? `grid`
                                    : `none`,
                                border: `.6em dashed white`,
                                color: `white`,
                                fontWeight: `bold`,
                                textAlign: `center`,
                                margin: `.3em auto`,
                                width: `150px`,
                                height: `200px`,
                                alignContent: `center`
                            }}
                            title="Clica ací per canviar la imatge"
                        >
                            <span
                                style={{
                                    alignSelf: `center`
                                }}
                            >Selecciona una imatge
                            </span>
                        </div>
                    </label>
                    <div style={{
                        display: `grid`,
                        gridArea: `"preview"`,
                        justifyContent: `center`,
                        alignContent: `center`
                    }}>
                        <Preview
                            imgSrc={this.state.imgSrc}
                            imgAlt={this.state.imgAlt}
                            imgTitle={this.state.imgTitle}
                            data-clau={this.props.clau}
                            ref={preview => this.preview = preview}
                        />
                        <BotoEstableix
                            exercici={this.props.exercici}
                            clau={this.props.clau}
                            imgSrc={this.state.imgSrc}
                            imgTitle={this.state.imgTitle}
                            editDate={new Date()}
                        />
                    </div>
                </div>
            </ReactFileReader>
        );
    }
}
