import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export default Clients = new Mongo.Collection("clients");

Clients.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; }
});

Clients.schema = new SimpleSchema({
  createdAt: {
    type: Date,
    optional: false
  },
  clientNom: {
    type: String,
    optional: false
  },
  clientCognoms: {
    type: String,
    optional: false
  },
  clientMobil: {
    type: String,
    optional: true
  },
  clientLabel: {
    type: String,
    optional: false
  },
  clientEmail: {
    type: String,
    optional: true
  },
  clientAddress: {
    type: String,
    optional: true
  },
  clientDateOfBirth: {
    type: Date,
    optional: true
  },
  clientObservacions: {
    type: String,
    optional: true
  },
  completed: {
    type: Boolean,
    optional: false
  },
  user: {
    type: String,
    optional: false
  },
  arrImatges: {
      type: Array,
      optional: true
  }
});

Meteor.methods({

    'clients.insert'(
        clientNom,
        clientCognoms,
        clientMobil,
        clientEmail,
        clientAddress,
        clientDateOfBirth,
        clientObservacions,
        arrImatges
    ) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }
        Clients.insert({
            clientNom,
            clientCognoms,
            clientLabel: `${clientCognoms}, ${clientNom}`,
            clientMobil,
            clientEmail,
            clientAddress,
            clientDateOfBirth,
            clientObservacions,
            completed: false,
            createdAt: new Date(),
            user: Meteor.userId(),
            arrImatges
        });
    },

    'clients.update'(
        clientNom, 
        clientCognoms, 
        clientMobil, 
        clientEmail, 
        clientAddress, 
        clientDayOfBirth, 
        clientMonthOfBirth, 
        clientYearOfBirth, 
        clientObservacions
    ){
    if (Meteor.userId() !== client.user){
      throw new Meteor.Error('not-authorized');
    }
    Clients.update(client._id, {
      $set: {
        completed: !client.completed
      }
    });
  },

  'clients.nom.update'(
      client,
      clientNom
  ) {
      if (Meteor.userId() !== client.user) {
          throw new Meteor.Error('not-authorized');
      }

      Clients.update(client._id, {
          $set: {
              clientNom,
              clientLabel: `${client.clientCognoms}, ${clientNom}`
          }
      });
  },

  'clients.cognoms.update'(
    client,
    clientCognoms
) {
    if (Meteor.userId() !== client.user) {
        throw new Meteor.Error('not-authorized');
    }

    Clients.update(client._id, {
        $set: {
            clientCognoms,
            clientLabel: `${clientCognoms}, ${client.clientNom}`
        }
    });
},

'clients.naixement.update'(
  client,
  clientDateOfBirth
) {
  if (Meteor.userId() !== client.user) {
      throw new Meteor.Error('not-authorized');
  }

  Clients.update(client._id, {
      $set: {
          clientDateOfBirth
      }
  });
},

'clients.telefon.update'(
  client,
  clientMobil
) {
  if (Meteor.userId() !== client.user) {
      throw new Meteor.Error('not-authorized');
  }

  Clients.update(client._id, {
      $set: {
          clientMobil
      }
  });
},

'clients.email.update'(
  client,
  clientEmail
) {
  if (Meteor.userId() !== client.user) {
      throw new Meteor.Error('not-authorized');
  }

  Clients.update(client._id, {
      $set: {
          clientEmail
      }
  });
},

'clients.address.update'(
  client,
  clientAddress
) {
  if (Meteor.userId() !== client.user) {
      throw new Meteor.Error('not-authorized');
  }

  Clients.update(client._id, {
      $set: {
          clientAddress
      }
  });
},

'clients.observacions.update'(
  client,
  clientObservacions
) {
  if (Meteor.userId() !== client.user) {
      throw new Meteor.Error('not-authorized');
  }

  Clients.update(client._id, {
      $set: {
          clientObservacions
      }
  });
},

'clients.imatge.update'(
  client,
  clau,
  imgSrc,
  imgTitle,
  editDate,
  rota
) {
  // if (Meteor.userId() !== client.user) {
  //     throw new Meteor.Error('not-authorized');
  // }
  console.log("Meteor.method: clients.imatge.update");

  let
      arrImatges = client.arrImatges,
      arrClau = arrImatges[clau],
      imgArx = arrClau.imgArx
  ;

  console.dir("arrImatges: ", arrImatges);
  console.dir("arrClau: ", arrClau);
  console.dir("imgArx: ", imgArx);

  arrImatges[clau].imgArx.buffer = imgSrc;
  arrImatges[clau].imgArx.name = imgTitle;
  arrImatges[clau].imgArx.editDate = editDate;
  arrImatges[clau].imgArx.rota = rota;

  console.dir("arrImatges: ", arrImatges);

  Clients.update({
      _id: client._id
  }, { $set: {
      arrImatges
  }});
},


'clients.imatgeText.update'(
  client,
  clau,
  nouText
) {
  // if (Meteor.userId() !== exercici.user) {
  //     throw new Meteor.Error('not-authorized');
  // }

  let
      arrImatges = client.arrImatges,
      arrClau = arrImatges[clau];

  arrImatges[clau].imgText = nouText;
  arrImatges[clau].editDate = new Date();

  Clients.update({
      _id: client._id
  }, { $set: {
      arrImatges
  }});
},


'clients.imatge.delete'(
  client,
  clau
) {
  // if (Meteor.userId() !== exercici.user) {
  //     throw new Meteor.Error('not-authorized');
  // }

  let
      arrImatges = client.arrImatges;

  client.arrImatges.splice(clau, 1);

  arrImatges.editDate = new Date();

  Clients.update({
      _id: client._id
  }, { $set: {
      arrImatges
  }});
},

'clients.imatges.afegeix'(
  client,
  arrNovesImatges
) {
  // if (Meteor.userId() !== client.user) {
  //     throw new Meteor.Error('not-authorized');
  // }

  let
      arrImatges = client.arrImatges;

  arrNovesImatges.map(
      (v,i,a) => arrImatges.push(v)
  );

  client.arrImatges.editDate = new Date();

  Clients.update({
      _id: client._id
  }, { $set: {
      arrImatges
  }});
},


  'clients.delete'(client){
    // if (Meteor.userId() !== client.user){
    //   throw new Meteor.Error('not-authorized');
    // }
    Clients.remove(client._id);
  }

});

// Meteor.publish("allResolutions", function(){
//   return Resolutions.find();
// });
