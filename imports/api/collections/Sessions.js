import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export default Sessions = new Mongo.Collection('sessions');

Meteor.methods({
    'sessions.insert'(
        nom,
        data,
        dates,
        periodeSessio,
        incImatges,
        gms,
        escalfament,
        exercicis,
        calma,
        observacionsSessio,
        opcionsImpressio
    ) {
    //    console.log("Meteor method: 'saveImage'.");
        return Sessions.insert({
            nom,
            createdAt: new Date(),
            user: Meteor.userId(),
            dataSessio: data,
            datesSessio: dates,
            periodeSessio,
            incImatges,
            gms,
            escalfament,
            exercicis,
            calma,
            observacionsSessio,
            opcionsImpressio
        });
        //console.log(`Meteor method: RIGHT AFTER Files.insert. \nBuffer: ${buffer}`);
    },

    'sessions.update'(
        sessio,
        sessioModificada
    ) {
        // if (Meteor.userId() !== exercici.user) {
        //     throw new Meteor.Error('not-authorized');
        // }

        Sessions.update({nom: sessio.nom}, {
            $set: {
                nom: sessioModificada.nom,
                gms: sessioModificada.gms,
                escalfament: sessioModificada.escalfament,
                exercicis: sessioModificada.exercicis,
                calma: sessioModificada.calma,
                observacionsSessio: sessioModificada.observacionsSessio,
                opcionsImpressio: sessioModificada.opcionsImpressio,
                datesSessio: sessioModificada.datesSessio,
                periodeSessio: sessioModificada.periode
            }
        });
    },
    //,

    // 'sessions.update'(imatge, tipusRef, idRef, adjunt) {
    //     if (Meteor.userId() !== imatge.user) {
    //         throw new Meteor.Error('not-authorized');
    //     }
    //
    //     Sessions.update(imatge._id, {
    //         $set: {
    //             tipusRef,
    //             idRef,
    //             adjunt
    //         }
    //     });
    // },
    //
    'sessions.delete'(sessioId) {
        // if (Meteor.userId() !== sessio.user) {
        //     throw new Meteor.Error('not-authorized');
        // }
        Sessions.remove(sessioId);
    }
});
