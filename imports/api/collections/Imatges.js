import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export default Sessions = new Mongo.Collection('sessions');

Meteor.methods({
    'sessions.insert'(data, gms, escalfament, exercicis, calma) {
    //    console.log("Meteor method: 'saveImage'.");
        return Sessions.insert({
            createdAt: new Date(),
            user: Meteor.userId(),
            data: buffer,
            tipusRef,
            idRef,
            adjunt
        });
        //console.log(`Meteor method: RIGHT AFTER Files.insert. \nBuffer: ${buffer}`);
    },

    'sessions.update'(imatge, tipusRef, idRef, adjunt) {
        if (Meteor.userId() !== imatge.user) {
            throw new Meteor.Error('not-authorized');
        }

        Sessions.update(imatge._id, {
            $set: {
                tipusRef,
                idRef,
                adjunt
            }
        });
    },

    'sessions.delete'(imatge) {
        if (Meteor.userId() !== imatge.user) {
            throw new Meteor.Error('not-authorized');
        }
        Sessions.remove(imatge._id);
    }
});
