import { Meteor } from 'meteor/meteor';
import '/imports/api/collections/Clients.js';
import '/imports/api/collections/GrupsMusculars.js';
import '/imports/api/collections/Exercicis.js';
import '/imports/api/collections/Sessions.js';
import '/imports/api/collections/Rutines.js';

import '/imports/server/publish.js';
//import '../imports/server/file.js';

// Meteor.startup(() => {
//   // code to run on server at startup
// });
